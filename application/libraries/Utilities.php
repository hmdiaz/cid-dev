<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Utilidades
 *
 * @author furbox
 */
class Utilities {

    protected $CI;

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->CI = & get_instance();
    }

    public function randomString($length) {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    public function randomStringLower($length) {
        $str = "";
        $characters = array_merge(range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    public function sendMail($email_a, $subject, $message) {
        $this->CI->load->library('email');

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => HOST_SMTP_EMAIL_CONTACT,
            'smtp_port' => PORT_SMPT_EMAIL_CONTACT,
            'smtp_user' => EMAIL_CONTACT,
            'smtp_pass' => PASS_EMAIL_CONTACT,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );
        //cargamos la configuración para enviar
        $this->CI->email->initialize($config);

        $this->CI->email->set_mailtype('html');
        $this->CI->email->from(EMAIL_CONTACT);
        $this->CI->email->to($email_a);
        $this->CI->email->subject($subject); 
        $this->CI->email->message($message);

        return $this->CI->email->send();
    }

    public function check_passwd($hash, $random_salt, $password) {

        if (is_php('5.5') && password_verify($password . config_item('encryption_key'), $hash)) {
            return TRUE;
        } else if ($hash === $this->hash_passwd($password, $random_salt)) {
            return TRUE;
        }

        return FALSE;
    }

    public function hash_passwd($password, $random_salt) {
        return is_php('5.5') ? password_hash($password . config_item('encryption_key'), PASSWORD_BCRYPT, array('cost' => 11)) : crypt($password . config_item('encryption_key'), '$2a$09$' . $random_salt . '$');
    }

    public function random_salt() {
        return md5(mt_rand());
    }

}
