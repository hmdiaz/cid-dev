<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sendmail
 *
 * @author furbox
 */
require dirname(__FILE__) . '/vendor/autoload.php';
include dirname(__FILE__) . "/lib/SendGrid.php";

class Sendmail extends SendGrid {

    public function __construct() {
        parent::__construct('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');
    }

    public function sendMail($to, $subject, $html) {

        $email = new SendGrid\Email();
        $email
                ->addTo($to)
                ->setFrom('sgth@cid.org.co')
                ->setSubject($subject)
                ->setHtml($html)
        ;
//$sendgrid->send($email);
// Or catch the error

        try {
            $this->send($email);
            return TRUE;
        } catch (\SendGrid\Exception $e) {
            echo $e->getCode();
            foreach ($e->getErrors() as $er) {
                echo $er;
            }
        }
    }

}
