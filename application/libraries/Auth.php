<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Auth
 *
 * @author furbox
 */
class Auth {

    //put your code here
    private $CI;

    public function __construct() {
        $this->CI = & get_instance();
    }
    
    public function is_session_start(){
        if(!@$_SESSION['is_logged_in']){
            $this->CI->session->set_flashdata('message_error', 'Necesitas iniciar session.');
            redirect('signin');
        }
    }

    public function is_access($modulos) {
        $this->CI->load->model([
            "Users_model",
            "PermisosModulo_model",
            "Modules_model"
        ]);
        $band = FALSE;
        foreach ($modulos as $modulo) {
            $module = $this->CI->Modules_model->getBySub($modulo);
            $permiso = $this->CI->PermisosModulo_model->getwhere($_SESSION['user_role_id'],$module->ModuloID);
            //print_r($permiso);exit;
            if ($permiso->EstadoID == 1) {
                $band = TRUE;
            }
            
        }
        if($_SESSION['user_role_id'] == 6){
            redirect('dashboard-aspirante');
        }
        if(!$band){
            redirect('dashboard');
        }
        return TRUE;
    }

    public function login($user) {

        $this->valida_login($user->NombreStatus);

        $this->CI->load->model('Users_model');
        $date = date('Y-m-d H:s:i');
        $db_data = [
            'FechaUltimaSesion' => $date
        ];
        $this->CI->Users_model->update($user->UsuarioID, $db_data);

        $_SESSION['id_user'] = $user->UsuarioID;
        $_SESSION['username'] = $user->Nombres.' '. $user->Apellidos;
        $_SESSION['email'] = $user->EmailUsuario;
        $_SESSION['img_profile'] = $user->FotoUsuario;
        $_SESSION['user_role_id'] = $user->RolID;
        $_SESSION['is_logged_in'] = TRUE;
    }

    public function valida_login($data) {

        if (!$data) {
            $this->CI->session->set_flashdata('message_error', 'Datos de Usuario incorrectos');
            redirect('signin');
        }

        if ($data == 'Inactivo') {
            $this->CI->session->set_flashdata('message_error', 'Tiene que validar su Email. Revise su correo electronico.');
            redirect('signin');
        }

        if ($data == 'Eliminado') {
            $this->CI->session->set_flashdata('message_error', 'Su cuenta esta Eliminada o dada de baja. Contacte al administrador de la pagina.');
            redirect('signin');
        }
    }

    public function check_password_strength($password) {
        $argument = TRUE;
        // (?=.{' . config_item('min_chars_for_password') . ',}) means string should be at least length specified in site definitions hook
        // (?=.*\d) means string should have at least one digit
        // (?=.*[a-z]) means string should have at least one lower case letter
        // (?=.*[A-Z]) means string should have at least one upper case letter
        // (?!.*\s) means no space, tab, or other whitespace chars allowed
        // (?!.*[\\\\\'"]) means no backslash, apostrophe or quote chars are allowed
        // (?=.*[@#$%^&+=]) means there has to be at least one of these characters in the password @ # $ % ^ & + =

        if ($argument[0] === 'FALSE' && empty($password)) {
            // If the password is not required, and if it is empty, no reason to proceed
            return TRUE;
        } else if (preg_match('/^(?=.{8,16})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s)(?!.*[\\\\\'"]).*$/', $password)) {
            return $password;
        } else {
            $this->CI->form_validation->set_message(
                    'check_password_strength', '<span class="redfield">%s</span> debe contener:
					<ol>
						<li>Al menos 8 caracteres</li>
						<li>No mas de 16 caracteres</li>
						<li>Un Numero</li>
                                                <li>Una letra Minuscula</li>
						<li>Una Letra Mayuscula</li>
						<li>Sin Espacios entre caracteres</li>
						<li>No debe contener Barra invertida, apóstrofe o comillas</li>
					</ol>
				</span>'
            );

            return FALSE;
        }
    }

    public function send_email($from_email, $to_email, $subject, $message) {
        
        $this->CI->load->library('email');

        $this->CI->email->from($from_email);
        $this->CI->email->to($to_email);

        $this->CI->email->subject($subject);
        $this->CI->email->message($message);

        if ($this->CI->email->send()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
