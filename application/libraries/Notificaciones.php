<?php
class Notificaciones
{
    private $CI;
    public function __construct()
    {
        $this->CI = & get_instance();
    }

    public function aprobacionesPendientesCurrentUser()
    {
        $this->CI->load->model([
            'Aprobaciones_model',
            'DatosUsuario_model',
            'Flujos_model'
        ]);

        $du = $this->CI->DatosUsuario_model->getByUsuarioID($_SESSION['id_user']);
        $_SESSION['du_id'] = $du->DatosUsuarioID;        
        return $this->CI->Aprobaciones_model->getAprobaciones($du->DatosUsuarioID, 6);
    }
}