<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'authentication/signin';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['home'] = "home/index";
//users
$route['lista-usuarios'] = "users/dash_list_user";
$route['lista-usuarios/(:num)'] = "users/dash_list_user/$1";
$route['nuevo-usuario'] = "users/dash_form_add_user";
$route['agregar-usuario'] = "users/dash_insert_new_user";

$route['editar-usuario/(:num)'] = "users/dash_form_edit_user/$1";
$route['editar-usuario'] = "users/dash_form_edit_user";
$route['eliminar-usuario/(:num)'] = "users/dash_delete_user/$1";
$route['actualizar-usuario'] = "users/dash_update_user";
$route['activar-usuario/(:num)'] = "users/active_user/$1";
$route['eliminar-usuario/(:num)'] = "users/delete_user/$1";
$route['cambiar-clave-usuario/(:num)'] = "users/form_change_password/$1";
$route['actualizar-clave-usuario'] = "users/update_password_user";
$route['registrarse'] = "users/form_register";
$route['perfil'] = "users/dash_perfil_user";
$route['update-perfil-user'] = "users/dash_update_perfil_user";

$route['lost_pass'] = "authentication/form_lost_pass";
$route['rec_pass'] = "authentication/send_pass_user";
$route['signin'] = "authentication/signin";
$route['signin-ext'] = "authentication/signin_ext";
$route['signin-linkedin'] = "authentication/signin_linkedin";
$route['verification/(:any)'] = "authentication/code_verification/$1";
$route['signout'] = "authentication/signout";
$route['signout-ext'] = "authentication/signout_ext";

$route['login-ext'] = "users/ext_login_form";
$route['registro-ext'] = "users/ext_register_form_user";
$route['create-user-ext'] = "users/ext_register_user";

$route['dashboard-aspirante'] = "dashboard/aspirante";
$route['editar-perfil'] = "datosUsuario/ext_form_edit_user";
$route['actualizar-perfil'] = "datosUsuario/ext_update_profile";
$route['municipios/(:num)'] = "datosUsuario/getMunicipiosByDepId/$1";

// projects
$route['nuevo-proyecto'] = "projects/dash_new_project";
$route['crear-proyecto'] = "projects/dash_create_project";
$route['lista-proyectos'] = "projects/list_projects";
$route['editar-proyecto/(:num)'] = "projects/edit_project/$1";
$route['actualizar-proyecto'] = "projects/update_project";
$route['eliminar-proyecto/(:num)'] = "projects/delete_project/$1";
$route['perfiles/(:num)'] = "projects/list_profiles_by_proyecto/$1";
$route['get-projects-by-tipo/(:num)'] = "projects/getByTipo/$1";
$route['get-socios-by-tipo/(:num)'] = "projects/getSociosByTipo/$1";
$route['perfiles/(:num)/(:num)'] = "projects/list_profiles_by_proyecto/$1/$2";
$route['delete-socio-coop-pro'] = "projects/dash_delete_socio_coop_proyecto";
$route['delete-mun-pro'] = "projects/dash_delete_municipio_proyecto";
$route['delete-poblacion-pro'] = "projects/dash_delete_poblacion_proyecto";
$route['add-socio-coop-pro'] = "projects/dash_add_socio_coop_proyecto";
$route['add-mun-pro'] = "projects/dash_add_municipio_proyecto";
$route['add-poblacion-pro'] = "projects/dash_add_poblacion_proyecto";
$route['lista-perfiles'] = "projects/list_profiles";
$route['listar-requisiciones'] = "projects/list_all_requisiciones";

//profiles
$route['nuevo-perfil'] = "profile/dash_new_profile";
$route['crear-perfil'] = "profile/dash_create_profile";
$route['modificar-perfil/(:num)'] = "profile/dash_edit_profile/$1";
$route['update-responsabilidad'] = "profile/dash_update_responsability";
$route['update-comp-competencia'] = "profile/dash_update_componente_competencia";
$route['delete-responsabilidad'] = "profile/dash_delete_responsability";
$route['delete-comp-competencia'] = "profile/dash_delete_item_catalogo";
$route['add-comp-competencia'] = "profile/dash_add_item_catalogo";
$route['add-responsabilidad'] = "profile/dash_add_responsabilidad";
$route['update-perfil'] = "profile/dash_update_profile";
$route['departamento-proyecto/(:num)'] = "profile/getDepartamentos_x_proyecto/$1";
$route['municipios-proyecto/(:num)/(:num)'] = "profile/getMunicipiosDepartamentos_x_proyecto/$1/$2";
$route['add-mun-perfil'] = "profile/dash_add_municipio_perfil";
$route['delete-mun-perfil'] = "profile/dash_delete_municipio_perfil";
$route['del-mun-perfil'] = "profile/delete_municipio_perfil";
$route['eliminar-perfil/(:num)'] = "profile/delete_profile/$1";
$route['get-niveles-educativos/(:num)'] = "profile/getNivelesEducativosByGrado/$1";
$route['get-vacantes-perfil']= "profile/getVacantesProfile";
$route['clonar-perfil/(:num)'] = "profile/form_clonar_profile/(:num)";
$route['duplicar-perfil'] = "profile/clonar_profile";
//Requisicion
$route['nueva-requisicion/(:num)'] = "requisicion/dash_new_requisicion/$1";
$route['editar-requisicion/(:num)/(:num)'] = "requisicion/dash_edit_requisicion/$1/$2";
$route['crear-requisicion'] = "requisicion/dash_add_requisicion";
$route['actualizar-requisicion'] = "requisicion/dash_update_requisicion";
$route['eliminar-requisicion/(:num)'] = "requisicion/delete_requisicion/$1";
$route['lista-requisiciones/(:num)'] = "requisicion/list_requisiciones/$1";
$route['municipios-perfil/(:num)/(:num)'] = "requisicion/getMunicipiosDepartamentos_x_perfil/$1/$2";
$route['add-mun-requisicion'] = "requisicion/dash_add_municipio_requisicion";
$route['delete-mun-requisicion'] = "requisicion/dash_delete_municipio_requisicion";
$route['preview-requisicion/(:num)'] = "requisicion/dash_view_requisicion/$1";
$route['get-suma-vacantes-perfil'] = "requisicion/getSumaVacantesPerfil";
//Aprobaciones
$route['lista-aprobaciones'] = "aprobaciones/list_aprobaciones";
$route['ver-requisicion/(:num)/(:num)'] = "aprobaciones/dash_requisicion/$1/$2";
$route['enviar-respuesta-requisicion'] = "aprobaciones/dash_accion_aprobacion";

//Convocatorias
$route['lista-convocatorias/(:num)'] = "convocatoria/dash_list_convocatorias/$1";
$route['nueva-convocatoria/(:num)'] = "convocatoria/dash_new_form_convocatoria/$1";
$route['crear-convocatoria'] = "convocatoria/dash_add_convocatoria";
$route['editar-convocatoria/(:num)'] = "convocatoria/dash_edit_convocatoria/$1";
$route['actualizar-convocatoria'] = "convocatoria/dash_update_convocatoria";
$route['listar-convocatorias'] = "convocatoria/dash_list_all_convocatorias";
$route['etapas/(:num)'] = "convocatoria/etapas_convocatorias/$1";
$route['seleccion/(:num)'] = "convocatoria/seleccion/$1";
$route['agregar-seleccion'] = "convocatoria/add_seleccion";
$route['ver-configurar-etapa/(:num)'] = "convocatoria/ver_configuracion/$1";
$route['resultados-finales/(:num)'] = "convocatoria/resultados_finales/$1";
$route['desactivar-convocatoria/(:num)'] = "convocatoria/unactive_convocatoria/$1";
$route['activar-convocatoria/(:num)'] = "convocatoria/active_convocatoria/$1";
// projects work offers
$route['ofertas-de-trabajo'] = "convocatoria/dash_list_convocatorias_activas";
$route['postularse-convocaria/(:num)'] = "convocatoria/add_user_convocartoria/$1";
$route['ver-postulantes/(:num)'] = "convocatoria/view_postulantes/$1";
$route['ver-coincidencias/(:num)/(:num)'] = "convocatoria/view_coincidencias/$1/$2";
$route['ver-coincidencias/(:num)'] = "convocatoria/view_coincidencias/$1";
// Socios Cooperantes
$route['lista-socios'] = "socioCooperante/list_socios";
$route['nuevo-socio'] = "socioCooperante/form_new_socio";
$route['agregar-socio'] = "socioCooperante/add_socio";
$route['editar-socio/(:num)'] = "socioCooperante/edit_socio/$1";
$route['eliminar-socio/(:num)'] = "socioCooperante/delete_socio/$1";
$route['update-socio'] = "socioCooperante/update_socio";

//Tipo de Socio
$route['lista-tipo-socio'] = "tipoSocio/list_tipo_socio";
$route['nuevo-tipo-socio'] = "tipoSocio/form_new_tipo_socio";
$route['agregar-tipo-socio'] = "tipoSocio/add_tipo_socio";
$route['editar-tipo-socio/(:num)'] = "tipoSocio/edit_tipo_socio/$1";
$route['eliminar-tipo-socio/(:num)'] = "tipoSocio/delete_tipo_socio/$1";
$route['update-tipo-socio'] = "tipoSocio/update_tipo_socio";

//Tipo de Responsabilidad
$route['lista-tipo-responsabilidad'] = "tipoResponsabilidad/list_tipo_responsabilidad";
$route['nuevo-tipo-responsabilidad'] = "tipoResponsabilidad/form_new_tipo_responsabilidad";
$route['agregar-tipo-responsabilidad'] = "tipoResponsabilidad/add_tipo_responsabilidad";
$route['editar-tipo-responsabilidad/(:num)'] = "tipoResponsabilidad/edit_tipo_responsabilidad/$1";
$route['eliminar-tipo-responsabilidad/(:num)'] = "tipoResponsabilidad/delete_tipo_responsabilidad/$1";
$route['update-tipo-responsabilidad'] = "tipoResponsabilidad/update_tipo_responsabilidad";

// Ubicación
$route['lista-ubicaciones'] = "ubicacion/list_ubicaciones";
$route['nueva-ubicacion'] = "ubicacion/form_new_ubicacion";
$route['agregar-ubicacion'] = "ubicacion/add_ubicacion";
$route['editar-ubicacion/(:num)'] = "ubicacion/edit_ubicacion/$1";
$route['eliminar-ubicacion/(:num)'] = "ubicacion/delete_ubicacion/$1";
$route['update-ubicacion'] = "ubicacion/update_ubicacion";

// Etapa
$route['lista-etapas'] = "etapa/list_etapas";
$route['nueva-etapa'] = "etapa/form_new_etapa";
$route['agregar-etapa'] = "etapa/add_etapa";
$route['editar-etapa/(:num)'] = "etapa/edit_etapa/$1";
$route['eliminar-etapa/(:num)'] = "etapa/delete_etapa/$1";
$route['update-etapa'] = "etapa/update_etapa";
$route['configurar-etapa/(:num)'] = "etapa/configurar_etapa/$1";
$route['config-etapa'] = "etapa/add_config";
// Areas
$route['lista-areas'] = "area/list_areas";
$route['nueva-area'] = "area/form_new_area";
$route['agregar-area'] = "area/add_area";
$route['editar-area/(:num)'] = "area/edit_area/$1";
$route['eliminar-area/(:num)'] = "area/delete_area/$1";
$route['update-area'] = "area/update_area";

// Licitaciones
$route['lista-licitaciones'] = "licitacion/list_licitaciones";
$route['nueva-licitacion'] = "licitacion/form_new_licitacion";
$route['agregar-licitacion'] = "licitacion/add_licitacion";
$route['editar-licitacion/(:num)'] = "licitacion/edit_licitacion/$1";
$route['eliminar-licitacion/(:num)'] = "licitacion/delete_licitacion/$1";
$route['update-licitacion'] = "licitacion/update_licitacion";
$route['convertir-a-proyecto/(:num)'] = "licitacion/convert_to_project/$1";

// Poblacion
$route['lista-poblaciones'] = "poblacion/list_poblaciones";
$route['nueva-poblacion'] = "poblacion/form_new_poblacion";
$route['agregar-poblacion'] = "poblacion/add_poblacion";
$route['editar-poblacion/(:num)'] = "poblacion/edit_poblacion/$1";
$route['eliminar-poblacion/(:num)'] = "poblacion/delete_poblacion/$1";
$route['update-poblacion'] = "poblacion/update_poblacion";

// Responsabilidad
$route['lista-responsabilidades'] = "responsabilidad/list_responsabilidades";
$route['nueva-responsabilidad'] = "responsabilidad/form_new_responsabilidad";
$route['crear-responsabilidad'] = "responsabilidad/add_responsabilidad";
$route['editar-responsabilidad/(:num)'] = "responsabilidad/edit_responsabilidad/$1";
$route['eliminar-responsabilidad/(:num)'] = "responsabilidad/delete_responsabilidad/$1";
$route['update-responsabilidad'] = "responsabilidad/update_responsabilidad";
$route['responsabilidades-x-tipo/(:num)'] = "profile/get_responsabilidades/$1";

//roles
$route['lista-roles'] = "roles/list_roles";
$route['lista-roles/(:num)'] = "roles/list_roles/$1";
$route['nuevo-rol'] = "roles/form_new_role";
$route['agregar-rol'] = "roles/add_role";
$route['actualizar-rol'] = "roles/update_role";

$route['desactivar-rol/(:num)'] = "roles/unactive_rol/$1";
$route['activar-rol/(:num)'] = "roles/active_rol/$1";
$route['permisos-rol/(:num)'] = "roles/form_permisions_rol/$1";
$route['eliminar-rol/(:num)'] = "roles/delete_rol/$1";
$route['editar-rol/(:num)'] = "roles/edit_rol/$1";
$route['actualizar-permisos'] = "roles/update_permissions";

//modules
$route['actualizar-modulo'] = "modules/update_module";
$route['lista-modulos'] = "modules/list_modules";
$route['lista-modulos/(:num)'] = "modules/list_modules/$1";
$route['nuevo-modulo'] = "modules/form_new_module";
$route['agregar-modulo'] = "modules/add_module";
$route['editar-modulo/(:num)'] = "modules/edit_module/$1";

//Componentes de Competencias
$route['nuevo-cpnte-edu'] = "cpntesCompe/form_new_lvl_education";
$route['nuevo-cpnte-form'] = "cpntesCompe/form_new_lvl_formation";
$route['nuevo-cpnte-exp'] = "cpntesCompe/form_new_exp_lab";
$route['nuevo-cpnte-hab'] = "cpntesCompe/form_new_desc_hab";

$route['agregar-cpnte'] = "cpntesCompe/add_cpnte";

$route['lista-cpnte-edu'] = "cpntesCompe/list_lvl_education";
$route['lista-cpnte-form'] = "cpntesCompe/list_lvl_formation";
$route['lista-cpnte-exp'] = "cpntesCompe/list_lvl_exp_lab";
$route['lista-cpnte-hab'] = "cpntesCompe/list_lvl_desc_hab";

$route['lista-cpnte-edu/(:num)'] = "cpntesCompe/list_lvl_education/$1";
$route['lista-cpnte-form/(:num)'] = "cpntesCompe/list_lvl_formation/$1";
$route['lista-cpnte-exp/(:num)'] = "cpntesCompe/list_lvl_exp_lab/$1";
$route['lista-cpnte-hab/(:num)'] = "cpntesCompe/list_lvl_desc_hab/$1";

$route['actualizar-cpnte'] = "cpntesCompe/update_cpnte";

$route['editar-cpnte/(:num)'] = "cpntesCompe/edit_cpnte/$1";
$route['eliminar-cpnte/(:num)'] = "cpntesCompe/delete_cpnte/$1";
$route['desactivar-cpnte/(:num)'] = "cpntesCompe/unactive_cpnte/$1";
$route['activar-cpnte/(:num)'] = "cpntesCompe/active_cpnte/$1";

$route['lista-cate-cpnte'] = "categoriasComponentes/list_cats";
$route['nuevo-cate-cpnte'] = "categoriasComponentes/form_new_cat_comp";
$route['agregar-cat-cpnte'] = "categoriasComponentes/add_cat";

$route['editar-cat/(:num)'] = "categoriasComponentes/edit_cat/$1";
$route['activar-cat/(:num)'] = "categoriasComponentes/active_cat/$1";
$route['eliminar-cat/(:num)'] = "categoriasComponentes/delete_cat/$1";
$route['desactivar-cat/(:num)'] = "categoriasComponentes/unactive_cat/$1";
$route['actualizar-cat'] = "categoriasComponentes/update_cat";

//Hoja de vida
$route['hoja-de-vida'] = "hv";
$route['agregar-hv-hijo'] = "hv/add_child";
$route['agregar-hv-inf-academic'] = "hv/add_infaca";
$route['agregar-hv-exp-lab'] = "hv/add_explab";
$route['agregar-hv-est-comp'] = "hv/add_estcomp";
$route['agregar-hv-ref-pers'] = "hv/add_ref";
$route['hv-nueva-actividad/(:num)'] = "hv/form_new_activity/$1";
$route['agregar-actividad'] = "hv/add_activity";
$route['agregar-hv-est-ext'] = "hv/add_est_ext";
$route['editar-est-ext/(:num)'] = "hv/edit_est_ext/$1";
$route['actualizar-hv-est-ext'] = "hv/update_est_ext";
$route['eliminar-est-ext/(:num)'] = "hv/delete_est_ext/$1";
//hijos
$route['editar-hijo/(:num)'] = 'hv/edit_child/$1';
$route['eliminar-hijo/(:num)'] = 'hv/delete_child/$1';
$route['actualizar-hv-hijo'] = "hv/update_child";
//Informacion Academica
$route['editar-info-aca/(:num)'] = 'hv/edit_info_acad/$1';
$route['eliminar-info-aca/(:num)'] = 'hv/delete_info_acad/$1';
$route['actualizar-hv-inf-academic'] = "hv/update_info_acad";
//Estudios Complementarios
$route['editar-est-comp/(:num)'] = 'hv/edit_est_comp/$1';
$route['eliminar-est-comp/(:num)'] = 'hv/delete_est_comp/$1';
$route['actualizar-hv-est-comp'] = "hv/update_est_comp";
//Referencias
$route['editar-ref/(:num)'] = 'hv/edit_ref/$1';
$route['eliminar-ref/(:num)'] = 'hv/delete_ref/$1';
$route['actualizar-hv-ref'] = "hv/update_ref";
//Descripcion de Habilidades
$route['agregar-hv-desc-hab'] = "hv/add_desc_hab";
$route['editar-desc-hab/(:num)'] = 'hv/edit_desc_hab/$1';
$route['eliminar-desc-hab/(:num)'] = 'hv/delete_desc_hab/$1';
$route['actualizar-hv-desc-habilidad'] = "hv/update_desc_hab";
//Experiencia Laboral
$route['hv-editar-exp-lab/(:num)'] = 'hv/edit_exp_lab/$1';
$route['hv-eliminar-exp-lab/(:num)'] = 'hv/delete_exp_lab/$1';
$route['actualizar-hv-exp-lab'] = "hv/update_exp_lab";

//Actividades de Experiencia Laboral
$route['hv-editar-actividad/(:num)/(:num)'] = 'hv/edit_act/$1/$2';
$route['hv-eliminar-actividad/(:num)/(:num)'] = 'hv/delete_act/$1/$2';
$route['actualizar-actividad'] = "hv/update_act";

//Cargos
$route['lista-cargos'] = "cargos/list_cargos";
$route['nuevo-cargo'] = "cargos/form_new_cargo";
$route['agregar-cargo'] = "cargos/add_cargo";
$route['editar-cargo/(:num)'] = "cargos/edit_cargo/$1";
$route['eliminar-cargo/(:num)'] = "cargos/delete_cargo/$1";
$route['actualizar-cargo'] = "cargos/update_cargo";
$route['activar-cargo/(:num)'] = "cargos/active_cargo/$1";
$route['desactivar-cargo/(:num)'] = "cargos/unactive_cargo/$1";

//pruebas
$route['lista-pruebas'] = "pruebas/list_test";
$route['nueva-prueba'] = "pruebas/form_new_test";
$route['agregar-prueba'] = "pruebas/add_test";
$route['ver-prueba/(:num)'] = "pruebas/view_test/$1";
$route['editar-prueba/(:num)'] = "pruebas/edit_test/$1";
$route['actualizar-prueba'] = "pruebas/update_test";
$route['desactivar-prueba/(:num)'] = "pruebas/unactive_test/$1";
$route['activar-prueba/(:num)'] = "pruebas/active_test/$1";
$route['eliminar-prueba/(:num)'] = "pruebas/delete_test/$1";
$route['duplicar-prueba/(:num)'] = "pruebas/duplicar_test/$1";

//preguntas
$route['lista-preguntas'] = "preguntas/list_questions";
$route['nueva-pregunta'] = "preguntas/form_new_question";
$route['agregar-pregunta'] = "preguntas/add_question";

//pruebas preguntas
$route['listar-preguntas/(:num)'] = "preguntas/list_test_questions/$1";
$route['agregar-preguntas'] = "preguntas/add_questions";
$route['editar-pregunta/(:num)'] = "preguntas/edit_question/$1";
$route['actualizar-pregunta'] = "preguntas/update_question";
$route['desactivar-pregunta/(:num)'] = "preguntas/unactive_question/$1";
$route['activar-pregunta/(:num)'] = "preguntas/active_question/$1";
$route['eliminar-pregunta/(:num)'] = "preguntas/delete_question/$1";

$route['test/(:num)'] = "pruebas/test/$1";
$route['comenzar-prueba'] = "pruebas/start_test";
$route['enviar-prueba'] = "pruebas/set_test";
$route['resultados/(:num)'] = "pruebas/test_result/$1";
$route['calificar-manual/(:num)'] = "pruebas/calificar/$1";
$route['guardar-calificacion'] = "pruebas/update_calificacion";

$route['generar-archivo/(:num)'] = "pruebas/generar_archivo/$1";
$route['hoja-de-vida-pdf/(:num)'] = "convocatoria/generar_pdf_hv/$1";

//grados
$route['lista-grados'] = "grados/list_grados";
$route['nuevo-grado'] = "grados/form_new_grado";
$route['agregar-grado'] = "grados/add_grado";
$route['editar-grado/(:num)'] = "grados/edit_grado/$1";
$route['activar-grado/(:num)'] = "grados/active_grado/$1";
$route['eliminar-grado/(:num)'] = "grados/delete_grado/$1";
$route['desactivar-grado/(:num)'] = "grados/unactive_grado/$1";
$route['actualizar-grado'] = "grados/update_grado";

//flujo de aprobaciones
$route['lista-flujo-aprobaciones'] = "flujos/list_flujo_aprobaciones";
$route['actualizar-flujo-aprobaciones'] = "flujos/update_orden_flujo_aprobarciones";
$route['editar-flujo/(:num)'] = "flujos/form_edit_flujo_aprobaciones/$1";
$route['actualizar-flujo'] = "flujos/update_flujo";
$route['nuevo-flujo-aprobacion'] = "flujos/form_new_flujo";
$route['agregar-flujo'] = "flujos/add_flujo";
$route['eliminar-flujo/(:num)'] = "flujos/delete_flujo/$1";