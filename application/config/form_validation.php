<?php

defined('BASEPATH') OR exit('No direct script access allowed');
$config = array(
    'dash_add_user' => array(
        array(
            'field' => 'user_email',
            'label' => 'Correo Electronico',
            'rules' => 'trim|valid_email|required|min_length[2]|max_length[150]|is_unique[usuario.EmailUsuario]'
        ),
        array(
            'field' => 'user_nombres',
            'label' => 'Nombres',
            'rules' => 'required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'user_apellidos',
            'label' => 'Apellidos',
            'rules' => 'required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'user_pass',
            'label' => 'Contraseña',
            'rules' => 'trim|required|callback_check_password_strength'
        ),
        array(
            'field' => 'user_pass_repeat',
            'label' => 'Repite Contraseña',
            'rules' => 'trim|required|matches[user_pass]'
        ),
        array(
            'field' => 'user_rol',
            'label' => 'Rol de Usuario',
            'rules' => 'trim|is_natural_no_zero'
        ),
        array(
            'field' => 'user_status',
            'label' => 'Status de Usuario',
            'rules' => 'trim|is_natural_no_zero'
        ),
        array(
            'field' => 'user_cargo',
            'label' => 'Cargo de Usuario',
            'rules' => 'trim|is_natural_no_zero'
        )
    ),
    'dash_update_user' => array(
        array(
            'field' => 'user_nombres',
            'label' => 'Nombres',
            'rules' => 'required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'user_apellidos',
            'label' => 'Apellidos',
            'rules' => 'required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'user_rol',
            'label' => 'Rol de Usuario',
            'rules' => 'trim|is_natural_no_zero'
        ),
        array(
            'field' => 'user_status',
            'label' => 'Status de Usuario',
            'rules' => 'trim|is_natural_no_zero'
        ),
        array(
            'field' => 'user_cargo',
            'label' => 'Cargo de Usuario',
            'rules' => 'trim|is_natural_no_zero'
        )
    ),
    'signin' => array(
        array(
            'field' => 'user_email',
            'label' => 'Correo Electronico',
            'rules' => 'trim|valid_email|required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'user_pass',
            'label' => 'Contraseña',
            'rules' => 'trim|required'
        )
    ),
    'ext_register_user' => array(
        array(
            'field' => 'user_email',
            'label' => 'Correo Electronico',
            'rules' => 'trim|valid_email|required|min_length[2]|max_length[150]|is_unique[usuario.EmailUsuario]'
        )
    ),
    'dash_update_password_user' => array(
        array(
            'field' => 'user_pass',
            'label' => 'Contraseña',
            'rules' => 'trim|required|callback_check_password_strength'
        ),
        array(
            'field' => 'user_pass_repeat',
            'label' => 'Repite Contraseña',
            'rules' => 'trim|required|matches[user_pass]'
        )
    ),
    'forgot_pass' => array(
        array(
            'field' => 'user_email',
            'label' => 'Correo Electronico',
            'rules' => 'trim|valid_email|required|min_length[2]|max_length[150]'
        )
    ),
    'ext_update_profile' => array(
        array(
            'field' => 'documento_identidad',
            'label' => 'Documento de Identidad',
            'rules' => 'trim|required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'tipo_documento',
            'label' => 'Tipo de Identificación',
            'rules' => 'trim|required|is_natural_no_zero'
        ),
        array(
            'field' => 'nombres',
            'label' => 'Nombre',
            'rules' => 'trim|required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'apellidos',
            'label' => 'Apellido',
            'rules' => 'trim|required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'fecha_nacimiento',
            'label' => 'Fecha de Nacimiento',
            'rules' => 'trim|required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'municipio_expedicion',
            'label' => 'Minicipio de Expedición',
            'rules' => 'trim|required|max_length[150]'
        ),
        array(
            'field' => 'celular',
            'label' => 'Celular',
            'rules' => 'trim|required|min_length[10]|max_length[150]|numeric'
        ),
        array(
            'field' => 'telefono',
            'label' => 'Telefono',
            'rules' => 'trim|min_length[7]|max_length[10]|is_natural'
        ),
        array(
            'field' => 'direccion',
            'label' => 'Dirección',
            'rules' => 'trim|required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'municipio_residencia',
            'label' => 'Municipio de Residencia',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'genero',
            'label' => 'Genero',
            'rules' => 'trim|required|max_length[150]'
        ),
        array(
            'field' => 'tarjeta_profesional',
            'label' => 'Número de Tarjeta Profesional',
            'rules' => 'trim|min_length[5]|max_length[15]|is_natural'
        ),
    ),
    'dash_add_role' => array(
        array(
            'field' => 'rol_name',
            'label' => 'Nombre del Rol',
            'rules' => 'required|alpha|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'rol_status',
            'label' => 'Status del Rol',
            'rules' => 'required|is_natural'
        )
    ),
    'dash_update_role' => array(
        array(
            'field' => 'rol_name',
            'label' => 'Nombre del Rol',
            'rules' => 'required|alpha|min_length[2]|max_length[150]'
        )
    ),    
    'dash_add_cargo' => array(
        array(
            'field' => 'cargo_name',
            'label' => 'Nombre del Cargo',
            'rules' => 'required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'cargo_status',
            'label' => 'Status del Rol',
            'rules' => 'required|is_natural'
        )
    ),    
    'dash_update_cargo' => array(
        array(
            'field' => 'cargo_name',
            'label' => 'Nombre del Cargo',
            'rules' => 'required|min_length[2]|max_length[150]'
        )
    ),
    'dash_new_project' => array(
        array(
            'field' => 'nombre_proyecto',
            'label' => 'Nombre del Proyecto',
            'rules' => 'required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'codigo_contrato',
            'label' => 'Código del Contrato',
            'rules' => 'min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'identificacion_proyecto',
            'label' => 'Identificación del Proyecto',
            'rules' => 'min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'fecha_inicio_contrato',
            'label' => 'Fecha de Inicio de Contrato',
            'rules' => 'required'
        )
    ),
    'dash_add_module' => array(
        array(
            'field' => 'module_nick',
            'label' => 'Nombre del Modulo',
            'rules' => 'required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'module_name',
            'label' => 'Modulo',
            'rules' => 'required|min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'module_submodule',
            'label' => 'submodulo',
            'rules' => 'required|min_length[2]|max_length[150]'
        )
    ),
    'dash_update_module' => array(
        array(
            'field' => 'module_nick',
            'label' => 'Nombre del Modulo',
            'rules' => 'min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'module_name',
            'label' => 'Modulo',
            'rules' => 'min_length[2]|max_length[150]'
        ),
        array(
            'field' => 'module_submodule',
            'label' => 'submodulo',
            'rules' => 'min_length[2]|max_length[150]'
        )
    ),
    'dash_add_cpnte' => array(
        array(
            'field' => 'cpnte_description',
            'label' => 'Descripcion',
            'rules' => 'required|min_length[5]'
        )
    ),
    'dash_update_cpnte' => array(
        array(
            'field' => 'cpnte_description',
            'label' => 'Descripcion',
            'rules' => 'required|min_length[5]'
        )
    ),
    'dash_new_profile' => array(
        array(
            'field' => 'area',
            'label' => 'Proyecto',
            'rules' => 'required'
        ),
        array(
            'field' => 'mision_cargo',
            'label' => 'Misión del  Cargo',
            'rules' => 'required'
        ),
        array(
            'field' => 'titulo_cargo',
            'label' => 'Título del Cargo',
            'rules' => 'required'
        ),
        array(
            'field' => 'numero_ocupantes',
            'label' => 'Número de Vacantes',
            'rules' => 'required|is_natural|callback_check_total_ocupantes_x_municipio'
        )
    ),
    'dash_add_socio' => array(
        array(
            'field' => 'socio_name',
            'label' => 'Nombre del Socio Cooperante',
            'rules' => 'required|min_length[1]|max_length[150]'
        ),
        array(
            'field' => 'tipo_socio',
            'label' => 'Nombre del Tipo de Socio',
            'rules' => 'required'
        )
    ),
    'dash_update_socio' => array(
        array(
            'field' => 'socio_name',
            'label' => 'Nombre del Socio Cooperante',
            'rules' => 'required|min_length[1]|max_length[150]'
        ),
        array(
            'field' => 'tipo_socio',
            'label' => 'Tipo de Socio',
            'rules' => 'required'
        )
    ),
    'dash_add_tipo_socio' => array(
        array(
            'field' => 'tipo_socio_name',
            'label' => 'Nombre del Tipo de Socio',
            'rules' => 'required|min_length[1]|max_length[150]'
        )
    ),
    'dash_update_tipo_socio' => array(
        array(
            'field' => 'tipo_socio_name',
            'label' => 'Tipo de Socio',
            'rules' => 'required|min_length[1]|max_length[150]'
        )
    ),
    'dash_add_licitacion' => array(
        array(
            'field' => 'licitacion_name',
            'label' => 'Nombre de la Licitación',
            'rules' => 'required|min_length[5]|max_length[150]'
        )
    ),
    'dash_add_tipo_responsabilidad' => array(
        array(
            'field' => 'tipo_responsabilidad_name',
            'label' => 'Nombre del Tipo de Responsabilidad',
            'rules' => 'required'
        )
    ),
    'dash_update_tipo_responsabilidad' => array(
        array(
            'field' => 'tipo_responsabilidad_name',
            'label' => 'Tipo de Responsabilidad',
            'rules' => 'required'
        )
    ),
    'dash_update_licitacion' => array(
        array(
            'field' => 'licitacion_name',
            'label' => 'Nombre de la Licitación',
            'rules' => 'required|min_length[5]|max_length[150]'
        )
    ),
    'dash_add_area' => array(
        array(
            'field' => 'area_name',
            'label' => 'Nombre del Área',
            'rules' => 'required|min_length[5]|max_length[150]'
        )
    ),
    'dash_update_area' => array(
        array(
            'field' => 'area_name',
            'label' => 'Nombre del Área',
            'rules' => 'required|min_length[5]|max_length[150]'
        )
    ),
    'dash_add_poblacion' => array(
        array(
            'field' => 'poblacion_name',
            'label' => 'Nombre de Población',
            'rules' => 'required|min_length[5]|max_length[150]'
        )
    ),
    'dash_update_poblacion' => array(
        array(
            'field' => 'poblacion_name',
            'label' => 'Nombre de Población',
            'rules' => 'required|min_length[5]|max_length[150]'
        )
    ),
    'dash_add_responsabilidad' => array(
        array(
            'field' => 'responsabilidad_name',
            'label' => 'Descripción de la Responsabilidad',
            'rules' => 'required|min_length[5]'
        ),
        array(
            'field' => 'tipo_responsabilidad',
            'label' => 'Tipo de Responsabilidad',
            'rules' => 'required'
        )
    ),
    'dash_update_responsabilidad' => array(
        array(
            'field' => 'responsabilidad_name',
            'label' => 'Descripción de la Responsabilidad',
            'rules' => 'required|min_length[5]'
        ),
        array(
            'field' => 'tipo_responsabilidad',
            'label' => 'Tipo de Responsabilidad',
            'rules' => 'required'
        )
    ),
    'dash_add_ubicacion' => array(
        array(
            'field' => 'ubicacion_name',
            'label' => 'Descripción de la Ubicación',
            'rules' => 'required|min_length[1]|max_length[150]'
        )
    ),
    'dash_update_ubicacion' => array(
        array(
            'field' => 'ubicacion_name',
            'label' => 'Descripción de la Ubicación',
            'rules' => 'required|min_length[1]|max_length[150]'
        )
    )
,
    'dash_add_etapa' => array(
        array(
            'field' => 'etapa_name',
            'label' => 'Nombre de la Etapa',
            'rules' => 'required|min_length[1]|max_length[150]'
        ),
        array(
            'field' => 'etapa_desc',
            'label' => 'Descripción de la Etapa',
            'rules' => 'required|min_length[1]|max_length[150]'
        )
    ),
    'dash_update_etapa' => array(
        array(
            'field' => 'etapa_name',
            'label' => 'Nombre de la Etapa',
            'rules' => 'required|min_length[1]|max_length[150]'
        ),
        array(
            'field' => 'etapa_desc',
            'label' => 'Descripción de la Etapa',
            'rules' => 'required|min_length[1]|max_length[150]'
        )
    ),
    'dash_add_hv_child' => [
        [
            'field' => 'hv_child_name',
            'label' => 'Nombre Completo',
            'rules' => 'required|min_length[10]|max_length[150]'
        ],
        [
            'field' => 'hv_child_birthdate',
            'label' => 'Fecha de Nacimiento',
            'rules' => 'required|min_length[8]|max_length[150]'
        ]
    ],
    'dash_update_hv_child' => [
        [
            'field' => 'hv_child_name',
            'label' => 'Nombre Completo',
            'rules' => 'required|min_length[10]|max_length[150]'
        ],
        [
            'field' => 'hv_child_birthdate',
            'label' => 'Fecha de Nacimiento',
            'rules' => 'required|min_length[8]|max_length[150]'
        ]
    ],
    'dash_add_hv_infaca' => [
        [
            'field' => 'hv_ia_titulo',
            'label' => 'Titulo Obtenido',
            'rules' => 'is_natural'
        ],
        [
            'field' => 'hv_name_inst',
            'label' => 'Nombre de la Institución',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_date_grade',
            'label' => 'Fecha de Nacimiento',
            'rules' => 'required|min_length[8]|max_length[150]'
        ]
    ],
    'dash_update_hv_infaca' => [
        [
            'field' => 'hv_ia_titulo',
            'label' => 'Titulo Obtenido',
            'rules' => 'required|is_natural'
        ],
        [
            'field' => 'hv_name_inst',
            'label' => 'Nombre de la Institución',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_date_grade',
            'label' => 'Fecha de Nacimiento',
            'rules' => 'required|min_length[8]|max_length[150]'
        ]
    ],
    'dash_add_hv_explab' => [
        [
            'field' => 'hv_explab_cargo',
            'label' => 'Cargo',
            'rules' => 'required|min_length[4]|max_length[150]'
        ],
        [
            'field' => 'hv_explab_dep',
            'label' => 'Departamento',
            'rules' => 'required|is_natural'
        ],
        [
            'field' => 'hv_explab_jefe_name',
            'label' => 'Jefe Inmediato',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_explab_date_ini',
            'label' => 'Fecha de Inicio',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_explab_date_fin',
            'label' => 'Fecha de Fenalizacion',
            'rules' => 'required|min_length[8]|max_length[150]'
        ]
    ],
    'dash_update_hv_explab' => [
        [
            'field' => 'hv_explab_cargo',
            'label' => 'Cargo',
            'rules' => 'required|min_length[4]|max_length[150]'
        ],
        [
            'field' => 'hv_explab_dep',
            'label' => 'Departamento',
            'rules' => 'required|is_natural'
        ],
        [
            'field' => 'hv_explab_jefe_name',
            'label' => 'Jefe Inmediato',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_explab_date_ini',
            'label' => 'Fecha de Inicio',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_explab_date_fin',
            'label' => 'Fecha de Fenalizacion',
            'rules' => 'required|min_length[8]|max_length[150]'
        ]
    ],
    'dash_add_hv_estcomp' => [
        [
            'field' => 'hv_ec_titulo',
            'label' => 'Titulo Obtenido',
            'rules' => 'required|is_natural'
        ],
        [
            'field' => 'hv_ec_inst',
            'label' => 'Nombre de la Institución Certificadora',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_ec_date_cert',
            'label' => 'Fecha de Certificacion',
            'rules' => 'required|min_length[8]|max_length[150]'
        ]
        
    ],
    'dash_update_hv_estcomp' => [
        [
            'field' => 'hv_ec_titulo',
            'label' => 'Titulo Obtenido',
            'rules' => 'required|is_natural'
        ],
        [
            'field' => 'hv_ec_inst',
            'label' => 'Nombre de la Institución Certificadora',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_ec_date_cert',
            'label' => 'Fecha de Certificacion',
            'rules' => 'required|min_length[8]|max_length[150]'
        ]
        
    ],
    'dash_add_hv_estext' => [
        [
            'field' => 'hv_ex_titulo',
            'label' => 'Estudio Complementario',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_ex_inst',
            'label' => 'Nombre de la Institución Certificadora',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_ex_date_cert',
            'label' => 'Fecha de Certificacion',
            'rules' => 'required|min_length[8]|max_length[150]'
        ]        
    ],
    'dash_update_hv_estext' => [
        [
            'field' => 'hv_ex_titulo',
            'label' => 'Estudio Complementario',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_ex_inst',
            'label' => 'Nombre de la Institución Certificadora',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_ex_date_cert',
            'label' => 'Fecha de Certificacion',
            'rules' => 'required|min_length[8]|max_length[150]'
        ]
        
    ],
    'dash_add_hv_ref' => [
        [
            'field' => 'hv_ref_per_name',
            'label' => 'Nombre Completo',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_ref_per_prof',
            'label' => 'Profesion/Ocupacion',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_ref_per_tel',
            'label' => 'Telefono',
            'rules' => 'required|min_length[7]|max_length[20]'
        ]
        
    ],
    'dash_update_hv_ref' => [
        [
            'field' => 'hv_ref_per_name',
            'label' => 'Nombre Completo',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_ref_per_prof',
            'label' => 'Profesion/Ocupacion',
            'rules' => 'required|min_length[8]|max_length[150]'
        ],
        [
            'field' => 'hv_ref_per_tel',
            'label' => 'Telefono',
            'rules' => 'required|min_length[7]|max_length[20]'
        ]
        
    ],
    'dash_add_hv_act' => [
        [
            'field' => 'hv_act_titulo',
            'label' => 'Actividad Desarrollada',
            'rules' => 'required|is_natural'
        ],
        [
            'field' => 'hv_act_exp_a',
            'label' => 'Años de Experiencia',
            'rules' => 'required|min_length[1]|is_natural'
        ],
        [
            'field' => 'hv_act_exp_m',
            'label' => 'Meses de Experiencia',
            'rules' => 'required|min_length[1]|is_natural'
        ],
        
    ],
    'dash_update_hv_act' => [
        [
            'field' => 'hv_act_titulo',
            'label' => 'Actividad Desarrollada',
            'rules' => 'required|is_natural'
        ],
        [
            'field' => 'hv_act_exp_a',
            'label' => 'Años de Experiencia',
            'rules' => 'required|min_length[1]|is_natural'
        ],
        [
            'field' => 'hv_act_exp_m',
            'label' => 'Meses de Experiencia',
            'rules' => 'required|min_length[1]|is_natural'
        ],
        
    ],
    'dash_add_cat' => [
        [
            'field' => 'cat_name',
            'label' => 'Nombre de la categoria',
            'rules' => 'required|min_length[5]|max_length[150]'
        ],
        [
            'field' => 'cat_status',
            'label' => 'Estatus de Categoria',
            'rules' => 'required|is_natural_no_zero'
        ],        
        
    ],
    'dash_add_grado' => [
        [
            'field' => 'grado_name',
            'label' => 'Nombre de Grado',
            'rules' => 'required|min_length[5]|max_length[150]'
        ],
        [
            'field' => 'grado_status',
            'label' => 'Estatus de Grado',
            'rules' => 'required|is_natural_no_zero'
        ],        
        
    ],
    'dash_update_cat' => [
        [
            'field' => 'cat_name',
            'label' => 'Nombre de la categoria',
            'rules' => 'required|min_length[5]|max_length[150]'
        ],
        
    ],
    'dash_update_grado' => [
        [
            'field' => 'grado_name',
            'label' => 'Nombre de Grado',
            'rules' => 'required|min_length[5]|max_length[150]'
        ],
        
    ],
    'dash_new_req' => [
        [
            'field' => 'tipo_contrato',
            'label' => 'Tipo de Contrato',
            'rules' => 'required'
        ],
        [
            'field' => 'fecha_inicio_labores',
            'label' => 'Fecha de Inicio de Labores',
            'rules' => 'required|callback_valida_fecha'
        ],
        [
            'field' => 'justificacion',
            'label' => 'Justificación',
            'rules' => 'required'
        ],
        [
            'field' => 'salario_asignado',
            'label' => 'Salario Asignado',
            'rules' => 'required'
        ],
        [
            'field' => 'motivos',
            'label' => 'Motivo',
            'rules' => 'required'
        ]
    ],
    'dash_edit_req' => [
        [
            'field' => 'tipo_contrato',
            'label' => 'Tipo de Contrato',
            'rules' => 'required'
        ],
        [
            'field' => 'fecha_inicio_labores',
            'label' => 'Fecha de Inicio de Labores',
            'rules' => 'required|callback_valida_fecha'
        ],
        [
            'field' => 'justificacion',
            'label' => 'Justificación',
            'rules' => 'required'
        ],
        [
            'field' => 'salario_asignado',
            'label' => 'Salario Asignado',
            'rules' => 'required'
        ],
        [
            'field' => 'motivos',
            'label' => 'Motivo',
            'rules' => 'required'
        ]
    ],
    
    'dash_add_test' => [
        [
            'field' => 'test_name',
            'label' => 'Nombre de la Prueba',
            'rules' => 'required|min_length[5]|max_length[250]'
        ],
        
    ],
    'dash_add_question' => [
        [
            'field' => 'pregunta_tipo',
            'label' => 'Tipo de Pregunta',
            'rules' => 'required'
        ],
        [
            'field' => 'pregunta_title',
            'label' => 'Pregunta',
            'rules' => 'required'
        ],
        [
            'field' => 'pregunta_options',
            'label' => 'Opciones de la pregunta',
            'rules' => 'min_length[2]'
        ],
        [
            'field' => 'pregunta_resp',
            'label' => 'Respuesta',
            'rules' => 'min_length[1]'
        ]
    ],
    'dash_add_desc_hab' => [
        [
            'field' => 'hv_desc_hab',
            'label' => 'Tipo de Pregunta',
            'rules' => 'required|numeric'
        ]
    ],
);
