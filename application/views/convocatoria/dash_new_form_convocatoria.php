<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-offset-2 col-lg-8 col-md-12">
        <div class="panel panel-white">
            <!--
            <div class="panel-heading">
                <h5 class="panel-title"></h5>
            </div>
            -->
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }

                ?>
                <h1 align="center">
                    <small>Información de la Convocatoria</small>
                </h1>
                <form role="form" action="<?php echo base_url('crear-convocatoria'); ?>" method="POST">
                    <input type="hidden" name="idPerfil" id="idPerfil" value="<?php echo $idPerfil ?>">
                    <input type="hidden" name="idReq" id="idReq" value="<?php echo $convocatoria->RequisicionID ?>">
                    <input type="hidden" id="baseUrl" name="baseUrl" value="<?php echo base_url(); ?>">

                    <div class="form-group">
                        <label for="nombre_cargo">
                            Título del Cargo
                        </label>
                        <input type="text" class="form-control" name="nombre_cargo" id="nombre_cargo" value="<?php echo $perfil->TituloCargo; ?>" placeholder="Nombre del Cargo" readonly>
                    </div>
                    <div class="form-group">
                        <label for="objeto_contrato">
                            Descripción
                        </label>
                        <textarea class="form-control autosize area-animated" id="descripcion_cargo" name="descripcion_cargo" data-autosize-on="true" style="overflow: hidden; resize: horizontal; word-wrap: break-word; height: 71px;" readonly><?php echo $convocatoria->MisionCargo; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="area">
                            Modalidad
                        </label>
                        <input type="text" class="form-control" name="tipo_contrato" value="<?php echo $convocatoria->NombreTipoContrato; ?>" placeholder="Nombre del Proyecto" readonly>
                    </div>
                    <div class="form-group">
                        <label for="cant_solicitadas">
                            Asignación Salarial
                        </label>
                        <input type="text" class="form-control" name="salario_asignado" id="salario_asignado" placeholder="Salario Asignado" value="<?php if(set_value('salario_asignado')){ echo set_value('salario_asignado'); }else{ echo number_format($convocatoria->SalarioAsignado); } ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="cant_solicitadas">
                            Duración del Contrato
                        </label>
                        <input type="text" class="form-control" name="duracion_contrato" id="duracion_contrato" placeholder="Salario Asignado" value="<?php if(set_value('duracion_contrato')){ echo set_value('duracion_contrato'); }else{ echo $convocatoria->DuracionContrato; } ?>" readonly>
                    </div>
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h4 class="panel-title">Lugar de Trabajo</h4>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered table-hover table-full-width" id="tbl_municipios_new_req">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Departamentos</th>
                                    <th>Municipios</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($muns as $mun)
                                {
                                    ?>
                                    <tr><td><?php echo $mun->MunicipioRequisicionID ?></td>
                                        <td><?php echo $mun->NombreDepartamento ?></td>
                                        <td><?php echo $mun->NombreMunicipio ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                            <br />
                        </div>
                    </div>
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h4 class="panel-title">Nivel Educativo</h4>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered table-hover table-full-width" id="tbl_nivel_educativo">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Item</th>
                                    <th>Peso</th>
                                    <th>EG</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($itemsCompetencias as $item)
                                {
                                    if($item->TipoCatalogoID == 1)
                                    {
                                        ?>
                                        <tr>
                                            <input type='hidden' id='hdItemsId' name='hdItemsId[]' value='<?php echo $item->CatalogoPerfilID ?>' />
                                            <td>
                                                <?php echo $item->ComponenteCompetenciaID ?>
                                            </td>
                                            <td><?php echo $item->Descripcion ?></td>
                                            <td><?php echo $item->Peso ?></td>
                                            <td><?php
                                                $exp = $item->TiempoExperiencia;
                                                $meses = 0;
                                                $anios = 0;

                                                while($exp > 12)
                                                {
                                                    $exp = $exp - 12;
                                                    $anios++;
                                                }
                                                $meses = $exp;

                                                echo $anios . ' años ' . $meses . ' meses';
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h4 class="panel-title">Nivel de Formación</h4>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered table-hover table-full-width" id="tbl_nivel_formacion">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Item</th>
                                    <th>Peso</th>
                                    <th>EG</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($itemsCompetencias as $item)
                                {
                                    if($item->TipoCatalogoID == 2)
                                    {
                                        ?>
                                        <tr>
                                            <input type='hidden' id='hdItemsId' name='hdItemsId[]' value='<?php echo $item->CatalogoPerfilID ?>' />
                                            <td><?php echo $item->ComponenteCompetenciaID ?></td>
                                            <td><?php echo $item->Descripcion ?></td>
                                            <td><?php echo $item->Peso ?></td>
                                            <td><?php
                                                $exp = $item->TiempoExperiencia;
                                                $meses = 0;
                                                $anios = 0;

                                                while($exp > 12)
                                                {
                                                    $exp = $exp - 12;
                                                    $anios++;
                                                }
                                                $meses = $exp;

                                                echo $anios . ' años ' . $meses . ' meses';
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h4 class="panel-title">Experiencia</h4>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered table-hover table-full-width" id="tbl_exp_general">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Item</th>
                                    <th>Peso</th>
                                    <th>EG</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($itemsCompetencias as $item)
                                {
                                    if($item->TipoCatalogoID == 3)
                                    {
                                        ?>
                                        <tr>
                                            <input type='hidden' id='hdItemsId' name='hdItemsId[]' value='<?php echo $item->CatalogoPerfilID ?>' />
                                            <td><?php echo $item->ComponenteCompetenciaID ?></td>
                                            <td><?php echo $item->Descripcion ?></td>
                                            <td><?php echo $item->Peso ?></td>
                                            <td><?php
                                                $exp = $item->TiempoExperiencia;
                                                $meses = 0;
                                                $anios = 0;

                                                while($exp > 12)
                                                {
                                                    $exp = $exp - 12;
                                                    $anios++;
                                                }
                                                $meses = $exp;

                                                echo $anios . ' años ' . $meses . ' meses';
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h4 class="panel-title">Habilidades</h4>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered table-hover table-full-width" id="tbl_habilidades">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Item</th>
                                    <th>Peso</th>
                                    <th>EG</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($itemsCompetencias as $item)
                                {
                                    if($item->TipoCatalogoID == 4)
                                    {
                                        ?>
                                        <tr>
                                            <input type='hidden' id='hdItemsId' name='hdItemsId[]' value='<?php echo $item->CatalogoPerfilID ?>' />
                                            <td><?php echo $item->ComponenteCompetenciaID ?></td>
                                            <td><?php echo $item->Descripcion ?></td>
                                            <td><?php echo $item->Peso ?></td>
                                            <td><?php
                                                $exp = $item->TiempoExperiencia;
                                                $meses = 0;
                                                $anios = 0;

                                                while($exp > 12)
                                                {
                                                    $exp = $exp - 12;
                                                    $anios++;
                                                }
                                                $meses = $exp;

                                                echo $anios . ' años ' . $meses . ' meses';
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h4 class="panel-title">Responsabilidades</h4>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered table-hover table-full-width" id="tbl_responsabilidades">
                                <thead>
                                <tr>
                                    <th>Tipo de Responsabilidad</th>
                                    <th>Descripción de Responsabilidad</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($responsabilidades as $responsabilidad)
                                {
                                    if($responsabilidad->TipoResponsabilidadID)
                                    {
                                        ?>
                                        <tr>
                                            <td><?php echo $responsabilidad->TipoRespDesc ?></td>
                                            <td><?php echo $responsabilidad->Descripcion ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php
                        foreach($etapas as $item)
                        {
                            ?>
                            <div class="panel panel-white">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><?php echo $item->Descripcion ?></h5>
                                </div>
                                <div class="panel-body">
                                    <div class="form-inline">
                                        <input type="hidden" name="hdEtapas[]" value="<?php echo $item->EtapaID ?>" />
                                        <div class="checkbox">
                                            <input type="checkbox" name="chk_activar_etapa[]" id="<?php echo $item->Nombre ?>" value="<?php echo $item->EtapaID ?>" class="js-switch" />
                                        </div>
                                        &nbsp;&nbsp;&nbsp;
                                        <label for="fecha_fin_contrato">
                                            Inicio
                                        </label>
                                        <p class="input-group input-append datepicker date">
                                            <input type="text" class="form-control dtFechaInicio fechaEtapa" name="fecha_inicio_etapa[]" id="fecha_inicio_etapa_<?php echo $item->EtapaID ?>" placeholder="Fecha de Inicio Etapa">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default">
                                                <i class="glyphicon glyphicon-calendar"></i>
                                            </button>
                                        </span>
                                        </p>
                                        <label for="fecha_fin_contrato">
                                            Fin
                                        </label>
                                        <p class="input-group input-append datepicker date">
                                            <input type="text" class="form-control dtFechaFin fechaEtapa" name="fecha_fin_etapa[]" id="fecha_fin_etapa_<?php echo $item->EtapaID ?>" placeholder="Fecha de Fin Etapa">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default">
                                                <i class="glyphicon glyphicon-calendar"></i>
                                            </button>
                                        </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="handleConvocatoriaError" style="display:none;">
                            <div class="alert alert-danger" role="alert">
                              <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
                              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                              <span class="texto"></span>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Crear Convocatoria
                    </button>
                    <a href="" class="btn btn-o btn-danger pull-right" onclick="window.history.back()">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
