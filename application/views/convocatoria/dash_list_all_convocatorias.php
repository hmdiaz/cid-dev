<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="table-responsive" style="min-height: 200px">
        <table class="table table-bordered table-hover" id="tbl_requisicion">
            <thead>
                <tr>
                    <th>#Proyecto</th>
                    <th>Perfil de la Convocatoria</th>
                    <th>Cantidad de Postulantes</th>
                    <th>Cantidad de Vacantes</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php echo $convocatorias; ?>
            </tbody>
        </table>
    </div>
</div>