<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="table-responsive" style="min-height: 200px">
        <table class="table table-bordered table-hover" id="tbl_postulantes">
            <thead>
                <tr>
                    <th>Nombre del Postulante</th>
                    <th>Coincidencias</th>
                    <th>Porcentaje Total</th>
                    <th>Nivel Academico</th>
                    <th>Estudios Complementarios</th>
                    <th>Experiencia General</th>
                    <th>Desarrollo de Habilidades</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>                
                <?php
                foreach ($postulantes as $postulante) {
                    $total_coincidencias = 0;
                    foreach ($coincidencias as $coincidencia) {
                        if ($postulante->UsuarioID == $coincidencia['id_user']) {
                            $coin_info_acad = $coincidencia['coincidencias_info_acad'];
                            $coin_est_comp = $coincidencia['coincidencias_est_comp'];
                            $coin_exp_gral = $coincidencia['coincidencias_exp_gral'];
                            $coin_des_hab = $coincidencia['coincidencias_desc_hab'];
                            $total = $coincidencia['total'];
                            $total_coincidencias = $coin_info_acad + $coin_est_comp + $coin_exp_gral + $coin_des_hab;
                            $detalles = $coincidencia['detalles'];
                            $porcentaje = 0;
                            $por_ia = 0;
                            $por_ec = 0;
                            $por_eg = 0;
                            $por_dh = 0;
                            foreach ($detalles['match_info_acad'] as $mia) {
                                $porcentaje += $mia['Coincidecia_porcentaje'];
                                $por_ia += $mia['Peso'];
                            }
                            foreach ($detalles['match_est_comp'] as $mec) {
                                $porcentaje += $mec['Coincidecia_porcentaje'];
                                $por_ec += $mec['Peso'];
                            }
                            foreach ($detalles['match_exp_gral'] as $meg) {
                                $porcentaje += $meg['Coincidecia_porcentaje'];
                                $por_eg += $meg['Peso'];
                            }
                            foreach ($detalles['match_desc_hab'] as $mdh) {
                                $porcentaje += $mdh['Coincidecia_porcentaje'];
                                $por_dh += $mdh['Peso'];
                            }
                            $total_porcentaje = ($por_ia+$por_ec+$por_eg+$por_dh) / 4;
                        }
                    }
                    echo '<tr>';
                    echo '<td>' . $postulante->Nombres . ' ' . $postulante->Apellidos . '</td>';
                    echo '<td>' . $total_coincidencias . ' de ' . $total . '</td>';
                    echo '<td>' . round($total_porcentaje, 2) . ' %</td>';
                    echo '<td>' . $por_ia . '</td>';
                    echo '<td>' . $por_ec . '</td>';
                    echo '<td>' . $por_eg . '</td>';
                    echo '<td>' . $por_dh . '</td>';
                    echo '<td>'
                    . '<a href="' . base_url('ver-coincidencias') . '/' . $id_convocatoria . '/' . $postulante->UsuarioID . '" class="btn btn-primary btn-xs btn-o">Ver Coincidencias</a>'
                    . '<a href="' . base_url('hoja-de-vida-pdf') . '/' . $postulante->UsuarioID . '" class="btn btn-success btn-xs btn-o" target="_blank">Hoja de Vida</a></td>';
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

