<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class=" col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-body">
                <h1 align="center"><small>Configuración de la Etapa</small></h1>
                
                <p><strong>Prueba: </strong><?php echo $configuracion->prueba_name; ?></p>
                <p><strong>Tiempo para la prueba: </strong><?php echo $configuracion->pruebasconfig_tiempo; ?></p>
                <a href="<?php echo base_url('etapas/' . $configuracion->ConvocatoriaID); ?>" class="btn btn-o btn-danger pull-right">
                    Regresar
                </a>
            </div>
        </div>
    </div>
</div>

