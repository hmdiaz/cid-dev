<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class=" col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-body">
                <h1 align="center">
                    <small>Etapas de la Convocatoria</small>
                </h1>


                <div class="table-responsive" style="min-height: 200px">
                    <table class="table table-bordered table-hover" id="tbl_etapas_convocatorias">
                        <thead>
                            <tr>
                                <th>Etapa</th>
                                <th>Nombre Etapa</th>
                                <th>Fecha de Inicio</th>
                                <th>Fecha de Fin</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($etapas_convocatorias as $ec):

                                $etapa = ($ec->EtapaID == 4) ? 0 : $ec->EtapaID;

                                echo '<tr>';
                                ?>
                                <?php echo '<td>' . $etapa . '</td>'; ?> 
                                <?php echo '<td>' . $ec->Descripcion . '</td>'; ?> 
                                <?php echo '<td>' . $ec->FechaInicio . '</td>'; ?>
                                <?php echo '<td>' . $ec->FechaFin . '</td>'; ?>   
                                <?php
                                echo '<td>';
                                if ($ec->EtapaID == 4) {
                                    ?>
                                <a href="<?php echo base_url('ver-postulantes/' . $id_convocatoria); ?>" class="btn btn-o btn-success"><i class="fa fa-eye"></i> Ver Postulantes</a>
                                <?php
                            } else {
                                $config = $this->Pruebasconfig_model->getConfigPruebaByConvocatoria($ec->EtapaConvocatoriaID);
                                if (count($config) == 0):
                                    ?>
                                    <a href="<?php echo base_url('configurar-etapa/' . $ec->EtapaConvocatoriaID); ?>" class="btn btn-o btn-danger"><i class="fa fa-cog"></i> Configurar Etapa</a>
                                    <?php
                                else:
                                    ?>
                                    <a href="<?php echo base_url('ver-configurar-etapa/' . $ec->EtapaConvocatoriaID); ?>" class="btn btn-o btn-danger"><i class="fa fa-cog"></i> Ver Configuracion</a>
                                    <a href="<?php echo base_url('seleccion/' . $ec->EtapaConvocatoriaID); ?>" class="btn btn-o btn-primary"><i class="fa fa-group"></i> Seleccion</a>
                                        
                                    <?php
                                    $seleccion = $this->PruebasSeleccion_model->getbyEtapaConvcatoria($ec->EtapaConvocatoriaID);
                                    
                                    if (count($seleccion) == 0):
                                        ?>
                                        <?php
                                    else:
                                        ?>
                                        <a href="<?php echo base_url('resultados/' . $ec->EtapaConvocatoriaID); ?>" class="btn btn-o btn-primary"><i class="fa fa-line-chart"></i> Resultados</a>
                                        <?php
                                        if ($ec->EtapaID == 2) {
                                            echo '<a href="' . base_url('generar-archivo/' . $id_convocatoria) . '" class="btn btn-o btn-primary">Generar Archivo de Respuestas</a>';
                                        }
                                    endif;
                                endif;
                            }

                            echo '</td>';
                            echo '</tr>';
                            ?>
                        <?php endforeach; ?>
                        </tbody>
                        <tr>
                            <td></td>
                            <td>Informe de Convocatoria</td>
                            <td></td>
                            <td></td>
                            <td><a href="<?php echo base_url('resultados-finales/' . $id_convocatoria); ?>" class="btn btn-o btn-primary"><i class="fa fa-line-chart"></i> Informe</a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
