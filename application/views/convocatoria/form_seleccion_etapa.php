<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-offset-2 col-lg-8 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Selección</h5>
            </div>
            <div class="panel-body">      
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('agregar-seleccion'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $id_etapa; ?>" name="id_etapa">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Selección</th>
                                <th>Nombre del Postulante</th>
                                <th>Calificacion</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>                
                            <?php
                            $todo_listo = TRUE;
                            foreach ($postulantes as $postulante) {
                                $pruebas = $this->PruebasSeleccionResult_model->getbyPruebaSeleccionID($postulante->pruebasseleccion_id);
                                if (count($pruebas) > 0) {
                                    $preguntas = json_decode($pruebas->pruebaseleccionresult_match);
                                    $matchs = json_decode($preguntas->match_resp);
                                    echo '<tr>';
                                    echo '<td><input type="checkbox" name="'.$postulante->UsuarioID.'" value="1"></td>';
                                    echo '<td>' . $postulante->Nombres . ' ' . $postulante->Apellidos . '</td>';
                                    echo '<td>' . $preguntas->puntos . '</td>';
                                    echo '<td><a href="' . base_url('hoja-de-vida-pdf') . '/' . $postulante->UsuarioID . '" class="btn btn-success btn-xs btn-o" target="_blank">Hoja de Vida</a></td></td>';
                                    echo '</tr>';
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-o btn-primary">
                        Seleccionar
                    </button>
                    <a href="<?php echo base_url('etapas/' . $id_convocatoria); ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>