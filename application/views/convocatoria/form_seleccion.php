<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Selección</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('agregar-seleccion'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $id_etapa; ?>" name="id_etapa">
                    <table class="table table-bordered table-hover" id="seleccion">
                        <thead>
                            <tr>
                                <th>Seleccion</th>
                                <th>Nombre del Postulante</th>
                                <th>Coincidencias</th>
                                <th>Porcentaje Total</th>
                                <th>Nivel Academico</th>
                                <th>Estudios Complementarios</th>
                                <th>Experiencia General</th>
                                <th>Desarrollo de Habilidades</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>                
                            <?php
                            foreach ($postulantes as $postulante) {
                                $total_coincidencias = 0;
                                foreach ($coincidencias as $coincidencia) {
                                    if ($postulante->UsuarioID == $coincidencia['id_user']) {
                                        $coin_info_acad = $coincidencia['coincidencias_info_acad'];
                                        $coin_est_comp = $coincidencia['coincidencias_est_comp'];
                                        $coin_exp_gral = $coincidencia['coincidencias_exp_gral'];
                                        $coin_des_hab = $coincidencia['coincidencias_desc_hab'];
                                        $total = $coincidencia['total'];
                                        $total_coincidencias = $coin_info_acad + $coin_est_comp + $coin_exp_gral + $coin_des_hab;
                                        $detalles = $coincidencia['detalles'];
                                        $porcentaje = 0;
                                        $por_ia = 0;
                                        $por_ec = 0;
                                        $por_eg = 0;
                                        $por_dh = 0;

                                        foreach ($detalles['match_info_acad'] as $mia) {
                                            $porcentaje += $mia['Coincidecia_porcentaje'];
                                            $por_ia += $mia['Peso'];
                                        }
                                        foreach ($detalles['match_est_comp'] as $mec) {
                                            $porcentaje += $mec['Coincidecia_porcentaje'];
                                            $por_ec += $mec['Peso'];
                                        }
                                        foreach ($detalles['match_exp_gral'] as $meg) {
                                            $porcentaje += $meg['Coincidecia_porcentaje'];
                                            $por_eg += $meg['Peso'];
                                        }
                                        foreach ($detalles['match_desc_hab'] as $mdh) {
                                            $porcentaje += $mdh['Coincidecia_porcentaje'];
                                            $por_dh += $mdh['Peso'];
                                        }
                                        $total_porcentaje = ($por_ia + $por_ec + $por_eg + $por_dh) / 4;
                                    }
                                }
                                echo '<tr>';
                                echo '<td><input type="checkbox" name="' . $postulante->PostulanteID . '" value="1"></td>';
                                echo '<td>' . $postulante->Nombres . ' ' . $postulante->Apellidos . '</td>';
                                echo '<td>' . $total_coincidencias . ' de ' . $total . '</td>';
                                echo '<td>' . round($total_porcentaje, 2) . ' %</td>';
                                echo '<td>' . $por_ia . '</td>';
                                echo '<td>' . $por_ec . '</td>';
                                echo '<td>' . $por_eg . '</td>';
                                echo '<td>' . $por_dh . '</td>';
                                echo '<td><a href="' . base_url('hoja-de-vida-pdf') . '/' . $postulante->PostulanteID . '" class="btn btn-success btn-xs btn-o" target="_blank">Hoja de Vida</a></td></td>';
                                echo '</tr>';
                            }
                            ?>
                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-o btn-primary">
                        Seleccionar
                    </button>
                    <a href="<?php echo base_url('etapas/' . $id_convocatoria); ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>