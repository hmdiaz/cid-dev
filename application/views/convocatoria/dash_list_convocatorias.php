<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="table-responsive" style="min-height: 200px">
        <table class="table table-bordered table-hover" id="tbl_requisiciones_rechazadas">
            <thead>
                <tr>
                    <th>Nombre del Cargo</th>
                    <th>Tipo Contrato</th>
                    <th>Salario Asignado</th>
                    <th>Cant.</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php echo $convocatorias; ?>
            </tbody>
        </table>
    </div>
</div>