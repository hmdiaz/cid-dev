<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-offset-2 col-lg-8 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Selección</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('agregar-seleccion'); ?>" method="POST">
                    <h4>Terna</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Seleccion</th>
                                <th>Nombre del Postulante</th>
                                <th>Convocatoria</th>
                                <?php
                                $cont_etapa = 0;
                                $ultima_etapa = 0;
                                foreach ($etapas as $etapa) {
                                    if ($etapa->EtapaID != 4) {
                                        echo '<th> Etapa ' . $etapa->EtapaID . '</th>';
                                        $ultima_etapa = $etapa->EtapaID;
                                        $cont_etapa++;
                                    }
                                }
                                ?>
                                <th>Resultado</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $candidatos = [];
                            $usuarios = [];
                            foreach ($postulantes_finales as $pf) {
                                if ($pf->pruebaseleccionresult_match != "") {
                                    if ($pf->EtapaID != 4) {
                                        if ($pf->EtapaID === $ultima_etapa) {
                                            $usuarios[] = [
                                                "etapa" => $pf->EtapaID,
                                                "user_id" => $pf->UsuarioID
                                            ];
                                        }
                                    }
                                }
                            }
                            $cont_us = 0;
                            foreach ($usuarios as $u) {
                                $sub_total = 0;
                                $total_coincidencias = 0;
                                foreach ($coincidencias as $c) {
                                    if ($u['user_id'] == $c['id_user']) {
                                        $coin_info_acad = $c['coincidencias_info_acad'];
                                        $coin_est_comp = $c['coincidencias_est_comp'];
                                        $coin_exp_gral = $c['coincidencias_exp_gral'];
                                        $coin_des_hab = $c['coincidencias_desc_hab'];
                                        $total = $c['total'];
                                        $total_coincidencias = $coin_info_acad + $coin_est_comp + $coin_exp_gral + $coin_des_hab;
                                        $detalles = $c['detalles'];
                                        $porcentaje = 0;
                                        foreach ($detalles['match_info_acad'] as $mia) {
                                            $porcentaje += $mia['Coincidecia_porcentaje'];
                                        }
                                        foreach ($detalles['match_est_comp'] as $mec) {
                                            $porcentaje += $mec['Coincidecia_porcentaje'];
                                        }
                                        foreach ($detalles['match_exp_gral'] as $meg) {
                                            $porcentaje += $meg['Coincidecia_porcentaje'];
                                        }
                                        foreach ($detalles['match_desc_hab'] as $mdh) {
                                            $porcentaje += $mdh['Coincidecia_porcentaje'];
                                        }
                                        $total_porcentaje = $porcentaje / $total;
                                    }
                                }
                                $etapas_string = "";
                                foreach ($etapas as $e) {
                                    if ($e->EtapaID != 4) {
                                        foreach ($postulantes_finales as $pf) {
                                            if ($e->EtapaID == $pf->EtapaID) {
                                                if ($u['user_id'] === $pf->UsuarioID) {
                                                    $nombre = $pf->Nombres;
                                                    $apellidos = $pf->Apellidos;
                                                    if ($pf->pruebaseleccionresult_match != "") {
                                                        $result = json_decode($pf->pruebaseleccionresult_match);
                                                        $sub_total += $result->puntos;
                                                        $etapas_string .= '<td>' . $result->puntos . '</td>';
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                $cont_us++;
                                if ($cont_us <= 3) {
                                    $total = ($sub_total + $total_porcentaje) / ($cont_etapa + 1);
                                    echo '<tr>';
                                    echo '<td></td>';
                                    echo '<td>' . $nombre . ' ' . $apellidos . '</td>';
                                    echo '<td>' . round($total_porcentaje, 2) . ' %</td>';
                                    echo $etapas_string;
                                    echo '<td>' . round($total, 2) . '</td>';
                                    echo '</tr>';
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                    <h4>Postulantes</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Seleccion</th>
                                <th>Nombre del Postulante</th>
                                <th>Convocatoria</th>
                                <?php
                                $cont_etapa = 0;
                                $ultima_etapa = 0;
                                foreach ($etapas as $etapa) {
                                    if ($etapa->EtapaID != 4) {
                                        echo '<th> Etapa ' . $etapa->EtapaID . '</th>';
                                        $ultima_etapa = $etapa->EtapaID;
                                        $cont_etapa++;
                                    }
                                }
                                ?>
                                <th>Resultado</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $candidatos = [];
                            $usuarios = [];
                            foreach ($postulantes_finales as $pf) {
                                if ($pf->pruebaseleccionresult_match != "") {
                                    if ($pf->EtapaID != 4) {
                                        if ($pf->EtapaID === $ultima_etapa) {
                                            $usuarios[] = [
                                                "etapa" => $pf->EtapaID,
                                                "user_id" => $pf->UsuarioID
                                            ];
                                        }
                                    }
                                }
                            }
                            $cont_us = 0;
                            foreach ($usuarios as $u) {
                                $sub_total = 0;
                                $total_coincidencias = 0;
                                foreach ($coincidencias as $c) {
                                    if ($u['user_id'] == $c['id_user']) {
                                        $coin_info_acad = $c['coincidencias_info_acad'];
                                        $coin_est_comp = $c['coincidencias_est_comp'];
                                        $coin_exp_gral = $c['coincidencias_exp_gral'];
                                        $coin_des_hab = $c['coincidencias_desc_hab'];
                                        $total = $c['total'];
                                        $total_coincidencias = $coin_info_acad + $coin_est_comp + $coin_exp_gral + $coin_des_hab;
                                        $detalles = $c['detalles'];
                                        $porcentaje = 0;
                                        foreach ($detalles['match_info_acad'] as $mia) {
                                            $porcentaje += $mia['Coincidecia_porcentaje'];
                                        }
                                        foreach ($detalles['match_est_comp'] as $mec) {
                                            $porcentaje += $mec['Coincidecia_porcentaje'];
                                        }
                                        foreach ($detalles['match_exp_gral'] as $meg) {
                                            $porcentaje += $meg['Coincidecia_porcentaje'];
                                        }
                                        foreach ($detalles['match_desc_hab'] as $mdh) {
                                            $porcentaje += $mdh['Coincidecia_porcentaje'];
                                        }
                                        $total_porcentaje = $porcentaje / $total;
                                    }
                                }
                                $etapas_string = "";
                                foreach ($etapas as $e) {
                                    foreach ($postulantes_finales as $pf) {
                                        if ($e->EtapaID != 4) {
                                            if ($e->EtapaID == $pf->EtapaID) {
                                                if ($u['user_id'] === $pf->UsuarioID) {
                                                    $nombre = $pf->Nombres;
                                                    $apellidos = $pf->Apellidos;
                                                    if ($pf->pruebaseleccionresult_match != "") {
                                                        $result = json_decode($pf->pruebaseleccionresult_match);
                                                        $sub_total += $result->puntos;
                                                        $etapas_string .= '<td>' . $result->puntos . '</td>';
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                $total = ($sub_total + $total_porcentaje) / ($cont_etapa + 1);
                                if ($cont_us > 3) {
                                    echo '<tr>';
                                    echo '<td></td>';
                                    echo '<td>' . $nombre . ' ' . $apellidos . '</td>';
                                    echo '<td>' . round($total_porcentaje, 2) . ' %</td>';
                                    echo $etapas_string;
                                    echo '<td>' . round($total, 2) . '</td>';
                                    echo '</tr>';
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-o btn-primary">
                        Seleccionar
                    </button>
                    <a href="<?php echo base_url('etapas/' . $id_convocatoria); ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>