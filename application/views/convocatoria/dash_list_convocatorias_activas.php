<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-offset-1 col-lg-10 col-md-12">
        <div class="panel-group accordion" id="accordion">
            <?php
            foreach ($convocatorias as $convocatoria):
                $conv = $this->Convocatoria_model->getConvocatoriaEtapa4($convocatoria->ConvocatoriaID);
                $fecha = date('U');
                if ($fecha >= strtotime($conv->FechaInicio) && $fecha <= strtotime($conv->FechaFin)) :
                    if (@$_SESSION['is_logged_in']) {
                        $postulada = $this->PostulantesConvocatorias_model->getPostulada($_SESSION['id_user'], $convocatoria->ConvocatoriaID);
                        $band = FALSE;
                        if (count($postulada) == 1) {
                            $band = TRUE;
                        }
                    } else {
                        $band = FALSE;
                    }
                    ?>
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <div class="row">
                                    <div class="col-lg-10 col-xs-9">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $convocatoria->ConvocatoriaID; ?>">
                                            <i class="icon-arrow"></i> <?php echo $convocatoria->TituloCargo; ?>
                                        </a>
                                    </div>
                                    <div class="col-lg-2 col-xs-3">
                                        <?php
                                        if ($band) {
                                            ?>
                                            <a href="<?php echo base_url('ver-coincidencias') . '/' . $convocatoria->ConvocatoriaID; ?>" class="btn btn-xs btn-success btn-o pull-right">Ve tus coincidencias</a>
                                            <?php
                                        } else {
                                            ?>
                                            <a href="<?php echo base_url('postularse-convocaria') . '/' . $convocatoria->ConvocatoriaID; ?>" class="btn btn-xs btn-primary btn-o pull-right">Aplicar</a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                            </h5>                    
                        </div>
                        <div id="collapse<?php echo $convocatoria->ConvocatoriaID; ?>" class="panel-collapse collapse">
                            <div class="panel-body">                         
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h3>Cargo: <?php echo $convocatoria->NombreCargo; ?> </h3>

                                        <h4>DEFINICIÓN DEL CARGO</h4>
                                        <p><?php echo $convocatoria->Justificacion; ?></p>

                                        <h4>MODALIDAD, TIEMPO Y LUGAR DE TRABAJO</h4>
                                        <h5>Modalidad y asignación económica:</h5>
                                        <p><?php echo $convocatoria->Nombre ?></p>
                                        <?php
                                        if ($convocatoria->mostrar_salario == 1):
                                            ?>
                                            <p>Salario $<?php echo $convocatoria->SalarioAsignado; ?></p>
                                        <?php endif; ?>
                                        <h5>Duración de contrato</h5>
                                        <p>
                                            <?php
                                            $dias = $convocatoria->DuracionContrato;
                                            $años = $dias / 365;
                                            $meses = $dias / 30;
                                            if ($años > 1) {
                                                $años = (int) $años;
                                                $dias_meses = $dias - ($años * 365);
                                                $meses = $dias_meses / 30;
                                                $meses = (int) $meses;
                                                $dias = $dias_meses - ($meses * 30);
                                                $dias = (int) $dias;
                                                echo $años . ' Años, ' . $meses . ' Meses, ' . $dias . ' Dias';
                                            } elseif ($años < 1 && $meses > 1) {
                                                $meses = $dias / 30;
                                                $meses = (int) $meses;
                                                $dias = $dias - ($meses * 30);
                                                $dias = (int) $dias;
                                                echo $meses . ' Meses, ' . $dias . ' Dias';
                                            } else {
                                                echo $dias . ' Dias';
                                            }
                                            ?> 
                                        </p>

                                        <h5>PERFIL</h5>
                                        <h3>Nivel Educativo</h3>
                                        <?php
                                        $comp_compe = $this->ComponenteCompetencia_model->getbyPerfil($convocatoria->PerfilID);

                                        foreach ($comp_compe as $cp1) {
                                            if ($cp1->TipoCatalogoID == 1) {
                                                echo '<p>' . $cp1->Descripcion . '</p>';
                                            }
                                        }
                                        ?>
                                        <h3>Nivel de Formacion</h3>
                                        <?php
                                        foreach ($comp_compe as $cp2) {
                                            if ($cp2->TipoCatalogoID == 2) {
                                                echo '<p>' . $cp2->Descripcion . '</p>';
                                            }
                                        }
                                        ?>
                                        <h3>Experiencia General</h3>
                                        <?php
                                        foreach ($comp_compe as $cp3) {
                                            if ($cp3->TipoCatalogoID == 3) {
                                                echo '<p> ' . $cp3->Descripcion . '. ';
                                                if ($cp3->TiempoExperiencia > 12) {
                                                    $años = $cp3->TiempoExperiencia / 12;
                                                    $años = (int) $años;
                                                    $meses = $cp3->TiempoExperiencia - ($años * 12);
                                                    $meses = (int) $meses;
                                                    echo $años . ' Años, ' . $meses . ' Meses';
                                                } else {
                                                    echo $cp3->TiempoExperiencia . ' Meses';
                                                }

                                                echo '</p>';
                                            }
                                        }
                                        ?>
                                        <h3>Descripcion de Habilidades</h3>
                                        <?php
                                        foreach ($comp_compe as $cp4) {
                                            if ($cp4->TipoCatalogoID == 4) {
                                                echo '<p>' . $cp4->Descripcion . '</p>';
                                            }
                                        }
                                        ?>
                                        <div class="row">
                                            <?php
                                            if ($band) {
                                                ?>
                                                <a href="<?php echo base_url('ver-coincidencias') . '/' . $convocatoria->ConvocatoriaID; ?>" class="btn btn-xs btn-success btn-o pull-left">Ve tus coincidencias</a>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="<?php echo base_url('postularse-convocaria') . '/' . $convocatoria->ConvocatoriaID; ?>" class="btn btn-xs btn-primary btn-o pull-left">Aplicar</a>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>            
        </div>
    </div>
</div>
