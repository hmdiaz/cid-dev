<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <?php if($_SESSION['user_role_id'] == 6): ?>
    <h4 class="text-danger">Te invitamos a revisar las las conincidencias y actualizar tu hoja de vida para tener mejores oportunidades.</h4>
    <?php endif; ?>
    <div class="table-responsive" style="min-height: 200px">
        <table class="table table-bordered table-hover" id="tbl_coincidencias">
            <thead>
                <tr>
                    <th>Competencia</th>
                    <th>Titulos/Experiencias/Habilidades</th>
                    <th>Coincidencia</th>
                    <th>Coincidencia %</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($match['match_info_acad'] as $ia) {
                    if($ia['Coincide'] == 'Si'){
                        $statusia = 'success';
                    }else{
                        $statusia = 'danger';
                    }
                    if($ia['Coincidecia_porcentaje'] >= 80){
                        $statusia = 'success';
                    }else{
                        $statusia = 'warning';
                    }
                    echo '<tr class="'.$statusia.'">';
                    echo '<td>' . $ia['Competencia'] . '</td>';
                    echo '<td>' . $ia['Nombre_coincidencia'] . '</td>';
                    echo '<td>' . $ia['Coincide'] . '</td>';
                    echo '<td>' . $ia['Peso'] . '</td>';
                    echo '</tr>';
                }
                foreach ($match['match_est_comp'] as $ec) {                    
                    if($ec['Coincide'] == 'Si'){
                        $statusec = 'success';
                    }else{
                        $statusec = 'danger';
                    }
                    if($ec['Coincidecia_porcentaje'] >= 80){
                        $statusec = 'success';
                    }else{
                        $statusec = 'warning';
                    }
                    echo '<tr class="'.$statusec.'">';
                    echo '<td>' . $ec['Competencia'] . '</td>';
                    echo '<td>' . $ec['Nombre_coincidencia'] . '</td>';
                    echo '<td>' . $ec['Coincide'] . '</td>';
                    echo '<td>' . $ec['Peso'] . '</td>';
                    echo '</tr>';
                }
                foreach ($match['match_exp_gral'] as $eg) {                    
                    if($eg['Coincide'] == 'Si'){
                        $statuseg = 'success';
                    }else{
                        $statuseg = 'danger';
                    }
                    if($eg['Coincidecia_porcentaje'] >= 80){
                        $statuseg = 'success';
                    }else{
                        $statuseg = 'warning';
                    }
                    echo '<tr class="'.$statuseg.'">';
                    echo '<td>' . $eg['Competencia'] . '</td>';
                    echo '<td>' . $eg['Nombre_coincidencia'] . '</td>';
                    echo '<td>' . $eg['Coincide'] . '</td>';
                    echo '<td>' . $eg['Peso'] . '</td>';
                    echo '</tr>';
                }
                foreach ($match['match_desc_hab'] as $dh) {                    
                    if($dh['Coincide'] == 'Si'){
                        $statusdh = 'success';
                    }else{
                        $statusdh = 'danger';
                    }
                    if($dh['Coincidecia_porcentaje'] >= 80){
                        $statusdh = 'success';
                    }else{
                        $statusdh = 'warning';
                    }
                    echo '<tr class="'.$statusdh.'">';
                    echo '<td>' . $dh['Competencia'] . '</td>';
                    echo '<td>' . $dh['Nombre_coincidencia'] . '</td>';
                    echo '<td>' . $dh['Coincide'] . '</td>';
                    echo '<td>' . $dh['Peso'] . '</td>';
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

