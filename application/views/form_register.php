<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!-- Template Name: Clip-Two - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <!-- start: HEAD -->
    <head>
        <title><?php echo $title; ?></title>
        <!-- start: META -->
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: GOOGLE FONTS -->
        <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        <!-- end: GOOGLE FONTS -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/fontawesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/themify-icons/themify-icons.min.css">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/animate.css/animate.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/switchery/switchery.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/select2/select2.min.css" rel="stylesheet" media="screen">
        <!-- end: MAIN CSS -->
        <!-- start: CLIP-TWO CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/styles.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/plugins.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/themes/theme-1.css" id="skin_color" />
        <!-- end: CLIP-TWO CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    </head>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body class="login">
        <!-- start: REGISTRATION -->
        <div class="row">
            <div class="main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
                <div class="logo margin-top-30">
                    <center>
                        <img src="assets/templates/admin/assets/images/LogoCID.png" alt="CID" style="margin-bottom: 20px" />
                    </center>
                </div>
                <!-- start: REGISTER BOX -->
                <div class="box-register">
                    <?php
                    if ($this->session->flashdata('message_error')) {
                        ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong><?php echo $this->session->flashdata('message_error'); ?></strong>
                        </div>
                        <?php
                    }
                    if ($this->session->flashdata('message_success')) {
                        ?>
                        <div role="alert" class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                    if (validation_errors()) {
                        echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                    }
                    ?> 
                    <form class="form-register" action="<?php echo base_url('agregar-usuario'); ?>" method="POST" enctype="multipart/form-data">
                        <fieldset>
                            <legend>
                                Registro de Usuario
                            </legend>
                            <div class="form-group">
                                <input type="text" class="form-control" required name="user_nombres" value="<?php echo set_value('user_nombres'); ?>" placeholder="Nombres">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" required name="user_apellidos" value="<?php echo set_value('user_apellidos'); ?>" placeholder="Apellidos">
                            </div>
                            <div class="form-group">
                                <span class="input-icon">
                                    <input type="email" class="form-control" required name="user_email" value="<?php echo set_value('user_email'); ?>" placeholder="Correo Electronico">
                                    <i class="fa fa-envelope"></i> </span>
                            </div>
                            <div class="form-group">
                                <span class="input-icon">
                                    <input type="password" required class="form-control" id="password" name="user_pass" placeholder="Contraseña">
                                    <i class="fa fa-lock"></i> </span>
                            </div>
                            <div class="form-group">
                                <span class="input-icon">
                                    <input type="password" required class="form-control" name="user_pass_repeat" placeholder="Repite Contraseña">
                                    <i class="fa fa-lock"></i> </span>
                            </div>
                            <div class="form-group">
                                <select id="user_cargo" name="user_cargo" class="users_select_cargo form-control">
                                    <option value="0" selected="selected">Selecciona Cargo</option>
                                    <?php echo $cargos; ?>
                                </select>
                            </div>
                            <div class="form-actions">
                                <p>
                                    Ya tienes una cuenta
                                    <a href="<?php echo base_url('signin'); ?>">
                                        Iniciar Sesion
                                    </a>
                                </p>
                                <button type="submit" name="form_register" value="register" class="btn btn-primary pull-right">
                                    Registarse <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </fieldset>
                    </form>
                    <!-- start: COPYRIGHT -->
                    <div class="copyright">
                        &copy; <span class="current-year"></span><span class="text-bold text-uppercase">Corporación Infancia y Desarrollo</span>. <span>Todos los derechos reservados</span></span>
                    </div>
                    <!-- end: COPYRIGHT -->
                </div>
                <!-- end: REGISTER BOX -->
            </div>
        </div>
        <!-- end: REGISTRATION -->
        <!-- start: MAIN JAVASCRIPTS -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/modernizr/modernizr.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery-cookie/jquery.cookie.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/switchery/switchery.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/select2/select2.min.js"></script>
        
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery-validation/jquery.validate.min.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <!-- start: CLIP-TWO JAVASCRIPTS -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/assets/js/main.js"></script>
        <!-- start: JavaScript Event Handlers for this page -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/assets/js/login.js"></script>
        <script>
            jQuery(document).ready(function () {
                Main.init();
                $(".users_select_cargo").select2();
            });
        </script>
        <!-- end: JavaScript Event Handlers for this page -->
        <!-- end: CLIP-TWO JAVASCRIPTS -->
    </body>
    <!-- end: BODY -->
</html>