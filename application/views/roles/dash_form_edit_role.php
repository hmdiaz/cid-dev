<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Rol</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-rol'); ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $rol->RolID; ?>" name="id">
                    
                    <div class="form-group">
                        <label for="rol_name">
                            Nombre
                        </label>
                        <input type="text" class="form-control" name="rol_name" value="<?php if(set_value('rol_name')){ echo set_value('rol_name'); }else{ echo $rol->Nombre; } ?>" id="rol_name" placeholder="Nombre de Rol">
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Guardar Cambios
                    </button>
                    <a href="<?php echo base_url('lista-roles');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>