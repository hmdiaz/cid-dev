<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nuevo Rol</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('agregar-rol'); ?>" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="rol_name">
                            Nombre del rol
                        </label>
                        <input type="text" class="form-control" name="rol_name" value="<?php echo set_value('rol_name'); ?>" id="rol_name" placeholder="Nombre Rol">
                    </div>
                    <div class="form-group">
                        <label for="rol_status">
                            Estatus del Rol
                        </label>
                        <select id="rol_status" name="rol_status" class="roles_select_add_rol form-control">
                            <option value="1" selected="selected">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Crear Rol
                    </button>
                    <a href="<?php echo base_url('lista-roles');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>