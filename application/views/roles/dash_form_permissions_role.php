<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
    <div class="col-md-12"> 
        <form method="POST" action="<?php echo base_url('actualizar-permisos'); ?>">
            <div class="table-responsive" style="min-height: 200px">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    <input type="hidden" value="<?php echo $rolID; ?>" name="idrol">
                    <?php $band = FALSE; foreach ($modules as $module): ?> 
                        <tr>
                            <td colspan="3" class="center success"><?php echo $module->modulo; ?></td>
                        </tr>
                        <?php foreach ($allmodules as $allmodule): ?>
                            <?php if ($module->modulo == $allmodule->modulo): ?>  
                        
                                <?php foreach ($rols as $rol): ?>
                                    <?php if ($rol->id_modulo == $allmodule->ModuloID): $band=TRUE; ?>
                                        <tr>
                                            <td><?php echo $allmodule->Nombre; ?></td>
                                            <td>
                                                <div class="checkbox">
                                                    <input type="checkbox" name="permission[]" value="<?php echo $allmodule->ModuloID; ?>_1" class="js-switch" <?php if($rol->status == 1){echo 'checked';} ?> />
                                                </div>
                                            </td>
                                        </tr>                                    
                                    <?php endif; ?>
                                <?php endforeach; ?> 
                                <?php if(!$band): ?>
                                    <tr>
                                        <td><?php echo $allmodule->Nombre; ?></td>
                                        <td>
                                            <div class="checkbox">
                                                <input type="checkbox" name="permission[]" value="<?php echo $allmodule->ModuloID; ?>_1" class="js-switch" />
                                            </div>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            <?php endif; ?>                            
                        <?php endforeach;  $band=FALSE?>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2">
                                <button type="submit" class="btn btn-wide btn-o btn-primary">
                                        Guardar
                                </button>
                                <a href="<?php echo base_url('lista-roles');  ?>" class="btn btn-o btn-danger pull-right">
                                    Cancelar
                                </a>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </form>
    </div>
</div>
