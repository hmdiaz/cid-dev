<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!-- Template Name: Clip-Two - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- start: HEAD -->
<!-- start: HEAD -->
<head>
    <title><?php echo $title; ?></title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- end: META -->
    <!-- start: GOOGLE FONTS -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <!-- end: GOOGLE FONTS -->
    <!-- start: MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/themify-icons/themify-icons.min.css">
    <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/animate.css/animate.min.css" rel="stylesheet" media="screen">
    <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" media="screen">
    <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/switchery/switchery.min.css" rel="stylesheet" media="screen">
    <!-- end: MAIN CSS -->
    <!-- start: CLIP-TWO CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/plugins.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/themes/theme-1.css" id="skin_color" />
    <!-- end: CLIP-TWO CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body class="login">
<!-- start: LOGIN -->
<div class="row">
    <div class="col-md-6 box-register main-login col-sm-8 col-md-6 col-md-offset-3">
        <?php
        defined('BASEPATH') OR exit('No direct script access allowed');
        ?>
        <div class="container-fluid container-fullw bg-white">
            <div class="logo margin-top-30">
                <center>
                    <img src="assets/templates/admin/assets/images/LogoCID.png" alt="CID" style="margin-bottom: 20px" />
                </center>
            </div>
            <div class="col-lg-offset-2 col-lg-8 col-md-12">
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h5 class="panel-title">Nuevo Usuario</h5>
                    </div>
                    <div class="panel-body">
                        <?php
                        if (validation_errors()) {
                            echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                        }
                        ?>
                        <form role="form" action="<?php echo base_url('create-user-ext'); ?>" method="POST">
                            <div class="form-group">
                                <label for="user_email">
                                    Correo electronico
                                </label>
                                <input type="email" class="form-control" name="user_email" value="<?php echo set_value('user_email'); ?>" id="user_email" placeholder="Correo electronico">
                            </div>
                            <div class="form-group">
                                <label for="user_pass">
                                    Contraseña
                                </label>
                                <input type="password" class="form-control" name="user_pass" id="user_pass" placeholder="Contraseña ">
                            </div>
                            <div class="form-group">
                                <label for="user_pass_repeat">
                                    Repite Contraseña
                                </label>
                                <input type="password" class="form-control" name="user_pass_repeat" id="user_pass_repeat" placeholder="Repite Contraseña">
                            </div>
                            <a href="login-ext" class="btn btn-primary">
                                <i class="ti-arrow-left"></i>
                                Volver
                            </a>
                            <button type="submit" class="btn btn-success">
                                Crear
                                <i class="ti-arrow-right"></i>
                            </button>
                        </form>
                    </div>
                </div>
                <!-- start: COPYRIGHT -->
                <div class="copyright">
                    &copy; <span class="current-year"></span><span class="text-bold text-uppercase">Corporación Infancia y Desarrollo</span>. <span>Todos los derechos reservados</span></span>
                </div>
                <!-- end: COPYRIGHT -->
            </div>
        </div>
    </div>
</div>
<!-- end: LOGIN -->
<!-- start: MAIN JAVASCRIPTS -->
<script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/modernizr/modernizr.js"></script>
<script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/switchery/switchery.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery-validation/jquery.validate.min.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<!-- start: CLIP-TWO JAVASCRIPTS -->
<script src="<?php echo base_url('assets/templates/admin'); ?>/assets/js/main.js"></script>
<!-- start: JavaScript Event Handlers for this page -->
<script src="<?php echo base_url('assets/templates/admin'); ?>/assets/js/login.js"></script>

<script>
    jQuery(document).ready(function () {
        Main.init();
        Login.init();            });
</script>
<!-- end: JavaScript Event Handlers for this page -->
<!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
<!-- end: BODY -->
</html>