<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Perfil de Usuario</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('update-perfil-user'); ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $user->UsuarioID; ?>" name="id">
                    <div class="form-group">
                        <label for="email">
                            Correo electronico
                        </label>
                        <input type="email" class="form-control" readonly disabled value="<?php echo $user->EmailUsuario; ?>" id="email" placeholder="Correo electronico">
                    </div>
                    <div class="form-group">
                        <label for="user_nombres">
                            Nombres
                        </label>
                        <input type="text" class="form-control" name="user_nombres" value="<?php if(set_value('user_nombres')){ echo set_value('user_nombres'); }else{ echo $userdatos->Nombres; } ?>" id="user_nickname" placeholder="Nombre de usuario">
                    </div>
                    <div class="form-group">
                        <label for="user_apellidos">
                            Apellidos
                        </label>
                        <input type="text" class="form-control" name="user_apellidos" value="<?php if(set_value('user_apellidos')){ echo set_value('user_apellidos'); }else{ echo $userdatos->Apellidos; } ?>" id="user_nickname" placeholder="Nombre de usuario">
                    </div>
                    <div class="form-group">
                        <label for="user_cargo">
                            Cargo de Usuario
                        </label>
                        <select id="user_cargo" name="user_cargo" class="users_select_cargo form-control">
                            <?php 
                            foreach($cargos as $cargo){
                                if($userdatos->CargoID == $cargo->cargoID){
                                    $selected = "selected";
                                }else{
                                    $selected = "";
                                }
                                echo '<option value="'.$cargo->cargoID.'" '.$selected.'>'.$cargo->nombreCargo.'</option>';
                            }
                                ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="file" name="userfile">
                    </div> 
                    <button type="submit" class="btn btn-o btn-primary">
                        Guardar Cambios
                    </button>
                    <a href="<?php echo base_url('dashboard');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>