<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nuevo Usuario</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('agregar-usuario'); ?>" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="user_email">
                            Correo electronico
                        </label>
                        <input type="email" class="form-control" name="user_email" value="<?php echo set_value('user_email'); ?>" id="user_email" placeholder="Correo electronico">
                    </div>
                    <div class="form-group">
                        <label for="user_nombres">
                            Nombres
                        </label>
                        <input type="text" class="form-control" name="user_nombres" value="<?php echo set_value('user_nombres'); ?>" id="user_nombres" placeholder="Nombres">
                    </div>
                    <div class="form-group">
                        <label for="user_apellidos">
                            Apellidos
                        </label>
                        <input type="text" class="form-control" name="user_apellidos" value="<?php echo set_value('user_apellidos'); ?>" id="user_apellidos" placeholder="Apellidos">
                    </div>
                    <div class="form-group">
                        <label for="user_pass">
                            Contraseña
                        </label>
                        <input type="password" class="form-control" name="user_pass" id="user_pass" placeholder="Contraseña ">
                    </div>
                    <div class="form-group">
                        <label for="user_pass_repeat">
                            Repite Contraseña
                        </label>
                        <input type="password" class="form-control" name="user_pass_repeat" id="user_pass_repeat" placeholder="Repite Contraseña">
                    </div>  

                    <div class="form-group">
                        <label for="user_rol">
                            Rol de Usuario
                        </label>
                        <select id="user_rol" name="user_rol" class="users_select_rol form-control">
                            <option value="0" selected="selected">Selecciona Rol</option>
                            <?php echo $roles; ?>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="user_status">
                            Estatus de Usuario
                        </label>
                        <select id="user_status" name="user_status" class="users_select_status form-control">
                            <option value="0" selected="selected">Selecciona Status</option>
                            <?php echo $status; ?>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="user_cargo">
                            Cargo de Usuario
                        </label>
                        <select id="user_cargo" name="user_cargo" class="users_select_cargo form-control">
                            <option value="0" selected="selected">Selecciona Cargo</option>
                            <?php echo $cargos; ?>
                        </select>
                    </div>
                    
                    <div class="form-group">

                        <input type="file" name="userfile">

                    </div> 
                    <button type="submit" class="btn btn-o btn-primary">
                        Crear
                    </button>
                    <a href="<?php echo base_url('lista-usuarios');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>