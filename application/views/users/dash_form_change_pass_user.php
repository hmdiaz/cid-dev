<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Cambio de Contraseña de Usuario</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-clave-usuario'); ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $id; ?>" name="id">
                    <div class="form-group">
                        <label for="user_pass">
                            Contraseña
                        </label>
                        <input type="password" class="form-control" name="user_pass" id="user_pass" placeholder="Contraseña ">
                    </div>
                    <div class="form-group">
                        <label for="user_pass_repeat">
                            Repite Contraseña
                        </label>
                        <input type="password" class="form-control" name="user_pass_repeat" id="user_pass_repeat" placeholder="Repite Contraseña">
                    </div>  
                    <button type="submit" class="btn btn-o btn-primary">
                        Guardar
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>