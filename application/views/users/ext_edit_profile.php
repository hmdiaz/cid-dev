<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Información de Usuario</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('actualizar-perfil'); ?>" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email">
                                    Correo electronico
                                </label>
                                <input type="email" class="form-control" readonly disabled value="<?php echo $user->EmailUsuario; ?>" id="email" placeholder="Correo electronico">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tipo_documento">
                                    Tipo de Documento
                                </label>
                                <select id="tipo_documento" name="tipo_documento" class="form-control">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    foreach ($tiposdocumentos as $tipodocumento){
                                        if($UserData->TipoDocumentoID == $tipodocumento->TipoDocumentoID){
                                            echo '<option value="'.$tipodocumento->TipoDocumentoID.'" selected="selected">'.$tipodocumento->Nombre.'</option>';
                                        }  else {
                                            echo '<option value="'.$tipodocumento->TipoDocumentoID.'">'.$tipodocumento->Nombre.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="user_nickname">
                                    Documento de Identidad
                                </label>
                                <input type="text" class="form-control" name="documento_identidad" value="<?php if(set_value('documento_identidad')){ echo set_value('documento_identidad'); }else{ echo $UserData->DocumentoIdentidad; } ?>" id="documento_identidad" placeholder="Documento de Identidad">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="user_nickname">
                                    Nombres
                                </label>
                                <input type="text" class="form-control" name="nombres" value="<?php if(set_value('nombres')){ echo set_value('nombres'); }else{ echo $UserData->Nombres; } ?>" id="nombres" placeholder="Nombres">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="user_nickname">
                                    Apellidos
                                </label>
                                <input type="text" class="form-control" name="apellidos" value="<?php if(set_value('apellidos')){ echo set_value('apellidos'); }else{ echo $UserData->Apellidos; } ?>" id="apellidos" placeholder="Apellidos">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="departamento">
                                    Departamento Expedición Documento
                                </label>
                                <select id="departamento_expedicion" name="departamento_expedicion" class="form-control"onchange="getMunicipios(this.options[this.selectedIndex].value, '#departamento_expedicion', '#municipio_expedicion')">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    foreach ($departamentos as $departamento){
                                        if($UserData->DepIdExpedicion == $departamento->DepartamentoID){
                                            echo '<option value="'.$departamento->DepartamentoID.'" selected="selected">'.$departamento->Nombre.'</option>';
                                        }  else {
                                            echo '<option value="'.$departamento->DepartamentoID.'">'.$departamento->Nombre.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="municipio_expedicion">
                                    Municipio Expedición Documento
                                </label>
                                <select id="municipio_expedicion" name="municipio_expedicion" class="form-control">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    if($UserData->MunIDExpDocumento > 0)
                                    {
                                        foreach ($MunicipiosExp as $muns)
                                        {
                                            if($UserData->MunIDExpDocumento == $muns->MunicipioID){
                                                echo '<option value="'.$muns->MunicipioID.'" selected="selected">'.$muns->Nombre.'</option>';
                                            }  else {
                                                echo '<option value="'.$muns->MunicipioID.'">'.$muns->Nombre.'</option>';
                                            }
                                        }
                                    }
                                    ?>
                                </select>                          
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="departamento_residencia">
                                    Departamento Residencia
                                </label>
                                <select id="departamento_residencia" name="departamento_residencia" class="form-control" onchange="getMunicipios(this.options[this.selectedIndex].value, '#departamento_residencia', '#municipio_residencia')">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    foreach ($departamentos as $departamento){
                                        if($UserData->DepIdResidencia == $departamento->DepartamentoID){
                                            echo '<option value="'.$departamento->DepartamentoID.'" selected="selected">'.$departamento->Nombre.'</option>';
                                        }  else {
                                            echo '<option value="'.$departamento->DepartamentoID.'">'.$departamento->Nombre.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="municipio_residencia">
                                    Municipio Residencia
                                </label>
                                <select id="municipio_residencia" name="municipio_residencia" class="form-control">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    if($UserData->MunIDResidencia > 0)
                                    {
                                        foreach ($MunicipiosRes as $muns){
                                            if($UserData->MunIDResidencia == $muns->MunicipioID){
                                                echo '<option value="'.$muns->MunicipioID.'" selected="selected">'.$muns->Nombre.'</option>';
                                            }  else {
                                                echo '<option value="'.$muns->MunicipioID.'">'.$muns->Nombre.'</option>';
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fecha_nacimiento">
                                    Fecha de Nacimiento
                                </label>
                                <p class="input-group input-append datepicker date">
                                    <input type="text" class="form-control" name="fecha_nacimiento" id="fecha_nacimiento" value="<?php if(set_value('fecha_nacimiento')){ echo set_value('fecha_nacimiento'); }else{ echo $UserData->FechaNacimiento; } ?>">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </button>
                                    </span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">    
                            <div class="form-group">
                                <label for="user_nickname">
                                    Teléfono
                                </label>
                                <input type="text" class="form-control" name="telefono" value="<?php if(set_value('telefono')){ echo set_value('telefono'); }else{ echo $UserData->Telefono; } ?>" id="telefono" placeholder="Teléfono">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="user_nickname">
                                    Celular
                                </label>
                                <input type="text" class="form-control" name="celular" value="<?php if(set_value('celular')){ echo set_value('celular'); }else{ echo $UserData->Celular; } ?>" id="celular" placeholder="Celular">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="user_nickname">
                                    Dirección
                                </label>
                                <input type="text" class="form-control" name="direccion" value="<?php if(set_value('direccion')){ echo set_value('direccion'); }else{ echo $UserData->Direccion; } ?>" id="celular" placeholder="Dirección">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="user_nickname">
                                    Número de Tarjeta Profesional
                                </label>
                                <input type="text" class="form-control" name="tarjeta_profesional" value="<?php if(set_value('tarjeta_profesional')){ echo set_value('tarjeta_profesional'); }else{ echo $UserData->NumeroTarjetaProfesional; } ?>" id="tarjeta_profesional" placeholder="Número de Tarjeta Profesional">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fecha_nacimiento">
                                    Fecha de Expedición Tarjeta Profesional
                                </label>
                                <p class="input-group input-append datepicker date">
                                    <input type="text" class="form-control" name="fecha_exp_tarjeta_profesional" id="fecha_exp_tarjeta_profesional" value="<?php if(set_value('fecha_exp_tarjeta_profesional')){ echo set_value('fecha_exp_tarjeta_profesional'); }else{ echo $UserData->FechaExpTarjetaProfesional; } ?>">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </button>
                                    </span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="estado_civil">
                                    Genero
                                </label>
                                <select id="genero" name="genero" class="form-control">
                                    <?php
                                        if($UserData->Genero == 'M')
                                        {
                                            ?>
                                                <option value="">Seleccione uno...</option>
                                                <option value="M" selected>Masculino</option>
                                                <option value="F">Femenino</option>
                                            <?php
                                        }
                                        else if($UserData->Genero == 'F')
                                        {
                                            ?>
                                                <option value="">Seleccione uno...</option>
                                                <option value="M">Masculino</option>
                                                <option value="F" selected>Femenino</option>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <option value="" selected>Seleccione uno...</option>
                                            <option value="M">Masculino</option>
                                            <option value="F">Femenino</option>
                                            <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="estado_civil">
                                    Estado Civil
                                </label>
                                <select id="estado_civil" name="estado_civil" class="form-control">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    foreach ($estadosciviles as $estadocivil){
                                        if($UserData->EstadoCivilID == $estadocivil->EstadoCivilID){
                                            echo '<option value="'.$estadocivil->EstadoCivilID.'" selected="selected">'.$estadocivil->Nombre.'</option>';
                                        }  else {
                                            echo '<option value="'.$estadocivil->EstadoCivilID.'">'.$estadocivil->Nombre.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="userfile">
                                    Foto
                                </label>
                                <input type="file" name="userfile">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" name="hojavida" value="perfil" class="btn btn-o btn-primary">
                                Actualizar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
