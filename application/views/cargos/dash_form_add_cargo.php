<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-offset-2 col-lg-8 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nuevo Cargo</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('agregar-cargo'); ?>" method="POST" >
                    <div class="form-group">
                        <label for="cargo_name">
                            Nombre del Cargo
                        </label>
                        <input type="text" class="form-control" name="cargo_name" value="<?php echo set_value('cargo_name'); ?>" id="cargo_name" placeholder="Nombre Cargo">
                    </div>
                    <div class="form-group">
                        <label for="cargo_area">
                            Area del Cargo
                        </label>
                        <select id="cargo_status" name="cargo_area" class="select_add_cargo_area form-control">
                            <?php
                            foreach ($areas as $area) {
                                echo '<option value="'.$area->ProyectoID.'">'.$area->NombreProyecto.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="cargo_status">
                            Estatus del Cargo
                        </label>
                        <select id="cargo_status" name="cargo_status" class="select_add_cargo form-control">
                            <option value="1" selected="selected">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Crear Cargo
                    </button>
                    <a href="<?php echo base_url('lista-cargos'); ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>