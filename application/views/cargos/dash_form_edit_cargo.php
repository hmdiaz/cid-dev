<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-offset-2 col-lg-8 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Cargo</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-cargo'); ?>" method="POST" >
                    <input type="hidden" value="<?php echo $cargo->cargoID; ?>" name="id">
                    <div class="form-group">
                        <label for="cargo_name">
                            Nombre del Cargo
                        </label>
                        <input type="text" class="form-control" name="cargo_name" value="<?php if(set_value('cargo_name')){ echo set_value('cargo_name');}else{ echo $cargo->nombreCargo; } ?>" id="cargo_name" placeholder="Nombre Cargo">
                    </div>
                    <div class="form-group">
                        <label for="cargo_area">
                            Area del Cargo
                        </label>
                        <select id="cargo_status" name="cargo_area" class="select_add_cargo_area form-control">
                            <?php
                            foreach ($areas as $area) {
                                if($area->ProyectoID == $cargo->AreaID){
                                    $selected = "selected";
                                }else{
                                    $selected = "";
                                }
                                echo '<option value="'.$area->ProyectoID.'" '.$selected.'>'.$area->NombreProyecto.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Actualizar Cargo
                    </button>
                    <a href="<?php echo base_url('lista-cargos');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>