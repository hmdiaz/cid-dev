<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="row">
        <div class="col-md-12">        
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width" id="list_cargos">
                    <thead>
                        <tr>
                            <th>Cargo</th>
                            <th>Area</th>
                            <th>Status</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $cargos; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>