<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Responsabilidad</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('update-responsabilidad'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $responsabilidad->ResponsabilidadID; ?>" name="id">
                    <div class="form-group">
                        <label for="responsabilidad_name">
                            Descripción
                        </label>
                        <textarea class="form-control autosize area-animated" id="responsabilidad_name" name="responsabilidad_name" data-autosize-on="true" style="overflow: hidden; resize: horizontal; word-wrap: break-word; height: 71px;"><?php if(set_value('responsabilidad_name')){ echo set_value('responsabilidad_name'); }else{ echo $responsabilidad->Descripcion; } ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="tipo_responsabilidad">
                            Tipo de Responsabilidad
                        </label>
                        <select id="tipo_responsabilidad" name="tipo_responsabilidad" class="form-control">
                            <option value="">Seleccione uno...</option>
                            <?php
                            foreach ($tipo_responsabilidad as $item){
                                if($item->TipoResponsabilidadID == $responsabilidad->TipoResponsabilidadID)
                                {
                                    echo '<option value="'.$item->TipoResponsabilidadID.'" selected>'.$item->Descripcion.'</option>';
                                }
                                else
                                {
                                    echo '<option value="'.$item->TipoResponsabilidadID.'">'.$item->Descripcion.'</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Actualizar
                    </button>
                    <a href="<?php echo base_url('lista-responsabilidades');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>