<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive" style="min-height: 200px">
            <table class="table table-bordered table-hover" id="tbl_poblaciones_mto">
                <thead>
                <tr>
                    <th>Nombre de la Población</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($poblaciones as $poblacion)
                {
                    ?>
                    <tr>
                        <td><?php echo $poblacion->Nombre;  ?></td>
                        <td>
                            <a class='btn btn-primary btn-xs btn-o' href='<?php echo base_url() . 'editar-poblacion/' . $poblacion->PoblacionID ?>'>
                                <i class='fa fa-edit'></i> Editar
                            </a>
                            <a class='btn btn-danger btn-xs btn-o delete' href='<?php echo base_url() . 'eliminar-poblacion/' . $poblacion->PoblacionID ?>'>
                                <i class='fa fa-trash'></i> Eliminar
                            </a>

                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div>
            <?php //echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
