<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nueva Población</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('update-poblacion'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $poblacion->PoblacionID; ?>" name="id">
                    <div class="form-group">
                        <label for="socio_name">
                            Nombre de Población
                        </label>
                        <input type="text" class="form-control" name="poblacion_name" value="<?php if(set_value('poblacion_name')){ echo set_value('poblacion_name'); }else{ echo $poblacion->Nombre; } ?>" id="poblacion_name" placeholder="Nombre de Población">
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Actualizar
                    </button>
                    <a href="<?php echo base_url('lista-poblaciones');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>