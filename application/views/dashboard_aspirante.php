<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html>
<!-- Template Name: Clip-Two - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <head>
        <title><?php echo $title; ?></title>
        <!-- start: META -->
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: GOOGLE FONTS -->
        <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        <!-- end: GOOGLE FONTS -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/fontawesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/themify-icons/themify-icons.min.css">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/animate.css/animate.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/switchery/switchery.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/sweetalert/sweet-alert.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/select2/select2.min.css" rel="stylesheet" media="screen">
        <!-- end: MAIN CSS -->
        <!-- start: CLIP-TWO CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/styles.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/plugins.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/themes/theme-1.css" />
        <!-- end: CLIP-TWO CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    </head>
    <!-- end: HEAD -->
    <body>
        <div id="app">
            <!-- sidebar -->
            <div class="sidebar app-aside" id="sidebar">
                <div class="sidebar-container perfect-scrollbar">
                    <nav>
                        <!-- start: MAIN NAVIGATION MENU -->
                        <div class="navbar-title">
                            <span>Main Navigation</span>
                        </div>
                        <ul class="main-navigation-menu">
                            <li <?php
                            if ($active == "home") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url('dashboard-aspirante'); ?>">
                                    <div class="item-content">
                                        <div class="item-media">
                                            <i class="ti-home"></i>
                                        </div>
                                        <div class="item-inner">
                                            <span class="title"> Dashboard </span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li <?php
                            if ($active == "cv") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url('hoja-de-vida'); ?>">
                                    <div class="item-content">
                                        <div class="item-media">
                                            <i class="ti-id-badge"></i>
                                        </div>
                                        <div class="item-inner">
                                            <span class="title"> Hoja de Vida</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li <?php
                            if ($active == "workoffers") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url('ofertas-de-trabajo'); ?>">
                                    <div class="item-content">
                                        <div class="item-media">
                                            <i class="ti-briefcase"></i>
                                        </div>
                                        <div class="item-inner">
                                            <span class="title"> Ofertas de Trabajo</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- end: MAIN NAVIGATION MENU -->
                    </nav>
                </div>
            </div>
            <!-- / sidebar -->
            <div class="app-content">
                <!-- start: TOP NAVBAR -->
                <header class="navbar navbar-default navbar-static-top">
                    <!-- start: NAVBAR HEADER -->
                    <div class="navbar-header">
                        <a href="#" class="sidebar-mobile-toggler pull-left hidden-md hidden-lg" class="btn btn-navbar sidebar-toggle" data-toggle-class="app-slide-off" data-toggle-target="#app" data-toggle-click-outside="#sidebar">
                            <i class="ti-align-justify"></i>
                        </a>
                        <a class="navbar-brand" href="#">
                            <img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="Clip-Two"/>
                        </a>
                        <a href="#" class="sidebar-toggler pull-right visible-md visible-lg" data-toggle-class="app-sidebar-closed" data-toggle-target="#app">
                            <i class="ti-align-justify"></i>
                        </a>
                        <a class="pull-right menu-toggler visible-xs-block" id="menu-toggler" data-toggle="collapse" href=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <i class="ti-view-grid"></i>
                        </a>
                    </div>
                    <!-- end: NAVBAR HEADER assets/images/users_img/default-user.png-->
                    <!-- start: NAVBAR COLLAPSE -->
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-right">
                            <!-- start: USER OPTIONS DROPDOWN -->
                            <li class="dropdown current-user">
                                <a href class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url("assets/images/users_img/".$_SESSION['img_profile']); ?>" alt="<?php echo $_SESSION['firstName']; ?>"> <span class="username"><?php echo $_SESSION['firstName'] ?> <i class="ti-angle-down"></i></i></span>
                                </a>
                                <ul class="dropdown-menu dropdown-dark">
                                    <li>
                                        <a href="<?php echo base_url('editar-perfil'); ?>">
                                            Perfil
                                        </a>
                                    </li>                                    
                                    <li>
                                        <a href="<?php echo base_url('signout-ext'); ?>">
                                            Log Out
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- end: USER OPTIONS DROPDOWN -->
                        </ul>
                        <!-- start: MENU TOGGLER FOR MOBILE DEVICES -->
                        <div class="close-handle visible-xs-block menu-toggler" data-toggle="collapse" href=".navbar-collapse">
                            <div class="arrow-left"></div>
                            <div class="arrow-right"></div>
                        </div>
                        <!-- end: MENU TOGGLER FOR MOBILE DEVICES -->
                    </div>
                    <!-- end: NAVBAR COLLAPSE -->
                </header>
                <!-- end: TOP NAVBAR -->
                <div class="main-content" >
                    <div class="wrap-content container" id="container">
                        <!-- start: PAGE TITLE -->
                        <section id="page-title">
                            <div class="row">
                                <div class="col-sm-8">
                                    <h1 class="mainTitle"><?php echo $maintitle; ?></h1>
                                    <span class="mainDescription"><?php echo $maindescription; ?></span>
                                </div>
                            </div>
                        </section>                        
                        <!-- end: PAGE TITLE -->
                        <?php
                        if ($this->session->flashdata('message_error')) {
                            ?>
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>
                            </div>
                            <?php
                        }
                        if ($this->session->flashdata('message_success')) {
                            ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                            </div>
                            <?php
                        }
                        ?>
                        <!-- start: YOUR CONTENT HERE -->
                        <div class="container-fluid container-fullw bg-white">
                            <?php $this->load->view($content_view); ?>
                        </div>
                        <!-- end: YOUR CONTENT HERE -->
                    </div>
                </div>
            </div>
            <!-- start: FOOTER -->
            <footer>
                <div class="footer-inner">
                    <div class="pull-left">
                        &copy; <span class="current-year"></span><span class="text-bold text-uppercase">Corporación Infancia y Desarrollo</span>.  <br class="hidden-md hidden-sm hidden-lg"><span>Todos los derechos reservados</span>
                    </div>
                    <div class="pull-right">
                        <span class="go-top"><i class="ti-angle-up"></i></span>
                    </div>
                </div>
            </footer>
            <!-- end: FOOTER -->            
        </div>
        <!-- start: MAIN JAVASCRIPTS -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/modernizr/modernizr.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery-cookie/jquery.cookie.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/switchery/switchery.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/sweetalert/sweet-alert.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/select2/select2.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery.steps.min.js"></script>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: CLIP-TWO JAVASCRIPTS -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/assets/js/main.js"></script>
        <!-- start: JavaScript Event Handlers for this page -->
        <!-- end: JavaScript Event Handlers for this page -->
        <!-- end: CLIP-TWO JAVASCRIPTS -->
        <script>
            jQuery(document).ready(function () {
                Main.init();
            });
        </script>
        
        <script type="text/javascript" src="//platform.linkedin.com/in.js">
            api_key: 7772
            t2d069kcih
            t2d069kcih
            authorize: true
            onLoad: onLinkedInLoad
        </script>
        <script type="text/javascript">
            // Setup an event listener to make an API call once auth is complete
            function onLinkedInLoad() {
                IN.Event.on(IN, "auth", getProfileData);
            }

            // Handle the successful return from the API call
            function onSuccess(data) {

                window.location = "dashboard-aspirante";
            }

            // Handle an error response from the API call
            function onError(error) {
                console.log(error);
            }

            // Use the API call wrapper to request the member's basic profile data
            function getProfileData() {
                //IN.API.Raw("/people/~").result(onSuccess).error(onError);
                console.log(IN.ENV.auth.oauth_token);
                IN.API.Profile("me")
                        .fields([
                            "firstName", "lastName", "headline", "positions:(company,title,summary,startDate,endDate,isCurrent)", "industry",
                            "location:(name,country:(code))", "pictureUrl", "publicProfileUrl", "emailAddress",
                            "educations", "dateOfBirth"])
                        .result(function (result) {
                            console.log(result.values[0]);
                        })
                        .error(function (err) {
                            alert(err);
                        });
            }

            $(document).ready(function () {
                $('#LinkedInLogout').on('click', function (e) {
                    e.preventDefault();
                    IN.User.logout();
                    window.location = "signout-ext";
                })
            });
        </script>

        <script type="text/javascript">
            function getMunicipios(id, idorigen, iddestino)
            {
                var depId = $(idorigen).val();
                $.ajax({
                    url: 'municipios/' + depId,
                    type: 'GET',
                    success: function (response) {
                        $(iddestino).html(response);
                    }
                })
            }
            $(document).ready(function () {
                $('#fecha_nacimiento').datepicker({
                    format: "yyyy-mm-d",
                    todayHighlight: true,
                    startDate: "-99y +0y",
                    endDate: "+0m +0d"
                    
                });

                $('#fecha_exp_tarjeta_profesional').datepicker({
                    format: "yyyy-mm-d",
                    todayHighlight: true,
                    startDate: "-99y +0y",
                    endDate: "+0m +0d"
                });

                $('#hv_child_birthdate').datepicker({
                    format: "yyyy-mm-d",
                    todayHighlight: true,
                    startDate: "-99y +0y",
                    endDate: "+0m +0d"
                });
                $('#hv_date_grade').datepicker({
                    format: "yyyy-mm-d",
                    todayHighlight: true,
                    startDate: "-99y +0y",
                    endDate: "+0m +0d"
                });
                $('#hv_explab_date_ini').datepicker({
                    format: "yyyy-mm-d",
                    todayHighlight: true,
                    startDate: "-99y +0y",
                    endDate: "+0m +0d"
                });
                $('#hv_explab_date_fin').datepicker({
                    format: "yyyy-mm-d",
                    todayHighlight: true,
                    startDate: "-99y +0y",
                    endDate: "+0m +0d"
                });
                $('#hv_ec_date_cert').datepicker({
                    format: "yyyy-mm-d",
                    todayHighlight: true,
                    startDate: "-99y +0y",
                    endDate: "+0m +0d"
                });
                
                $('#hv_ex_date_cert').datepicker({
                    format: "yyyy-mm-d",
                    todayHighlight: true,
                    startDate: "-99y +0y",
                    endDate: "+0m +0d"
                });

                $("input[name='hv_act_exp_a']").TouchSpin({
                    min:0,
                    verticalbuttons: true,
                    verticalupclass: 'fa fa-plus',
                    verticaldownclass: 'fa fa-minus'
                });
                
                $("input[name='hv_act_exp_m']").TouchSpin({
                    min:0,
                    max:11,
                    verticalbuttons: true,
                    verticalupclass: 'fa fa-plus',
                    verticaldownclass: 'fa fa-minus'
                });

                $("input[name='hv_desc_hab_time']").TouchSpin({
                    verticalbuttons: true,
                    verticalupclass: 'fa fa-plus',
                    verticaldownclass: 'fa fa-minus'
                });
                $("input[name='hv_ie_exp']").TouchSpin({
                    verticalbuttons: true,
                    verticalupclass: 'fa fa-plus',
                    verticaldownclass: 'fa fa-minus'
                });
                $("input[name='hv_ec_experiencia']").TouchSpin({
                    verticalbuttons: true,
                    verticalupclass: 'fa fa-plus',
                    verticaldownclass: 'fa fa-minus'
                });
            });
            //hoja de vida
            //$("#hv_ia_titulo").select2();


            $(".delete").on("click", function (e) {
                e.preventDefault();
                var url = this.href;
                swal({
                    title: "Estás seguro?",
                    text: "Vas a eliminar un registro!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#007AFF",
                    confirmButtonText: "Si, borrar!",
                    closeOnConfirm: false,
                    closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm)
                    {
                        window.location = url;
                    }
                });
            });

            /*
             $("#departamentos").on('change', function(e){
             var depId = $("#departamentos").val();
             $.ajax({
             url: 'municipios/' + depId,
             type: 'GET',
             success: function(response){
             $("#municipios").html(response);
             }
             })
             });
             */
            

        </script>
    </body>
</html>