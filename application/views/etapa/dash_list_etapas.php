<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive" style="min-height: 200px">
            <table class="table table-bordered table-hover" id="tbl_etapas">
                <thead>
                <tr>
                    <th>Decripción de la Etapa</th>
                    <th>Nombre de la Etapa</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($etapas as $etapa)
                {
                    ?>
                    <tr>
                        <td><?php echo $etapa->Descripcion;  ?></td>
                        <td><?php echo $etapa->Nombre;  ?></td>
                        <td>
                            <a class='btn btn-primary btn-xs btn-o' href='<?php echo base_url() . 'editar-etapa/' . $etapa->EtapaID ?>'>
                                <i class='fa fa-edit'></i> Editar
                            </a>
                            <a class='btn btn-danger btn-xs btn-o delete' href='<?php echo base_url() . 'eliminar-etapa/' . $etapa->EtapaID ?>'>
                                <i class='fa fa-trash'></i> Eliminar
                            </a>

                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div>
            <?php //echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
