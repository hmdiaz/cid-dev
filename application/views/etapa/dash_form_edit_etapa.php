<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nueva Etapa</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('update-etapa'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $etapa->EtapaID; ?>" name="id">
                    <div class="form-group">
                        <label for="etapa_desc">
                            Descripción de Etapa
                        </label>
                        <input type="text" class="form-control" name="etapa_desc" value="<?php if(set_value('etapa_desc')){ echo set_value('etapa_desc'); }else{ echo $etapa->Descripcion; } ?>" id="etapa_desc" placeholder="Descripcion de Etapa">
                    </div>
                    <div class="form-group">
                        <label for="etapa_name">
                            Nombre de Etapa
                        </label>
                        <input type="text" class="form-control" name="etapa_name" value="<?php if(set_value('etapa_name')){ echo set_value('etapa_name'); }else{ echo $etapa->Nombre; } ?>" id="etapa_name" placeholder="Nombre de Etapa">
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Actualizar
                    </button>
                    <a href="<?php echo base_url('lista-etapas');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>