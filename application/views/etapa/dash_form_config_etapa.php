<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Configurar Etapa</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('config-etapa'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $id_etapa_convocatoria; ?>" name="id">
                    <div class="form-group">
                        <label for="etapa_test">
                            Prueba para la etapa
                        </label>
                        <select id="etapa_test" name="etapa_test" class="roles_select_add_rol form-control">
                            <?php 
                                foreach ($tests as $test){
                                    echo '<option value="'.$test->prueba_id.'">'.$test->prueba_name.'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="etapa_time">
                            Duracion de la Prueba (en minutos)
                        </label>
                        <input type="text" class="form-control" name="etapa_time" value="<?php echo set_value('etapa_time'); ?>" id="etapa_time" placeholder="Duracion de la Prueba">
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Crear
                    </button>
                    <a href="<?php echo base_url('etapas/'.$etapa->ConvocatoriaID);  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>