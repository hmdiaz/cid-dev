<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="row">
        <div class="col-md-12">        
            <div class="table-responsive" style="min-height: 200px">
                <table class="table table-bordered table-hover" id="list_compe">
                    <thead>
                        <tr>
                            <?php if(isset($tipo_componente) && $tipo_componente == 1): ?>
                            <th>Grado</th>
                            <?php endif; ?>
                            <th>Descripcion</th>
                            <th>Categoria</th>
                            <th>Status</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $cpntes; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>