<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nuevo <?php echo $maintitle; ?></h5>
            </div>
            <div class="panel-body">

                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('agregar-cpnte'); ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $cpnte_type; ?>" name="cpnte_type">
                    <?php if ($cpnte_type == 1): ?>
                        <div class="form-group">
                            <label for="cpnte_grado">
                                Grado
                            </label>
                            <select id="cpnte_status" name="cpnte_grado" class="cpnte_select_add_grado form-control">
                                <?php
                                foreach ($grados as $grado) {
                                    echo '<option value="'.$grado->grado_id.'">'.$grado->grado_nombre.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    <?php endif; ?>
                    <?php if ($cpnte_type == 1 || $cpnte_type == 2): ?>
                    <div class="form-group">
                        <label for="cpnte_description">
                            Titulo Obtenido
                        </label>
                        <textarea class="form-control" name="cpnte_description" id="cpnte_description" placeholder="Titulo Obtenido"><?php echo set_value('cpnte_description'); ?></textarea>
                    </div>
                    <?php endif; ?>
                    <?php if ($cpnte_type == 3): ?>
                    <div class="form-group">
                        <label for="cpnte_description">
                            Descripcion
                        </label>
                        <textarea class="form-control" name="cpnte_description" id="cpnte_description" placeholder="Descripcion"><?php echo set_value('cpnte_description'); ?></textarea>
                    </div>
                    <?php endif; ?>
                    <?php if ($cpnte_type == 4): ?>
                    <div class="form-group">
                        <label for="cpnte_description">
                            Descripcion Habilidad
                        </label>
                        <textarea class="form-control" name="cpnte_description" id="cpnte_description" placeholder="Descripcion Habilidad"><?php echo set_value('cpnte_description'); ?></textarea>
                    </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <label for="cpnte_cat">
                            Categoria
                        </label>
                        <select id="cpnte_cat" name="cpnte_cat" class="cpnte_select_add_cat form-control">
                            <option value="0" selected="selected">Agregar una categoria</option>
                            <?php
                            foreach ($categorias as $cat) {
                                echo '<option value="' . $cat->CategoriaPerfilID . '">' . $cat->NombreCategoria . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="cpnte_status">
                            Estatus
                        </label>
                        <select id="cpnte_status" name="cpnte_status" class="cpnte_select_add_status form-control">
                            <option value="1" selected="selected">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-o btn-primary">
                            Crear
                        </button>
                        <?php
                        if ($cpnte_type == 1) {
                            echo '<a href="' . base_url("lista-cpnte-edu") . '" class="btn btn-o btn-danger pull-right">
                                    Cancelar
                                </a>';
                        }
                        if ($cpnte_type == 2) {
                            echo '<a href="' . base_url("lista-cpnte-form") . '" class="btn btn-o btn-danger pull-right">
                                    Cancelar
                                </a>';
                        }
                        if ($cpnte_type == 3) {
                            echo '<a href="' . base_url("lista-cpnte-exp") . '" class="btn btn-o btn-danger pull-right">
                                    Cancelar
                                </a>';
                        }
                        if ($cpnte_type == 4) {
                            echo '<a href="' . base_url("lista-cpnte-hab") . '" class="btn btn-o btn-danger pull-right">
                                    Cancelar
                                </a>';
                        }
                        ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>