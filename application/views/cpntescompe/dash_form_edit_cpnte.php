<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Componente de Competencia</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-cpnte'); ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $cpnte->CatalogoPerfilID; ?>" name="id">
                    <input type="hidden" value="<?php echo $cpnte->TipoCatalogoID; ?>" name="cpnte_type">
                    <?php if ($cpnte->TipoCatalogoID == 1): ?>
                        <div class="form-group">
                            <label for="cpnte_grado">
                                Grado
                            </label>
                            <select id="cpnte_status" name="cpnte_grado" class="cpnte_select_add_grado form-control">
                                <?php
                                foreach ($grados as $grado) {
                                    if($cpnte->tipo_grado_id == $grado->grado_id){
                                        $selected = "selected";
                                    }else{
                                        $selected = "";
                                    }
                                    echo '<option value="'.$grado->grado_id.'" '.$selected.'>'.$grado->grado_nombre.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    <?php endif; ?>
                    <?php if ($cpnte->TipoCatalogoID == 1 || $cpnte->TipoCatalogoID == 2): ?>
                    <div class="form-group">
                        <label for="cpnte_description">
                            Titulo Obtenido
                        </label>
                        <textarea class="form-control" name="cpnte_description" id="cpnte_description" placeholder="Titulo Obtenido"><?php if(set_value('cpnte_description')){ echo set_value('cpnte_description');}else{ echo $cpnte->Descripcion;} ?></textarea>
                    </div>
                    <?php endif; ?>
                    <?php if ($cpnte->TipoCatalogoID == 3): ?>
                    <div class="form-group">
                        <label for="cpnte_description">
                            Descripcion
                        </label>
                        <textarea class="form-control" name="cpnte_description" id="cpnte_description" placeholder="Descripcion"><?php if(set_value('cpnte_description')){ echo set_value('cpnte_description');}else{ echo $cpnte->Descripcion;} ?></textarea>
                    </div>
                    <?php endif; ?>
                    <?php if ($cpnte->TipoCatalogoID == 4): ?>
                    <div class="form-group">
                        <label for="cpnte_description">
                            Descripcion Habilidad
                        </label>
                        <textarea class="form-control" name="cpnte_description" id="cpnte_description" placeholder="Descripcion Habilidad"><?php if(set_value('cpnte_description')){ echo set_value('cpnte_description');}else{ echo $cpnte->Descripcion;} ?></textarea>
                    </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <label for="cpnte_cat">
                            Categoria
                        </label>
                        <select id="cpnte_cat" name="cpnte_cat" class="cpnte_select_add_cat form-control">
                            <?php
                            foreach ($categorias as $cat) {
                                if($cpnte->CategoriaPerfilID == $cat->CategoriaPerfilID){
                                    $selected = "selected";
                                }  else {
                                    $selected = "";
                                }
                                echo '<option value="' . $cat->CategoriaPerfilID . '"  '.$selected.'>' . $cat->NombreCategoria . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Guardar Cambios
                    </button>
                    <?php
                        if($cpnte->TipoCatalogoID == 1){
                            echo '<a href="'.base_url("lista-cpnte-edu").'" class="btn btn-o btn-danger pull-right">
                                    Cancelar
                                </a>';
                        }
                        if($cpnte->TipoCatalogoID == 2){
                           echo '<a href="'.base_url("lista-cpnte-form").'" class="btn btn-o btn-danger pull-right">
                                    Cancelar
                                </a>';
                        }
                        if($cpnte->TipoCatalogoID == 3){
                            echo '<a href="'.base_url("lista-cpnte-exp").'" class="btn btn-o btn-danger pull-right">
                                    Cancelar
                                </a>';
                        }
                        if($cpnte->TipoCatalogoID == 4){
                            echo '<a href="'.base_url("lista-cpnte-hab").'" class="btn btn-o btn-danger pull-right">
                                    Cancelar
                                </a>';
                        }                    
                    ?>
                </form>
            </div>
        </div>
    </div>
</div>