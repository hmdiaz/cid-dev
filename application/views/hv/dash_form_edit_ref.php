<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Referencia</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-hv-ref'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $ref->HvRefID; ?>" name="id">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="hv_ref_per_name">
                                Nombre Completo
                            </label>
                            <input type="text" class="form-control" required name="hv_ref_per_name" value="<?php if(set_value('hv_ref_per_name')){ echo set_value('hv_ref_per_name'); }else{ echo $ref->NombreReferencia; } ?>" id="hv_ref_per_name" placeholder="Nombre Completo">
                        </div>
                        <div class="form-group">
                            <label for="hv_ref_per_prof">
                                Profesion/Ocupacion
                            </label>
                            <input type="text" class="form-control" required name="hv_ref_per_prof" value="<?php if(set_value('hv_ref_per_prof')){ echo set_value('hv_ref_per_prof'); }else{ echo $ref->ProfesionOcupacion; } ?>" id="hv_ref_per_prof" placeholder="Profesion/Ocupacion">
                        </div>
                        <div class="form-group">
                            <label for="hv_ref_per_tel">
                                Telefono
                            </label>
                            <input type="text" class="form-control" required name="hv_ref_per_tel" value="<?php if(set_value('hv_ref_per_tel')){ echo set_value('hv_ref_per_tel'); }else{ echo $ref->Telefono; }  ?>" id="hv_ref_per_tel" placeholder="Telefono">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Actualizar
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>