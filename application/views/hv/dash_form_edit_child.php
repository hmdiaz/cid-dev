<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Hijo</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-hv-hijo'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $child->HvHijoID; ?>" name="id">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="hv_child_name">
                                Nombre Completo
                            </label>
                            <input type="text" class="form-control" required name="hv_child_name" value="<?php if(set_value('hv_child_name')){ echo set_value('hv_child_name'); }else{ echo $child->NombreCompleto ; } ?>" id="hv_child_name" placeholder="Nombre Completo">
                        </div>
                        <div class="form-group">
                            <label for="hv_child_birthdate">
                                Fecha de nacimiento
                            </label>
                            <input type="text" class="form-control datepicker" required name="hv_child_birthdate" value="<?php if(set_value('hv_child_birthdate')){ echo set_value('hv_child_birthdate'); }else{ echo $child->FechaNacimiento;} ?>" id="hv_child_birthdate">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Actualizar
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>