<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">  
    <div class="col-md-12">
        <?php
        if (validation_errors()) {
            echo '<div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                </button>
                ' . validation_errors() . '
            </div>';
        }
        ?>                 
    </div>
    <p class="text-danger">Te invitamos siempre a tener actualizada tu hoja de vida, revisa las ofertas de trabajo a las que te postulaste y revisa si puedes agregar mas informacion para tu hoja de vida.</p>
    <div class="tabbable">
        <ul id="myTab1" class="nav nav-tabs">
            <li class="active">
                <a href="#informacionpersonal" data-toggle="tab">
                    Paso 1<br><small>Información<br>Personal</small>
                </a>
            </li>
            <li>
                <a href="#informacionfamiliar" data-toggle="tab">
                    Paso 2<br><small>Información<br>Familiar</small>
                </a>
            </li>
            <li>
                <a href="#informacionacademica" data-toggle="tab">
                    Paso 3<br><small>Información<br>Academica</small>
                </a>
            </li>
            <li>
                <a href="#informacionlaboral" data-toggle="tab">
                    Paso 4<br><small>Experiencia<br>Laboral</small>
                </a>
            </li>
            <li>
                <a href="#nivelacademico" data-toggle="tab">
                    Paso 5<br><small>Nivel de<br>Formación</small>
                </a>
            </li>
            <li>
                <a href="#estudioscomplementarios" data-toggle="tab">
                    Paso 6<br><small>Estudios<br>Complementarios</small>
                </a>
            </li>
            <li>
                <a href="#referenciaspersonales" data-toggle="tab">
                    Paso 7<br><small>Referencias<br>Personales</small>
                </a>
            </li>
            <li>
                <a href="#referenciasprofesionales" data-toggle="tab">
                    Paso 8<br><small>Referencias<br>Profesionales</small>
                </a>
            </li>
             <li>
                <a href="#habilidades" data-toggle="tab">
                    Paso 9<br><small>Descripción de<br>Habilidades</small>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="informacionpersonal">
                <form role="form" action="<?php echo base_url('actualizar-perfil'); ?>" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email">
                                    Correo electronico
                                </label>
                                <input type="email" class="form-control" readonly disabled value="<?php echo $user->EmailUsuario; ?>" id="email" placeholder="Correo electronico">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tipo_documento">
                                    Tipo de Documento
                                </label>
                                <select id="tipo_documento" name="tipo_documento" class="form-control">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    foreach ($tiposdocumentos as $tipodocumento){
                                        if($UserData->TipoDocumentoID == $tipodocumento->TipoDocumentoID){
                                            echo '<option value="'.$tipodocumento->TipoDocumentoID.'" selected="selected">'.$tipodocumento->Nombre.'</option>';
                                        }  else {
                                            echo '<option value="'.$tipodocumento->TipoDocumentoID.'">'.$tipodocumento->Nombre.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="user_nickname">
                                    Documento de Identidad
                                </label>
                                <input type="text" class="form-control" name="documento_identidad" value="<?php if(set_value('documento_identidad')){ echo set_value('documento_identidad'); }else{ echo $UserData->DocumentoIdentidad; } ?>" id="documento_identidad" placeholder="Documento de Identidad">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="user_nickname">
                                    Nombres
                                </label>
                                <input type="text" class="form-control" name="nombres" value="<?php if(set_value('nombres')){ echo set_value('nombres'); }else{ echo $UserData->Nombres; } ?>" id="nombres" placeholder="Nombres">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="user_nickname">
                                    Apellidos
                                </label>
                                <input type="text" class="form-control" name="apellidos" value="<?php if(set_value('apellidos')){ echo set_value('apellidos'); }else{ echo $UserData->Apellidos; } ?>" id="apellidos" placeholder="Apellidos">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="departamento">
                                    Departamento Expedición Documento
                                </label>
                                <select id="departamento_expedicion" name="departamento_expedicion" class="form-control"onchange="getMunicipios(this.options[this.selectedIndex].value, '#departamento_expedicion', '#municipio_expedicion')">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    foreach ($departamentos as $departamento){
                                        if($UserData->DepIdExpedicion == $departamento->DepartamentoID){
                                            echo '<option value="'.$departamento->DepartamentoID.'" selected="selected">'.$departamento->Nombre.'</option>';
                                        }  else {
                                            echo '<option value="'.$departamento->DepartamentoID.'">'.$departamento->Nombre.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="municipio_expedicion">
                                    Municipio Expedición Documento
                                </label>
                                <select id="municipio_expedicion" name="municipio_expedicion" class="form-control">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    if($UserData->MunIDExpDocumento > 0)
                                    {
                                        foreach ($MunicipiosExp as $muns)
                                        {
                                            if($UserData->MunIDExpDocumento == $muns->MunicipioID){
                                                echo '<option value="'.$muns->MunicipioID.'" selected="selected">'.$muns->Nombre.'</option>';
                                            }  else {
                                                echo '<option value="'.$muns->MunicipioID.'">'.$muns->Nombre.'</option>';
                                            }
                                        }
                                    }
                                    ?>
                                </select>                          
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="departamento_residencia">
                                    Departamento Residencia
                                </label>
                                <select id="departamento_residencia" name="departamento_residencia" class="form-control" onchange="getMunicipios(this.options[this.selectedIndex].value, '#departamento_residencia', '#municipio_residencia')">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    foreach ($departamentos as $departamento){
                                        if($UserData->DepIdResidencia == $departamento->DepartamentoID){
                                            echo '<option value="'.$departamento->DepartamentoID.'" selected="selected">'.$departamento->Nombre.'</option>';
                                        }  else {
                                            echo '<option value="'.$departamento->DepartamentoID.'">'.$departamento->Nombre.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="municipio_residencia">
                                    Municipio Residencia
                                </label>
                                <select id="municipio_residencia" name="municipio_residencia" class="form-control">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    if($UserData->MunIDResidencia > 0)
                                    {
                                        foreach ($MunicipiosRes as $muns){
                                            if($UserData->MunIDResidencia == $muns->MunicipioID){
                                                echo '<option value="'.$muns->MunicipioID.'" selected="selected">'.$muns->Nombre.'</option>';
                                            }  else {
                                                echo '<option value="'.$muns->MunicipioID.'">'.$muns->Nombre.'</option>';
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fecha_nacimiento">
                                    Fecha de Nacimiento
                                </label>
                                <p class="input-group input-append datepicker date">
                                    <input type="text" class="form-control" name="fecha_nacimiento" id="fecha_nacimiento" value="<?php if(set_value('fecha_nacimiento')){ echo set_value('fecha_nacimiento'); }else{ echo $UserData->FechaNacimiento; } ?>">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </button>
                                    </span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">    
                            <div class="form-group">
                                <label for="user_nickname">
                                    Teléfono
                                </label>
                                <input type="text" class="form-control" name="telefono" value="<?php if(set_value('telefono')){ echo set_value('telefono'); }else{ echo $UserData->Telefono; } ?>" id="telefono" placeholder="Teléfono">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="user_nickname">
                                    Celular
                                </label>
                                <input type="text" class="form-control" name="celular" value="<?php if(set_value('celular')){ echo set_value('celular'); }else{ echo $UserData->Celular; } ?>" id="celular" placeholder="Celular">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="user_nickname">
                                    Dirección
                                </label>
                                <input type="text" class="form-control" name="direccion" value="<?php if(set_value('direccion')){ echo set_value('direccion'); }else{ echo $UserData->Direccion; } ?>" id="celular" placeholder="Dirección">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="user_nickname">
                                    Número de Tarjeta Profesional
                                </label>
                                <input type="text" class="form-control" name="tarjeta_profesional" value="<?php if(set_value('tarjeta_profesional')){ echo set_value('tarjeta_profesional'); }else{ echo $UserData->NumeroTarjetaProfesional; } ?>" id="tarjeta_profesional" placeholder="Número de Tarjeta Profesional">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fecha_nacimiento">
                                    Fecha de Expedición Tarjeta Profesional
                                </label>
                                <p class="input-group input-append datepicker date">
                                    <input type="text" class="form-control" name="fecha_exp_tarjeta_profesional" id="fecha_exp_tarjeta_profesional" value="<?php if(set_value('fecha_exp_tarjeta_profesional')){ echo set_value('fecha_exp_tarjeta_profesional'); }else{ echo $UserData->FechaExpTarjetaProfesional; } ?>">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </button>
                                    </span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="estado_civil">
                                    Genero
                                </label>
                                <select id="genero" name="genero" class="form-control">
                                    <?php
                                        if($UserData->Genero == 'M')
                                        {
                                            ?>
                                                <option value="">Seleccione uno...</option>
                                                <option value="M" selected>Masculino</option>
                                                <option value="F">Femenino</option>
                                            <?php
                                        }
                                        else if($UserData->Genero == 'F')
                                        {
                                            ?>
                                                <option value="">Seleccione uno...</option>
                                                <option value="M">Masculino</option>
                                                <option value="F" selected>Femenino</option>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <option value="" selected>Seleccione uno...</option>
                                            <option value="M">Masculino</option>
                                            <option value="F">Femenino</option>
                                            <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="estado_civil">
                                    Estado Civil
                                </label>
                                <select id="estado_civil" name="estado_civil" class="form-control">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    foreach ($estadosciviles as $estadocivil){
                                        if($UserData->EstadoCivilID == $estadocivil->EstadoCivilID){
                                            echo '<option value="'.$estadocivil->EstadoCivilID.'" selected="selected">'.$estadocivil->Nombre.'</option>';
                                        }  else {
                                            echo '<option value="'.$estadocivil->EstadoCivilID.'">'.$estadocivil->Nombre.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="userfile">
                                    Foto
                                </label>
                                <input type="file" name="userfile">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" name="hojavida" value="perfil" class="btn btn-o btn-primary">
                                Actualizar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="informacionfamiliar">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Hijos</h3>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-o pull-right" data-toggle="modal" data-target="#hijos">
                            Agregar Hijo
                        </button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nombre Completo</th>
                                <th>Fecha de Nacimiento</th>
                                <th>Edad(Años)</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $childs; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade" id="informacionacademica">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Informacion Academica <small><br>Diligenciar únicamente bachillerato y titulos de educación superior (Técnicos, Tecnológicos, Universitarios, Post graduales) en orden cronológico relacionando primero el último titulo obtenido.</small></h3>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-o pull-right" data-toggle="modal" data-target="#academic">
                            Agregar Info
                        </button>
                    </div> 
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Fecha de Grado <small><br>*Segun diploma o acto de Grado</small></th>
                                <th>Titulo Obtenido</th>
                                <th>Nombre de la Institucion</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $infoaca; ?>
                        </tbody>
                    </table>    
                </div>
            </div>
            <div class="tab-pane fade" id="informacionlaboral">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Experiencia Laboral <small><br>*Diligenciar esta información en orden cronológico primero el cargo mas reciente.<br>*Esta Información debe estar respaldada por las respectivas certificaciones laborales.</small></h3>

                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-o pull-right" data-toggle="modal" data-target="#explab">
                            Agregar Experiencia
                        </button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Cargo</th>
                                <th>Entidad</th>
                                <th>Jefe Inmediato</th>
                                <th>Telefono</th>
                                <th>Inicio</th>
                                <th>Finalización</th>                        
                                <th>Principales actividades desarrolladas</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $explab; ?>
                        </tbody>
                    </table>            
                </div>
            </div>
            <div class="tab-pane fade" id="nivelacademico">

                <div class="row">
                    <div class="col-md-12">
                        <h3>Nivel de Formación<small><br>*Diplomados, cursos, congresos, seminarios, etc.</small></h3>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-o pull-right" data-toggle="modal" data-target="#nivform">
                            Agregar Nivel de Formación
                        </button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Fecha de Certificiación</th>
                                <th>Nivel de Formacion</th>
                                <th>Nombre de la Institucion Certificadora</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $nivform; ?>
                        </tbody>
                    </table>            
                </div>
            </div>
            <div class="tab-pane fade" id="estudioscomplementarios">

                <div class="row">
                    <div class="col-md-12">
                        <h3>Estudios Complementarios<small><br>*Diplomados, cursos, congresos, seminarios, etc.</small></h3>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-o pull-right" data-toggle="modal" data-target="#estcomp">
                            Agregar Estudios Complementarios
                        </button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Fecha de Certificiación</th>
                                <th>Titulo Obtenido</th>
                                <th>Nombre de la Institucion Certificadora</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $estcomp; ?>
                        </tbody>
                    </table>            
                </div>
            </div>
            <div class="tab-pane fade" id="referenciaspersonales">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Referencias Personales</h3>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-o pull-right" data-toggle="modal" data-target="#refper">
                            Agregar Referenia Personal
                        </button>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Profesión/Ocupación</th>
                                <th>Telefono</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $refpers; ?>
                        </tbody>
                    </table>            
                </div>
            </div>
            <div class="tab-pane fade" id="referenciasprofesionales">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Referencias Profesionales</h3>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-o pull-right" data-toggle="modal" data-target="#refprof">
                            Agregar Referenia Profesionales
                        </button>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Profesión/Ocupación</th>
                                <th>Telefono</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $refprof; ?>
                        </tbody>
                    </table>            
                </div>
            </div>
            <div class="tab-pane fade" id="habilidades">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Descripción de Habilidades</h3>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-o pull-right" data-toggle="modal" data-target="#deschab">
                            Agregar Habilidades
                        </button>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Habilidad</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $deschab; ?>
                        </tbody>
                    </table>            
                </div>
            </div>
        </div>
    </div>
</div>

<!--Modal Hijos-->
<div class="modal fade" id="hijos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Hijos</h4>
            </div>
            <div class="modal-body">                   
                <form role="form" action="<?php echo base_url('agregar-hv-hijo'); ?>" method="POST">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="hv_child_name">
                                Nombre Completo
                            </label>
                            <input type="text" class="form-control" required name="hv_child_name" value="<?php echo set_value('hv_child_name'); ?>" id="hv_child_name" placeholder="Nombre Completo">
                        </div>
                        <div class="form-group">
                            <label for="hv_child_birthdate">
                                Fecha de nacimiento
                            </label>
                            <input type="text" class="form-control datepicker" required name="hv_child_birthdate" value="<?php echo set_value('hv_child_birthdate'); ?>" id="hv_child_birthdate">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Agregar
                    </button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-o" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
<!--Fin Modal Hijos-->

<!--Modal Informacion Academica-->
<div class="modal fade" id="academic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Informacion Academica</h4>
            </div>
            <div class="modal-body">                
                <form role="form" action="<?php echo base_url('agregar-hv-inf-academic'); ?>" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="hv_ia_titulo">
                                    Titulo Obtenido
                                </label>
                                <select class="form-control" required name="hv_ia_titulo" id="hv_ia_titulo">
                                    <option value="0" selected>Selecciona un Titulo</option>
                                    <?php
                                    foreach ($infoAcad as $infAca) {
                                        echo '<option value="' . $infAca->CatalogoPerfilID . '">' . $infAca->Descripcion . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="hv_option_ia_titulo">
                                    Si no encuentra su Titulo puede agregar uno aqui.
                                </label>
                                <input type="text" class="form-control" required name="hv_option_ia_titulo" value="<?php echo set_value('hv_option_ia_titulo'); ?>" id="hv_option_ia_titulo" placeholder="Agrega un titulo">
                            </div>  
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="hv_name_inst">
                                    Nombre de la Institucion Educativa
                                </label>
                                <input type="text" class="form-control" required name="hv_name_inst" value="<?php echo set_value('hv_name_inst'); ?>" id="hv_name_inst" placeholder="Nombre de la Institucion Educativa">
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="hv_date_grade">
                                    Fecha de Grado
                                </label>
                                <input type="text" class="form-control datepicker" required name="hv_date_grade" value="<?php echo set_value('hv_date_grade'); ?>" id="hv_date_grade">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Agregar
                    </button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-o" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

<!--Fin Modal Informacion Academica-->

<!--Modal Experiencia Laboral-->     
<div class="modal fade" id="explab" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Experiencia Laboral</h4>
            </div>
            <div class="modal-body">                   
                <form role="form" action="<?php echo base_url('agregar-hv-exp-lab'); ?>" method="POST">
                    <div class="row">
                        <div class="col-md-8 col-xs-12">
                            <div class="form-group">
                                <label for="hv_explab_cargo">
                                    Cargo
                                </label>
                                <input type="text" class="form-control" required name="hv_explab_cargo" value="<?php echo set_value('hv_explab_cargo'); ?>" id="hv_explab_cargo" placeholder="Cargo">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label for="hv_explab_dep">
                                    Departamento
                                </label>
                                <select class="form-control" required name="hv_explab_dep" id="hv_explab_dep">
                                    <?php
                                    foreach ($Departamentos as $dep) {
                                        echo '<option value="' . $dep->DepartamentoID . '">' . $dep->Nombre . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="hv_explab_jefe_name">
                                    Jefe Inmediato
                                </label>
                                <input type="text" class="form-control" required name="hv_explab_jefe_name" value="<?php echo set_value('hv_explab_jefe_name'); ?>" id="hv_explab_jefe_name" placeholder="Jefe Inmediato">
                            </div> 
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="hv_explab_date_ini">
                                    Fecha de Inicio
                                </label>
                                <input type="text" class="form-control datepicker" required name="hv_explab_date_ini" value="<?php echo set_value('hv_explab_date_ini'); ?>" id="hv_explab_date_ini">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="hv_explab_date_fin">
                                    Fecha de Finalizacion
                                </label>
                                <input type="text" class="form-control datepicker" required name="hv_explab_date_fin" value="<?php echo set_value('hv_explab_date_fin'); ?>" id="hv_explab_date_fin">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="hv_explab_tel">
                                    Telefono
                                </label>
                                <input type="text" class="form-control" required name="hv_explab_tel" value="<?php echo set_value('hv_explab_tel'); ?>" id="hv_explab_tel" placeholder="Telefono">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Agregar
                    </button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-o" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>   
<!--Fin Modal Experiencia Laboral-->
<!--Modal Estudios Complementarios-->
<div class="modal fade" id="estcomp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Estudios Complementarios</h4>
            </div>
            <div class="modal-body">                 
                <form role="form" action="<?php echo base_url('agregar-hv-est-ext'); ?>" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="hv_ex_titulo">
                                    Estudio Complementario
                                </label>
                                <input type="text" class="form-control" required name="hv_ex_titulo" value="<?php echo set_value('hv_ex_titulo'); ?>" id="hv_ex_titulo" placeholder="Estudio Complementario">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="hv_ex_inst">
                                    Nombre de la Institucion Certificadora
                                </label>
                                <input type="text" class="form-control" required name="hv_ex_inst" value="<?php echo set_value('hv_ex_inst'); ?>" id="hv_ex_inst" placeholder="Nombre de la Institucion Certificadora">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="hv_ex_date_cert">
                                    Fecha de Certificacion
                                </label>
                                <input type="text" class="form-control datepicker" required name="hv_ex_date_cert" value="<?php echo set_value('hv_ex_date_cert'); ?>" id="hv_ex_date_cert">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Agregar
                    </button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-o" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
<!--Fin Modal Estudios Complementarios-->

<!--Modal Nivel de Formacion-->
<div class="modal fade" id="nivform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Nivel de Formacion</h4>
            </div>
            <div class="modal-body">                 
                <form role="form" action="<?php echo base_url('agregar-hv-est-comp'); ?>" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="hv_ec_titulo">
                                    Nivel de Formacion
                                </label>
                                <select class="form-control" required name="hv_ec_titulo" id="hv_ec_titulo">
                                    <?php
                                    foreach ($EstComp as $estcom) {
                                        echo '<option value="' . $estcom->CatalogoPerfilID . '">' . $estcom->Descripcion . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="hv_ec_inst">
                                    Nombre de la Institucion Certificadora
                                </label>
                                <input type="text" class="form-control" required name="hv_ec_inst" value="<?php echo set_value('hv_ec_inst'); ?>" id="hv_ec_inst" placeholder="Nombre de la Institucion Certificadora">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="hv_ec_date_cert">
                                    Fecha de Certificacion
                                </label>
                                <input type="text" class="form-control datepicker" required name="hv_ec_date_cert" value="<?php echo set_value('hv_ec_date_cert'); ?>" id="hv_ec_date_cert">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Agregar
                    </button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-o" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
<!--Fin Modal Nivel de Formacion-->
<!--Modal Referencias Personales-->
<div class="modal fade" id="refper" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Referencias Personales</h4>
            </div>
            <div class="modal-body">                 
                <form role="form" action="<?php echo base_url('agregar-hv-ref-pers'); ?>" method="POST">
                    <input type="hidden" value="1" name="type_ref_pers">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="hv_ref_per_name">
                                    Nombre Completo
                                </label>
                                <input type="text" class="form-control" required name="hv_ref_per_name" value="<?php echo set_value('hv_ref_per_name'); ?>" id="hv_ref_per_name" placeholder="Nombre Completo">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="hv_ref_per_prof">
                                    Profesion/Ocupacion
                                </label>
                                <input type="text" class="form-control" required name="hv_ref_per_prof" value="<?php echo set_value('hv_ref_per_prof'); ?>" id="hv_ref_per_prof" placeholder="Profesion/Ocupacion">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="hv_ref_per_tel">
                                    Telefono
                                </label>
                                <input type="text" class="form-control" required name="hv_ref_per_tel" value="<?php echo set_value('hv_ref_per_tel'); ?>" id="hv_ref_per_tel" placeholder="Telefono">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Agregar
                    </button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-o" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
<!--Fin Modal Referencias Personales-->
<!--Modal Referencias Profesionales-->
<div class="modal fade" id="refprof" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Referencias Profesionales</h4>
            </div>
            <div class="modal-body">              
                <form role="form" action="<?php echo base_url('agregar-hv-ref-pers'); ?>" method="POST">
                    <input type="hidden" value="2" name="type_ref_pers">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="hv_ref_per_name">
                                    Nombre Completo
                                </label>
                                <input type="text" class="form-control" required name="hv_ref_per_name" value="<?php echo set_value('hv_ref_per_name'); ?>" id="hv_ref_per_name" placeholder="Nombre Completo">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="hv_ref_per_prof">
                                    Profesion/Ocupacion
                                </label>
                                <input type="text" class="form-control" required name="hv_ref_per_prof" value="<?php echo set_value('hv_ref_per_prof'); ?>" id="hv_ref_per_prof" placeholder="Profesion/Ocupacion">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="hv_ref_per_tel">
                                    Telefono
                                </label>
                                <input type="text" class="form-control" required name="hv_ref_per_tel" value="<?php echo set_value('hv_ref_per_tel'); ?>" id="hv_ref_per_tel" placeholder="Telefono">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Agregar
                    </button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-o" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
<!--Fin Modal Referencias Profesionales-->
<!--Modal Descripcion de Habilidades-->
<div class="modal fade" id="deschab" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Referencias Profesionales</h4>
            </div>
            <div class="modal-body">              
                <form role="form" action="<?php echo base_url('agregar-hv-desc-hab'); ?>" method="POST">
                    <div class="form-group">
                        <label for="hv_desc_hab">
                            Habilidad
                        </label>
                        <select class="form-control" required name="hv_desc_hab" id="hv_desc_hab">
                            <?php
                            foreach ($DescHab as $dh) {
                                echo '<option value="' . $dh->CatalogoPerfilID . '">' . $dh->Descripcion . '</option>';                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Agregar
                    </button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-o" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
<!--Fin Modal Descripcion de Habilidades-->