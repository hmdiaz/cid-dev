<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$tiempo = $explab->Experiencia;
if ($tiempo > 11) {
    $a = $tiempo / 12;
    $anios = (int) $a;
    $m =  $tiempo - ($anios * 12);
    $meses = (int)$m;
} else {
    $anios = 0;
    $meses = $tiempo;
}
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nueva Actividad</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-actividad'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $explab->HvExpLabID; ?>" name="id_explab">
                    <input type="hidden" value="<?php echo $expgralID; ?>" name="id_expgral">                    
                    <div class="Row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="hv_ec_titulo">
                                    Actividad Desarrollada
                                </label>
                                <select class="form-control" name="hv_act_titulo" id="hv_ec_titulo">
                                    <?php
                                    foreach ($ExpLab as $exp) {
                                        if ($expgralID == $exp->CatalogoPerfilID) {
                                            $selected = 'selected';
                                        } else {
                                            $selected = '';
                                        }
                                        echo '<option value="' . $exp->CatalogoPerfilID . '" ' . $selected . '>' . $exp->Descripcion . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">                           
                            <h5>Experiencia</h5>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Años
                                </label>
                                <input id="hv_act_exp_a" type="text" value="<?php echo $anios; ?>" name="hv_act_exp_a" touchspin data-verticalbuttons="true" data-verticalupclass="ti-angle-up" data-verticaldownclass="ti-angle-down">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Meses
                                </label>                                   
                                <input id="hv_act_exp_m" type="text" value="<?php echo $meses; ?>" name="hv_act_exp_m" touchspin data-verticalbuttons="true" data-verticalupclass="ti-angle-up" data-verticaldownclass="ti-angle-down">
                            </div>
                        </div>
                        <div class="col-md-12">  
                            <button type="submit" class="btn btn-o btn-primary">
                                Actualizar
                            </button>
                            <a href="<?php echo base_url('hoja-de-vida'); ?>" class="btn btn-o btn-danger pull-right">
                                Cancelar
                            </a>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>