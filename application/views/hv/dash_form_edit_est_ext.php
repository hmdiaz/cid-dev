<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Estudio Complementario</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-hv-est-ext'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $estcomp->HvEstExtraID; ?>" name="id">
                    <div class="form-group">
                        <div class="form-group">
                                <label for="hv_ex_titulo">
                                    Estudio Complementario
                                </label>
                                <input type="text" class="form-control" required name="hv_ex_titulo" value="<?php if(set_value('hv_ex_titulo')){ echo set_value('hv_ex_titulo'); }else{ echo $estcomp->EstudioExtra;}?>" id="hv_ex_titulo" placeholder="Estudio Complementario">
                            </div>
                        <div class="form-group">
                            <label for="hv_ec_inst">
                                Nombre de la Institucion Certificadora
                            </label>
                            <input type="text" class="form-control" required name="hv_ex_inst" value="<?php if(set_value('hv_ex_inst')){ echo set_value('hv_ex_inst');}else{ echo $estcomp->NombreInstitucion;} ?>" id="hv_ex_inst" placeholder="Nombre de la Institucion Certificadora">
                        </div>
                        <div class="form-group">
                            <label for="hv_ex_date_cert">
                                Fecha de Certificacion
                            </label>
                            <input type="text" class="form-control datepicker" required name="hv_ex_date_cert" value="<?php if(set_value('hv_ex_date_cert')){ echo set_value('hv_ex_date_cert'); }else{ echo $estcomp->FechaCertificacion;}?>" id="hv_ex_date_cert">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Actualizar
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>