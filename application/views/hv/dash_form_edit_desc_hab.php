<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Habilidad</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-hv-desc-habilidad'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $habilidad->HvHab_id; ?>" name="id">
                    <div class="form-group">
                        <label for="hv_desc_hab">
                            Titulo Obtenido
                        </label>
                        <select class="form-control" required name="hv_desc_hab" id="hv_ia_titulo">
                            <?php
                            foreach ($DescHab as $dh) {
                                if ($habilidad->CatalogoPerfilID == $dh->CatalogoPerfilID) {
                                    $selected = 'selected';
                                } else {
                                    $selected = '';
                                }
                                echo '<option value="' . $dh->CatalogoPerfilID . '" ' . $selected . '>' . $dh->Descripcion . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Actualizar
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>