<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Experiencia Laboral</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-hv-exp-lab'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $explab->HvExpLabID; ?>" name="id">
                        <div class="row">
                            <div class="col-md-8 col-xs-12">
                                <div class="form-group">
                                    <label for="hv_explab_cargo">
                                        Cargo
                                    </label>
                                    <input type="text" class="form-control" required name="hv_explab_cargo" value="<?php if(set_value('hv_explab_cargo')){ echo set_value('hv_explab_cargo'); }else{ echo $explab->NombreCargo; }?>" id="hv_explab_cargo" placeholder="Cargo">
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label for="hv_explab_dep">
                                        Departamento
                                    </label>
                                    <select class="form-control" required name="hv_explab_dep" id="hv_explab_dep">
                                        <?php
                                        foreach ($Departamentos as $dep) {
                                            if($explab->DepartamentoID == $dep->DepartamentoID){
                                                $selected = 'selected';
                                            }else{
                                                $selected = '';
                                            }
                                            echo '<option value="' . $dep->DepartamentoID . '" '.$selected.'>' . $dep->Nombre . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="hv_explab_jefe_name">
                                        Jefe Inmediato
                                    </label>
                                    <input type="text" class="form-control" required name="hv_explab_jefe_name" value="<?php if(set_value('hv_explab_jefe_name')){ echo set_value('hv_explab_jefe_name'); }else{ echo $explab->NombreJefeInmediato; } ?>" id="hv_explab_jefe_name" placeholder="Jefe Inmediato">
                                </div>  
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label for="hv_explab_date_ini">
                                        Fecha de Inicio
                                    </label>
                                    <input type="text" class="form-control datepicker" required name="hv_explab_date_ini" value="<?php if(set_value('hv_explab_date_ini')){ echo set_value('hv_explab_date_ini'); }else{ echo $explab->FechaInicio; } ?>" id="hv_explab_date_ini">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label for="hv_explab_date_fin">
                                        Fecha de Finalizacion
                                    </label>
                                    <input type="text" class="form-control datepicker" required name="hv_explab_date_fin" value="<?php if(set_value('hv_explab_date_fin')){ echo set_value('hv_explab_date_fin'); }else{ echo $explab->FechaFinalizacion; } ?>" id="hv_explab_date_fin">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label for="hv_explab_cargo">
                                        Telefono
                                    </label>
                                    <input type="text" class="form-control" required name="hv_explab_tel" value="<?php if(set_value('hv_explab_tel')){ echo set_value('hv_explab_tel'); }else{ echo $explab->Telefono; }?>" id="hv_explab_tel" placeholder="Telefono">
                                </div>
                            </div>                            
                        </div>
                        <button type="submit" class="btn btn-o btn-primary">
                            Agregar
                        </button>
                    </form>
            </div>
        </div>
    </div>
</div>