<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Informacion Academica</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-hv-inf-academic'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $infoacad->HvInfAcadID; ?>" name="id">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="hv_ia_titulo">
                                    Titulo Obtenido
                                </label>
                                <select class="form-control" required name="hv_ia_titulo" id="hv_ia_titulo">
                                    <?php
                                    foreach ($infoAcad as $infAca) {
                                        if($infoacad->CatalogoPerfilID == $infAca->CatalogoPerfilID){
                                            $selected = 'selected';
                                        }else{
                                            $selected = '';
                                        }
                                        echo '<option value="' . $infAca->CatalogoPerfilID . '" '.$selected.'>' . $infAca->Descripcion . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="hv_name_inst">
                                    Nombre de la Institucion Educativa
                                </label>
                                <input type="text" class="form-control" required name="hv_name_inst" value="<?php if(set_value('hv_name_inst')){ echo set_value('hv_name_inst');}else{ echo $infoacad->NombreInstitucion; } ?>" id="hv_name_inst" placeholder="Nombre de la Institucion Educativa">
                            </div>                            
                            <div class="form-group">
                                <label for="hv_date_grade">
                                    Fecha de Grado
                                </label>
                                <input type="text" class="form-control datepicker" required name="hv_date_grade" value="<?php if(set_value('hv_date_grade')){ echo set_value('hv_date_grade'); }else{ echo $infoacad->FechaGrado; }?>" id="hv_date_grade">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-o btn-primary">
                            Actualizar
                        </button>
                    </form>
            </div>
        </div>
    </div>
</div>