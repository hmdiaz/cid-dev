<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nueva Actividad</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('agregar-actividad'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $id_explab; ?>" name="id_explab">                    
                    <div class="Row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="hv_ec_titulo">
                                    Actividad Desarrollada
                                </label>
                                <select class="form-control" name="hv_act_titulo" id="hv_ec_titulo">
                                    <?php
                                    foreach ($ExpLab as $exp) {
                                        echo '<option value="' . $exp->CatalogoPerfilID . '">' . $exp->Descripcion . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">                            
                            <h5>Experiencia</h5>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Años
                                </label>
                                <input id="hv_act_exp_a" type="text" value="0" name="hv_act_exp_a" touchspin data-verticalbuttons="true" data-verticalupclass="ti-angle-up" data-verticaldownclass="ti-angle-down">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Meses
                                </label>
                                <input id="hv_act_exp_m" type="text" value="0" name="hv_act_exp_m" touchspin data-verticalbuttons="true" data-verticalupclass="ti-angle-up" data-verticaldownclass="ti-angle-down">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-o btn-primary">
                                Agregar
                            </button>
                            <a href="<?php echo base_url('hoja-de-vida'); ?>" class="btn btn-o btn-danger pull-right">
                                Cancelar
                            </a>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>