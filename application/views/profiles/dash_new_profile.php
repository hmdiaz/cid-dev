<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <!--
            <div class="panel-heading">
                <h5 class="panel-title"></h5>
            </div>
            -->
            <div class="panel-body">
                <div class="alert alert-block alert-danger" id="errorContainer">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="alert-heading margin-bottom-10"><i class="ti-close"></i> Atenci&oacuten a los siguientes errores!</h4>
                </div>
                <h1 align="center">
                    <small>Información del Perfil</small>
                </h1>
                <form id="frmPerfil" name="frmPerfil" role="form" action="<?php echo base_url('crear-perfil'); ?>" method="POST">
                    <input type="hidden" id="module" name="module" value="new">
                    <input type="hidden" id="baseUrl" name="baseUrl" value="<?php echo base_url(); ?>">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="tipo_proyecto">
                                Tipo de Proyecto
                            </label>
                            <select id="tipo_proyecto" name="tipo_proyecto" class="form-control">
                                <option value="">Seleccione uno...</option>
                                <?php
                                foreach ($tipos_proyectos as $item) {
                                    echo '<option value="' . $item->TipoProyectoID . '">' . $item->Descripcion . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="area">
                                Proyecto / Área / Licitación
                            </label>
                            <select id="area" name="area" class="form-control">
                                <option value="">Seleccione uno...</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="mision_cargo">
                                Misión del Cargo
                            </label>
                            <input type="text" class="form-control" value="<?php echo set_value('mision_cargo'); ?>" name="mision_cargo" id="mision_cargo" placeholder="Misión del Cargo">
                        </div>
                        <div class="col-md-4">
                            <label for="titulo_cargo">
                                Título del Cargo
                            </label>
                            <input type="text" class="form-control" value="<?php echo set_value('titulo_cargo'); ?>" name="titulo_cargo" id="titulo_cargo" placeholder="Título del Cargo">
                        </div>
                        <div class="col-md-4">
                            <label for="titulo_cargo">
                                Número de Vacantes
                            </label>
                            <input type="text" class="form-control" value="<?php echo set_value('numero_ocupantes'); ?>" name="numero_ocupantes" id="numero_ocupantes" placeholder="Número de Vacantes">
                        </div>
                    </div>
                    <h1 align="center">
                        <small>Componentes de Competencias</small>
                    </h1>
                    <div class="tabbable">
                        <ul id="myTab2" class="nav nav-tabs nav-justified">
                            <li class="active">
                                <a href="#myTab2_example1" data-toggle="tab">
                                    Nivel Educativo
                                </a>
                            </li>
                            <li>
                                <a href="#myTab2_example2" data-toggle="tab">
                                    Nivel Formación
                                </a>
                            </li>
                            <li>
                                <a href="#myTab2_example3" data-toggle="tab">
                                    Experiencia
                                </a>
                            </li>
                            <li>
                                <a href="#myTab2_example4" data-toggle="tab">
                                    Habilidades
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="myTab2_example1">
                                <div class="form-group">
                                    <select name="grado" id="grado" class="form-control">
                                        <option>Seleccione uno...</option>
                                        <?php
                                        foreach ($grados as $item) {
                                            echo '<option value="' . $item->grado_id . '">'. $item->grado_nombre . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="nivel_educativo" id="nivel_educativo" class="form-control">
                                        <option>Seleccione uno...</option>
                                        <?php
                                        /*foreach ($nivel_educativo as $item) {
                                            echo '<option value="' . $item->CatalogoPerfilID . '">' . $item->Descripcion . '</option>';
                                        }*/
                                        ?>
                                    </select>
                                </div>

                                <!---->
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control only-numeric" value="<?php echo set_value('pesoNE'); ?>" name="pesoNE" id="pesoNE" placeholder="Puntaje">
                                    </div>
                                    <a class="btn btn-primary" href="#" id="addItemNivelEducativo">
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </a>
                                </div>
                                <br />
                                <a class='btn btn-red del' id="delRowsNivelEducativo" href='javascript:;'>
                                    <i class='fa fa-trash-o'></i> Quitar Seleccionados
                                </a>
                                <br />
                                <br />
                                <table class="table table-bordered table-hover table-full-width" id="tbl_nivel_educativo">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Grado Id</th>
                                            <th>Catalogo Perfil ID</th>
                                            <th>Grado</th>
                                            <th>Item</th>
                                            <th>Puntaje</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="myTab2_example2">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <select name="nivel_formacion" id="nivel_formacion" class="form-control" style="width: 100%;">
                                            <?php
                                            foreach ($nivel_formacion as $item) {
                                                echo '<option value="' . $item->CatalogoPerfilID . '">' . $item->Descripcion . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <!---->
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <input type="text" class="form-control only-numeric" name="pesoNF" id="pesoNF" placeholder="Puntaje">
                                        </div>
                                        <a class="btn btn-primary" href="#" id="addItemNivelFormacion">
                                            <i class="glyphicon glyphicon-plus"></i>
                                        </a>
                                    </div>
                                    <br />
                                    <a class='btn btn-red del' id="delRowsNivelFormacion" href='javascript:;'>
                                        <i class='fa fa-trash-o'></i> Quitar Seleccionados
                                    </a>
                                    <br />
                                    <br />
                                    <table class="table table-bordered table-hover table-full-width" id="tbl_nivel_formacion">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Item</th>
                                                <th>Puntaje</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="myTab2_example3">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <select name="experiencia_general" id="experiencia_general" class="form-control" style="width: 100%;">
                                            <?php
                                            foreach ($experiencia_general as $item) {
                                                echo '<option value="' . $item->CatalogoPerfilID . '">' . $item->Descripcion . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <!---->
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <input type="text" class="form-control only-numeric" name="pesoEG" id="pesoEG" placeholder="Puntaje">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control only-numeric" name="anios_eg" id="anios_eg" placeholder="Años">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control only-numeric" name="meses_eg" id="meses_eg" placeholder="Meses">
                                        </div>
                                        <a class="btn btn-primary" href="#" id="addItemExperienciaGeneral">
                                            <i class="glyphicon glyphicon-plus"></i>
                                        </a>
                                    </div>
                                    <br />
                                    <a class='btn btn-red del' id="delRowsExperienciaGeneral" href='javascript:;'>
                                        <i class='fa fa-trash-o'></i> Quitar Seleccionados
                                    </a>
                                    <br />
                                    <br />
                                    <table class="table table-bordered table-hover table-full-width" id="tbl_exp_general">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Item</th>
                                                <th>Puntaje</th>
                                                <th>EG</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="myTab2_example4">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <select name="habilidades" id="habilidades" class="form-control" style="width: 100%;">
                                            <?php
                                            foreach ($habilidades as $item) {
                                                echo '<option value="' . $item->CatalogoPerfilID . '">' . $item->Descripcion . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <!---->
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <input type="text" class="form-control only-numeric" name="pesoHAB" id="pesoHAB" placeholder="Puntaje">
                                        </div>
                                        <a class="btn btn-primary" href="#" id="addItemHabilidad">
                                            <i class="glyphicon glyphicon-plus"></i>
                                        </a>
                                    </div>
                                    <br />
                                    <a class='btn btn-red del' id="delRowsHabilidad" href='javascript:;'>
                                        <i class='fa fa-trash-o'></i> Quitar Seleccionados
                                    </a>
                                    <br />
                                    <br />
                                    <table class="table table-bordered table-hover table-full-width" id="tbl_habilidades">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Item</th>
                                                <th>Puntaje</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h3>
                                <small>Responsabilidades</small>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="tipo_responsabilidad">
                                        Tipo de Responsabilidad
                                    </label>
                                    <select name="tipo_responsabilidad" id="tipo_responsabilidad" class="form-control">
                                        <option value>Seleccione uno...</option>
                                        <?php
                                        foreach ($tipos_responsabilidad as $item) {
                                            echo '<option value="' . $item->TipoResponsabilidadID . '">' . $item->Descripcion . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="responsabilidades">
                                        Responsabilidad
                                    </label>
                                    <select name="responsabilidades" id="responsabilidades" class="form-control">
                                        <option value>Seleccione uno...</option>
                                    </select>
                                </div>
                                <a class="btn btn-primary" href="#" id="addItemResponsabilidad">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </a>

                                <a class='btn btn-red del' id="delRowsResponsabilidadesCargo" href='javascript:;'>
                                    <i class='fa fa-trash-o'></i> Quitar Seleccionados
                                </a>
                                <br />
                                <br />
                                <table class="table table-bordered table-hover table-full-width" id="tbl_responsabilidadesCargo">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Tipo Responsabilidad</th>
                                            <th>Item</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <br />
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="departamento">
                                        Departamento
                                    </label>
                                    <select id="departamento_new_profile" name="departamento_new_profile" class="form-control"onchange="getMunicipios('municipios', this.options[this.selectedIndex].value, '#departamento', '#municipio')">
                                        <option value="">Seleccione uno...</option>
                                        <?php
                                        foreach ($departamentos as $departamento) {
                                            echo '<option value="' . $departamento->DepartamentoID . '">' . $departamento->Nombre . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="municipio">
                                        Municipio
                                    </label>
                                    <select id="municipio_new_profile" name="municipio_new_profile" class="form-control">
                                        <option value="">Seleccione uno...</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control only-numeric" name="vacantes_x_municipio" id="vacantes_x_municipio" placeholder="Vacantes">
                                </div>
                                <a class="btn btn-primary" href="#" id="addItemMunicipio_new_profile">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </a>
                            </div>
                            <a class='btn btn-red del' id="delRowsMunicipiosNewProfile" href='javascript:;'>
                                <i class='fa fa-trash-o'></i> Quitar Seleccionado
                            </a>
                            <br />                                
                            <br />
                            <table class="table table-bordered table-hover table-full-width" id="tbl_municipios_new_profile">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Departamentos</th>
                                        <th>Municipios</th>
                                        <th>Vacantes</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                            <br />
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Crear Perfil
                    </button>
                    <a href="" class="btn btn-o btn-danger pull-right" onclick="window.history.back()">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>