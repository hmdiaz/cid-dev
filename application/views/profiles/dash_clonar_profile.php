<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <!--
            <div class="panel-heading">
                <h5 class="panel-title"></h5>
            </div>
            -->
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <h1 align="center">
                    <small>Clonar Perfil</small>
                </h1>
                <form role="form" action="<?php echo base_url('duplicar-perfil'); ?>" method="POST">
                    <input type="hidden" id="baseUrl" name="baseUrl" value="<?php echo base_url(); ?>">
                    <input type="hidden" name="id_perfil" value="<?php echo $id_profile; ?>">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="tipo_proyecto">
                                Tipo de Proyecto
                            </label>
                            <select id="tipo_proyecto" name="tipo_proyecto" class="form-control">
                                <option value="">Seleccione uno...</option>
                                <?php
                                foreach ($tipos_proyectos as $item) {
                                    echo '<option value="' . $item->TipoProyectoID . '">' . $item->Descripcion . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="area">
                                Proyecto / Área / Licitación
                            </label>
                            <select id="area" name="area" class="form-control">
                                <option value="">Seleccione uno...</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Clonar Perfil
                    </button>
                    <a href="" class="btn btn-o btn-danger pull-right" onclick="window.history.back()">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>