<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class=" col-lg-12 col-md-12">
        <div class="panel panel-white">
            <!--
            <div class="panel-heading">
                <h5 class="panel-title"></h5>
            </div>
            -->
            <div class="panel-body">
                <div class="alert alert-block alert-danger" id="errorContainer">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="alert-heading margin-bottom-10"><i class="ti-close"></i> Atenci&oacuten a los siguientes errores!</h4>
                </div>
                <h1 align="center">
                    <small>Editar Perfil</small>
                </h1>
                <form id="frmPerfil" name="frmPerfil" role="form" action="<?php echo base_url('update-perfil'); ?>" method="POST">
                    <div class="row">
                        <input type="hidden" id="baseUrl" name="baseUrl" value="<?php echo base_url(); ?>">
                        <input type="hidden" id="PerfilID" name="PerfilID" value="<?php echo $profileData->PerfilID; ?>">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tipo_proyecto">
                                    Tipo de Proyecto
                                </label>
                                <select id="tipo_proyecto" name="tipo_proyecto" class="form-control">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    foreach ($tipos_proyectos as $item){
                                        if($item->TipoProyectoID == $tipo_proyecto_id)
                                        {
                                            echo '<option value="'.$item->TipoProyectoID.'" selected>'.$item->Descripcion.'</option>';
                                        }
                                        else
                                        {
                                            echo '<option value="'.$item->TipoProyectoID.'">'.$item->Descripcion.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="area">
                                    Área / Proyecto
                                </label>
                                <select id="area_edit" name="area" class="form-control">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    foreach ($Projects as $Project){
                                        if($profileData->ProyectoID == $Project->ProyectoID)
                                        {
                                            echo '<option value="'.$Project->ProyectoID.'" selected>'.$Project->NombreProyecto.'</option>';
                                        }
                                        else
                                        {
                                            echo '<option value="'.$Project->ProyectoID.'">'.$Project->NombreProyecto.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="mision_cargo">
                                    Misión del Cargo
                                </label>
                                <input type="text" class="form-control" name="mision_cargo" id="mision_cargo" value="<?php if(set_value('mision_cargo')){ echo set_value('mision_cargo'); }else{ echo $profileData->MisionCargo; } ?>" placeholder="Misión del Cargo">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="titulo_cargo">
                                    Título del Cargo
                                </label>
                                <input type="text" class="form-control" name="titulo_cargo" id="titulo_cargo" value="<?php if(set_value('titulo_cargo')){ echo set_value('titulo_cargo'); }else{ echo $profileData->TituloCargo; } ?>" placeholder="Título del Cargo">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="titulo_cargo">
                                    Número de Vacantes
                                </label>
                                <input type="text" class="form-control" name="numero_ocupantes" id="numero_ocupantes" value="<?php if(set_value('numero_ocupantes')){ echo set_value('numero_ocupantes'); }else{ echo $profileData->NumeroOcupantes; } ?>" placeholder="Número de Vacantes">
                            </div>
                        </div>
                    </div>
                        <h1 align="center">
                            <small>Componentes de Competencias</small>
                        </h1>
                        <div class="tabbable">
                            <ul id="myTab2" class="nav nav-tabs nav-justified">
                                <li class="active">
                                    <a href="#myTab2_example1" data-toggle="tab">
                                        Nivel Educativo
                                    </a>
                                </li>
                                <li>
                                    <a href="#myTab2_example2" data-toggle="tab">
                                        Nivel Formación
                                    </a>
                                </li>
                                <li>
                                    <a href="#myTab2_example3" data-toggle="tab">
                                        Experiencia
                                    </a>
                                </li>
                                <li>
                                    <a href="#myTab2_example4" data-toggle="tab">
                                        Habilidades
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="myTab2_example1">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <select name="grado" id="grado" class="form-control">
                                                <option>Seleccione uno...</option>
                                                <?php
                                                foreach ($grados as $item) {
                                                    echo '<option value="' . $item->grado_id . '">'. $item->grado_nombre . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select name="nivel_educativo" id="nivel_educativo" class="form-control">
                                                <option>Seleccione uno...</option>
                                                <?php
                                                /*foreach ($nivel_educativo as $item){
                                                    echo '<option value="'.$item->CatalogoPerfilID.'">'.$item->Descripcion.'</option>';
                                                }*/
                                                ?>
                                            </select>
                                        </div>
                                        <!---->
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <input type="text" class="form-control only-numeric" name="pesoNE" id="pesoNE" placeholder="Puntaje">
                                            </div>
                                            <a class="btn btn-primary" href="#" id="addItemNivelEducativoEditProfile">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </a>
                                        </div>
                                        <br />
                                        <br />
                                        <h1 align="center">
                                            <small>Items agregados</small>
                                        </h1>
                                        <br />
                                        <a class='btn btn-red del' id="delRowsNivelEducativoEditProfile" href='javascript:;'>
                                            <i class='fa fa-trash-o'></i> Quitar Seleccionados
                                        </a>

                                        <a class='btn btn-green del' id="editRowsNivelEducativo" href='javascript:;'>
                                            <i class='fa fa-edit'></i> Editar Competencia
                                        </a>

                                        <br />
                                        <br />
                                        <table class="table table-bordered table-hover table-full-width" id="tbl_nivel_educativo">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Grado Id</th>
                                                <th>Catalogo Perfil ID</th>
                                                <th>Grado</th>
                                                <th>Item</th>
                                                <th>Puntaje</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach($items as $item)
                                            {
                                                if($item->TipoCatalogoID == 1)
                                                {
                                                    $grado = $this->Grados_model->get($item->tipo_grado_id);
                                                    ?>
                                                    <tr>
                                                        <input type='hidden' id='hdItemsId' name='hdItemsId[]' value='<?php echo $item->CatalogoPerfilID ?>' />
                                                        <td>
                                                            <?php echo $item->ComponenteCompetenciaID ?>
                                                        </td>                                                        
                                                        <td>
                                                            <?php echo $grado->grado_id ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $item->CatalogoPerfilID ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $grado->grado_nombre ?>
                                                        </td>

                                                        <td><?php echo $item->Descripcion ?></td>
                                                        <td><?php echo $item->Peso ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="myTab2_example2">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <select name="nivel_formacion" id="nivel_formacion" class="form-control" style="width: 100%;">
                                                <?php
                                                foreach ($nivel_formacion as $item){
                                                    echo '<option value="'.$item->CatalogoPerfilID.'">'.$item->Descripcion.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <!---->
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <input type="text" class="form-control only-numeric" name="pesoNF" id="pesoNF" placeholder="Puntaje">
                                            </div>
                                            <a class="btn btn-primary" href="#" id="addItemNivelFormacionEditProfile">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </a>
                                        </div>

                                        <br />
                                        <br />
                                        <h1 align="center">
                                            <small>Items agregados</small>
                                        </h1>
                                        <br />
                                        <a class='btn btn-red del' id="delRowsNivelFormacionEditProfile" href='javascript:;'>
                                            <i class='fa fa-trash-o'></i> Quitar Seleccionados
                                        </a>
                                        <br />
                                        <br />
                                        <table class="table table-bordered table-hover table-full-width" id="tbl_nivel_formacion">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Item</th>
                                                <th>Puntaje</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach($items as $item)
                                            {
                                                if($item->TipoCatalogoID == 2)
                                                {
                                                    ?>
                                                    <tr>
                                                        <input type='hidden' id='hdItemsId' name='hdItemsId[]' value='<?php echo $item->CatalogoPerfilID ?>' />
                                                        <td><?php echo $item->ComponenteCompetenciaID ?></td>
                                                        <td><?php echo $item->Descripcion ?></td>
                                                        <td><?php echo $item->Peso ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="myTab2_example3">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <select name="experiencia_general" id="experiencia_general" class="form-control" style="width: 100%;">
                                                <?php
                                                foreach ($experiencia_general as $item){
                                                    echo '<option value="'.$item->CatalogoPerfilID.'">'.$item->Descripcion.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <!---->
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <input type="text" class="form-control only-numeric" name="pesoEG" id="pesoEG" placeholder="Puntaje">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control only-numeric" name="anios_eg" id="anios_eg" placeholder="Años">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control only-numeric" name="meses_eg" id="meses_eg" placeholder="Meses">
                                            </div>
                                            <a class="btn btn-primary" href="#" id="addItemExperienciaGeneralEditProfile">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </a>
                                        </div>
                                        <br />
                                        <br />
                                        <h1 align="center">
                                            <small>Items agregados</small>
                                        </h1>
                                        <br />
                                        <a class='btn btn-red del' id="delRowsExperienciaGeneralEditProfile" href='javascript:;'>
                                            <i class='fa fa-trash-o'></i> Quitar Seleccionados
                                        </a>
                                        <br />
                                        <br />
                                        <table class="table table-bordered table-hover table-full-width" id="tbl_exp_general">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Item</th>
                                                <th>Puntaje</th>
                                                <th>EG</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach($items as $item)
                                            {
                                                if($item->TipoCatalogoID == 3)
                                                {
                                                    ?>
                                                    <tr>
                                                        <input type='hidden' id='hdItemsId' name='hdItemsId[]' value='<?php echo $item->CatalogoPerfilID ?>' />
                                                        <td><?php echo $item->ComponenteCompetenciaID ?></td>
                                                        <td><?php echo $item->Descripcion ?></td>
                                                        <td><?php echo $item->Peso ?></td>
                                                        <td><?php
                                                            $exp = $item->TiempoExperiencia;
                                                            $meses = 0;
                                                            $anios = 0;

                                                            while($exp > 12)
                                                            {
                                                                $exp = $exp - 12;
                                                                $anios++;
                                                            }
                                                            $meses = $exp;

                                                            echo $anios . ' años ' . $meses . ' meses';
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="myTab2_example4">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <select name="habilidades" id="habilidades" class="form-control" style="width: 100%;">
                                                <?php
                                                foreach ($habilidades as $item){
                                                    echo '<option value="'.$item->CatalogoPerfilID.'">'.$item->Descripcion.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <!---->
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <input type="text" class="form-control only-numeric" name="pesoHAB" id="pesoHAB" placeholder="Puntaje">
                                            </div>
                                            <a class="btn btn-primary" href="#" id="addItemHabilidadEditProfile">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </a>
                                        </div>
                                        <br />
                                        <br />
                                        <h1 align="center">
                                            <small>Items agregados</small>
                                        </h1>
                                        <br />
                                        <a class='btn btn-red del' id="delRowsHabilidadEditProfile" href='javascript:;'>
                                            <i class='fa fa-trash-o'></i> Quitar Seleccionados
                                        </a>
                                        <br />
                                        <br />
                                        <table class="table table-bordered table-hover table-full-width" id="tbl_habilidades">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Item</th>
                                                <th>Puntaje</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach($items as $item)
                                            {
                                                if($item->TipoCatalogoID == 4)
                                                {
                                                    ?>
                                                    <tr>
                                                        <input type='hidden' id='hdItemsId' name='hdItemsId[]' value='<?php echo $item->CatalogoPerfilID ?>' />
                                                        <td><?php echo $item->ComponenteCompetenciaID ?></td>
                                                        <td><?php echo $item->Descripcion ?></td>
                                                        <td><?php echo $item->Peso ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h3>
                                    <small>Responsabilidades</small>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="tipo_responsabilidad">
                                            Tipo de Responsabilidad
                                        </label>
                                        <select name="tipo_responsabilidad" id="tipo_responsabilidad" class="form-control">
                                            <option value>Seleccione uno...</option>
                                            <?php
                                            foreach ($tipos_responsabilidad as $item) {
                                                echo '<option value="' . $item->TipoResponsabilidadID . '">' . $item->Descripcion . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="responsabilidades">
                                            Responsabilidad
                                        </label>
                                        <select name="responsabilidades" id="responsabilidades" class="form-control">
                                            <option value>Seleccione uno...</option>
                                        </select>
                                    </div>
                                    <a class="btn btn-primary" href="#" id="addItemResponsabilidadCargoEditProfile">
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </a>
                                    <a class='btn btn-red del' id="delRowsResponsabilidadesCargoEditProfile" href='javascript:;'>
                                        <i class='fa fa-trash-o'></i> Quitar Seleccionados
                                    </a>
                                    <br />
                                    <br />
                                    <table class="table table-bordered table-hover table-full-width" id="tbl_responsabilidadesCargo">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Tipo Responsabilidad</th>
                                            <th>Item</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            foreach($responsabilidades_x_perfil as $responsabilidad)
                                            {
                                                ?>
                                                <tr>
                                                    <input type='hidden' id='hdItemsResponsabilidadId' name='hdItemsResponsabilidadId[]' value='<?php echo $responsabilidad->ResponsabilidadID ?>' />
                                                    <td><?php echo $responsabilidad->ResponsabilidadPerfilID ?></td>
                                                    <td><?php echo $responsabilidad->TipoRespDesc ?></td>
                                                    <td><?php echo $responsabilidad->Descripcion ?></td>
                                                </tr>
                                                <?php
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-white">
                            <div class="panel-body">
                                <br />
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label for="departamento">
                                            Departamento
                                        </label>
                                        <select id="departamento_edit_profile" name="departamento_edit_profile" class="form-control">
                                            <option value="">Seleccione uno...</option>
                                            <?php
                                            foreach ($departamentos as $departamento){
                                                echo '<option value="'.$departamento->DepartamentoID.'">'.$departamento->Nombre.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="municipio">
                                            Municipio
                                        </label>
                                        <select id="municipio_edit_profile" name="municipio_edit_profile" class="form-control">
                                            <option value="">Seleccione uno...</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control only-numeric" name="vacantes_x_municipio" id="vacantes_x_municipio" placeholder="Vacantes">
                                    </div>
                                    <a class="btn btn-primary" href="#" id="addItemMunicipio_edit_profile">
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </a>
                                </div>
                                <br />
                                <a class='btn btn-red del' id="delRowsMunicipiosEditProfile" href='javascript:;'>
                                    <i class='fa fa-trash-o'></i> Quitar Seleccionado
                                </a>
                                <br />
                                <br />
                                <table class="table table-bordered table-hover table-full-width" id="tbl_municipios_edit_profile">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Departamentos</th>
                                        <th>Municipios</th>
                                        <th>Vacantes</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        foreach($munsPerfil as $muns)
                                        {
                                            ?>
                                                <tr>
                                                    <input type='hidden' id='hdItemsMunicipiosId' name='hdItemsMunicipiosId[]' value='<?php echo $muns->MunicipioID ?>' />
                                                    <input type='hidden' id='vacantesxmun' name='vacantesxmun[]' value='<?php echo $muns->municipios_x_perfil_vacantes ?>' />
                                                    <td><?php echo $muns->MunicipiosPerfilID ?></td>
                                                    <td><?php echo $muns->NombreDepartamento ?></td>
                                                    <td><?php echo $muns->NombreMunicipio ?></td>
                                                    <td><?php echo $muns->municipios_x_perfil_vacantes ?></td>
                                                </tr>
                                            <?php
                                        }
                                    ?>
                                    </tbody>
                                </table>
                                <br />
                            </div>
                        </div>
                        <button type="submit" class="btn btn-o btn-primary">
                            Actualizar Perfil
                        </button>
                        <a href="" class="btn btn-o btn-danger pull-right" onclick="window.history.back()">
                            Cancelar
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>