<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 $aprobaciones = $this->notificaciones->aprobacionesPendientesCurrentUser();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html>
<!-- Template Name: Clip-Two - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <head>
        <title><?php echo $title; ?></title>
        <!-- start: META -->
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: GOOGLE FONTS -->
        <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        <!-- end: GOOGLE FONTS -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/fontawesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/themify-icons/themify-icons.min.css">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/animate.css/animate.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/switchery/switchery.min.css" rel="stylesheet" media="screen">

        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/sweetalert/sweet-alert.css" rel="stylesheet" media="screen">
        <!-- end: MAIN CSS -->
        <!-- start: CLIP-TWO CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/styles.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/plugins.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/themes/theme-1.css" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css'); ?>/DashBoardCustomStyle.css">
        <!-- end: CLIP-TWO CSS -->
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/select2/select2.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/DataTables/css/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    </head>
    <!-- end: HEAD -->
    <body>
        <div id="app">
            <!-- sidebar -->
            <div class="sidebar app-aside" id="sidebar">
                <div class="sidebar-container perfect-scrollbar">
                    <nav>
                        <!-- start: MAIN NAVIGATION MENU -->
                        <div class="navbar-title">
                            <span>Main Navigation</span>
                        </div>
                        <ul class="main-navigation-menu">
                            <li <?php
                            if ($active == "home") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="<?php echo base_url('dashboard'); ?>">
                                    <div class="item-content">
                                        <div class="item-media">
                                            <i class="ti-home"></i>
                                        </div>
                                        <div class="item-inner">
                                            <span class="title"> Dashboard </span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li <?php
                            if ($active == "users") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="javascript:void(0)">
                                    <div class="item-content">
                                        <div class="item-media">
                                            <i class="ti-user"></i>
                                        </div>
                                        <div class="item-inner">
                                            <span class="title">Usuarios </span><i class="icon-arrow"></i>
                                        </div>
                                    </div>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo base_url('lista-usuarios'); ?>">
                                            <span class="title"> Listar </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('nuevo-usuario'); ?>">
                                            <span class="title"> Crear </span>
                                        </a>
                                    </li>                                    
                                </ul>
                            </li>
                            <li <?php
                            if ($active == "roles") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="javascript:void(0)">
                                    <div class="item-content">
                                        <div class="item-media">
                                            <i class="ti-lock"></i>
                                        </div>
                                        <div class="item-inner">
                                            <span class="title">Roles </span><i class="icon-arrow"></i>
                                        </div>
                                    </div>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo base_url('lista-roles'); ?>">
                                            <span class="title"> Listar </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('nuevo-rol'); ?>">
                                            <span class="title"> Crear </span>
                                        </a>
                                    </li>                                    
                                </ul>
                            </li> 
                            <li <?php
                            if ($active == "modules") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="javascript:void(0)">
                                    <div class="item-content">
                                        <div class="item-media">
                                            <i class="ti-lock"></i>
                                        </div>
                                        <div class="item-inner">
                                            <span class="title">Modulos </span><i class="icon-arrow"></i>
                                        </div>
                                    </div>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo base_url('lista-modulos'); ?>">
                                            <span class="title"> Listar </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('nuevo-modulo'); ?>">
                                            <span class="title"> Crear </span>
                                        </a>
                                    </li>                                    
                                </ul>
                            </li>
                            <li <?php
                            if ($active == "projects") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="javascript:void(0)">
                                    <div class="item-content">
                                        <div class="item-media">
                                            <i class="ti-bag"></i>
                                        </div>
                                        <div class="item-inner">
                                            <span class="title">Proyectos </span><i class="icon-arrow"></i>
                                        </div>
                                    </div>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo base_url('lista-proyectos'); ?>">
                                            <span class="title"> Listar Proyectos</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('lista-perfiles'); ?>">
                                            <span class="title"> Listar Perfiles</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('listar-requisiciones'); ?>">
                                            <span class="title"> Listar Requisiciones</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('nuevo-proyecto'); ?>">
                                            <span class="title"> Crear  Proyecto</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('nuevo-perfil'); ?>">
                                            <span class="title"> Crear  Perfil</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li <?php
                            if ($active == "convocatorias") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="javascript:void(0)">
                                    <div class="item-content">
                                        <div class="item-media">
                                            <i class="ti-briefcase"></i>
                                        </div>
                                        <div class="item-inner">
                                            <span class="title">Convocatorias </span><i class="icon-arrow"></i>
                                        </div>
                                    </div>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo base_url('listar-convocatorias'); ?>">
                                            <span class="title"> Listar Convocatorias</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li <?php
                            if ($active == "pruebas") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="javascript:void(0)">
                                    <div class="item-content">
                                        <div class="item-media">
                                            <i class="ti-file"></i>
                                        </div>
                                        <div class="item-inner">
                                            <span class="title">Pruebas </span><i class="icon-arrow"></i>
                                        </div>
                                    </div>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo base_url('lista-pruebas'); ?>">
                                            <span class="title"> Listar </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('nueva-prueba'); ?>">
                                            <span class="title"> Crear </span>
                                        </a>
                                    </li>                                    
                                </ul>
                            </li>
                            <li <?php
                            if ($active == "preguntas") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="javascript:void(0)">
                                    <div class="item-content">
                                        <div class="item-media">
                                            <i class="ti-file"></i>
                                        </div>
                                        <div class="item-inner">
                                            <span class="title">Preguntas </span><i class="icon-arrow"></i>
                                        </div>
                                    </div>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo base_url('lista-preguntas'); ?>">
                                            <span class="title"> Listar </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('nueva-pregunta'); ?>">
                                            <span class="title"> Crear </span>
                                        </a>
                                    </li>                                    
                                </ul>
                            </li>
                            <li <?php
                            if ($active == "cpntes") {
                                echo 'class="active"';
                            }
                            ?>>
                                <a href="javascript:void(0)">
                                    <div class="item-content">
                                        <div class="item-media">
                                            <i class="ti-settings"></i>
                                        </div>
                                        <div class="item-inner">
                                            <span class="title"> Configuraciones </span><i class="icon-arrow"></i>
                                        </div>
                                    </div>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="javascript:;">
                                            <span>Componentes de Compe.</span> <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="javascript:;">
                                                    <span>Nivel Educativo</span> <i class="icon-arrow"></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="<?php echo base_url('lista-cpnte-edu'); ?>">
                                                            Listar
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="<?php echo base_url('nuevo-cpnte-edu'); ?>">
                                                            Crear
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span>Nivel de Formacion</span> <i class="icon-arrow"></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="<?php echo base_url('lista-cpnte-form'); ?>">
                                                            Listar
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="<?php echo base_url('nuevo-cpnte-form'); ?>">
                                                            Crear
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span>Experiencia</span> <i class="icon-arrow"></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="<?php echo base_url('lista-cpnte-exp'); ?>">
                                                            Listar
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="<?php echo base_url('nuevo-cpnte-exp'); ?>">
                                                            Crear
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span>Descripcion de Habilidades</span> <i class="icon-arrow"></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="<?php echo base_url('lista-cpnte-hab'); ?>">
                                                            Listar
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="<?php echo base_url('nuevo-cpnte-hab'); ?>">
                                                            Crear
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span>Categorias de Componentes</span> <i class="icon-arrow"></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="<?php echo base_url('lista-cate-cpnte'); ?>">
                                                            Listar
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="<?php echo base_url('nuevo-cate-cpnte'); ?>">
                                                            Crear
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span>Grados de Escolaridad</span> <i class="icon-arrow"></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="<?php echo base_url('lista-grados'); ?>">
                                                            Listar
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="<?php echo base_url('nuevo-grado'); ?>">
                                                            Crear
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span>Socios Cooperantes</span> <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo base_url('lista-socios'); ?>">
                                                    <span class="title"> Listar</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url('nuevo-socio'); ?>">
                                                    <span class="title"> Crear</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span>Tipos de Socios</span> <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo base_url('lista-tipo-socio'); ?>">
                                                    <span class="title"> Listar</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url('nuevo-tipo-socio'); ?>">
                                                    <span class="title"> Crear</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span>Poblaciones</span> <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo base_url('lista-poblaciones'); ?>">
                                                    <span class="title"> Listar</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url('nueva-poblacion'); ?>">
                                                    <span class="title"> Crear</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span>Cargos</span> <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo base_url('lista-cargos'); ?>">
                                                    <span class="title"> Listar</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url('nuevo-cargo'); ?>">
                                                    <span class="title"> Crear</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span>Áreas</span> <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo base_url('lista-areas'); ?>">
                                                    <span class="title"> Listar</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url('nueva-area'); ?>">
                                                    <span class="title"> Crear</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span>Flujo de Aprobaciones</span> <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo base_url('lista-flujo-aprobaciones'); ?>">
                                                    <span class="title"> Listar</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url('nuevo-flujo-aprobacion'); ?>">
                                                    <span class="title"> Crear</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span>Responsabilidades</span> <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo base_url('lista-responsabilidades'); ?>">
                                                    <span class="title"> Listar</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url('nueva-responsabilidad'); ?>">
                                                    <span class="title"> Crear</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span>Tipos de Responsabilidad</span> <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo base_url('lista-tipo-responsabilidad'); ?>">
                                                    <span class="title"> Listar</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url('nuevo-tipo-responsabilidad'); ?>">
                                                    <span class="title"> Crear</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span>Ubicación</span> <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo base_url('lista-ubicaciones'); ?>">
                                                    <span class="title"> Listar</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url('nueva-ubicacion'); ?>">
                                                    <span class="title"> Crear</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span>Etapas de Convocatoria</span> <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo base_url('lista-etapas'); ?>">
                                                    <span class="title"> Listar</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url('nueva-etapa'); ?>">
                                                    <span class="title"> Crear</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- end: MAIN NAVIGATION MENU -->
                    </nav>
                </div>
            </div>
            <!-- / sidebar -->
            <div class="app-content">
                <!-- start: TOP NAVBAR -->
                <header class="navbar navbar-default navbar-static-top">
                    <!-- start: NAVBAR HEADER -->
                    <div class="navbar-header">
                        <a href="#" class="sidebar-mobile-toggler pull-left hidden-md hidden-lg" class="btn btn-navbar sidebar-toggle" data-toggle-class="app-slide-off" data-toggle-target="#app" data-toggle-click-outside="#sidebar">
                            <i class="ti-align-justify"></i>
                        </a>
                        <a class="navbar-brand" href="<?php echo base_url('dashboard'); ?>">
                            <img src="<?php echo base_url('assets'); ?>/images/logo.png" alt="cid"/>
                        </a>
                        <a href="#" class="sidebar-toggler pull-right visible-md visible-lg" data-toggle-class="app-sidebar-closed" data-toggle-target="#app">
                            <i class="ti-align-justify"></i>
                        </a>
                        <a class="pull-right menu-toggler visible-xs-block" id="menu-toggler" data-toggle="collapse" href=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <i class="ti-view-grid"></i>
                        </a>
                    </div>
                    <!-- end: NAVBAR HEADER -->
                    <!-- start: NAVBAR COLLAPSE -->
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-right">
                            <!-- start: MESSAGES DROPDOWN -->
                            <li class="dropdown">
                                <a href class="dropdown-toggle" data-toggle="dropdown">
                                    <?php
                                    if(count($aprobaciones) > 0)
                                    {
                                        ?>
                                            <span class="dot-badge partition-red"></span>
                                        <?php
                                    }
                                    ?>
                                    <i class="ti-check-box"></i> <span>Aprobaciones</span>
                                </a>
                                <ul class="dropdown-menu dropdown-light dropdown-messages dropdown-large">
                                    <li>
                                        <span class="dropdown-header"> Aprobaciones pendientes</span>
                                    </li>
                                    <li>
                                        <div class="drop-down-wrapper ps-container">
                                            <ul>
                                                <?php
                                                    foreach($aprobaciones as $item)
                                                    {
                                                ?>
                                                <li class="unread">
                                                    <a href="<?php echo base_url('ver-requisicion') . '/' . $item->RequisicionID . '/' . $item->ProyectoID ?>" class="unread">
                                                        <div class="clearfix">
                                                            <div class="thread-content">
                                                                <span class="author"><?php echo $item->NombreCargo; ?></span>
                                                                <span class="preview">
                                                                    <?php echo substr($item->Observaciones, 0, 100); ?>...
                                                                </span>
                                                                <span class="time"> <?php echo $item->FechaSolicitud; ?></span>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <?php
                                                    }
                                                ?>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="view-all">
                                        <a href="<?php echo base_url('lista-aprobaciones'); ?>">
                                            Ver todos
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- end: MESSAGES DROPDOWN -->
                            <!-- start: USER OPTIONS DROPDOWN -->
                            <li class="dropdown current-user">
                                <a href class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url('assets/images/users_img'); ?>/<?php echo $_SESSION['img_profile']; ?>" alt="<?php echo $_SESSION['username']; ?>"> <span class="username"><?php echo $_SESSION['username']; ?> <i class="ti-angle-down"></i></i></span>
                                </a>
                                <ul class="dropdown-menu dropdown-dark">                                   
                                    <li>
                                        <a href="<?php echo base_url('perfil'); ?>">
                                            Perfil
                                        </a>
                                        <a href="<?php echo base_url('signout'); ?>">
                                            Cerrar Sesión
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- end: USER OPTIONS DROPDOWN -->
                        </ul>
                        <!-- start: MENU TOGGLER FOR MOBILE DEVICES -->
                        <div class="close-handle visible-xs-block menu-toggler" data-toggle="collapse" href=".navbar-collapse">
                            <div class="arrow-left"></div>
                            <div class="arrow-right"></div>
                        </div>
                        <!-- end: MENU TOGGLER FOR MOBILE DEVICES -->
                    </div>
                    <!-- end: NAVBAR COLLAPSE -->
                </header>
                <!-- end: TOP NAVBAR -->
                <div class="main-content" >
                    <div class="wrap-content container" id="container">
                        <!-- start: PAGE TITLE -->
                        <section id="page-title">
                            <div class="row">
                                <div class="col-sm-8">
                                    <h1 class="mainTitle"><?php echo $maintitle; ?></h1>
                                    <span class="mainDescription"><?php echo $maindescription; ?></span>
                                </div>
                            </div>
                        </section>                        
                        <!-- end: PAGE TITLE -->
                        <?php
                        if ($this->session->flashdata('message_error')) {
                            ?>
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>
                            </div>
                            <?php
                        }
                        if ($this->session->flashdata('message_success')) {
                            ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                            </div>
                            <?php
                        }
                        ?>

                        <!-- start: YOUR CONTENT HERE -->
                        <?php $this->load->view($content_view); ?>
                        <!-- end: YOUR CONTENT HERE -->
                    </div>
                    <div id="loadMessage" style="display:none;"> 
                        <span>
                            <img src="<?php echo base_url('assets/images'); ?>/loader.gif" /> Estamos procesando, espere un momento...
                        </span>
                    </div> 
                </div>
            </div>
            <!-- start: FOOTER -->
            <footer>
                <div class="footer-inner">
                    <div class="pull-left">
                        &copy; <span class="current-year"></span><span class="text-bold text-uppercase">Corporación Infancia y Desarrollo</span>.  <br class="hidden-md hidden-sm hidden-lg"><span>Todos los derechos reservados</span>
                    </div>
                    <div class="pull-right">
                        <span class="go-top"><i class="ti-angle-up"></i></span>
                    </div>
                </div>
            </footer>
            <!-- end: FOOTER -->            
        </div>
        <!-- start: MAIN JAVASCRIPTS -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/modernizr/modernizr.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery-cookie/jquery.cookie.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/switchery/switchery.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/selectFx/classie.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/selectFx/selectFx.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/select2/select2.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/DataTables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/sweetalert/sweet-alert.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/maskedinput/jquery.maskedinput.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery-block-ui/jquery.blockUI.js"></script>

        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: CLIP-TWO JAVASCRIPTS -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/assets/js/main.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/DashBoardCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/SocioCooperanteCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/PoblacionesCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/ProyectoCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/PerfilesCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/RequisicionCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/AprobacionesCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/ConvocatoriaCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/LicitacionCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/ResponsabilidadCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/EtapaCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/AreaCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/NuevaConvocatoria.js"></script>
        <!-- start: JavaScript Event Handlers for this page -->
        <script>
            jQuery(document).ready(function () {
                Main.init();
            });
        </script>
        <!-- end: JavaScript Event Handlers for this page -->
        <!-- end: CLIP-TWO JAVASCRIPTS -->
    </body>
</html>
