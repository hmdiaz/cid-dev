<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Selección</h5>
            </div>
            <div class="panel-body">                
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nombre del Postulante</th>
                            <th>Calificacion</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>                
                        <?php
                        $todo_listo = FALSE;
                        $band_presento = FALSE;
                        foreach ($postulantes as $postulante) {
                            $calificacion = 0;
                            $status = '<span class="label label-danger">Aun no Presenta</span>';
                            foreach ($matchs as $match) {
                                $band_presento = FALSE;
                                $pruebaseleccionresult_id = $match['pruebaseleccionresult_id'];
                                $db_match = $this->PruebasSeleccionResult_model->get($pruebaseleccionresult_id);
                                $archivo = $db_match->pruebaseleccionresult_file;
                                $exist = FALSE;
                                if ($db_match->pruebaseleccionresult_match != "" || $db_match->pruebaseleccionresult_match != NULL) {
                                    $exist = TRUE;
                                    $match_db = json_decode($db_match->pruebaseleccionresult_match);
                                }
                                if ($match['user_id'] == $postulante->UsuarioID) {
                                    if ($exist == TRUE) {
                                        $band_presento = TRUE;
                                        $calificacion = $match_db->puntos;
                                        if ($match_db->preg_abiertas === TRUE) {
                                            $status = '<span class="label label-warning">Falta Calificacion Manual</span>';
                                            $todo_listo = FALSE;
                                        } else {
                                            $todo_listo = TRUE;
                                            $status = '<span class="label label-success">Calificado</span>';
                                        }
                                    } else {
                                        $band_presento = TRUE;
                                        $calificacion = $match['puntos'];
                                        if ($match['preg_abiertas'] === TRUE) {
                                            $status = '<span class="label label-warning">Falta Calificacion Manual</span>';
                                            $todo_listo = FALSE;
                                        } else {
                                            $todo_listo = TRUE;
                                            $status = '<span class="label label-success">Calificado</span>';
                                        }
                                    }
                                }
                            }
                            echo '<tr>';
                            echo '<td>' . $postulante->Nombres . ' ' . $postulante->Apellidos . '</td>';
                            echo '<td>' . $calificacion . '</td>';
                            echo '<td>' . $status . '</td>';
                            echo '<td>';
                            if ($band_presento && $todo_listo == FALSE) {
                                echo '<a href="' . base_url('calificar-manual/' . $pruebaseleccionresult_id) . '" class="btn btn-xs btn-o btn-primary">Calificar</a>';
                            }

                            if ($etapa->EtapaID == 2 && $todo_listo && isset($archivo)) {
                                echo '<a target="_blank" href="' . base_url() . 'assets/files/' . $archivo . '" class="btn btn-xs btn-o btn-primary">Archivo</a>';
                            }

                            echo '<a href="' . base_url('hoja-de-vida-pdf') . '/' . $postulante->UsuarioID . '" class="btn btn-success btn-xs btn-o" target="_blank">Hoja de Vida</a></td>';
                            echo'</td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
                <a href="<?php echo base_url('etapas/' . $ConvocatoriaID); ?>" class="btn btn-o btn-danger pull-right">
                    Regresar
                </a>
            </div>
        </div>
    </div>
</div>