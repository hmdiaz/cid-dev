<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Prueba</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('agregar-prueba'); ?>" method="POST">
                    <?php foreach ($preguntas as $pregunta): ?>
                        <?php if ($pregunta->pregunta_tipo == "text"): ?>
                            <div class="form-group">
                                <label for="quest_<?php echo $pregunta->pregunta_id; ?>">
                                    <?php echo $pregunta->pregunta_title; ?>
                                </label>
                                <input type="text" class="form-control" name="quest_<?php echo $pregunta->pregunta_id; ?>"  id="quest_<?php echo $pregunta->pregunta_id; ?>" placeholder="<?php echo $pregunta->pregunta_title; ?>">
                            </div>
                        <?php endif; ?>
                        <?php if ($pregunta->pregunta_tipo == "textarea"): ?>
                            <div class="form-group">
                                <label for="quest_<?php echo $pregunta->pregunta_id; ?>">
                                    <?php echo $pregunta->pregunta_title; ?>
                                </label>
                                <textarea class="form-control" name="quest_<?php echo $pregunta->pregunta_id; ?>"  id="quest_<?php echo $pregunta->pregunta_id; ?>" placeholder="<?php echo $pregunta->pregunta_title; ?>"></textarea>
                            </div>
                        <?php endif; ?>
                        <?php if ($pregunta->pregunta_tipo == "select"): ?>
                            <div class="form-group">
                                <label for="quest_<?php echo $pregunta->pregunta_id; ?>">
                                    <?php echo $pregunta->pregunta_title; ?>
                                </label>
                                <select id="quest_<?php echo $pregunta->pregunta_id; ?>" name="quest_<?php echo $pregunta->pregunta_id; ?>" class="form-control">
                                    <?php
                                    $data = explode(",", $pregunta->pregunta_options);
                                    foreach ($data as $key => $value) {
                                        echo '<option value="' . $value . '">' . $value . '</option>';
                                    }
                                    ?>

                                </select>
                            </div>
                        <?php endif; ?>
                        <?php if ($pregunta->pregunta_tipo == "checkbox"): ?>
                            <div class="form-group">
                                <p>
                                    <?php echo $pregunta->pregunta_title; ?>
                                </p>
                                <?php
                                $data = explode(",", $pregunta->pregunta_options);
                                $i = 0;
                                foreach ($data as $key => $value): ?>
                                <div class="checkbox clip-check check-primary checkbox-inline">
                                    <input type="checkbox" value="<?php echo $value; ?>" id="quest_<?php echo $pregunta->pregunta_id; ?><?php echo $i; ?>" name="quest_<?php echo $pregunta->pregunta_id; ?>[]">
                                    <label for="quest_<?php echo $pregunta->pregunta_id; ?><?php echo $i; ?>">
                                        <?php echo $value; ?>
                                    </label>
                                </div>
                                <?php $i++; endforeach; ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($pregunta->pregunta_tipo == "radio"): ?>
                            <div class="form-group">
                                <p>
                                    <?php echo $pregunta->pregunta_title; ?>
                                </p>
                                <?php
                                $data = explode(",", $pregunta->pregunta_options);
                                $i = 0;
                                foreach ($data as $key => $value): ?>
                                <div class="radio clip-radio radio-danger radio-inline">
                                    <input type="radio" value="<?php echo $value; ?>" id="quest_<?php echo $pregunta->pregunta_id; ?><?php echo $i; ?>" name="quest_<?php echo $pregunta->pregunta_id; ?>">
                                    <label for="quest_<?php echo $pregunta->pregunta_id; ?><?php echo $i; ?>">
                                        <?php echo $value; ?>
                                    </label>
                                </div>
                                <?php $i++; endforeach; ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <button type="submit" class="btn btn-o btn-primary">
                        Crear Prueba
                    </button>
                    <a href="<?php echo base_url('lista-pruebas'); ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>