<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$fecha = date('U');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Prueba</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>     
                <p>Duración de la Prueba : <span><?php echo $seleccion->pruebasconfig_tiempo; ?> Minutos</span></p>
                <p>Fecha de Inicio : <span><?php echo $seleccion->FechaInicio; ?> </span></p>
                <p>Fecha de Fin : <span><?php echo $seleccion->FechaFin; ?></span></p>
                <?php 
                    if(isset($exist)){
                        echo '<p>Tu prueba comenzo: '.$exist->pruebaseleccionresult_start.'</p>';
                        echo '<p>Tu prueba Finaliza: '.$exist->pruebaseleccionresult_end.'</p>';
                    }
                ?>
                <form role="form" action="<?php echo base_url('comenzar-prueba'); ?>"  method="POST">
                    <input type="hidden" value="<?php echo $seleccion->pruebasseleccion_id; ?>" name="id">
                    <?php if($fecha >= strtotime($seleccion->FechaInicio) && $fecha <= strtotime($seleccion->FechaFin)): ?>
                    <button type="submit" class="btn btn-o btn-primary">
                        Comenzar Prueba
                    </button>
                    <?php endif; ?>
                    <a href="<?php echo base_url('dashboard-aspirante'); ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
//    var minutes = document.getElementById("time").value;
//    var mili = minutes * 60*1000;
//    //En 1 segundo se crea un nuevo elemento
//    setTimeout(function(){
//	
//    },mili);
</script>