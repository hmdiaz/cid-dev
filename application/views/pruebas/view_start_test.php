<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Prueba</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                if($prueba->prueba_observaciones != ""){
                    echo '<h4>Observaciones</h4>';
                    $observaciones = explode(';', $prueba->prueba_observaciones);
                    foreach ($observaciones as $o){
                        echo '<p>'.$o.'</p>';
                    }
                    echo '<br>';
                }
                ?>  
                
                <form role="form" action="<?php echo base_url('enviar-prueba'); ?>"  method="POST">
                    <input type="hidden" value="<?php echo $exist->pruebaseleccionresult_id; ?>" name="id">
                    <input type="hidden" id="time" value="<?php echo $seleccion->pruebasconfig_tiempo; ?>">
                    <?php foreach ($preguntas as $pregunta): ?>
                        <?php if ($pregunta->pregunta_tipo == "text"): ?>
                            <div class="form-group">
                                <label for="<?php echo $pregunta->pregunta_id; ?>">
                                    <?php echo $pregunta->pregunta_title; ?>
                                </label>
                                <input type="text" class="form-control" name="<?php echo $pregunta->pregunta_id; ?>"  id="<?php echo $pregunta->pregunta_id; ?>" placeholder="<?php echo $pregunta->pregunta_title; ?>">
                            </div>
                        <?php endif; ?>
                        <?php if ($pregunta->pregunta_tipo == "textarea"): ?>
                            <div class="form-group">
                                <label for="<?php echo $pregunta->pregunta_id; ?>">
                                    <?php echo $pregunta->pregunta_title; ?>
                                </label>
                                <textarea class="form-control" name="<?php echo $pregunta->pregunta_id; ?>"  id="<?php echo $pregunta->pregunta_id; ?>" placeholder="<?php echo $pregunta->pregunta_title; ?>"></textarea>
                            </div>
                        <?php endif; ?>
                        <?php if ($pregunta->pregunta_tipo == "select"): ?>
                            <div class="form-group">
                                <label for="<?php echo $pregunta->pregunta_id; ?>">
                                    <?php echo $pregunta->pregunta_title; ?>
                                </label>
                                <select id="<?php echo $pregunta->pregunta_id; ?>" name="<?php echo $pregunta->pregunta_id; ?>" class="form-control">
                                    <?php
                                    $data = explode(",", $pregunta->pregunta_options);
                                    foreach ($data as $key => $value) {
                                        echo '<option value="' . $value . '">' . $value . '</option>';
                                    }
                                    ?>

                                </select>
                            </div>
                        <?php endif; ?>
                        <?php if ($pregunta->pregunta_tipo == "checkbox"): ?>
                            <div class="form-group">
                                <p>
                                    <?php echo $pregunta->pregunta_title; ?>
                                </p>
                                <?php
                                $data = explode(",", $pregunta->pregunta_options);
                                $i = 0;
                                foreach ($data as $key => $value):
                                    ?>
                                    <div class="checkbox clip-check check-primary checkbox-inline">
                                        <input type="checkbox" value="<?php echo $value; ?>" id="<?php echo $pregunta->pregunta_id; ?><?php echo $i; ?>" name="<?php echo $pregunta->pregunta_id; ?>[]">
                                        <label for="<?php echo $pregunta->pregunta_id; ?><?php echo $i; ?>">
                                            <?php echo $value; ?>
                                        </label>
                                    </div>
                                <?php $i++;
                            endforeach; ?>
                            </div>
                            <?php endif; ?>
                                <?php if ($pregunta->pregunta_tipo == "radio"): ?>
                            <div class="form-group">
                                <p>
                                <?php echo $pregunta->pregunta_title; ?>
                                </p>
                                <?php
                                $data = explode(",", $pregunta->pregunta_options);
                                $i = 0;
                                foreach ($data as $key => $value):
                                    ?>
                                    <div class="radio clip-radio radio-danger radio-inline">
                                        <input type="radio" value="<?php echo $value; ?>" id="<?php echo $pregunta->pregunta_id; ?><?php echo $i; ?>" name="<?php echo $pregunta->pregunta_id; ?>">
                                        <label for="<?php echo $pregunta->pregunta_id; ?><?php echo $i; ?>">
                                    <?php echo $value; ?>
                                        </label>
                                    </div>
                                <?php $i++;
                            endforeach; ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <button type="submit" class="btn btn-o btn-primary">
                        Enviar Prueba
                    </button>
                    <a href="<?php echo base_url('dashboard-aspirante'); ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
//    var minutes = document.getElementById("time").value;
//    var mili = minutes * 60*1000;
//    //En 1 segundo se crea un nuevo elemento
//    setTimeout(function(){
//	
//    },mili);
</script>