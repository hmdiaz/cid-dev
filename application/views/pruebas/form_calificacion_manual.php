<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Calificacion</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('guardar-calificacion'); ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $preguntas->pruebaseleccionresult_id ?>" name="pruebaseleccionresult_id" >
                    <?php
                    $preguntas_match = json_decode($preguntas->match_resp);
                    foreach ($preguntas_match as $pm):
                        if ($pm->valor_pregunta == 0):
                            ?>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <label for="test_name">
                                            <strong>Pregunta:</strong> <?php echo $pm->pregunta; ?><br>
                                            <strong>Respuesta Sistema:</strong> <?php echo $pm->respuesta_pregunta; ?><br>
                                            <strong>Valor Pregunta Sistema:</strong> <?php echo $pm->valor_pregunta; ?><br>
                                            <strong>Respuesta:</strong>  <?php echo $pm->respuesta_user; ?>
                                        </label>
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="<?php echo $pm->pregunta_id; ?>" id="test_name" placeholder="Calificacion">
                                    </div>
                                </div>                            
                            </div>
                            <?php
                        else:
                            ?>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <label for="test_name">
                                            <strong>Pregunta:</strong> <?php echo $pm->pregunta; ?><br>
                                            <strong>Pregunta Sistema:</strong> <?php echo $pm->respuesta_pregunta; ?><br>
                                            <strong>Valor Pregunta Sistema:</strong> <?php echo $pm->valor_pregunta; ?><br>
                                            <strong>Respuesta:</strong>  <?php if(is_array($pm->respuesta_user)){ foreach($pm->respuesta_user as $ru){echo $ru.',';}}else{ echo $pm->respuesta_user;}  ?>
                                        </label>
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" disabled id="test_name" placeholder="Calificacion">
                                    </div>
                                </div>                            
                            </div>
                        <?php
                        endif;
                    endforeach;
                    
                    if($etapa->EtapaID == 2):
                    ?>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-2">
                                <label for="userfile">
                                    Subir Archivo
                                </label>
                            </div>
                            <div class="col-lg-6">
                                <input type="file" class="form-control" name="userfile">
                            </div>
                        </div>
                    </div>                    
                    <?php 
                    endif;
                    ?>
                    <button type="submit" class="btn btn-o btn-primary">
                        Guardar Calificacion
                    </button>
                    <a href="<?php echo base_url('resultados').'/'.$etapa->EtapaConvocatoriaID; ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>