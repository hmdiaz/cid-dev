<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nueva Prueba</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('agregar-prueba'); ?>" method="POST">
                    <div class="form-group">
                        <label for="test_name">
                            Nombre de la prueba
                        </label>
                        <input type="text" class="form-control" name="test_name" value="<?php echo set_value('test_name'); ?>" id="rol_name" placeholder="Nombre la Prueba">
                    </div>
                    <div class="form-group">
                        <label for="test_categoria">
                            Categoria de la Prueba
                        </label>
                        <select id="rol_status" name="test_categoria" class="roles_select_add_rol form-control">
                            <?php 
                                foreach ($categorias as $cat){
                                    echo '<option value="'.$cat->CategoriaPerfilID.'">'.$cat->NombreCategoria.'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="test_observaciones">
                            Observaciones
                        </label>
                        <textarea class="form-control" name="test_observaciones" placeholder="Observaciones"><?php echo set_value('test_observaciones'); ?></textarea>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Crear Prueba
                    </button>
                    <a href="<?php echo base_url('lista-pruebas');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>