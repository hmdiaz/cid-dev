<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="row">
        <div class="col-md-12">        
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width" id="list_roles">
                    <thead>
                        <tr>
                            <th>Prueba</th>
                            <th>Fecha de creacion</th>
                            <th>Status</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $tests; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>