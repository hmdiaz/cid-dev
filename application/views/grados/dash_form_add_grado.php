<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-offset-2 col-lg-8 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nuevo Grado</h5>
            </div>
            <div class="panel-body">

                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('agregar-grado'); ?>" method="POST">
                    <div class="form-group">
                        <label for="grado_name">
                            Nombre de Grado
                        </label>
                        <input type="text" class="form-control" name="grado_name" id="cat_name" value="<?php echo set_value('grado_name'); ?>" placeholder="Nombre de Grado">
                    </div>
                    <div class="form-group">
                        <label for="grado_status">
                            Estatus de Grado
                        </label>
                        <select id="user_status" name="grado_status" class="grados_select_status form-control">
                            <option value="0" selected="selected">Selecciona Status</option>
                            <option value="1">Activo</option>
                            <option value="2">Inactivo</option>
                            <option value="3">Eliminado</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Crear
                    </button>
                    <a href="<?php echo base_url("lista-grados"); ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>