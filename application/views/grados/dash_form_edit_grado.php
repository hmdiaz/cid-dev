<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-offset-2 col-lg-8 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Grado</h5>
            </div>
            <div class="panel-body">

                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-grado'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $grado->grado_id; ?>" name="id">
                    <div class="form-group">
                        <label for="grado_name">
                            Nombre de Grado
                        </label>
                        <input type="text" class="form-control" name="grado_name" id="grado_name" value="<?php if(set_value('grado_name')){ echo set_value('grado_name'); }else{ echo $grado->grado_nombre; } ?>" placeholder="Nombre de Grado">
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Guardar
                    </button>
                    <a href="<?php echo base_url("lista-grados"); ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>