<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="row">
        <div class="col-md-12">        
            <div class="table-responsive" style="min-height: 200px">
                <table class="table table-striped table-bordered table-hover table-full-width" id="list_cats">
                    <thead>
                        <tr>
                            <th>Nombre Grado</th>
                            <th>Status</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $grados; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>