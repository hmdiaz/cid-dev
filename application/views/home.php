<!DOCTYPE html>
<!-- Template Name: Clip-Two - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <head>
        <title><?php echo $title; ?></title>
        <!-- start: META -->
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: GOOGLE FONTS -->
        <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        <!-- end: GOOGLE FONTS -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/fontawesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/themify-icons/themify-icons.min.css">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/animate.css/animate.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/switchery/switchery.min.css" rel="stylesheet" media="screen">

        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/sweetalert/sweet-alert.css" rel="stylesheet" media="screen">
        <!-- end: MAIN CSS -->
        <!-- start: CLIP-TWO CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/styles.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/plugins.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/themes/theme-1.css" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css'); ?>/DashBoardCustomStyle.css">
        <!-- end: CLIP-TWO CSS -->
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/select2/select2.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/DataTables/css/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    </head>
    <!-- end: HEAD -->
    <body>
        <div id="app">
            <!-- sidebar -->
            
            <!-- / sidebar -->
            <div class="app-content" >
                <!-- start: TOP NAVBAR -->
                
                <!-- end: TOP NAVBAR -->
                <div class="main-content " >
                    <div class="wrap-content container" id="container">
                        <!-- start: PAGE TITLE -->
                        <section id="page-title">
                            <div class="row">
                                <div class="col-sm-8">
                                    <h1 class="mainTitle"><?php echo $maintitle; ?></h1>
                                    <span class="mainDescription"><?php echo $maindescription; ?></span>
                                </div>
                            </div>
                        </section>                        
                        <!-- end: PAGE TITLE -->
                        <?php
                        if ($this->session->flashdata('message_error')) {
                            ?>
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>
                            </div>
                            <?php
                        }
                        if ($this->session->flashdata('message_success')) {
                            ?>
                            <div role="alert" class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                            </div>
                            <?php
                        }
                        ?>

                        <!-- start: YOUR CONTENT HERE -->
                        <?php $this->load->view($content_view); ?>
                        <!-- end: YOUR CONTENT HERE -->
                    </div>
                </div>
            </div>
            <!-- start: FOOTER -->
            <footer>
                <div class="footer-inner">
                    <div class="pull-left">
                        &copy; <span class="current-year"></span><span class="text-bold text-uppercase">Corporación Infancia y Desarrollo</span>.  <br class="hidden-md hidden-sm hidden-lg"><span>Todos los derechos reservados</span>
                    </div>
                    <div class="pull-right">
                        <span class="go-top"><i class="ti-angle-up"></i></span>
                    </div>
                </div>
            </footer>
            <!-- end: FOOTER -->            
        </div>
        <!-- start: MAIN JAVASCRIPTS -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/modernizr/modernizr.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery-cookie/jquery.cookie.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/switchery/switchery.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/selectFx/classie.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/selectFx/selectFx.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/select2/select2.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/DataTables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/sweetalert/sweet-alert.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/maskedinput/jquery.maskedinput.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery-validation/jquery.validate.min.js"></script>

        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: CLIP-TWO JAVASCRIPTS -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/assets/js/main.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/DashBoardCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/SocioCooperanteCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/PoblacionesCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/ProyectoCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/PerfilesCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/RequisicionCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/AprobacionesCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/ConvocatoriaCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/LicitacionCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/ResponsabilidadCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/EtapaCustomScripts.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/AreaCustomScripts.js"></script>
        <!-- start: JavaScript Event Handlers for this page -->
        <script>
            jQuery(document).ready(function () {
                Main.init();
            });
        </script>
        <!-- end: JavaScript Event Handlers for this page -->
        <!-- end: CLIP-TWO JAVASCRIPTS -->
    </body>
</html>
