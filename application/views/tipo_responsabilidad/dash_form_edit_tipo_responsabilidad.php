<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Tipo Responsabilidad</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('update-tipo-responsabilidad'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $tipo_responsabilidad->TipoResponsabilidadID; ?>" name="id">
                    <div class="form-group">
                        <label for="tipo_responsabilidad_name">
                            Nombre del Tipo de Responsabilidad
                        </label>
                        <input type="text" class="form-control" name="tipo_responsabilidad_name" value="<?php if(set_value('tipo_responsabilidad_name')){ echo set_value('tipo_responsabilidad_name'); }else{ echo $tipo_responsabilidad->Descripcion; } ?>" id="tipo_responsabilidad_name" placeholder="Nombre Tipo de Responsabilidad">
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Actualizar
                    </button>
                    <a href="<?php echo base_url('lista-tipo-responsabilidad');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>