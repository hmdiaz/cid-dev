<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="row">
        <div class="col-md-12">        
            <div class="table-responsive" style="min-height: 200px">
                <table class="table table-striped table-bordered table-hover table-full-width" id="list_modules">
                    <thead>
                        <tr>
                            <th>Nombre Modulo</th>
                            <th>Modulo</th>
                            <th>Submodulo</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $modules; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>