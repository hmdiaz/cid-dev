<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-offset-2 col-lg-8 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Modulo</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-modulo'); ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $module->ModuloID; ?>" name="id">
                    
                    <div class="form-group">
                        <label for="module_nick">
                            Nombre
                        </label>
                        <input type="text" class="form-control" name="module_nick" value="<?php if(set_value('module_nick')){ echo set_value('module_nick'); }else{ echo $module->Nombre; } ?>" id="module_nick" placeholder="Nombre del Modulo">
                    </div>
                    <div class="form-group">
                        <label for="module_name">
                            Modulo
                        </label>
                        <input type="text" class="form-control" name="module_name" value="<?php if(set_value('module_name')){ echo set_value('module_name'); }else{ echo $module->modulo; } ?>" id="module_name" placeholder="Modulo">
                    </div>
                    <div class="form-group">
                        <label for="module_submodule">
                            Submodulo
                        </label>
                        <input type="text" class="form-control" name="module_submodule" value="<?php if(set_value('module_submodule')){ echo set_value('module_submodule'); }else{ echo $module->submodulo; } ?>" id="module_submodule" placeholder="Submodulo">
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Guardar Cambios
                    </button>
                    <a href="<?php echo base_url('lista-modulos');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>