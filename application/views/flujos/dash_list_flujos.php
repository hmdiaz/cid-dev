<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo base_url('actualizar-flujo-aprobaciones'); ?>" method="POST">
            <div class="table-responsive" style="min-height: 200px">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Flujo</th>
                            <th>Cargo</th>
                            <th style="width: 10%" >Orden</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>                    
                        <?php
                        foreach ($flujos as $flujo) {
                            ?>
                            <tr>
                                <td><?php echo $flujo->flujo_nombre_flujo; ?></td>
                                <td><?php echo $flujo->nombreCargo; ?></td>
                                <td>
                                    <input type="text" name="flujo_<?php echo $flujo->flujo_id; ?>" class="form-control" max="3" value="<?php echo $flujo->flujo_orden; ?>">
                                </td>
                                <td>
                                    <a class='btn btn-primary btn-xs btn-o' href='<?php echo base_url() . 'editar-flujo/' . $flujo->flujo_id ?>'>
                                        <i class='fa fa-edit'></i> Editar
                                    </a>
                                    <a class='btn btn-danger btn-xs btn-o delete' href='<?php echo base_url() . 'eliminar-flujo/' . $flujo->flujo_id ?>'>
                                        <i class='fa fa-trash'></i> Eliminar
                                    </a>

                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div>
                <button type="submit" class="btn btn-primary btn-o">
                    Actualizar
                </button>
            </div>
        </form>        
    </div>
</div>