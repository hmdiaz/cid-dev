<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Flujo</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('actualizar-flujo'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $flujo->flujo_id; ?>" name="id">
                    <div class="form-group">
                        <label for="flujo_name">
                            Nombre del Flujo
                        </label>
                        <input type="text" class="form-control" name="flujo_name" value="<?php if(set_value('flujo_name')){ echo set_value('flujo_name'); }else{ echo $flujo->flujo_nombre_flujo; } ?>" id="flujo_name" placeholder="Nombre del Flujo">
                    </div>
                    <div class="form-group">
                        <label for="etapa_name">
                            Cargo del Flujo
                        </label>
                        <select class="form-control" name="flujo_cargo">
                            <?php 
                                foreach($cargos as $c){
                                    if($flujo->flujo_id_cargo === $c->cargoID){
                                        $selected = "selected";
                                    }else{
                                        $selected = "";
                                    }
                                    echo '<option value="'.$c->cargoID.'" '.$selected.'>'.$c->nombreCargo.'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Actualizar
                    </button>
                    <a href="<?php echo base_url('lista-flujo-aprobaciones');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>