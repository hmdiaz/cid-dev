<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nuevo Flujo</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('agregar-flujo'); ?>" method="POST">
                    <div class="form-group">
                        <label for="flujo_name">
                            Nombre del Flujo
                        </label>
                        <input type="text" class="form-control" required name="flujo_name" value="<?php echo set_value('flujo_name');  ?>" id="flujo_name" placeholder="Nombre del Flujo">
                    </div>
                    <div class="form-group">
                        <label for="etapa_name">
                            Cargo del Flujo
                        </label>
                        <select class="form-control" name="flujo_cargo">
                            <?php 
                                foreach($cargos as $c){
                                    echo '<option value="'.$c->cargoID.'" '.$selected.'>'.$c->nombreCargo.'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Crear
                    </button>
                    <a href="<?php echo base_url('lista-flujo-aprobaciones');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>