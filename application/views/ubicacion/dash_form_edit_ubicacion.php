<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nueva Población</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('update-ubicacion'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $ubicacion->UbicacionID; ?>" name="id">
                    <div class="form-group">
                        <label for="ubicacion_name">
                            Descripción
                        </label>
                        <input type="text" class="form-control" name="ubicacion_name" value="<?php if(set_value('ubicacion_name')){ echo set_value('ubicacion_name'); }else{ echo $ubicacion->Descripcion; } ?>" id="ubicacion_name" placeholder="Descripción">
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Actualizar
                    </button>
                    <a href="<?php echo base_url('lista-ubicaciones');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>