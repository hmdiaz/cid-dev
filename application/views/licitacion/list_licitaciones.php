<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive" style="min-height: 200px">
            <table class="table table-bordered table-hover" id="tbl_licitaciones">
                <thead>
                <tr>
                    <th>Nombre de la Licitación</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($licitaciones as $licitacion)
                {
                    ?>
                    <tr>
                        <td><?php echo $licitacion->NombreProyecto;  ?></td>
                        <td>
                            <a class='btn btn-info btn-xs btn-o convertir' href='<?php echo base_url() . 'convertir-a-proyecto/' . $licitacion->ProyectoID ?>'>
                                <i class='fa fa-refresh'></i> Convertir a Proyecto
                            </a>
                            <a class='btn btn-primary btn-xs btn-o' href='<?php echo base_url() . 'editar-licitacion/' . $licitacion->ProyectoID ?>'>
                                <i class='fa fa-edit'></i> Editar
                            </a>
                            <a class='btn btn-danger btn-xs btn-o delete' href='<?php echo base_url() . 'eliminar-licitacion/' . $licitacion->ProyectoID ?>'>
                                <i class='fa fa-trash'></i> Eliminar
                            </a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div>
            <?php //echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
