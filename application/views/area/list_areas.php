<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive" style="min-height: 200px">
            <br />
            <a href="<?php echo base_url('nueva-area'); ?>" class="btn btn-o btn-default">
                Nueva Área
            </a>
            <br />
            <br />
            <table class="table table-bordered table-hover" id="tbl_areas">
                <thead>
                <tr>
                    <th>Nombre del Área</th>
                    <th>Aprueba Requisiciones</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($areas as $area)
                {
                    ?>
                    <tr>
                        <td><?php echo $area->NombreProyecto;  ?></td>
                        <td align="center"><?php
                            if($area->ApruebaRequisicion == 1)
                            {
                            ?>
                                <span class="fa fa-check"></span>
                            <?php
                            }
                            else
                            {
                            ?>
                                <span class="fa fa-close" style="color: red"></span>
                            <?php
                            }
                            ?>
                        </td>
                        <td>
                            <a class='btn btn-primary btn-xs btn-o' href='<?php echo base_url() . 'editar-area/' . $area->ProyectoID ?>'>
                                <i class='fa fa-edit'></i> Editar
                            </a>
                            <a class='btn btn-danger btn-xs btn-o delete' href='<?php echo base_url() . 'eliminar-area/' . $area->ProyectoID ?>'>
                                <i class='fa fa-trash'></i> Eliminar
                            </a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
