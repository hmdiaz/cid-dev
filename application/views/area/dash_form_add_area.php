<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-offset-2 col-lg-8 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nueva Área</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('agregar-area'); ?>" method="POST">
                    <div class="form-group">
                        <label for="area_name">
                            Nombre del Área
                        </label>
                        <input type="text" class="form-control" name="area_name" value="<?php echo set_value('area_name'); ?>" id="area_name" placeholder="Nombre del Área">
                    </div>
                    <div class="form-group">
                        <label for="chk">
                            Aprueba Requisiciones
                        </label>
                        <div class="checkbox">
                            <input type="checkbox" name="chkApruebaReq" id="chkApruebaReq" value="" class="js-switch" />
                        </div>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Crear
                    </button>
                    <a href="<?php echo base_url('lista-areas');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>