<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!-- Template Name: Clip-Two - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <!-- start: HEAD -->
    <head>
        <title><?php echo $title; ?></title>
        <!-- start: META -->
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: GOOGLE FONTS -->
        <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        <!-- end: GOOGLE FONTS -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/fontawesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/vendor/themify-icons/themify-icons.min.css">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/animate.css/animate.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/perfect-scrollbar/perfect-scrollbar.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('assets/templates/admin'); ?>/vendor/switchery/switchery.min.css" rel="stylesheet" media="screen">
        <!-- end: MAIN CSS -->
        <!-- start: CLIP-TWO CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/styles.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/plugins.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/templates/admin'); ?>/assets/css/themes/theme-1.css" id="skin_color" />
        <!-- end: CLIP-TWO CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    </head>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body class="login">
        <!-- start: LOGIN -->        
        <div class="row">           
            <div class="main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
                <?php
                if ($this->session->flashdata('message_error')) {
                    ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong><?php echo $this->session->flashdata('message_error'); ?></strong>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('message_success')) {
                    ?>
                    <div role="alert" class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                    </div>
                    <?php
                }
                ?>
                <div class="logo margin-top-30">
                    <img src="<?php echo base_url('assets/templates/admin'); ?>/assets/images/logo.png" alt="Clip-Two"/>
                </div>
                <!-- start: LOGIN BOX -->
                <div class="box-login">
                    <?php
                    if (validation_errors()) {
                        echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                    }
                    ?>  
                    <form class="form-login" action="<?php echo base_url('rec_pass'); ?>" method="POST" >
                        <fieldset>
                            <legend>
                                Recuperaci&oacute;n de Contraseña
                            </legend>
                            <p>
                                Introduzca el email con el cual registr&oacute; su cuenta para el env&iacute;o de la nueva contraseña
                            </p>
                            <div class="form-group">
                                <span class="input-icon">
                                    <input type="email" class="form-control" value="<?php echo set_value('user_email'); ?>" name="user_email" placeholder="Correo Electronico">
                                    <i class="fa fa-user"></i> </span>
                            </div>
                            <div class="form-actions">
                                <a class="btn btn-primary btn-o" href="<?php echo base_url('signin'); ?>">
                                    <i class="fa fa-chevron-circle-left"></i> Iniciar Sesión
                                </a>
                                <button type="submit" class="btn btn-primary pull-right">
                                    Recuperar <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div> 
                        </fieldset>
                    </form>
                    <!-- start: COPYRIGHT -->
                    <div class="copyright">
                        &copy; <span class="current-year"></span><span class="text-bold text-uppercase"> ClipTheme</span>. <span>All rights reserved</span>
                    </div>
                    <!-- end: COPYRIGHT -->
                </div>
                <!-- end: LOGIN BOX -->
            </div>
        </div>
        <!-- end: LOGIN -->
        <!-- start: MAIN JAVASCRIPTS -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/modernizr/modernizr.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery-cookie/jquery.cookie.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/switchery/switchery.min.js"></script>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/vendor/jquery-validation/jquery.validate.min.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <!-- start: CLIP-TWO JAVASCRIPTS -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/assets/js/main.js"></script>
        <!-- start: JavaScript Event Handlers for this page -->
        <script src="<?php echo base_url('assets/templates/admin'); ?>/assets/js/login.js"></script>
        <script>
            jQuery(document).ready(function () {
                Main.init();
                Login.init();
            });
        </script>
        <!-- end: JavaScript Event Handlers for this page -->
        <!-- end: CLIP-TWO JAVASCRIPTS -->
    </body>
    <!-- end: BODY -->
</html>