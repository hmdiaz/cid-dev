<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Pregunta</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-pregunta'); ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $pregunta->pregunta_id; ?>" name="id">
                    <div class="form-group">
                        <label for="pregunta_tipo">
                            Tipo de Pregunta
                        </label>
                        <select class="form-control" name="pregunta_tipo" >
                            <option value="text" <?php if($pregunta->pregunta_tipo == 'text'){echo 'selected';} ?>>Texto Corto (text)</option>
                            <option value="textarea" <?php if($pregunta->pregunta_tipo == 'textarea'){echo 'selected';} ?>>Texto Largo (textarea)</option>
                            <option value="select" <?php if($pregunta->pregunta_tipo == 'select'){echo 'selected';} ?>>De Selección (select)</option>
                            <option value="checkbox" <?php if($pregunta->pregunta_tipo == 'checkbox'){echo 'selected';} ?>>De Selección Multiple (checkbox)</option>
                            <option value="radio" <?php if($pregunta->pregunta_tipo == 'radio'){echo 'selected';} ?>>De Un Solo Valor (Radio)</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="pregunta_title">
                            Pregunta
                        </label>
                        <input type="text" class="form-control" required name="pregunta_title" value="<?php if(set_value('pregunta_title')){echo set_value('pregunta_title');}else{echo $pregunta->pregunta_title;} ?>" id="pregunta_title" placeholder="Pregunta">
                    </div>
                    <div class="form-group">
                        <label for="pregunta_options">
                            Opciones de la pregunta. (Separadas por Comas)
                        </label>
                        <textarea class="form-control" name="pregunta_options"  id="pregunta_options" placeholder="Si,No,Quizas,De vez en Cuando"><?php if(set_value('pregunta_options')){echo set_value('pregunta_options');}else{echo $pregunta->pregunta_options;} ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="pregunta_resp">
                            Respuesta (Si es Verdadero = 1 o Falso = 0)
                        </label>
                        <input type="text" class="form-control" name="pregunta_resp" value="<?php if(set_value('pregunta_resp')){echo set_value('pregunta_resp');}else{echo $pregunta->pregunta_resp;} ?>" id="pregunta_resp" placeholder="Respuesta">
                    </div>
                    <div class="form-group">
                        <label for="pregunta_cat">
                            Estatus de la Pregunta
                        </label>
                        <select class="form-control" required name="pregunta_cat" >
                            <?php
                            foreach ($categorias as $cat) {
                                if($cat->CategoriaPerfilID == $pregunta->CategoriaPerfilID){
                                    $selected = "selected";
                                }  else {
                                    $selected = "";
                                }
                                
                                echo '<option value="' . $cat->CategoriaPerfilID . '" '.$selected.'>' . $cat->NombreCategoria . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Guardar Cambios
                    </button>
                    <a href="<?php echo base_url('lista-pruebas'); ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>