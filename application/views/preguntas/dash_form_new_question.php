<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nueva Pregunta</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('agregar-pregunta'); ?>" method="POST">
                    <div class="form-group">
                        <label for="pregunta_tipo">
                            Tipo de Pregunta
                        </label>
                        <select class="form-control" name="pregunta_tipo" >
                            <option value="text">Texto Corto (text)</option>
                            <option value="textarea">Texto Largo (textarea)</option>
                            <option value="select">De Selección (select)</option>
                            <option value="checkbox">De Selección Multiple (checkbox)</option>
                            <option value="radio">De Un Solo Valor (Radio)</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="pregunta_title">
                            Pregunta
                        </label>
                        <input type="text" class="form-control" required name="pregunta_title" value="<?php echo set_value('pregunta_title'); ?>" id="pregunta_title" placeholder="Pregunta">
                    </div>
                    <div class="form-group">
                        <label for="pregunta_options">
                            Opciones de la pregunta. (Separadas por Comas)
                        </label>
                        <textarea class="form-control" name="pregunta_options"  id="pregunta_options" placeholder="Si,No,Quizas,De vez en Cuando"><?php echo set_value('pregunta_options'); ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="pregunta_resp">
                            Respuesta (Si es Verdadero = 1 o Falso = 0)
                        </label>
                        <input type="text" class="form-control" name="pregunta_resp" value="<?php echo set_value('pregunta_resp'); ?>" id="pregunta_resp" placeholder="Respuesta">
                    </div>
                    <div class="form-group">
                        <label for="pregunta_status">
                            Estatus de la Pregunta
                        </label>
                        <select class="form-control" required name="pregunta_status" >
                            <?php
                            foreach ($status as $stat) {
                                echo '<option value="' . $stat->EstadoID . '">' . $stat->Nombre . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="pregunta_cat">
                            Categoria
                        </label>
                        <select class="form-control" required name="pregunta_cat" >
                            <?php
                            foreach ($categorias as $cat) {
                                echo '<option value="' . $cat->CategoriaPerfilID . '">' . $cat->NombreCategoria . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Agregar
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>