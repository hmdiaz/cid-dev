<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="row">
        <div class="col-xs-12">
            <button class="pull-right btn btn-o btn-primary" data-toggle="modal" data-target="#preguntas"><i class="ti-plus"></i> Preguntas</button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">        
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-full-width" id="list_roles">
                    <thead>
                        <tr>
                            <th>Pregunta</th>
                            <th>Tipo de Pregunta</th>
                            <th>Opciones</th>
                            <th>Respuesta</th>
                            <th>Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $questions; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!--Modal Preguntas-->
<div class="modal fade" id="preguntas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Preguntas</h4>
            </div>
            <div class="modal-body">                   
                <form role="form" action="<?php echo base_url('agregar-preguntas'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $id_test ?>" name="id">
                    <?php if (count($preguntas) > 0): ?>
                        <?php foreach ($preguntas as $preg): ?>
                            <?php if (count($preg_act) > 0): ?>
                                <?php $band = FALSE; foreach ($preg_act as $pa): ?>
                                    <?php if ($preg->pregunta_id == $pa->pregunta_id): $band=TRUE;?>
                                        <div class="checkbox clip-check check-primary">
                                            <input type="checkbox" name="check_<?php echo $preg->pregunta_id; ?>" id="checkbox<?php echo $preg->pregunta_id; ?>" value="1" checked>
                                            <label for="checkbox<?php echo $preg->pregunta_id; ?>">
                                                <p><?php echo $preg->pregunta_title . ' ('.$preg->pregunta_tipo.')'; ?></p>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label for="test_name">
                                                valor de la pregunta
                                            </label>
                                            <input type="text" class="form-control" name="preg_<?php echo $preg->pregunta_id; ?>" value="<?php if(set_value('preg_' . $preg->pregunta_id)){ echo set_value('preg_' . $preg->pregunta_id);}else{ echo $pa->valor ;} ?>" id="preg_<?php echo $preg->pregunta_id; ?>" placeholder="Valor la Pregunta">
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <?php if($band==FALSE): ?>
                                    <div class="checkbox clip-check check-primary">
                                        <input type="checkbox" name="check_<?php echo $preg->pregunta_id; ?>" id="checkbox<?php echo $preg->pregunta_id; ?>" value="1">
                                        <label for="checkbox<?php echo $preg->pregunta_id; ?>">
                                            <p><?php echo $preg->pregunta_title . ' ('.$preg->pregunta_tipo.')'; ?></p>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="test_name">
                                            valor de la pregunta
                                        </label>
                                        <input type="text" class="form-control" name="preg_<?php echo $preg->pregunta_id; ?>" value="<?php echo set_value('preg_' . $preg->pregunta_id); ?>" id="preg_<?php echo $preg->pregunta_id; ?>" placeholder="Valor la Pregunta">
                                    </div>
                                <?php endif; ?>
                            <?php else: ?>
                                <div class="checkbox clip-check check-primary">
                                    <input type="checkbox" name="check_<?php echo $preg->pregunta_id; ?>" id="checkbox<?php echo $preg->pregunta_id; ?>" value="1">
                                    <label for="checkbox<?php echo $preg->pregunta_id; ?>">
                                        <p><?php echo $preg->pregunta_title . ' ('.$preg->pregunta_tipo.')'; ?></p>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="test_name">
                                        valor de la pregunta
                                    </label>
                                    <input type="text" class="form-control" name="preg_<?php echo $preg->pregunta_id; ?>" value="<?php echo set_value('preg_' . $preg->pregunta_id); ?>" id="preg_<?php echo $preg->pregunta_id; ?>" placeholder="Valor la Pregunta">
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>                    
                        <button type="submit" class="btn btn-o btn-primary">
                            Agregar
                        </button>
                    <?php else: ?>
                        <p>No hay preguntas para esta prueba</p>
                    <?php endif; ?>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-o" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
<!--Fin Modal Preguntas-->