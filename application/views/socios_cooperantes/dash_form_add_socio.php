<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nuevo Socio Cooperante</h5>
            </div>
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>
                <form role="form" action="<?php echo base_url('agregar-socio'); ?>" method="POST">
                    <div class="form-group">
                        <label for="socio_name">
                            Nombre del Socio Cooperante
                        </label>
                        <input type="text" class="form-control" name="socio_name" value="<?php echo set_value('socio_name'); ?>" id="socio_name" placeholder="Nombre Socio Cooperante">
                    </div>
                    <div class="form-group">
                        <label for="tipo_socio">
                            Tipo de Socio
                        </label>
                        <select id="tipo_socio" name="tipo_socio" class="form-control">
                            <option value="">Seleccione uno...</option>
                            <?php
                            foreach ($tipos_socios as $tipo_socio){
                                echo '<option value="'.$tipo_socio->TipoSocioID.'">'.$tipo_socio->Nombre.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Crear
                    </button>
                    <a href="<?php echo base_url('lista-socios');  ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>