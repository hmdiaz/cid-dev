<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive" style="min-height: 200px">
            <table class="table table-bordered table-hover" id="tbl_socios_cooperantes_mto">
                <thead>
                <tr>
                    <th>Tipo de Socio</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($tipos_socios as $tipo_socio)
                {
                    ?>
                    <tr>
                        <td><?php echo $tipo_socio->Nombre;  ?></td>
                        <td>
                            <a class='btn btn-primary btn-xs btn-o' href='<?php echo base_url() . 'editar-tipo-socio/' . $tipo_socio->TipoSocioID ?>'>
                                <i class='fa fa-edit'></i> Editar
                            </a>
                            <a class='btn btn-danger btn-xs btn-o delete' href='<?php echo base_url() . 'eliminar-tipo-socio/' . $tipo_socio->TipoSocioID ?>'>
                                <i class='fa fa-trash'></i> Eliminar
                            </a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div>
            <?php //echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
