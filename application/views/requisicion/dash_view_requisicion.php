<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <!--
            <div class="panel-heading">
                <h5 class="panel-title"></h5>
            </div>
            -->
            <div class="panel-body">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }

                ?>
                <h1 align="center">
                    <small>Información de la Requisición</small>
                </h1>

                <div class="panel panel-white" id="panel1">
                    <div class="panel-heading">
                        <h4 class="panel-title">Aprobaciones</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover" id="tbl_aprob_requisiciones">
                            <thead>
                            <tr>
                                <th>Area</th>
                                <th>Estado</th>
                                <th>Encargado</th>
                                <th>Fecha Aprobacion</th>
                                <th>Observaciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                foreach($aprobaciones as $item){
                                    if($item->FechaActualizacion){
                                        $fecha = $item->FechaActualizacion;
                                    }else{
                                        $fecha = "-";
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $item->NombreProyecto ?></td>
                                        <td><?php echo $item->status_name ?></td>
                                        <td><?php echo $item->Nombre ?></td>
                                        <td><?php echo $fecha; ?></td>
                                        <td><?php echo $item->aprobaciones_historials_obsevaciones ?></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <form role="form" action="<?php echo base_url('actualizar-requisicion'); ?>" method="POST">
                    <input type="hidden" name="idPerfil" id="idPerfil" value="<?php echo $idPerfil ?>">
                    <input type="hidden" name="idReq" id="idReq" value="<?php echo $req->RequisicionID ?>">
                    <input type="hidden" id="baseUrl" name="baseUrl" value="<?php echo base_url(); ?>">
                    <div class="form-group">
                        <label for="area">
                            Tipo de Contrato
                        </label>
                        <select id="tipo_contrato" name="tipo_contrato" class="form-control" disabled>
                            <option value="">Seleccione uno...</option>
                            <?php
                            foreach ($tipos_contrato as $tipo_contrato){
                                if($req->TipoContratoID == $tipo_contrato->TipoContratoID)
                                {
                                    echo '<option value="'.$tipo_contrato->TipoContratoID.'" selected>'.$tipo_contrato->Nombre.'</option>';
                                }
                                else
                                {
                                    echo '<option value="'.$tipo_contrato->TipoContratoID.'">'.$tipo_contrato->Nombre.'</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nombre_cargo">
                            Solicitante
                        </label>
                        <input type="text" class="form-control" name="solicitante" id="solicitante" value="<?php echo $datos_usuario->Nombres . ' ' . $datos_usuario->Apellidos; ?>" placeholder="Nombre del Cargo" disabled>
                        <input type="hidden" class="form-control" name="datos_usuario_id" id="datos_usuario_id" value="<?php echo $datos_usuario->DatosUsuarioID; ?>">
                    </div>
                    <div class="form-group">
                        <label for="nombre_cargo">
                            Fecha de Solicitud
                        </label>
                        <input type="text" class="form-control" name="fecha_solicitud" id="fecha_solicitud" value="<?php echo date("Y-m-d") ?>" disabled>
                    </div>
                    <div class="form-group">
                        <label for="nombre_cargo">
                            Nombre del Cargo
                        </label>
                        <input type="text" class="form-control" name="nombre_cargo" id="nombre_cargo" value="<?php echo $perfil->TituloCargo; ?>" placeholder="Nombre del Cargo" disabled>
                    </div>
                    <div class="form-group">
                        <label for="nombre_proyecto">
                            Proyecto
                        </label>
                        <input type="text" class="form-control" name="nombre_proyecto" id="nombre_proyecto" value="<?php echo $project->NombreProyecto; ?>" placeholder="Nombre del Proyecto" disabled>
                    </div>
                    <div class="form-group">
                        <label for="cant_solicitadas">
                            Cantidad de Personas Solicitadas
                        </label>
                        <input type="text" class="form-control only-numeric" name="cant_solicitadas" id="cant_solicitadas" placeholder="Cantidad de Personas Solicitadas" value="<?php if(set_value('cant_solicitadas')){ echo set_value('cant_solicitadas'); }else{ echo $req->CantidadPersonasSolicitadas; } ?>" disabled>
                    </div>
                    <div class="form-group">
                        <label for="fecha_inicio_labores">
                            Fecha de Inicio de Labores
                        </label>
                        <p class="input-group input-append datepicker date">
                            <input type="text" class="form-control" name="fecha_inicio_labores" id="fecha_inicio_labores" placeholder="Fecha de Inicio de Labores" value="<?php if(set_value('fecha_inicio_labores')){ echo set_value('fecha_inicio_labores'); }else{ echo $req->FechaInicioLabores; } ?>" disabled>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default">
                                    <i class="glyphicon glyphicon-calendar"></i>
                                </button>
                            </span>
                        </p>
                    </div>
                    <div class="form-group">
                        <label for="fecha_fin_labores">
                            Fecha de Fin de Labores
                        </label>
                        <p class="input-group input-append datepicker date">
                            <input type="text" class="form-control" name="fecha_fin_labores" id="fecha_fin_labores" placeholder="Fecha de Fin de Labores" value="<?php if(set_value('fecha_fin_labores')){ echo set_value('fecha_fin_labores'); }else{ echo $req->FechaFinLabores; } ?>" disabled>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default">
                                    <i class="glyphicon glyphicon-calendar"></i>
                                </button>
                            </span>
                        </p>
                    </div>
                    <div class="form-group">
                        <label for="objeto_contrato">
                            Justificación
                        </label>
                        <textarea class="form-control autosize area-animated" id="justificacion" name="justificacion" data-autosize-on="true" style="overflow: hidden; resize: horizontal; word-wrap: break-word; height: 71px;" disabled><?php  echo $req->Justificacion; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="objeto_contrato">
                            Observaciones
                        </label>
                        <textarea class="form-control autosize area-animated" id="observaciones" name="observaciones" data-autosize-on="true" style="overflow: hidden; resize: horizontal; word-wrap: break-word; height: 71px;" disabled><?php echo $req->Observaciones; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="cant_solicitadas">
                            Salario Asignado
                        </label>
                        <input type="text" class="form-control" name="salario_asignado" id="salario_asignado" placeholder="Salario Asignado" value="<?php if(set_value('salario_asignado')){ echo set_value('salario_asignado'); }else{ echo $req->SalarioAsignado; } ?>" disabled>
                    </div>
                    <h1 align="center">
                        <small>Lugar de Trabajo</small>
                    </h1>
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <table class="table table-bordered table-hover table-full-width" id="tbl_municipios_new_req">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Departamentos</th>
                                    <th>Municipios</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($muns as $mun)
                                {
                                    ?>
                                    <tr>
                                        <input type='hidden' id='hdItemsMunicipiosId' name='hdItemsMunicipiosId[]' value='<?php echo $mun->MunicipioID ?>' />
                                        <td><?php echo $mun->MunicipioRequisicionID ?></td>
                                        <td><?php echo $mun->NombreDepartamento ?></td>
                                        <td><?php echo $mun->NombreMunicipio ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                            <br />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
