<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="table-responsive" style="min-height: 200px"><!---->
        <a href="<?php echo base_url('nueva-requisicion') . '/' . $perfilId; ?>" class="btn btn-o btn-default">
            Nueva Requisición
        </a>
        <br />
        <br />
        <table class="table table-bordered table-hover" id="tbl_requisiciones">
            <thead>
            <tr>
                <th>Nombre del Cargo</th>
                <th>Salario Asignado</th>
                <th>Aprob.</th>
                <th>Fecha de Solicitud</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php echo $requisiciones; ?>
            </tbody>
        </table>
    </div><!---->
</div>