<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <!--
            <div class="panel-heading">
                <h5 class="panel-title"></h5>
            </div>
            -->
            <div class="panel-body">
                <div class="alert alert-block alert-danger" id="errorContainer">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="alert-heading margin-bottom-10"><i class="ti-close"></i> Atenci&oacuten a los siguientes errores!</h4>
                </div>
                <h1 align="center">
                    <small>Información de la Requisición</small>
                </h1>
                <form id="frmReq" name="frmReq" role="form" action="<?php echo base_url('actualizar-requisicion'); ?>" method="POST">
                    <input type="hidden" name="idPerfil" id="idPerfil" value="<?php echo $idPerfil ?>">
                    <input type="hidden" name="idReq" id="idReq" value="<?php echo $req->RequisicionID ?>">
                    <input type="hidden" id="baseUrl" name="baseUrl" value="<?php echo base_url(); ?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="area">
                                    Tipo de Contrato
                                </label>
                                <select id="tipo_contrato" name="tipo_contrato" class="form-control">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    foreach ($tipos_contrato as $tipo_contrato){
                                        if($req->TipoContratoID == $tipo_contrato->TipoContratoID)
                                        {
                                            echo '<option value="'.$tipo_contrato->TipoContratoID.'" selected>'.$tipo_contrato->Nombre.'</option>';
                                        }
                                        else
                                        {
                                            echo '<option value="'.$tipo_contrato->TipoContratoID.'">'.$tipo_contrato->Nombre.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre_cargo">
                                    Solicitante
                                </label>
                                <input type="text" class="form-control" name="solicitante" id="solicitante" value="<?php echo $datos_usuario->Nombres . ' ' . $datos_usuario->Apellidos; ?>" placeholder="Nombre del Cargo" readonly>
                                <input type="hidden" class="form-control" name="datos_usuario_id" id="datos_usuario_id" value="<?php echo $datos_usuario->DatosUsuarioID; ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre_cargo">
                                    Fecha de Solicitud
                                </label>
                                <input type="text" class="form-control" name="fecha_solicitud" id="fecha_solicitud" value="<?php echo date("Y-m-d") ?>" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre_cargo">
                                    Nombre del Cargo
                                </label>
                                <input type="text" class="form-control" name="nombre_cargo" id="nombre_cargo" value="<?php echo $perfil->TituloCargo; ?>" placeholder="Nombre del Cargo" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nombre_proyecto">
                                    Proyecto
                                </label>
                                <input type="text" class="form-control" name="nombre_proyecto" id="nombre_proyecto" value="<?php echo $project->NombreProyecto; ?>" placeholder="Nombre del Proyecto" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fecha_inicio_labores">
                                    Fecha de Inicio de Labores
                                </label>
                                <p class="input-group input-append datepicker date">
                                    <input type="text" class="form-control" name="fecha_inicio_labores" id="fecha_inicio_labores" placeholder="Fecha de Inicio de Labores" value="<?php if(set_value('fecha_inicio_labores')){ echo set_value('fecha_inicio_labores'); }else{ echo $req->FechaInicioLabores; } ?>">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </button>
                                    </span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fecha_fin_labores">
                                    Fecha de Fin de Labores
                                </label>
                                <p class="input-group input-append datepicker date">
                                    <input type="text" class="form-control" name="fecha_fin_labores" id="fecha_fin_labores" placeholder="Fecha de Fin de Labores" value="<?php if(set_value('fecha_fin_labores')){ echo set_value('fecha_fin_labores'); }else{ echo $req->FechaFinLabores; } ?>">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </button>
                                    </span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="objeto_contrato">
                                    Justificación
                                </label>
                                <textarea class="form-control autosize area-animated" id="justificacion" name="justificacion" data-autosize-on="true" style="overflow: hidden; resize: horizontal; word-wrap: break-word; height: 71px;"><?php  echo $req->Justificacion; ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="objeto_contrato">
                                    Observaciones
                                </label>
                                <textarea class="form-control autosize area-animated" id="observaciones" name="observaciones" data-autosize-on="true" style="overflow: hidden; resize: horizontal; word-wrap: break-word; height: 71px;"><?php echo $req->Observaciones; ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="mostrar_salario">
                                    Mostrar Salario
                                </label>
                                <div class="checkbox">
                                    <input type="checkbox" name="mostrar_salario" class="js-switch" value="1" <?php if($req->mostrar_salario == 1){echo "checked";} ; ?>/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="cant_solicitadas">
                                    Salario Asignado
                                </label>
                                <input type="text" class="form-control" name="salario_asignado" id="salario_asignado" placeholder="Salario Asignado" value="<?php if(set_value('salario_asignado')){ echo set_value('salario_asignado'); }else{ echo $req->SalarioAsignado; } ?>">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="area">
                                    Motivo
                                </label>
                                <select id="motivos" name="motivos" class="form-control">
                                    <option value="">Seleccione uno...</option>
                                    <?php
                                    foreach ($motivos as $motivo) {
                                        if ($req->MotivoID == $motivo->MotivoID) {
                                            echo '<option value="'.$motivo->MotivoID.'" selected>'.$motivo->Descripcion.'</option>';
                                        } else
                                        {
                                            echo '<option value="'.$motivo->MotivoID.'">'.$motivo->Descripcion.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h1 align="center">
                                <small>Lugar de Trabajo</small>
                            </h1>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <br />
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label for="departamento_req">
                                                Departamento
                                            </label>
                                            <select id="departamento_req" name="departamento_req" class="form-control">
                                                <option value="">Seleccione uno...</option>
                                                <?php
                                                foreach ($departamentos as $departamento){
                                                    echo '<option value="'.$departamento->DepartamentoID.'">'.$departamento->Nombre.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="municipio_req">
                                                Municipio
                                            </label>
                                            <select id="municipio_req" name="municipio_req" class="form-control">
                                                <option value="">Seleccione uno...</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control only-numeric" name="vacantes_x_municipio" id="vacantes_x_municipio" placeholder="Vacantes">
                                        </div>
                                        <a class="btn btn-primary" href="#" id="addItemMunicipio_edit_req">
                                            <i class="glyphicon glyphicon-plus"></i>
                                        </a>
                                        <a class='btn btn-red del' id="delRowsMunicipiosEditReq" href='javascript:;'>
                                            <i class='fa fa-trash-o'></i> Quitar Seleccionado
                                        </a>
                                    </div>
                                    <br />
                                    <table class="table table-bordered table-hover table-full-width" id="tbl_municipios_new_req">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Departamentos</th>
                                            <th>Municipios</th>
                                            <th>Vacantes</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach($muns as $mun)
                                        {
                                            ?>
                                            <tr>
                                                <input type='hidden' id='hdItemsMunicipiosId' name='hdItemsMunicipiosId[]' value='<?php echo $mun->MunicipioID ?>' />
                                                <input type='hidden' id='vacxmun' name='vacxmun[]' value='<?php echo $mun->MunicipioID ?>' />
                                                <td><?php echo $mun->MunicipioRequisicionID ?></td>
                                                <td><?php echo $mun->NombreDepartamento ?></td>
                                                <td><?php echo $mun->NombreMunicipio ?></td>
                                                <td><?php echo $mun->municipios_x_requisicion_vacantes ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <br />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" name="edit" value="1" class="btn btn-o btn-primary">
                                Actualizar Requisición
                            </button>
                            <a href="" class="btn btn-o btn-danger pull-right" onclick="window.history.back()">
                                Cancelar
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
