<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="tabbable">
        <ul id="myTab2" class="nav nav-tabs nav-justified">
            <li class="active">
                <a href="#myTab2_pendientes" data-toggle="tab">
                    Pendientes
                </a>
            </li>
            <li>
                <a href="#myTab2_aprobadas" data-toggle="tab">
                    Aprobadas
                </a>
            </li>
            <li>
                <a href="#myTab2_rechazadas" data-toggle="tab">
                    Rechazadas
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="myTab2_pendientes">
                <div class="table-responsive" style="min-height: 200px">
                    <table class="table table-bordered table-hover" id="tbl_requisiciones_pendientes">
                        <thead>
                        <tr>
                            <th>Nombre del Cargo</th>
                            <th>Salario Asignado</th>
                            <th>Cant.</th>
                            <th>Fecha de Solicitud</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php echo $aprobaciones_pendientes; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane fade in active" id="myTab2_aprobadas">
                <div class="tab-pane fade in active" id="myTab2_pendientes">
                    <div class="table-responsive" style="min-height: 200px">
                        <table class="table table-bordered table-hover" id="tbl_requisiciones_aceptadas">
                            <thead>
                            <tr>
                                <th>Nombre del Cargo</th>
                                <th>Salario Asignado</th>
                                <th>Cant.</th>
                                <th>Fecha de Solicitud</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php echo $aprobaciones_aprobadas; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade in active" id="myTab2_rechazadas">
                <div class="tab-pane fade in active" id="myTab2_pendientes">
                    <div class="table-responsive" style="min-height: 200px">
                        <table class="table table-bordered table-hover" id="tbl_requisiciones_rechazadas">
                            <thead>
                            <tr>
                                <th>Nombre del Cargo</th>
                                <th>Salario Asignado</th>
                                <th>Cant.</th>
                                <th>Fecha de Solicitud</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php echo $aprobaciones_rechazadas; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>