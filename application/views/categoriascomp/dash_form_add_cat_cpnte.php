<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-offset-2 col-lg-8 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Nueva <?php echo $maintitle; ?></h5>
            </div>
            <div class="panel-body">

                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('agregar-cat-cpnte'); ?>" method="POST">
                    <div class="form-group">
                        <label for="cat_name">
                            Nombre de la categoria
                        </label>
                        <input type="text" class="form-control" name="cat_name" id="cat_name" value="<?php echo set_value('cat_name'); ?>" placeholder="Nombre de la categoria">
                    </div>
                    <div class="form-group">
                        <label for="cat_status">
                            Estatus de Categoria
                        </label>
                        <select id="user_status" name="cat_status" class="cat_select_status form-control">
                            <option value="0" selected="selected">Selecciona Status</option>
                            <option value="1">Activo</option>
                            <option value="2">Inactivo</option>
                            <option value="3">Eliminado</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Crear
                    </button>
                    <a href="<?php echo base_url("lista-cate-cpnte"); ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>