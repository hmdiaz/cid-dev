<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-offset-2 col-lg-8 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Editar <?php echo $maintitle; ?></h5>
            </div>
            <div class="panel-body">

                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                            </button>
                            ' . validation_errors() . '
                        </div>';
                }
                ?>                
                <form role="form" action="<?php echo base_url('actualizar-cat'); ?>" method="POST">
                    <input type="hidden" value="<?php echo $cat->CategoriaPerfilID; ?>" name="id">
                    <div class="form-group">
                        <label for="cat_name">
                            Nombre de la categoria
                        </label>
                        <input type="text" class="form-control" name="cat_name" id="cat_name" value="<?php if(set_value('cat_name')){ echo set_value('cat_name'); }else{ echo $cat->NombreCategoria; } ?>" placeholder="Nombre de la categoria">
                    </div>
                    <button type="submit" class="btn btn-o btn-primary">
                        Guardar
                    </button>
                    <a href="<?php echo base_url("lista-cate-cpnte"); ?>" class="btn btn-o btn-danger pull-right">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>