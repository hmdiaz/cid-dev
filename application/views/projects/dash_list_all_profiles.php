<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="table-responsive" style="min-height: 200px">
        <a href="<?php echo base_url('nuevo-perfil'); ?>" class="btn btn-o btn-default">
            Nuevo Perfil
        </a>
        <br />
        <br />
        <table class="table table-bordered table-hover" id="tbl_all_profiles">
            <thead>
                <tr>
                    <th>#Proyecto</th>
                    <th>Nombre del Proyecto</th>
                    <th>Misión del Cargo</th>
                    <th>Título del Cargo</th>
                    <th>Número de Ocupantes</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>


                <?php
                foreach ($profiles as $p) {
                    echo '<tr>';
                    echo '<td>'.$p->ProyectoID.'</td>';
                    echo '<td>';
                    echo "<a href='".base_url('perfiles')."/".$p->ProyectoID."'>".$p->NombreProyecto."</a>";
                    echo '</td>';
                    echo '<td>'.$p->MisionCargo.'</td>';
                    echo '<td>'.$p->TituloCargo.'</td>';
                    echo '<td>'.$p->NumeroOcupantes.'</td>';
                    echo '<td>';
                    echo "<a class='btn btn-primary btn-xs btn-o' href='" . base_url('lista-requisiciones') . "/" . $p->PerfilID . "'>
                            <i class='fa fa-file'></i> Requisiciones
                        </a>
                        <a class='btn btn-primary btn-xs btn-o' href='" . base_url('modificar-perfil') . "/" . $p->PerfilID . "'>
                            <i class='fa fa-edit'></i> Editar
                        </a>
                        <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-perfil') . "/" . $p->PerfilID . "'>
                            <i class='fa fa-trash'></i> Eliminar
                        </a>
                        <a class='btn btn-default btn-xs btn-o' href='" . base_url('clonar-perfil') . "/" . $p->PerfilID . "'>
                            <i class='fa fa-files-o'></i> Clonar
                        </a>
                        ";
                    echo '</td>';
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</div>