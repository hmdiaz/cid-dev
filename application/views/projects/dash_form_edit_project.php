<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Completar Información del Proyecto</h5>
            </div>
            <div class="panel-body">
                <div class="alert alert-block alert-danger" id="errorContainer">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="alert-heading margin-bottom-10"><i class="ti-close"></i> Atenci&oacuten a los siguientes errores!</h4>
                </div>
                <form name="frmProyecto" id="frmProyecto" role="form" action="<?php echo base_url('actualizar-proyecto'); ?>" method="POST">
                    <input type="hidden" id="id" name="id" value="<?php echo $project->ProyectoID;  ?>">
                    <input type="hidden" id="baseUrl" name="baseUrl" value="<?php echo base_url(); ?>">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="licitacion">
                                Licitacion
                            </label>
                            <div class="checkbox">
                                <input type="checkbox" name="licitacion" class="js-switch" value="1" />
                            </div>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="fecha_firma_contrato">
                                Fecha de Firma de Contrato
                            </label>
                            <p class="input-group input-append datepicker date">
                                <input type="text" class="form-control" name="fecha_firma_contrato" id="fecha_firma_contrato" value="<?php if(set_value('fecha_firma_contrato')){ echo set_value('fecha_firma_contrato'); }else{ echo $project->FechaFirmaContrato; } ?>" placeholder="Fecha de Firma de Contrato">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </button>
                                </span>
                            </p>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="fecha_firma_acta_inicio">
                                Fecha de Firma de Acta de Inicio en Documento
                            </label>
                            <p class="input-group input-append datepicker date">
                                <input type="text" class="form-control" name="fecha_firma_acta_inicio" id="fecha_firma_acta_inicio" value="<?php if(set_value('fecha_firma_acta_inicio')){ echo set_value('fecha_firma_acta_inicio'); }else{ echo $project->FechaFirmaActaInicioDoc; } ?>" placeholder="Fecha de Firma de Acta de Inicio">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </button>
                                </span>
                            </p>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="fecha_firma_acta_inicio_cron">
                                Fecha de Firma de Acta de Inicio Cronograma
                            </label>
                            <p class="input-group input-append datepicker date">
                                <input type="text" class="form-control" name="fecha_firma_acta_inicio_cron" id="fecha_firma_acta_inicio_cron" value="<?php if(set_value('fecha_firma_acta_inicio_cron')){ echo set_value('fecha_firma_acta_inicio_cron'); }else{ echo $project->FechaFirmaActaInicioCron; } ?>" placeholder="Fecha de Firma de Acta de Inicio Cronograma" />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </button>
                                </span>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label for="nombre_proyecto">
                                Nombre del Proyecto
                            </label>
                            <input type="text" class="form-control" name="nombre_proyecto" id="nombre_proyecto" value="<?php if(set_value('nombre_proyecto')){ echo set_value('nombre_proyecto'); }else{ echo $project->NombreProyecto; } ?>" placeholder="Nombre del Proyecto" />
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="codigo_contrato">
                                Código del Contrato
                            </label>
                            <input type="text" class="form-control" name="codigo_contrato" id="codigo_contrato" value="<?php if(set_value('codigo_contrato')){ echo set_value('codigo_contrato'); }else{ echo $project->CodigoContrato; } ?>" placeholder="Código del Contrato" />
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="identificacion_proyecto">
                                Identificación del Proyecto
                            </label>
                            <input type="text" class="form-control" name="identificacion_proyecto" id="identificacion_proyecto" value="<?php if(set_value('identificacion_proyecto')){ echo set_value('identificacion_proyecto'); }else{ echo $project->IdentificacionProyecto; } ?>" placeholder="Identificación del Proyecto">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label for="fecha_inicio_contrato">
                                Fecha de Inicio de Contrato
                            </label>
                            <p class="input-group input-append datepicker date">
                                <input type="text" class="form-control" name="fecha_inicio_contrato" id="fecha_inicio_contrato" value="<?php if(set_value('fecha_inicio_contrato')){ echo set_value('fecha_inicio_contrato'); }else{ echo $project->FechaInicioContrato; } ?>" placeholder="Fecha de Inicio de Contrato">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </button>
                                </span>
                            </p>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="fecha_fin_contrato">
                                Fecha de Fin de Contrato
                            </label>
                            <p class="input-group input-append datepicker date">
                                <input type="text" class="form-control" name="fecha_fin_contrato" id="fecha_fin_contrato" value="<?php if(set_value('fecha_fin_contrato')){ echo set_value('fecha_fin_contrato'); }else{ echo $project->FechaFinContrato; } ?>" placeholder="Fecha de Fin de Contrato">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </button>
                                </span>
                            </p>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="tiempo_ejecucion_efectiva">
                                Tiempo de Atención Efectiva
                            </label>
                            <input type="text" class="form-control" name="tiempo_ejecucion_efectiva" id="tiempo_ejecucion_efectiva" value="<?php if(set_value('tiempo_ejecucion_efectiva')){ echo set_value('tiempo_ejecucion_efectiva'); }else{ echo $project->TiempoEjecucionEfectiva; } ?>" placeholder="Tiempo de Ejecución Efectiva">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="objeto_contrato">
                            Objeto del Contrato
                        </label>
                        <textarea class="form-control autosize area-animated" id="objeto_contrato" name="objeto_contrato" data-autosize-on="true" style="overflow: hidden; resize: horizontal; word-wrap: break-word; height: 71px;"><?php if(set_value('objeto_contrato')){ echo set_value('objeto_contrato'); }else{ echo $project->ObjetoContrato; } ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="contribucion_institucional">
                            Contribución Institucional
                        </label>
                        <textarea class="form-control autosize area-animated" id="contribucion_institucional" name="contribucion_institucional" data-autosize-on="true" style="overflow: hidden; resize: horizontal; word-wrap: break-word; height: 71px;"><?php if(set_value('contribucion_institucional')){ echo set_value('contribucion_institucional'); }else{ echo $project->ContribucionInstitucional; } ?></textarea>
                    </div>
                    <div class="panel panel-white" id="panel1">
                        <div class="panel-heading">
                            <h4 class="panel-title">Socios Cooperantes</h4>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="tipo_socio">
                                        Tipos de Socio
                                    </label>
                                    <select id="tipo_socio" name="tipo_socio" class="form-control">
                                        <option value="">Seleccione uno...</option>
                                        <?php
                                        foreach ($tipos_socios as $item){
                                            echo '<option value="'.$item->TipoSocioID.'">'.$item->Nombre.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="socio_cooperante">
                                        Socio Cooperante
                                    </label>
                                    <select id="socio_cooperante" name="socio_cooperante" class="form-control">
                                        <option value="">Seleccione uno...</option>
                                    </select>
                                </div>
                            </div>
                            <a class="btn btn-primary" href="#" id="addItemSocioCooperanteEditProject">
                                <i class="glyphicon glyphicon-plus"></i> Agregar Elemento
                            </a>
                            <br />
                            <br />
                            <a class='btn btn-red del' id="delRowsSociosCooperantesEditProject" href='javascript:;'>
                                <i class='fa fa-trash-o'></i> Quitar Seleccionados
                            </a>
                            <br />
                            <br />
                            <table class="table table-bordered table-hover table-full-width" id="tbl_socios_cooperantes">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Socio Cooperante</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($socios_x_proyecto as $item)
                                {
                                    ?>
                                    <tr>
                                        <input type='hidden' id='hdItemsSocioId' name='hdItemsSocioId[]' value='<?php echo $item->SocioCooperanteID ?>' />
                                        <td><?php echo $item->SocioProyectoID ?></td>
                                        <td><?php echo $item->Nombre ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-white" id="panel1">
                        <div class="panel-heading">
                            <h4 class="panel-title">Cobertura</h4>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-6 form-group">
                                    <label for="departamento">
                                        Departamento
                                    </label>
                                    <select id="departamento" name="departamento" class="form-control"onchange="getMunicipios('../municipios', this.options[this.selectedIndex].value, '#departamento', '#municipio')">
                                        <option value="">Seleccione uno...</option>
                                        <?php
                                        foreach ($departamentos as $departamento){
                                            echo '<option value="'.$departamento->DepartamentoID.'">'.$departamento->Nombre.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-md-6 form-group">
                                    <label for="municipio">
                                        Municipio
                                    </label>
                                    <select id="municipio" name="municipio" class="form-control">
                                        <option value="">Seleccione uno...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-xs-12 col-md-4 form-group">
                                    <label for="socio_cooperante">
                                        Población
                                    </label>
                                    <select id="poblacion" name="poblacion" class="form-control">
                                        <option value="">Seleccione uno...</option>
                                        <?php
                                        foreach ($poblaciones as $poblacion){
                                            echo '<option value="'.$poblacion->PoblacionID.'">'.$poblacion->Nombre.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-md-4 form-group">
                                    <label for="ubicacion">
                                        Ubicación
                                    </label>
                                    <select id="ubicacion" name="ubicacion" class="form-control">
                                        <option value="">Seleccione uno...</option>
                                        <?php
                                        foreach ($ubicaciones as $ubicacion){
                                            echo '<option value="'.$ubicacion->UbicacionID.'">'.$ubicacion->Descripcion.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                
                                <div class="col-xs-12 col-md-4 form-group">
                                    <label for="cobertura">
                                        Cobertura
                                    </label>
                                    <input type="text" class="form-control" name="cobertura" id="cobertura" placeholder="Cobertura">
                                </div>
                            </div>
                            <a class="btn btn-primary" href="#" id="addItemPoblacionEditProject">
                                <i class="glyphicon glyphicon-plus"></i> Agregar Elemento
                            </a>
                            <br />
                            <br />
                            <a class='btn btn-red del' id="delRowsPoblacionesEditProject" href='javascript:;'>
                                <i class='fa fa-trash-o'></i> Quitar Seleccionados
                            </a>
                            <br />
                            <br />
                            <table class="table table-bordered table-hover table-full-width" id="tbl_poblaciones">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Departamentos</th>
                                    <th>Municipios</th>
                                    <th>Población</th>
                                    <th>Ubicación</th>
                                    <th>Cobertura</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($municipios_x_proyecto as $item)
                                {
                                    ?>
                                    <tr>
                                        <input type='hidden' id='hdItemsMunicipiosId' name='hdItemsMunicipiosId[]' value='<?php echo $item->MunicipioID ?>' />
                                        <td><?php echo $item->MunicipioID ?></td>
                                        <td><?php echo $item->NombreDepartamento ?></td>
                                        <td><?php echo $item->NombreMunicipio ?></td>
                                        <td><?php echo $item->NombrePoblacion ?></td>
                                        <td><?php echo $item->NombreUbicacion ?></td>
                                        <td><?php echo $item->Cobertura ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br />
                    <button type="submit" class="btn btn-o btn-primary">
                        Actualizar Proyecto
                    </button>
                    <a href="" class="btn btn-o btn-danger pull-right" onclick="window.history.back()">
                        Cancelar
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
