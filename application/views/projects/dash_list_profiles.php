<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
        <div class="table-responsive" style="min-height: 200px">
            <a href="<?php echo base_url('nuevo-perfil'); ?>" class="btn btn-o btn-default">
                Nuevo Perfil
            </a>
            <br />
            <br />
            <table class="table table-bordered table-hover" id="tbl_profiles">
                <thead>
                <tr>
                    <th>Misión del Cargo</th>
                    <th>Título del Cargo</th>
                    <th>Número de Ocupantes</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                    <?php echo $profiles; ?>
                </tbody>
            </table>
        </div>
</div>
