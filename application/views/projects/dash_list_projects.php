<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="table-responsive">
        <a href="<?php echo base_url('nuevo-proyecto'); ?>" class="btn btn-o btn-default">
            Nuevo Proyecto
        </a>
        <br />
        <br />
        <table class="table table-bordered table-hover" id="sample-table-1">
            <thead>
            <tr>
                <th>#ID</th>
                <th>Nombre del Proyecto</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php echo $projects; ?>
            </tbody>
        </table>
    </div>
</div>
