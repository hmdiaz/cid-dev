<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid container-fullw bg-white">
    <div class="table-responsive">
        <br />
        <br />
        <table class="table table-bordered table-hover" id="tbl_all_requisiciones">
            <thead>
                <tr>
                    <th>#Proyecto</th>
                    <th>Nombre del Proyecto</th>
                    <th>Perfil</th>
                    <th>Nombre del Cargo</th>
                    <th>Salario Asignado</th>
                    <th>Aprob.</th>
                    <th>Fecha de Solicitud</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>

                <?php
                setlocale(LC_MONETARY, 'es_ES');
                foreach ($requisiciones as $r) {
                    $aprobaciones = $this->Aprobaciones_model->VerificarRequisicionesAprobadas($r->RequisicionID);
                    echo '<tr>';
                    echo '<td>' . $r->ProyectoID . '</td>';
                    echo '<td>';
                    echo "<a href='".base_url('perfiles')."/".$r->ProyectoID."'>" . $r->NombreProyecto . "</a>";
                    echo '</td>';
                    echo '<td><a href="'.base_url('lista-requisiciones').'/'.$r->PerfilID.'">' . $r->MisionCargo . '</a></td>';
                    echo '<td>' . $r->NombreCargo . '</td>';
                    echo '<td>' . number_format($r->SalarioAsignado) . '</td>';
                    echo '<td>' . $aprobaciones['CantRequisicionesAprobadas'] . '</td>';
                    echo '<td>' . $r->FechaSolicitud . '</td>';
                    echo '<td>';
                    if ($aprobaciones['RequisicionAprobada'] == 'Si') {
                        
                        $convocatoria = $this->Convocatoria_model->getConvocatoriaByRequisicion($r->RequisicionID);
                        if(!count($convocatoria) > 0){
                            echo "<a class='btn btn-success btn-xs btn-o' href='" . base_url('nueva-convocatoria') . "/" . $r->RequisicionID . "'>
                                                <i class='fa fa-bullhorn'></i> Nueva Convocatoria
                                            </a>";
                        }
                        echo "<a class='btn btn-info btn-xs btn-o' href='" . base_url('lista-convocatorias') . "/" . $r->RequisicionID . "'>
                                            <i class='fa fa-server'></i> Lista Convocatorias
                                        </a>";
                    }
                    echo "<a class='btn btn-default btn-xs btn-o' href='" . base_url('nueva-requisicion') . "/" . $r->PerfilID . "'>
                                <i class='fa fa-plus'></i> Nueva Requisicion
                            </a>";

                    echo "<a class='btn btn-success btn-xs btn-o' href='" . base_url('preview-requisicion') . "/" . $r->RequisicionID . "'>
                                            <i class='fa fa-info'></i> Ver
                                        </a>";

                    echo "<a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-requisicion') . "/" . $r->RequisicionID . "/" . $r->PerfilID . "'>
                                            <i class='fa fa-edit'></i> Editar
                                        </a>";


                    echo "<a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-requisicion') . "/" . $r->RequisicionID . "'>
                                            <i class='fa fa-trash'></i> Eliminar
                                        </a>";
                    echo '</td>';
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </div><!---->
</div>
