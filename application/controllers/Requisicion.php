<?php

class Requisicion extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model([
            'Projects_model',
            'Perfil_model',
            'MunicipiosPerfil_model',
            'MunicipiosRequisicion_model',
            'Requisicion_model',
            'TipoContrato_model',
            'Motivo_model',
            'DatosUsuario_model',
            'Aprobaciones_model',
            'Flujos_model',
            'Users_model',
            'AprobacionesHistorial_model',
            'Cargos_model'
        ]);

        $this->load->library(['Auth']);
        $this->auth->is_session_start();
    }

    public function dash_new_requisicion($id = FALSE) {
        $data = new stdClass();
        $data->idPerfil = $id;
        $data->tipos_contrato = $this->TipoContrato_model->get(FALSE, 1);
        $data->motivos = $this->Motivo_model->get(FALSE, 1);
        $data->datos_usuario = $this->getDatosUsuario($_SESSION['id_user']);
        $data->project = $this->getProyectoByPerfilID($id);
        $data->departamentos = $this->MunicipiosRequisicion_model->getGetDepartamentoPerfil($id);
        $data->perfil = $this->Perfil_model->getPerfil($id);
        $data->content_view = "requisicion/dash_new_requisicion";
        $data->title = APP_NAME . "::Nueva Requisición";
        $data->active = "projects";
        $data->maintitle = "Requisición";
        $data->maindescription = "Crear Nueva Requisición";

        $this->load->view('dashboard', $data);
    }

    public function list_requisiciones($id) {
        $data = new stdClass();
        $data->content_view = "requisicion/dash_list_requisiciones";
        $data->title = APP_NAME . "::Lista de Requisiciones";
        $data->active = "projects";
        $data->maintitle = "Requisiciones";
        $data->maindescription = "Lista de Requisiciones";
        $data->perfil = $this->Perfil_model->getPerfil($id);
        $data->perfilId = $id;
        $data->requisiciones = $this->dash_list_requisiciones($id);
        $this->load->view('dashboard', $data);
    }

    public function dash_list_requisiciones($id) {
        $requisiciones = $this->Requisicion_model->getRequisiciones_x_Perfil($id, 1);

        $requisiciones_table = "";

        setlocale(LC_MONETARY, 'es_ES');
        if (count($requisiciones) > 0) {
            foreach ($requisiciones as $key => $value) {
                $aprobaciones = $this->Aprobaciones_model->VerificarRequisicionesAprobadas($value->RequisicionID);
                $requisiciones_table .= "<tr>";
                $requisiciones_table .= "<td>{$value->NombreCargo}</td>";
                $requisiciones_table .= "<td>$ " . number_format($value->SalarioAsignado) . "</td>";
                $requisiciones_table .= "<td align='center'>" . $aprobaciones['CantRequisicionesAprobadas'] . ' de ' . $aprobaciones['Total'] . "</td>";
                // $requisiciones_table .= "<td align='center'><button type=\"button\" class=\"btn btn-wide btn-o btn-success\"  data-toggle=\"modal\" data-target=\"#myModal\">" . $aprobaciones['CantRequisicionesAprobadas'] . ' de ' . $aprobaciones['Total'] ."</button></td>";
                // $requisiciones_table .= "<td>{$value->CantidadPersonasSolicitadas}</td>";
                $requisiciones_table .= "<td>{$value->FechaSolicitud}</td>";
                $requisiciones_table .= "<td>";

                if ($aprobaciones['RequisicionAprobada'] == 'Si') {
                    $this->load->model("Convocatoria_model");
                    $convocatoria = $this->Convocatoria_model->getConvocatoriaByRequisicion($value->RequisicionID);
                    if (!count($convocatoria) > 0) {
                        $requisiciones_table .= "<a class='btn btn-success btn-xs btn-o' href='" . base_url('nueva-convocatoria') . "/" . $value->RequisicionID . "'>
                                            <i class='fa fa-bullhorn'></i> Nueva Convocatoria
                                        </a>";
                    }
                    $requisiciones_table .= "<a class='btn btn-info btn-xs btn-o' href='" . base_url('lista-convocatorias') . "/" . $value->RequisicionID . "'>
                                            <i class='fa fa-server'></i> Lista Convocatorias
                                        </a>";
                }
                $requisiciones_table .= "
                                        <a class='btn btn-default btn-xs btn-o' href='" . base_url('nueva-requisicion') . "/" . $value->PerfilID . "'>
                                            <i class='fa fa-plus'></i> Nueva Requisicion
                                        </a>";

                $requisiciones_table .= "
                                        <a class='btn btn-success btn-xs btn-o' href='" . base_url('preview-requisicion') . "/" . $value->RequisicionID . "'>
                                            <i class='fa fa-info'></i> Ver
                                        </a>";

                $requisiciones_table .= "
                                        <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-requisicion') . "/" . $value->RequisicionID . "/" . $value->PerfilID . "'>
                                            <i class='fa fa-edit'></i> Editar
                                        </a>";


                $requisiciones_table .= "
                                        <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-requisicion') . "/" . $value->RequisicionID . "'>
                                            <i class='fa fa-trash'></i> Eliminar
                                        </a>";
                $requisiciones_table .= "</td>";
                $requisiciones_table .= "</tr>";
            }
        }

        return $requisiciones_table;
    }

    public function dash_edit_requisicion($idReq, $idPerfil) {
        $data = new stdClass();
        $data->idPerfil = $idPerfil;
        $data->req = $this->Requisicion_model->get($idReq, FALSE);
        $data->tipos_contrato = $this->TipoContrato_model->get(FALSE, 1);
        $data->motivos = $this->Motivo_model->get(FALSE, 1);
        $data->project = $this->getProyectoByPerfilID($idPerfil);
        $data->datos_usuario = $this->getDatosUsuario($_SESSION['id_user']);
        $data->departamentos = $this->MunicipiosRequisicion_model->getGetDepartamentoPerfil($idPerfil);
        $data->perfil = $this->Perfil_model->getPerfil($idPerfil);
        $data->muns = $this->MunicipiosRequisicion_model->getMunicipiosRequisicion($idReq);
        $data->content_view = "requisicion/dash_edit_requisicion";
        $data->title = APP_NAME . "::Editar Requisición";
        $data->active = "projects";
        $data->maintitle = "Requisición";
        $data->maindescription = "Editar Requisición";

        $this->load->view('dashboard', $data);
    }

    public function dash_view_requisicion($idReq) {
        $data = new stdClass();
        $req = $this->Requisicion_model->get($idReq, FALSE);
        $data->req = $req;
        $idPerfil = $req->PerfilID;
        $data->idPerfil = $idPerfil;
        $data->tipos_contrato = $this->TipoContrato_model->get(FALSE, 1);
        $data->motivos = $this->Motivo_model->get(FALSE, 1);
        $data->aprobaciones = $this->Aprobaciones_model->getListadoAprobacionesXRequisicion($idReq);
        $data->project = $this->getProyectoByPerfilID($idPerfil);
        $data->datos_usuario = $this->getDatosUsuario($_SESSION['id_user']);
        $data->departamentos = $this->MunicipiosRequisicion_model->getGetDepartamentoPerfil($idPerfil);
        $data->perfil = $this->Perfil_model->getPerfil($idPerfil);
        $data->muns = $this->MunicipiosRequisicion_model->getMunicipiosRequisicion($idReq);
        $data->content_view = "requisicion/dash_view_requisicion";
        $data->title = APP_NAME . "::Editar Requisición";
        $data->active = "projects";
        $data->maintitle = "Requisición";
        $data->maindescription = "Editar Requisición";

        $this->load->view('dashboard', $data);
    }

    public function dash_update_requisicion() {
        $this->load->library('form_validation');

        $req_id = $this->input->post('idReq');
        $id_perfil = $this->input->post('idPerfil');

        if ($this->input->post("mostrar_salario") == 1) {
            $mostar_salario = 1;
        } else {
            $mostar_salario = 0;
        }
        $data = array(
            'TipoContratoID' => $this->input->post('tipo_contrato'),
            'CantidadPersonasSolicitadas' => $this->input->post('cant_solicitadas'),
            'Justificacion' => $this->input->post('justificacion'),
            'SalarioAsignado' => $this->input->post('salario_asignado'),
            'MotivoID' => $this->input->post('motivos'),
            'Observaciones' => $this->input->post('observaciones'),
            'FechaInicioLabores' => $this->input->post('fecha_inicio_labores'),
            'FechaFinLabores' => $this->input->post('fecha_fin_labores'),
            'NombreCargo' => $this->input->post('nombre_cargo'),
            'DatosUsuarioSolitanteID' => $this->input->post('datos_usuario_id'),
            'FechaSolicitud' => date("Y-m-d H:i:s"),
            'FechaUltimaActualizacion' => date('Y-m-d H:i:s'),
            'mostrar_salario' => $mostar_salario
        );

        if ($this->form_validation->run('dash_edit_req') == FALSE) {
            $this->dash_edit_requisicion($req_id, $id_perfil);
        } else {
            $num_rows = $this->Requisicion_model->update($req_id, $data);

            $aprob = $this->Aprobaciones_model->getXRequisicion($req_id);

            foreach ($aprob as $item) {
                if ($item->EstadoID == 5) {
                    $data = array(
                        'EstadoID' => 6,
                        'FechaActualizacion' => date('Y-m-d H:i:s'),
                        'mostrar_notificaciones' => 0
                    );
                    $this->Aprobaciones_model->update($item->AprobacionID, $data);

                    $historial = [
                        "aprobaciones_historial_id_aprobacion" => $item->AprobacionID,
                        "aprobaciones_historials_obsevaciones" => "Actualizacion Notificacion",
                        "aprobaciones_historial_update" => date('Y-m-d H:i:s'),
                        "aprobaciones_historial_status" => 6
                    ];
                    $this->AprobacionesHistorial_model->insert($historial);
                }
            }
            $this->enviar_notificacion($req_id);
            $this->session->set_flashdata('message_success', 'La Requisicion se actualizo correctamente.');
            redirect('lista-requisiciones/' . $id_perfil);
        }
    }

    public function dash_add_requisicion() {
        $datos_us_id = $this->input->post('datos_usuario_id');
        $this->load->library('form_validation');
        $this->load->library('Sendmail');
        $idPerfil = $this->input->post('idPerfil');
        if ($this->form_validation->run('dash_new_req') == FALSE) {
            $this->dash_new_requisicion($idPerfil);
        } else {

            if ($this->input->post("mostrar_salario") == 1) {
                $mostar_salario = 1;
            } else {
                $mostar_salario = 0;
            }

            $data = array(
                'TipoContratoID' => $this->input->post('tipo_contrato'),
                'CantidadPersonasSolicitadas' => $this->input->post('cant_solicitadas'),
                'Justificacion' => $this->input->post('justificacion'),
                'SalarioAsignado' => $this->input->post('salario_asignado'),
                'MotivoID' => $this->input->post('motivos'),
                'Observaciones' => $this->input->post('observaciones'),
                'FechaInicioLabores' => $this->input->post('fecha_inicio_labores'),
                'FechaFinLabores' => $this->input->post('fecha_fin_labores'),
                'PerfilID' => $this->input->post('idPerfil'),
                'NombreCargo' => $this->input->post('nombre_cargo'),
                'DatosUsuarioSolitanteID' => $datos_us_id,
                'FechaSolicitud' => date("Y-m-d H:i:s"),
                'FechaCreacion' => date('Y-m-d H:i:s'),
                'mostrar_salario' => $mostar_salario
            );

            $req_id = $this->Requisicion_model->insert($data);

            for ($i = 0; $i < count($this->input->post('hdItemsMunicipiosId')); $i++) {
                $dataMuns = array(
                    'MunicipioID' => $this->input->post('hdItemsMunicipiosId')[$i],
                    'RequisicionID' => $req_id,
                    'municipios_x_requisicion_vacantes' => $this->input->post('vacxmun')[$i],
                );
                $this->MunicipiosRequisicion_model->insert($dataMuns);
            }

            // Creación de las aprobaciones
            $proyectos = $this->Projects_model->getProyectosAprueba();
            foreach ($proyectos as $p) {
                $data = array(
                    'ProyectoID' => $p->ProyectoID,
                    'AprobarID' => $req_id,
                    'DatosUsuarioID' => $datos_us_id,
                    'tipo_aprobacion' => 1
                );
                $id_aprob = $this->Aprobaciones_model->insert($data);
                $historial = [
                    "aprobaciones_historial_id_aprobacion" => $id_aprob,
                    "aprobaciones_historials_obsevaciones" => "Notificación",
                    "aprobaciones_historial_update" => date('Y-m-d H:i:s'),
                    "aprobaciones_historial_status" => 6
                ];
                $this->AprobacionesHistorial_model->insert($historial);
            }

            $this->enviar_notificacion($req_id, 1);

            if ($req_id > 0) {
                $this->session->set_flashdata('message_success', 'La requisición se agrego correctamente.');
                redirect('lista-requisiciones/' . $idPerfil);
            }
        }
    }

    public function valida_fecha() {
        $fecha_ini = strtotime($this->input->post('fecha_inicio_labores'));
        $fecha_fin = strtotime($this->input->post('fecha_fin_labores'));
        
        if ($fecha_ini > $fecha_fin) {
            $this->form_validation->set_message(
                    'valida_fecha', 'La fecha de inicio de labores no puede ser mayor a la fecha de fin de labores'
            );
            return FALSE;
        }
        return TRUE;
    }

    public function getMunicipiosDepartamentos_x_perfil($idPerfil, $idDepto) {

        $muns = $this->MunicipiosRequisicion_model->getMunicipiosDepartamentoPerfil($idPerfil, $idDepto);

        echo '<option value="">Seleccione uno...</option>';
        foreach ($muns as $mun) {
            echo '<option value="' . $mun->MunicipioID . '">' . $mun->Nombre . '</option>';
            //echo $dto->DepartamentoID  . '  ' .$dto->Nombre . '<br />';
        }
    }

    public function dash_add_municipio_requisicion() {
        $RequisicionID = $this->input->post('RequisicionID');
        $MunicipioID = $this->input->post('MunicipioID');

        echo $this->MunicipiosRequisicion_model->insert([
            'RequisicionID' => $RequisicionID,
            'MunicipioID' => $MunicipioID
        ]);
    }

    public function dash_delete_municipio_requisicion() {
        $id = $this->input->post('MunicipioRequisicionID');
        echo $this->MunicipiosRequisicion_model->delete($id);
    }

    public function getDatosUsuario($usuarioId) {
        return $this->DatosUsuario_model->get($usuarioId);
    }

    public function getProyectoByPerfilID($id) {
        $perfil = $this->Perfil_model->get($id, FALSE);
        return $this->Projects_model->get($perfil->ProyectoID, FALSE);
    }

    public function getSumaVacantesPerfil() {
        $id_perfil = $this->input->post('PerfilID');
        $id_municipio = $this->input->post('MunicipioID');
        $data = $this->MunicipiosRequisicion_model->getSumaVacatesReqxPerfil($id_perfil, $id_municipio);
        if ($data->suma === NULL) {
            echo 0;
        } else {
            echo $data->suma;
        }
    }

    public function delete_requisicion($id = FALSE) {
        $req = $this->Requisicion_model->get($id, FALSE);
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Requisición no valida');
            redirect('lista-requisiciones/' . $req->PerfilID);
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-requisiciones/' . $req->PerfilID);
        }

        if (!count($req) == 1) {
            $this->session->set_flashdata('message_error', 'Requisición no encontrada');
            redirect('lista-requisiciones/' . $req->PerfilID);
        }

        $num_rows = $this->Requisicion_model->update($id, ['EstadoID' => 3]);
        if ($num_rows > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'La Requisición se ha eliminado.');
            redirect('lista-requisiciones/' . $req->PerfilID);
        }
    }

    function valida_personas_solicitadas_requisicion() {
        $perfil = $this->Perfil_model->getPerfil($this->input->post('idPerfil'));
        $total_personas_db = $this->Requisicion_model->getCantidadPersonasSocilitadas($this->input->post('idPerfil'));
        $suma = $total_personas_db->total + $this->input->post('cant_solicitadas');
        if ($this->input->post('edit')) {
            if ($this->input->post('edit') == 1) {
                $req = $this->Requisicion_model->getRequisicion($this->input->post('idReq'));
                if (count($req) == 1) {
                    $vacantes = $req->CantidadPersonasSolicitadas;
                    $suma = $suma - $vacantes;
                }
            }
        }
        if ($perfil->NumeroOcupantes >= $suma) {
            return TRUE;
        } else {
            $this->form_validation->set_message(
                    'valida_personas_solicitadas_requisicion', 'La Cantidad de Personas Solicitadas no puede superar al numero total de vacantes en el Perfil'
            );
            return FALSE;
        }
    }

    public function check_req_total_ocupantes_x_municipio() {
        $suma_vacantes_x_mun = 0;
        for ($i = 0; $i < count($this->input->post('hdItemsMunicipiosId')); $i++) {
            $suma_vacantes_x_mun += $this->input->post('vacxmun')[$i];
        }
        if ($suma_vacantes_x_mun == $this->input->post('cant_solicitadas')) {
            return TRUE;
        }
        $this->form_validation->set_message(
                'check_req_total_ocupantes_x_municipio', 'El numero de personas solicitadas debe ser igual a la suma de vacantes por cada municipio'
        );
        return FALSE;
    }

    public function enviar_notificacion($req_id) {
        $this->load->library('Sendmail');
        $envio = TRUE;
        $requisicion = $this->Requisicion_model->getRequisicion($req_id);
        $flujos = $this->Flujos_model->getFlujo(1);
        $aprobaciones = $this->Aprobaciones_model->getXRequisicion($req_id);
        foreach ($flujos as $flujo) {
            $cargo = $this->Cargos_model->get($flujo->flujo_id_cargo);
            foreach ($aprobaciones as $a) {
                if ($cargo->AreaID == $a->ProyectoID) {
                    if ($a->EstadoID == 6 && $a->mostrar_notificaciones == 0) {
                        if ($envio) {
                            $users = $this->Users_model->getUserByCargo($flujo->flujo_id_cargo);
                            foreach ($users as $u) {
                                $to = $u->EmailUsuario;
                                $subject = "Aprobacion de Requisicion";
                                $html = "Tienes una requisicion pendiente para aprobar";
                                $html .= "<br>Requisición: " . $requisicion->NombreCargo;
                                $this->sendmail->sendMail($to, $subject, $html);

                                $update = [
                                    "mostrar_notificaciones" => 1
                                ];

                                $this->Aprobaciones_model->aprobarRequisicion($u->AreaID, $req_id, $update);
                            }
                        }
                        $envio = FALSE;
                    }
                }
            }
        }
    }

}
