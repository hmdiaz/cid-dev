<?php
class Responsabilidad extends CI_Controller
{
//put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model([
            "Responsabilidad_model",
            "TipoResponsabilidad_model"
        ]);
        $this->load->library([
            'Auth'
        ]);

        $this->auth->is_session_start();
    }

    public function list_responsabilidades() {
        $data = new stdClass();
        $data->content_view = "responsabilidad/dash_list_responsabilidades";
        $data->title = APP_NAME . "::Lista Poblaciones";
        $data->active = "cpntes";
        $data->maintitle = "Responsabilidades";
        $data->maindescription = "Lista de Responsabilidades";
        $data->responsabilidades = $this->Responsabilidad_model->get(FALSE, 1);
        $this->load->view('dashboard', $data);
    }

    public function form_new_responsabilidad() {
        $data = new stdClass();
        $data->tipo_responsabilidad = $this->TipoResponsabilidad_model->get(FALSE,1);
        $data->content_view = "responsabilidad/dash_form_add_responsabilidad";
        $data->title = APP_NAME . "::Nueva Responsabilidad";
        $data->active = "cpntes";
        $data->maintitle = "Responsabiidades";
        $data->maindescription = "Crear Nueva Responsabilidad";

        $this->load->view('dashboard', $data);
    }

    public function add_responsabilidad() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_responsabilidad') == FALSE) {
            $this->form_new_responsabilidad();
        } else {

            $responsabilidad_name = $this->input->post('responsabilidad_name');
            $tipo_responsabilidad = $this->input->post('tipo_responsabilidad');
            $insert = [
                "Descripcion" => $responsabilidad_name,
                "TipoResponsabilidadID" => $tipo_responsabilidad
            ];

                $responsabilidad_id = $this->Responsabilidad_model->insert($insert);
            if ($responsabilidad_id > 0) {
                $this->session->set_flashdata('message_success', 'La Responsabilidad se agrego correctamente.');
            }
            redirect('lista-responsabilidades');
        }
    }

    public function edit_responsabilidad($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Responsabilidad no valida');
            redirect('lista-responsabilidades');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-responsabilidades');
        }
        $responsabilidad = $this->Responsabilidad_model->get($id);
        if (!count($responsabilidad) == 1) {
            $this->session->set_flashdata('message_error', 'Responsabilidad no encontrada');
            redirect('lista-responsabilidades');
        }
        $data = new stdClass();
        $data->tipo_responsabilidad = $this->TipoResponsabilidad_model->get(FALSE,1);
        $data->content_view = "responsabilidad/dash_form_edit_responsabilidad";
        $data->title = APP_NAME . "::Editar Responsabilidad";
        $data->active = "cpntes";
        $data->maintitle = "Responsabilidad";
        $data->maindescription = "Editar Responsabilidad";

        $data->responsabilidad = $responsabilidad;

        $this->load->view('dashboard', $data);
    }

    public function update_responsabilidad() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_responsabilidad') == FALSE) {
            $this->edit_responsabilidad($id);
        } else {
            $responsabilidad_name = $this->input->post('responsabilidad_name');

            $update = [
                "Descripcion" => $responsabilidad_name
            ];

            $rol_id = $this->Responsabilidad_model->update($id, $update);
            if ($rol_id > 0) {
                $this->session->set_flashdata('message_success', 'La Responsabilidad se actualizo correctamente.');
            }
            redirect('lista-responsabilidades');
        }
    }

    public function delete_responsabilidad($id = FALSE) {
        $perfil = $this->Responsabilidad_model->get($id);
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'responsabilidad no valida');
            redirect('lista-responsabilidades');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-responsabilidades');
        }

        if (!count($perfil) == 1) {
            $this->session->set_flashdata('message_error', 'Responsabilidad no encontrada');
            redirect('lista-responsabilidades');
        }

        $num_rows = $this->Responsabilidad_model->update($id, ['EstadoID' => 3]);
        if ($num_rows > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'La Responsabilidad se ha eliminado.');
            redirect('lista-responsabilidades');
        }
    }
}