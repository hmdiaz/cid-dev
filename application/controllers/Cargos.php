<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cargos
 *
 * @author furbox
 */
class Cargos extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('Cargos_model');
        $this->load->model('Area_model');
    }

    public function list_cargos() {
        $data = new stdClass();
        $data->content_view = "cargos/dash_list_cargos";
        $data->title = APP_NAME . "::Lista Cargos";
        $data->active = "cpntes";
        $data->maintitle = "Cargos";
        $data->maindescription = "Lista de Cargos";
        $data->cargos = $this->create_cargos_table();
        $this->load->view('dashboard', $data);
    }

    public function form_new_cargo() {
        $data = new stdClass();
        $data->content_view = "cargos/dash_form_add_cargo";
        $data->title = APP_NAME . "::Nuevo Cargo";
        $data->active = "cpntes";
        $data->maintitle = "Cargos";
        $data->maindescription = "Crear Nuevo Cargo";
        $data->areas = $this->Area_model->getAreasActivas();
        $this->load->view('dashboard', $data);
    }

    public function add_cargo() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_cargo') == FALSE) {
            $this->form_new_cargo();
        } else {

            $cargo_name = $this->input->post('cargo_name');
            $cargo_status = $this->input->post('cargo_status');
            $cargo_area = $this->input->post('cargo_area');
            $insert = [
                "NombreCargo" => $cargo_name,
                "AreaID" => $cargo_area,
                "EstadoID" => $cargo_status
            ];

            $id = $this->Cargos_model->insert($insert);
            if ($id > 0) {
                $this->session->set_flashdata('message_success', 'El Cargo se agrego correctamente.');
                redirect('lista-cargos');
            }
        }
    }

    public function create_cargos_table() {
        $cargos = $this->Cargos_model->get_pagination();

        $cargos_table = "";

        if (count($cargos) > 0) {
            foreach ($cargos as $key => $value) {
                $area = $this->Area_model->get($value->AreaID,FALSE);
                $links = "";

                if ($value->EstadoID == 1) {
                    $status = "<span class='label label-success'>Activo</span>";
                    $links .= '<a class="btn btn-warning btn-xs btn-o" href="' . base_url('desactivar-cargo') . '/' . $value->cargoID . '">
                                    <i class="fa fa-ban"></i> Desactivar
                                </a>';
                }
                if ($value->EstadoID == 2) {
                    $status = "<span class='label label-warning'>Inactivo</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-cargo') . '/' . $value->cargoID . '">
                                    <i class="fa fa-ban"></i> Activar
                                </a>';
                }
                if ($value->EstadoID == 3) {
                    $status = "<span class='label label-danger'>Eliminado</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-cargo') . '/' . $value->cargoID . '">
                                    <i class="fa fa-check"></i> Activar
                                </a>';
                }

                $cargos_table .= "<tr>";
                $cargos_table .= "<td>{$value->nombreCargo}</td>";
                $cargos_table .= "<td>{$area->NombreProyecto}</td>";
                $cargos_table .= "<td>{$status}</td>";
                $cargos_table .= "<td>
                                    <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-cargo') . "/" . $value->cargoID . "'>
                                        <i class='fa fa-edit'></i> Editar
                                    </a>
                                    " . $links . "
                                    <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-cargo') . "/" . $value->cargoID . "'>
                                        <i class='fa fa-trash'></i> Eliminar
                                    </a>
                                </td>";
                $cargos_table .= "</tr>";
            }
        } else {
            $cargos_table .= "<tr><td colspan='3'>Sin Datos</td></tr>";
        }

        return $cargos_table;
    }

    public function edit_cargo($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Cargo no valido');
            redirect('lista-cargos');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-cargos');
        }
        $cargo = $this->Cargos_model->get($id);
        if (!count($cargo) == 1) {
            $this->session->set_flashdata('message_error', 'Cargo no encontrado');
            redirect('lista-cargos');
        }
        $data = new stdClass();
        $data->content_view = "cargos/dash_form_edit_cargo";
        $data->title = APP_NAME . "::Editar Cargo";
        $data->active = "cpntes";
        $data->maintitle = "Cargos";
        $data->maindescription = "Editar Cargo";

        $data->areas = $this->Area_model->getAreasActivas();
        $data->cargo = $cargo;

        $this->load->view('dashboard', $data);
    }

    public function update_cargo() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_cargo') == FALSE) {
            $this->edit_cargo($id);
        } else {

            $cargo_name = $this->input->post('cargo_name');
            $cargo_area = $this->input->post('cargo_area');
            $update = [
                "nombreCargo" => $cargo_name,
                "AreaID" => $cargo_area
            ];

            $id = $this->Cargos_model->update($id, $update);
            if ($id > 0) {
                $this->session->set_flashdata('message_success', 'El Cargo se actualizo correctamente.');
                redirect('lista-cargos');
            }
        }
    }

    public function active_cargo($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Cargo no valido');
            redirect('lista-cargos');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-cargos');
        }
        $cargo = $this->Cargos_model->get($id);
        if (!count($cargo) == 1) {
            $this->session->set_flashdata('message_error', 'Cargo no encontrado');
            redirect('lista-cargos');
        }
        $update = [
            'EstadoID' => 1
        ];
        $id = $this->Cargos_model->update($id, $update);
        if ($id > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Cargo se ha Activado.');
            redirect('lista-cargos');
        }
    }

    public function unactive_cargo() {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Cargo no valido');
            redirect('lista-cargos');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-cargos');
        }
        $cargo = $this->Cargos_model->get($id);
        if (!count($cargo) == 1) {
            $this->session->set_flashdata('message_error', 'Cargo no encontrado');
            redirect('lista-cargos');
        }
        $update = [
            'EstadoID' => 2
        ];
        $id = $this->Cargos_model->update($id, $update);
        if ($id > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Cargo se ha Desactivado.');
            redirect('lista-cargos');
        }
    }

    public function delete_cargo($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Cargo no valido');
            redirect('lista-cargos');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-cargos');
        }
        $cargo = $this->Cargos_model->get($id);
        if (!count($cargo) == 1) {
            $this->session->set_flashdata('message_error', 'Cargo no encontrado');
            redirect('lista-cargos');
        }
        $update = [
            'EstadoID' => 3
        ];
        $id_user = $this->Cargos_model->update($id, $update);
        if ($id_user > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Cargo se ha eliminado.');
            redirect('lista-cargos');
        }
    }

}
