<?php

class Convocatoria extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model([
            'Projects_model',
            'CatalogoPerfil_model',
            'Perfil_model',
            'ComponenteCompetencia_model',
            'Responsabilidad_model',
            'MunicipiosProyecto_model',
            'MunicipiosPerfil_model',
            'Aprobaciones_model',
            'Requisicion_model',
            'TipoContrato_model',
            'Motivo_model',
            'DatosUsuario_model',
            'MunicipiosRequisicion_model',
            'Convocatoria_model',
            'EtapaConvocatoria_model',
            'Etapa_model',
            'PostulantesConvocatorias_model',
            'ResponsabilidadPerfil_model',
            'PruebasSeleccion_model',
            'Pruebasconfig_model',
            'Users_model'
        ]);

        $this->load->library(['Auth', 'Sendmail']);
    }

    public function dash_new_form_convocatoria($idReq) {
        $this->auth->is_session_start();
        $data = new stdClass();
        $convocatoria = $this->Convocatoria_model->getDatosConvocatoria($idReq);
        $idPerfil = $convocatoria->PerfilID;
        $data->idPerfil = $idPerfil;
        $data->convocatoria = $convocatoria;
        //$data->tipos_contrato = $this->TipoContrato_model->get(FALSE, 1);
        //$data->motivos = $this->Motivo_model->get(FALSE, 1);
        $data->project = $this->Projects_model->get($idPerfil, FALSE);
        $data->etapas = $this->Etapa_model->get(FALSE, 1);
        $data->itemsCompetencias = $this->Perfil_model->getItemsCatalogoPerfil($idPerfil);
        $data->responsabilidades = $this->ResponsabilidadPerfil_model->getResponsabilidadesPerfil($idPerfil);
        $data->datos_usuario = $this->DatosUsuario_model->get($_SESSION['id_user']);
        //$data->departamentos = $this->MunicipiosRequisicion_model->getGetDepartamentoPerfil($idPerfil);
        $data->perfil = $this->Perfil_model->getPerfil($idPerfil);
        $data->muns = $this->MunicipiosRequisicion_model->getMunicipiosRequisicion($idReq);
        $data->content_view = "convocatoria/dash_new_form_convocatoria";
        $data->title = APP_NAME . "::Convocatorias";
        $data->active = "projects";
        $data->maintitle = "Creación de Convocatorias";
        $data->maindescription = "Creación de Convocatorias";

        $this->load->view('dashboard', $data);
    }

    public function dash_add_convocatoria() {
        $this->auth->is_session_start();
        $id_requisicion = $this->input->post('idReq');
        $fechaCreacion = date('Y-m-d h:i:s');
        $insert = [
            'RequisicionID' => $id_requisicion,
            'FechaCreacion' => $fechaCreacion,
        ];

        $conv_id = $this->Convocatoria_model->insert($insert);

        for ($j = 0; $j < count($this->input->post('chk_activar_etapa')); $j++) {

            $data = array(
                'EtapaID' => $this->input->post('chk_activar_etapa')[$j],
                'ConvocatoriaID' => $conv_id,
                'FechaInicio' => $this->input->post('fecha_inicio_etapa')[$j],
                'FechaFin' => $this->input->post('fecha_fin_etapa')[$j],
                'FechaCreacion' => $fechaCreacion
            );

            $this->EtapaConvocatoria_model->insert($data);
        }

        if ($conv_id > 0) {
            $this->session->set_flashdata('message_success', 'La convocatoria se creó satisfactoriamente.');
            redirect('lista-proyectos');
        }
    }

    public function dash_list_convocatorias($idReq) {
        $this->auth->is_session_start();
        $data = new stdClass();
        $data->content_view = "convocatoria/dash_list_convocatorias";
        $data->title = APP_NAME . "::Lista de Convocatorias";
        $data->active = "projects";
        $data->maintitle = "Convocatorias";
        $data->maindescription = "Lista de Convocatorias";
        $data->convocatorias = $this->list_convocatorias($idReq);
        $this->load->view('dashboard', $data);
    }

    public function list_convocatorias($idReq) {
        $this->auth->is_session_start();
        $convocatorias = $this->Convocatoria_model->getListaConvocatorias_x_Requisicion($idReq);
        $convocatorias_table = "";

        setlocale(LC_MONETARY, 'es_ES');
        if (count($convocatorias) > 0) {
            foreach ($convocatorias as $key => $value) {
                $convocatorias_table .= "<tr>";
                $convocatorias_table .= "<td>{$value->NombreCargo}</td>";
                $convocatorias_table .= "<td>{$value->NombreTipoContrato}</td>";
                $convocatorias_table .= "<td>$ " . number_format($value->SalarioAsignado) . "</td>";
                $convocatorias_table .= "<td>{$value->CantidadPersonasSolicitadas}</td>";
                $convocatorias_table .= "<td>
                                        <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-convocatoria') . "/" . $value->ConvocatoriaID . "'>
                                            <i class='fa fa-edit'></i> Editar
                                        </a>
                                        <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-convocatoria') . "/" . $value->ConvocatoriaID . "'>
                                            <i class='fa fa-trash'></i> Eliminar
                                        </a>
                                </td>";
                $convocatorias_table .= "</tr>";
            }
        }
        return $convocatorias_table;
    }

    public function dash_edit_convocatoria($id) {
        $this->auth->is_session_start();
        $data = new stdClass();
        $convocatoria = $this->Convocatoria_model->getConvocatoria($id);
        $etapas_x_convocatoria = $this->EtapaConvocatoria_model->getEtapas_x_Convocatoria($id);
        $data->convocatoria = $convocatoria;
        $data->etapas_x_convocatoria = $etapas_x_convocatoria;
        $data->tipos_contrato = $this->TipoContrato_model->get(FALSE, 1);
        $data->muns = $this->MunicipiosRequisicion_model->getMunicipiosRequisicion($convocatoria->RequisicionID);
        $data->itemsCompetencias = $this->Perfil_model->getItemsCatalogoPerfil($convocatoria->PerfilID);
        $data->responsabilidades = $this->ResponsabilidadPerfil_model->getResponsabilidadesPerfil($convocatoria->PerfilID);
        $data->content_view = "convocatoria/dash_edit_form_convocatoria";
        $data->title = APP_NAME . "::Editar Convocatoria";
        $data->active = "projects";
        $data->maintitle = "Convocatoria";
        $data->maindescription = "Editar Convocatoria";

        $this->load->view('dashboard', $data);
    }

    public function dash_update_convocatoria() {
        $etapas = [];

        for ($l = 0; $l < count($this->input->post('hdEtapas')); $l++) {
            $activo = FALSE;
            for ($m = 0; $m < count($this->input->post('chk_activar_etapa')); $m++) {
                if ($this->input->post('chk_activar_etapa')[$m] == $this->input->post('hdEtapas')[$l]) {
                    $etapas[$l] = $this->input->post('chk_activar_etapa')[$m];
                    $activo = TRUE;
                }
            }
            if (!$activo) {
                $etapas[$l] = NULL;
            }
        }


        $this->auth->is_session_start();
        $id_conv = $this->input->post('idConvocatoria');
        $cnt = 0;
        $fechaActual = date('Y-m-d h:i:s');
        $data = array(
        );

        $etapas_c1 = $this->EtapaConvocatoria_model->getEtapas_Existentes_x_Convocatoria($id_conv);


        if (count($etapas_c1) > 0) {
            for ($i = 0; $i < count($etapas_c1); $i++) {
                $eliminar = TRUE;
                for ($k = 0; $k < count($etapas); $k++) {
                    if (($etapas_c1[$i]->EtapaID == $etapas[$k]) && $etapas_c1[$i]->ConvocatoriaID == $id_conv) {
                        $eliminar = FALSE;
                    }
                }
                if ($eliminar) {
                    $this->EtapaConvocatoria_model->delete($etapas_c1[$i]->EtapaConvocatoriaID);
                }
            }
        }

        $etapas_c2 = $this->EtapaConvocatoria_model->getEtapas_Existentes_x_Convocatoria($id_conv);
        for ($k = 0; $k < count($etapas); $k++) {
            if (count($etapas_c2) > 0) {
                $exist = FALSE;
                for ($i = 0; $i < count($etapas_c2); $i++) {
                    if ($etapas[$k]) {
                        if (($etapas_c2[$i]->EtapaID == $etapas[$k]) && $etapas_c2[$i]->ConvocatoriaID == $id_conv) {
                            $exist = TRUE;
                            $data = array(
                                'ConvocatoriaID' => $id_conv,
                                'FechaInicio' => $this->input->post('fecha_inicio_etapa')[$k],
                                'FechaFin' => $this->input->post('fecha_fin_etapa')[$k],
                                'FechaUltimaActualizacion' => $fechaActual
                            );

                            $this->EtapaConvocatoria_model->update($etapas_c2[$i]->EtapaConvocatoriaID, $data);
                            $cnt++;
                        }
                    }
                }

                if ($exist == FALSE && $etapas[$k]) {
                    $data = array(
                        'EtapaID' => $etapas[$k],
                        'ConvocatoriaID' => $id_conv,
                        'FechaInicio' => $this->input->post('fecha_inicio_etapa')[$k],
                        'FechaFin' => $this->input->post('fecha_fin_etapa')[$k],
                        'FechaCreacion' => $fechaActual
                    );

                    $this->EtapaConvocatoria_model->insert($data);
                    $cnt++;
                }
            } else {
                $data = array(
                    'EtapaID' => $etapas[$k],
                    'ConvocatoriaID' => $id_conv,
                    'FechaInicio' => $this->input->post('fecha_inicio_etapa')[$k],
                    'FechaFin' => $this->input->post('fecha_fin_etapa')[$k],
                    'FechaCreacion' => $fechaActual
                );

                $this->EtapaConvocatoria_model->insert($data);
                $cnt++;
            }
        }

        if ($cnt > 0) {
            $convocatoria = $this->Convocatoria_model->get($id_conv);
            $this->session->set_flashdata('message_success', 'La convocatoria se actualizó satisfactoriamente.');
            redirect('lista-convocatorias/' . $convocatoria->RequisicionID);
        }
    }

    public function dash_list_convocatorias_activas() {
        $this->auth->is_session_start();
        $data = new stdClass();
        $data->content_view = "convocatoria/dash_list_convocatorias_activas";
        $data->title = APP_NAME . "::Lista de Convocatorias";
        $data->active = "workoffers";
        $data->maintitle = "Convocatorias";
        $data->maindescription = "Lista de Convocatorias";
        $data->convocatorias = $this->Convocatoria_model->getListaConvocatorias();
        $data->postuladas = $this->PostulantesConvocatorias_model->getConvocatoriasPostuladas($_SESSION['id_user']);
        $this->load->view('dashboard_aspirante', $data);
    }

    public function dash_list_all_convocatorias() {
        $this->auth->is_session_start();
        $data = new stdClass();
        $data->content_view = "convocatoria/dash_list_all_convocatorias";
        $data->title = APP_NAME . "::Lista de Convocatorias";
        $data->active = "convocatorias";
        $data->maintitle = "Convocatorias";
        $data->maindescription = "Lista de Convocatorias";
        $data->convocatorias = $this->list_all_convocatorias();
        $this->load->view('dashboard', $data);
    }

    public function add_user_convocartoria() {
        if (!@$_SESSION['is_logged_in']) {
            $this->session->set_flashdata('message_error', 'Necesitas iniciar session.');
            redirect('login-ext');
        }
        $id_convocatoria = $this->uri->segment(2);
        $convocatoria = $this->Convocatoria_model->getConvocatoriaEtapa4($id_convocatoria);

        if (count($convocatoria) == 1) {

//            if ($fecha >= strtotime($convocatoria->FechaInicio) && $fecha <= strtotime($convocatoria->FechaInicio)) {
            $cp = $this->PostulantesConvocatorias_model->getPostulada($_SESSION['id_user'], $id_convocatoria);

            if (count($cp) == 0) {
                $postulantes = $this->Users_model->getUsuarioPostulante($_SESSION['id_user']);
                $coincidencias = $this->postulante_match_convocatoria($postulantes, $convocatoria->ConvocatoriaID);                
                
                $cont = 0;
                foreach ($coincidencias as $c) {
                    
                    $detalles = $c['detalles'];
                    $match_info_acad = $detalles['match_info_acad'];
                    foreach ($match_info_acad as $mia) {
                        if ($mia['Coincide'] == 'Si') {
                            $cont++;
                        }
                    }
                }
                if ($cont == 0) {
                    $this->session->set_flashdata('message_error', 'Para aplicar para esta convocatoria es necesario cumplas almenos uno de los requisitos de Nivel Educativo.');
                    redirect('ofertas-de-trabajo');
                }
                echo "<pre>";
                print_r($coincidencias);
                exit;
                $insert = [
                    "PostulanteID" => $_SESSION['id_user'],
                    "convocatoriaID" => $id_convocatoria
                ];

                $success = $this->PostulantesConvocatorias_model->insert($insert);
                if ($success > 0) {
                    $this->session->set_flashdata('message_success', 'Te Postulaste Correctamente a la convocatoria.');
                    redirect(base_url('ver-coincidencias/' . $id_convocatoria));
                }
            } else {
                $this->session->set_flashdata('message_error', 'Ya has aplicado para esta convocatoria.');
                redirect('ofertas-de-trabajo');
            }
        } else {
            $this->session->set_flashdata('message_error', 'No existe convocatoria.');
            redirect('ofertas-de-trabajo');
        }
    }

    public function view_postulantes() {
        $this->auth->is_session_start();
        $id_convocatoria = $this->uri->segment(2);
        $convocatoria = $this->Convocatoria_model->get($id_convocatoria);
        if (count($convocatoria) == 1) {
            $postulantes = $this->PostulantesConvocatorias_model->getPostulantesbyConvocatoria($convocatoria->ConvocatoriaID);
            $coincidencias = $this->postulante_match_convocatoria($postulantes, $convocatoria->ConvocatoriaID);
            $data = new stdClass();
            $data->content_view = "convocatoria/dash_list_postulantes";
            $data->title = APP_NAME . "::Lista de Postulantes";
            $data->active = "convocatorias";
            $data->maintitle = "Convocatorias";
            $data->maindescription = "Lista de Postulantes";
            $data->postulantes = $postulantes;
            $data->coincidencias = $coincidencias;
            $data->id_convocatoria = $id_convocatoria;
            $this->load->view('dashboard', $data);
        } else {
            $this->session->set_flashdata('message_error', 'No existe convocatoria.');
            redirect('listar-convocatorias');
        }
    }

    public function postulante_match_convocatoria($postulantes, $id_convocatoria) {
        $this->auth->is_session_start();
        $this->load->model([
            "HvInfAcad_model",
            "CpntesCompe_model",
            "HvExpLab_model",
            "HvExpLabExpGral_model",
            "HvEstComp_model",
            "HvDescHab_model",
            "ComponenteCompetencia_model"
        ]);
        $data = [];
        $total = 0;
        $convocatoria = $this->Convocatoria_model->getConvocatoria($id_convocatoria);
        $componentesComp = $this->ComponenteCompetencia_model->getbyPerfil($convocatoria->PerfilID);
        $total = count($componentesComp);
        $convocatoria_info_academica = [];
        $convocatoria_est_comp = [];
        $convocatoria_exp_gral = [];
        $convocatoria_desc_hab = [];
        foreach ($componentesComp as $cc) {

            if ($cc->TipoCatalogoID == 1) {
                $convocatoria_info_academica[] = $cc;
            }
            if ($cc->TipoCatalogoID == 2) {
                $convocatoria_est_comp[] = $cc;
            }
            if ($cc->TipoCatalogoID == 3) {
                $convocatoria_exp_gral[] = $cc;
            }
            if ($cc->TipoCatalogoID == 4) {
                $convocatoria_desc_hab[] = $cc;
            }
        }

        foreach ($postulantes as $postulante) {
            $postulante_info_academica = $this->HvInfAcad_model->getByUser($postulante->UsuarioID);
            $postulante_exp_gral = $this->HvExpLab_model->getExpGralByUser($postulante->UsuarioID);
            $postulante_estudios_complementarios = $this->HvEstComp_model->getByUser($postulante->UsuarioID);
            $postulante_descripcion_habilidades = $this->HvDescHab_model->getDescHabByUser($postulante->UsuarioID);

            $cont_info_academica = 0;
            $cont_Exp_gral = 0;
            $cont_estudios_complementarios = 0;
            $cont_desc_hab = 0;

            $match_info_academica = [];
            $match_est_comp = [];
            $match_exp_gral = [];
            $match_dech_hab = [];

            foreach ($convocatoria_info_academica as $cia) {
                $bandx = FALSE;
                foreach ($postulante_info_academica as $pia) {                    
                    if ($cia->CatalogoPerfilID == $pia->CatalogoPerfilID) {
                        $cont_info_academica++;
                        $bandx = TRUE;
                    }
                }
                $match_info_academica[] = [
                    "Competencia" => $cia->tipocat,
                    "Nombre_coincidencia" => $cia->Descripcion,
                    "Coincide" => $bandx == TRUE ? 'Si' : 'No',
                    "Peso" => $bandx == TRUE ? $cia->Peso : 0,
                    "Coincidecia_porcentaje" => $bandx == TRUE ? 100 : 0
                ];
            }

            foreach ($convocatoria_est_comp as $cec) {
                $bandy = FALSE;
                foreach ($postulante_estudios_complementarios as $pec) {
                    if ($cec->CatalogoPerfilID == $pec->CatalogoPerfilID) {
                        $cont_estudios_complementarios++;
                        $bandy = TRUE;
                    }
                }
                $match_est_comp[] = [
                    "Competencia" => $cec->tipocat,
                    "Nombre_coincidencia" => $cec->Descripcion,
                    "Coincide" => $bandy == TRUE ? 'Si' : 'No',
                    "Peso" => $bandy == TRUE ? $cec->Peso : 0,
                    "Coincidecia_porcentaje" => $bandy == TRUE ? 100 : 0
                ];
            }

            foreach ($convocatoria_exp_gral as $ceg) {
                $bandz = FALSE;
                $peso_ceg = 0;
                $porcentaje = 0;
                foreach ($postulante_exp_gral as $peg) {
                    if ($ceg->CatalogoPerfilID == $peg->CatalogoPerfilID) {
                        $cont_Exp_gral++;
                        $bandz = TRUE;
                        $postulante_exp = $peg->Experiencia;
                        $convocatoria_exp = $ceg->TiempoExperiencia;
                        if ($convocatoria_exp > $postulante_exp) {
                            $resp = $convocatoria_exp - $postulante_exp;
                            $porcentaje = ($resp * 100) / $convocatoria_exp;                            
                            $peso_ceg = ($porcentaje/100) * $ceg->Peso;
                        } else {
                            $porcentaje = 100;
                            $peso_ceg = $ceg->Peso;
                        }
                    } else {
                        $peso_ceg = 0;
                    }
                }

                $match_exp_gral[] = [
                    "Competencia" => $ceg->tipocat,
                    "Nombre_coincidencia" => $ceg->Descripcion,
                    "Coincide" => $bandz == TRUE ? 'Si' : 'No',
                    "Peso" => $peso_ceg,
                    "Coincidecia_porcentaje" => $porcentaje
                ];
            }

            foreach ($convocatoria_desc_hab as $cdh) {
                $bandh = FALSE;
                foreach ($postulante_descripcion_habilidades as $pdh) {

                    if ($cdh->CatalogoPerfilID == $pdh->CatalogoPerfilID) {
                        $cont_desc_hab++;
                        $bandh = TRUE;
                    }
                }

                $match_dech_hab[] = [
                    "Competencia" => $cdh->tipocat,
                    "Nombre_coincidencia" => $cdh->Descripcion,
                    "Coincide" => $bandh == TRUE ? 'Si' : 'No',
                    "Peso" => $bandh == TRUE ? $cdh->Peso : 0,
                    "Coincidecia_porcentaje" => $bandh == TRUE ? 100 : 0
                ];
            }

            $data[] = [
                "id_user" => $postulante->UsuarioID,
                "total" => $total,
                "coincidencias_info_acad" => $cont_info_academica,
                "coincidencias_est_comp" => $cont_estudios_complementarios,
                "coincidencias_exp_gral" => $cont_Exp_gral,
                "coincidencias_desc_hab" => $cont_desc_hab,
                "detalles" => [
                    "match_info_acad" => $match_info_academica,
                    "match_est_comp" => $match_est_comp,
                    "match_exp_gral" => $match_exp_gral,
                    "match_desc_hab" => $match_dech_hab
                ]
            ];
        }
        return $data;
    }

    public function list_all_convocatorias() {
        $this->auth->is_session_start();
        $convocatorias = $this->Convocatoria_model->getListaConvocatorias();
        $convocatorias_table = "";
        setlocale(LC_MONETARY, 'es_ES');
        if (count($convocatorias) > 0) {
            foreach ($convocatorias as $key => $value) {
                $postulantes = $this->PostulantesConvocatorias_model->getPostulantesbyConvocatoria($value->ConvocatoriaID);
                $num_postulantes = count($postulantes);
                $links = "";
                if ($value->status == 1) {
                    $status = "<span class='label label-success'>Activo</span>";
                    $links .= '<a class="btn btn-warning btn-xs btn-o" href="' . base_url('desactivar-convocatoria') . '/' . $value->ConvocatoriaID . '">
                                    <i class="fa fa-ban"></i> Desactivar
                                </a>';
                }
                if ($value->status == 2) {
                    $status = "<span class='label label-warning'>Inactivo</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-convocatoria') . '/' . $value->ConvocatoriaID . '">
                                    <i class="fa fa-check"></i> Activar
                                </a>';
                }
                $convocatorias_table .= "<tr>";
                $convocatorias_table .= "<td>{$value->ProyectoID}</td>";
                $convocatorias_table .= "<td>{$value->NombreCargo}</td>";
                $convocatorias_table .= "<td>{$num_postulantes}</td>";
                $convocatorias_table .= "<td>{$value->CantidadPersonasSolicitadas}</td>";
                $convocatorias_table .= "<td>{$status}</td>";
                $convocatorias_table .= "<td>
                                        <a class='btn btn-primary btn-xs btn-o' href='" . base_url('etapas') . "/" . $value->ConvocatoriaID . "'>
                                            <i class='fa fa-eye'></i> Etapas
                                        </a>
                                        " . $links . "
                                </td>";
                $convocatorias_table .= "</tr>";
            }
        }
        return $convocatorias_table;
    }

    public function view_coincidencias() {
        $this->auth->is_session_start();
        $id_convocatoria = $this->uri->segment(2);
        if ($this->uri->segment(3)) {
            $id_postulante = $this->uri->segment(3);
        } else {
            $id_postulante = $_SESSION['id_user'];
        }

        $postulantes = $this->PostulantesConvocatorias_model->getPostulantesbyConvocatoria($id_convocatoria);
        $coincidencias = $this->postulante_match_convocatoria($postulantes, $id_convocatoria);
        echo '<pre>';
        print_r($coincidencias);
        exit;
        foreach ($coincidencias as $coincidencia) {
            if ($coincidencia['id_user'] == $id_postulante) {
                $match = $coincidencia['detalles'];
            }
        }
        $data = new stdClass();
        $data->content_view = "convocatoria/dash_list_coincidencias";
        $data->title = APP_NAME . "::Lista de Coincidencias";
        $data->active = "convocatorias";
        $data->maintitle = "Modulo de Convocatorias";
        $data->maindescription = "Lista de Coincidencias";
        $data->match = $match;
        if ($this->uri->segment(3)) {
            $this->load->view('dashboard', $data);
        } else {
            $this->load->view('dashboard_aspirante', $data);
        }
    }

    function porcentaje($total, $parte, $redondear = 2) {
        $this->auth->is_session_start();
        return round($parte / $total * 100, $redondear);
    }

    public function etapas_convocatorias() {
        $this->auth->is_session_start();
        $id_convocatoria = $this->uri->segment(2);
        $etapas_convocatorias = $this->EtapaConvocatoria_model->getEtapas_Existentes_x_Convocatoria($id_convocatoria);
        $data = new stdClass();
        $data->content_view = "convocatoria/dash_view_etapas";
        $data->title = APP_NAME . "::Etapas de Convocatoria";
        $data->active = "convocatorias";
        $data->maintitle = "Modulo de Convocatorias";
        $data->maindescription = "Etapas de Convocatoria";
        $data->etapas_convocatorias = $etapas_convocatorias;
        $data->id_convocatoria = $id_convocatoria;
        $this->load->view('dashboard', $data);
    }

    public function seleccion() {
        $this->auth->is_session_start();
        $id_etapa_convocatoria = $this->uri->segment(2);
        $etapa = $this->EtapaConvocatoria_model->get($id_etapa_convocatoria);
        $id_convocatoria = $etapa->ConvocatoriaID;
        $id_etapa = $etapa->EtapaID;
        $convocatoria = $this->Convocatoria_model->get($id_convocatoria);
        if (count($convocatoria) == 1) {
            $data = new stdClass();
            if ($id_etapa == 1) {
                $postulantes = $this->PostulantesConvocatorias_model->getPostulantesbyConvocatoria($convocatoria->ConvocatoriaID);
                $coincidencias = $this->postulante_match_convocatoria($postulantes, $convocatoria->ConvocatoriaID);
                $data->postulantes = $postulantes;
                $data->coincidencias = $coincidencias;
                $data->id_etapa = $id_etapa_convocatoria;
                $data->id_convocatoria = $id_convocatoria;
                $data->content_view = "convocatoria/form_seleccion";
            }

            if ($id_etapa == 2) {
                $this->load->model("Pruebasconfig_model");
                $this->load->model("PruebasSeleccionResult_model");
                $etapa_convocatoria = $this->EtapaConvocatoria_model->getByIdConvocatoriaAndIdEtapa(1, $id_convocatoria);
                if (count($etapa_convocatoria) == 1) {
                    $postulantes = $this->PruebasSeleccion_model->getbyUsuariosEtapaConvcatoria($etapa_convocatoria->EtapaConvocatoriaID);
                    $data->postulantes = $postulantes;
                    $data->id_etapa = $id_etapa_convocatoria;
                    $data->etapa_id = $id_etapa;
                    $data->id_convocatoria = $id_convocatoria;
                    $data->content_view = "convocatoria/form_seleccion_etapa";
                } else {
                    $postulantes = $this->PostulantesConvocatorias_model->getPostulantesbyConvocatoria($convocatoria->ConvocatoriaID);
                    $coincidencias = $this->postulante_match_convocatoria($postulantes, $convocatoria->ConvocatoriaID);
                    $data->postulantes = $postulantes;
                    $data->coincidencias = $coincidencias;
                    $data->id_etapa = $id_etapa_convocatoria;
                    $data->id_convocatoria = $id_convocatoria;
                    $data->content_view = "convocatoria/form_seleccion";
                }
            }

            if ($id_etapa == 3) {
                $this->load->model("Pruebasconfig_model");
                $this->load->model("PruebasSeleccionResult_model");
                $etapa_convocatoria = $this->EtapaConvocatoria_model->getByIdConvocatoriaAndIdEtapa(2, $id_convocatoria);
                if (count($etapa_convocatoria) == 1) {
                    $postulantes = $this->PruebasSeleccion_model->getbyUsuariosEtapaConvcatoria($etapa_convocatoria->EtapaConvocatoriaID);
                    $data->postulantes = $postulantes;
                    $data->id_etapa = $id_etapa_convocatoria;
                    $data->etapa_id = $id_etapa;
                    $data->id_convocatoria = $id_convocatoria;
                    $data->content_view = "convocatoria/form_seleccion_etapa";
                } else {
                    $etapa_convocatoria = $this->EtapaConvocatoria_model->getByIdConvocatoriaAndIdEtapa(1, $id_convocatoria);
                    if (count($etapa_convocatoria) == 1) {
                        $postulantes = $this->PruebasSeleccion_model->getbyUsuariosEtapaConvcatoria($etapa_convocatoria->EtapaConvocatoriaID);
                        $data->postulantes = $postulantes;
                        $data->id_etapa = $id_etapa_convocatoria;
                        $data->etapa_id = $id_etapa;
                        $data->id_convocatoria = $id_convocatoria;
                        $data->content_view = "convocatoria/form_seleccion_etapa";
                    } else {
                        $postulantes = $this->PostulantesConvocatorias_model->getPostulantesbyConvocatoria($convocatoria->ConvocatoriaID);
                        $coincidencias = $this->postulante_match_convocatoria($postulantes, $convocatoria->ConvocatoriaID);
                        $data->postulantes = $postulantes;
                        $data->coincidencias = $coincidencias;
                        $data->id_etapa = $id_etapa_convocatoria;
                        $data->id_convocatoria = $id_convocatoria;
                        $data->content_view = "convocatoria/form_seleccion";
                    }
                }
            }
            $data->title = APP_NAME . "::Etapas de Convocatoria";
            $data->active = "convocatorias";
            $data->maintitle = "Modulo de Convocatorias";
            $data->maindescription = "Etapas de Convocatoria";

            $this->load->view('dashboard', $data);
        } else {
            $this->session->set_flashdata('message_error', 'No existe convocatoria.');
            redirect('listar-convocatorias');
        }
    }

    public function add_seleccion() {
        $this->auth->is_session_start();
        $this->load->model('Users_model');
        $id_etapa_convocatoria = $this->input->post('id_etapa');
        $etapa = $this->EtapaConvocatoria_model->get($id_etapa_convocatoria);
        $cont = 0;
        foreach ($_POST as $key => $value) {
            if ($cont > 0) {
                if ($this->input->post($key) == 1) {

                    $insert = [
                        "EtapaConvocatoriaID" => $id_etapa_convocatoria,
                        "UsuarioID" => $key
                    ];
                    $id_prueba_seleccion = $this->PruebasSeleccion_model->insert($insert);
                    //test/id_prueba_seleccion
                    //enviar correo a usuario test/id_prueba_seleccion
                    //enviar correo a los que no pasaron
                    if (count($id_prueba_seleccion) > 0) {
                        $user = $this->Users_model->getUserInfo($key);
                        $email_a = $user->EmailUsuario;
                        $subject = "Selección";
                        $message = "";
                        $message .= "<p>Hola " . $user->Nombres . " " . $user->Apellidos . "</p>";
                        $message .= "<p>Ha sido elegido para la siguiente Etapa</p>";
                        $message .= "<p>Antes de Presentar la prueba le invitamos a <a href='" . base_url('login-ext') . "'>Iniciar Sesion</a>  y despues de clic en el enlace para presentarla</p>";
                        $message .= "<a href='" . base_url('test') . "/" . $id_prueba_seleccion . "'>Ir a la Prueba</a>";
                        $message .= "<br>Fecha de Inicio: " . $etapa->FechaInicio;
                        $message .= "<br>Fecha de Fin: " . $etapa->FechaFin;
                        $message .= "<p>Exitos!!!</p>";
                        $this->sendmail->sendMail($email_a, $subject, $message);
                    }
                }
            }
            $cont++;
        }
        $this->session->set_flashdata('message_success', 'Candidatos Seleccionados se agregaron correctamente.');
        redirect('etapas/' . $etapa->ConvocatoriaID);
    }

    public function ver_configuracion() {
        $this->auth->is_session_start();
        $EtapaConvocatoriaID = $this->uri->segment(2);
        $config = $this->Pruebasconfig_model->getConfigPruebaByConvocatoria($EtapaConvocatoriaID);
        $data = new stdClass();
        $data->content_view = "convocatoria/dash_view_config_etapas";
        $data->title = APP_NAME . "::Etapas de Convocatoria";
        $data->active = "convocatorias";
        $data->maintitle = "Modulo de Convocatorias";
        $data->maindescription = "Configuración de etapa de Convocatoria";
        $data->configuracion = $config;
        $this->load->view('dashboard', $data);
    }

    public function resultados_finales() {
        $this->auth->is_session_start();
        $this->load->model("PruebasSeleccionResult_model");
        $id_convocatoria = $this->uri->segment(2);
        //postulantes
        $postulantes = $this->PostulantesConvocatorias_model->getPostulantesbyConvocatoria($id_convocatoria);
        $coincidencias = $this->postulante_match_convocatoria($postulantes, $id_convocatoria);
        //etapas
        $etapas_convocatorias = $this->EtapaConvocatoria_model->getEtapas_Existentes_x_Convocatoria($id_convocatoria);

        $postulantes_finales = $this->EtapaConvocatoria_model->postulantes_finales($id_convocatoria);

        $data = new stdClass();
        $data->content_view = "convocatoria/dash_view_result_finales";
        $data->title = APP_NAME . "::Informe de Convocatoria";
        $data->active = "convocatorias";
        $data->maintitle = "Informe de Convocatoria";
        $data->maindescription = "Seleccion de postulantes";
        $data->id_convocatoria = $id_convocatoria;
        $data->etapas = $etapas_convocatorias;
        $data->coincidencias = $coincidencias;
        $data->postulantes_finales = $postulantes_finales;

        $this->load->view('dashboard', $data);
    }

    public function generar_pdf_hv() {
        $this->auth->is_session_start();
        $this->load->model([
            "Users_model",
            "HvHijos_model",
            "HvInfAcad_model",
            "HvExpLab_model",
            "HvEstComp_model",
            "HvRef_model",
            "HvDescHab_model",
            "HvExpLabExpGral_model"
        ]);
        $user_id = $this->uri->segment(2);
        $user = $this->Users_model->getUserInfo($user_id);
        $user_hijos = $this->HvHijos_model->getByUser($user_id);
        $user_info_academica = $this->HvInfAcad_model->getByUser($user_id);
        $user_exp_laboral = $this->HvExpLab_model->getExpUser($user_id);
        $user_estudios_complementarios = $this->HvEstComp_model->getByUser($user_id);
        $user_referencias_personales = $this->HvRef_model->getByUser($user_id, 1);
        $user_referencias_profesionales = $this->HvRef_model->getByUser($user_id, 2);
        $user_descripcion_habilidades = $this->HvDescHab_model->getByUser($user_id);
        $fecha = date('Y-m-d');
        $this->load->library('Pdf');
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator('CORPORACIÓN INFANCIA Y DESARROLLO');
        $pdf->SetAuthor('CORPORACIÓN INFANCIA Y DESARROLLO');
        $pdf->SetTitle('Hoja de Vida');
        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'HOJA DE VIDA', $fecha, array(0, 0, 0), array(0, 64, 128));
        $pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));

// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//relación utilizada para ajustar la conversión de los píxeles
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


// ---------------------------------------------------------
// establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

// Establecer el tipo de letra
//Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
// Helvetica para reducir el tamaño del archivo.
        $pdf->SetFont('helvetica', '', 12, '', true);

// Añadir una página
// Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage();

//fijar efecto de sombra en el texto
        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

// Establecemos el contenido para imprimir
        //preparamos y maquetamos el contenido a crear
        $html = '';
        $html .= '<table width="100%" border="1" cellpadding="4">'
                . '<tr>
                    <th align="center">Informacion Personal y Familiar</th>
                </tr>'
                . '</table><br><br>';
        $html .= '<table width="100%" border="1" cellpadding="4">'
                . '<tr>
                    <td colspan="3"> Nombre </td>
                    <td colspan="9"> ' . $user->Nombres . ' ' . $user->Apellidos . '</td>
                </tr>'
                . '<tr>
                    <td colspan="3"> Tipo de Identificacion </td>
                    <td colspan="3"> ' . $user->Nombre . '</td>
                    <td colspan="6"> <table border="1" cellpadding="4"><tr><td>' . $user->DocumentoIdentidad . '</td><td>' . $user->NombreMunDoc . '</td></tr><tr><td>No. de Indentificación </td><td> De </td></tr></table> </td>
                </tr>'
                . '<tr>
                    <td colspan="3"> Fecha Nacimiento </td>
                    <td colspan="2"> ' . $user->FechaNacimiento . '</td>
                    <td colspan="2"> Direccion </td>
                    <td colspan="5"> ' . $user->Direccion . '</td>
                </tr>'
                . '<tr>
                    <td colspan="3"> Departamento </td>
                    <td colspan="3"> ' . $user->NombreDepRes . '</td>
                    <td colspan="3"> Municipio </td>
                    <td colspan="3"> ' . $user->NombreMunRes . '</td>
                </tr>'
                . '<tr>
                    <td colspan="3"> Celular </td>
                    <td colspan="3"> ' . $user->Telefono . '</td>
                    <td colspan="3"> Telefono Fijo </td>
                    <td colspan="3"> ' . $user->Celular . '</td>
                </tr>'
                . '<tr>
                    <td colspan="3"> Correo Electronico </td>
                    <td colspan="4"> ' . $user->EmailUsuario . '</td>
                    <td colspan="2"> Estado Civil </td>
                    <td colspan="3"> ' . $user->NombreEstCivil . '</td>
                </tr>'
                . '<tr>
                    <td colspan="3"> No Tarjeta Profesional </td>
                    <td colspan="3"> ' . $user->NumeroTarjetaProfesional . '</td>
                    <td colspan="3"> Fecha de Expedicion </td>
                    <td colspan="3"> ' . $user->FechaExpTarjetaProfesional . '</td>
                </tr>'
                . '</table><br><br>';

        $html .= '<table width="100%" border="1" cellpadding="4">'
                . '<tr>
                    <th> Hijos Nombre Completo </th>
                    <th> Fecha de Nacimiento </th>
                    <th> Edad </th>
                </tr>';

        foreach ($user_hijos as $uh) {
            $birthdate = new DateTime($uh->FechaNacimiento);
            $hoy = new DateTime(date('Y-m-d'));

            $years = date_diff($birthdate, $hoy);
            $html .= '<tr>';
            $html .= '<td>' . $uh->NombreCompleto . '</td>';
            $html .= '<td>' . $uh->FechaNacimiento . '</td>';
            $html .= '<td>' . $years->format('%y Años') . '</td>';
            $html .= '</tr>';
        }

        $html .= '</table><br><br>';


        $html .= '<table width="100%" border="1" cellpadding="4">'
                . '<tr>
                    <th align="center">Información Academica</th>
                </tr>'
                . '</table><br><br>';
        $html .= '<table width="100%" border="1" cellpadding="4">'
                . '<tr>
                    <th> Fecha de Grado </th>
                    <th> Titulo Obtenido </th>
                    <th> Nombre de la Institucion Academica </th>
                    <th> Experiencia </th>
                </tr>';

        foreach ($user_info_academica as $uia) {
            $html .= '<tr>';
            $html .= '<td>' . $uia->FechaGrado . '</td>';
            $html .= '<td>' . $uia->Descripcion . '</td>';
            $html .= '<td>' . $uia->NombreInstitucion . '</td>';
            $html .= '<td>' . $uia->HvInfAcad_exp . ' Meses </td>';
            $html .= '</tr>';
        }

        $html .= '</table><br><br>';

        $html .= '<table width="100%" border="1" cellpadding="4">'
                . '<tr>
                    <th align="center">Experiencia Laboral</th>
                </tr>'
                . '</table><br><br>';
        $html .= '<table width="100%" border="1" cellpadding="4">';

        foreach ($user_exp_laboral as $uel) {
            $html .= '<tr>';
            $html .= '<td colspan="2"> Cargo </td>';
            $html .= '<td colspan="6">' . $uel->NombreCargo . '</td>';
            $html .= '<td colspan="2"> Inicio </td>';
            $html .= '<td colspan="2">' . $uel->FechaInicio . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td colspan="2"> Entidad </td>';
            $html .= '<td colspan="2">' . $uel->Nombre . '</td>';
            $html .= '<td colspan="2"> Telefono </td>';
            $html .= '<td colspan="2">' . $uel->Telefono . '</td>';
            $html .= '<td colspan="2"> Finalizacion </td>';
            $html .= '<td colspan="2">' . $uel->FechaFinalizacion . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td colspan="12"> Principales actividades desarrolladas:';
            $expgral = $this->HvExpLabExpGral_model->getByUser($uel->HvExpLabID);
            foreach ($expgral as $eg) {
                $html .= "<p>" . $eg->Descripcion . ", " . $eg->Experiencia . " Meses de Experiencia</p>";
            }
            $html .= '</td>';
            $html .= '</tr>';
        }

        $html .= '</table><br><br>';


        $html .= '<table width="100%" border="1" cellpadding="4">'
                . '<tr>
                    <th align="center">Estudios Complementarios</th>
                </tr>'
                . '</table><br><br>';
        $html .= '<table width="100%" border="1" cellpadding="4">'
                . '<tr>
                    <th> Fecha de Certificacion </th>
                    <th> Titulo Obtenido </th>
                    <th> Nombre de la Institucion Certificadora </th>
                    <th> Experiencia </th>
                </tr>';

        foreach ($user_estudios_complementarios as $uec) {
            $html .= '<tr>';
            $html .= '<td>' . $uec->FechaCertificacion . '</td>';
            $html .= '<td>' . $uec->Descripcion . '</td>';
            $html .= '<td>' . $uec->NombreInstitucion . '</td>';
            $html .= '<td>' . $uec->HvEstComp_exp . ' Meses </td>';
            $html .= '</tr>';
        }

        $html .= '</table><br><br>';


        $html .= '<table width="100%" border="1" cellpadding="4">'
                . '<tr>
                    <th align="center">Descripcion de Habilidades</th>
                </tr>'
                . '</table><br><br>';
        $html .= '<table width="100%" border="1" cellpadding="4">'
                . '<tr>
                    <th> Descripcion de habilidad </th>
                    <th> Experiencia </th>
                </tr>';

        foreach ($user_descripcion_habilidades as $dh) {
            $html .= '<tr>';
            $html .= '<td>' . $dh->Descripcion . '</td>';
            $html .= '<td>' . $dh->HvHab_experiencia . ' Meses </td>';
            $html .= '</tr>';
        }

        $html .= '</table><br><br>';


        $html .= '<table width="100%" border="1" cellpadding="4">'
                . '<tr>
                    <th align="center">Referencias Personales</th>
                </tr>'
                . '</table><br><br>';
        $html .= '<table width="100%" border="1" cellpadding="4">';

        foreach ($user_referencias_personales as $rp) {
            $html .= '<tr>';
            $html .= '<td> Nombre </td>';
            $html .= '<td>' . $rp->NombreReferencia . '</td>';
            $html .= '<td> Profesión/Ocupación </td>';
            $html .= '<td>' . $rp->ProfesionOcupacion . '</td>';
            $html .= '<td> Telefono </td>';
            $html .= '<td>' . $rp->Telefono . '</td>';
            $html .= '</tr>';
        }

        $html .= '</table><br><br>';

        $html .= '<table width="100%" border="1" cellpadding="4">'
                . '<tr>
                    <th align="center">Referencias Profesionales</th>
                </tr>'
                . '</table><br><br>';
        $html .= '<table width="100%" border="1" cellpadding="4">';

        foreach ($user_referencias_profesionales as $rpro) {
            $html .= '<tr>';
            $html .= '<td> Nombre </td>';
            $html .= '<td>' . $rpro->NombreReferencia . '</td>';
            $html .= '<td> Profesión/Ocupación </td>';
            $html .= '<td>' . $rpro->ProfesionOcupacion . '</td>';
            $html .= '<td> Telefono </td>';
            $html .= '<td>' . $rpro->Telefono . '</td>';
            $html .= '</tr>';
        }

        $html .= '</table><br><br>';

// Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

// ---------------------------------------------------------
// Cerrar el documento PDF y preparamos la salida
// Este método tiene varias opciones, consulte la documentación para más información.
        $nombre = 'HV_' . $user->UsuarioID . '_' . $user->Nombres . '_' . $user->Apellidos;
        $nombre_archivo = utf8_decode($nombre);
        $pdf->Output($nombre_archivo, 'I');
    }

    public function active_convocatoria($id = FALSE) {
        $this->auth->is_session_start();
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Convocatoria no valida');
            redirect('listar-convocatorias');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('listar-convocatorias');
        }
        $convocatoria = $this->Convocatoria_model->get($id);
        if (!count($convocatoria) == 1) {
            $this->session->set_flashdata('message_error', 'Convocatoria no encontrada');
            redirect('listar-convocatorias');
        }
        $update = [
            'EstadoID' => 1
        ];
        $success = $this->Convocatoria_model->update($id, $update);
        if ($success > 0) {
            $this->session->set_flashdata('message_success', 'LA Convocatoria se ha Activado.');
            redirect('listar-convocatorias');
        }
    }

    public function unactive_convocatoria() {
        $this->auth->is_session_start();
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Convocatoria no valida');
            redirect('listar-convocatorias');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('listar-convocatorias');
        }
        $convocatoria = $this->Convocatoria_model->get($id);
        if (!count($convocatoria) == 1) {
            $this->session->set_flashdata('message_error', 'Convocatoria no encontrada');
            redirect('listar-convocatorias');
        }
        $update = [
            'EstadoID' => 2
        ];
        $success = $this->Convocatoria_model->update($id, $update);
        if ($success > 0) {
            $this->session->set_flashdata('message_success', 'La Convocatoria se ha Desactivado.');
            redirect('listar-convocatorias');
        }
    }

}
