<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CpntesCompe
 *
 * @author furbox
 */
class CpntesCompe extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->library([
            'Auth'
        ]);
        $this->load->model("CpntesCompe_model");
        $this->load->model("CategoriasComponentes_model");
        $this->load->model("Grados_model");
        $this->auth->is_session_start();
    }

    //Nivel Educativo
    public function form_new_lvl_education() {
        $data = new stdClass();
        $data->content_view = "cpntescompe/dash_form_add_cpnte";
        $data->title = APP_NAME . "::Nuevo Componente de Competencias para Nivel Educativo";
        $data->active = "cpntes";
        $data->maintitle = "Nivel Educativo";
        $data->maindescription = "Nuevo Componente de Competencias para Nivel Educativo";
        $data->categorias = $this->CategoriasComponentes_model->getActives();
        $data->grados = $this->Grados_model->getActives();
        $data->cpnte_type = 1;
        $this->load->view('dashboard', $data);
    }

    public function list_lvl_education() {
        $data = new stdClass();
        $data->content_view = "cpntescompe/dash_list_cpnte";
        $data->title = APP_NAME . "::Lista Componente de Competencias para Nivel Educativo";
        $data->active = "cpntes";
        $data->maintitle = "Componente de Competencias";
        $data->maindescription = "Lista de Componentes de Competencias para Nivel Educativo";
        $data->cpntes = $this->create_cpntes_table(1);
        $data->tipo_componente = 1;
        $this->load->view('dashboard', $data);
    }

    //Nivel de Formacion
    public function form_new_lvl_formation() {

        $data = new stdClass();
        $data->content_view = "cpntescompe/dash_form_add_cpnte";
        $data->title = APP_NAME . "::Nuevo Componente de Competencias para Nivel de Formacion";
        $data->active = "cpntes";
        $data->maintitle = "Nivel de Formacion";
        $data->maindescription = "Nuevo Componente de Competencias para Nivel de Formacion";
        $data->categorias = $this->CategoriasComponentes_model->getActives();
        $data->cpnte_type = 2;
        $this->load->view('dashboard', $data);
    }

    public function list_lvl_formation() {
        $data = new stdClass();
        $data->content_view = "cpntescompe/dash_list_cpnte";
        $data->title = APP_NAME . "::Lista Componente de Competencias para Nivel de Formacion";
        $data->active = "cpntes";
        $data->maintitle = "Componente de Competencias";
        $data->maindescription = "Lista de Componente de Competencias para Nivel de Formacion";
        $data->cpntes = $this->create_cpntes_table(2);
        $this->load->view('dashboard', $data);
    }

    //Experiencia General
    public function form_new_exp_lab() {
        $data = new stdClass();
        $data->content_view = "cpntescompe/dash_form_add_cpnte";
        $data->title = APP_NAME . "::Nuevo Componente de Competencias para Experiencia General";
        $data->active = "cpntes";
        $data->maintitle = "Experiencia";
        $data->maindescription = "Nuevo Componente de Competencias para Experiencia General";
        $data->categorias = $this->CategoriasComponentes_model->getActives();
        $data->cpnte_type = 3;
        $this->load->view('dashboard', $data);
    }

    public function list_lvl_exp_lab() {
        $data = new stdClass();
        $data->content_view = "cpntescompe/dash_list_cpnte";
        $data->title = APP_NAME . "::Lista Componente de Competencias para Experiencia General";
        $data->active = "cpntes";
        $data->maintitle = "Componente de Competencias";
        $data->maindescription = "Lista de Componente de Competencias para Experiencia General";
        $data->cpntes = $this->create_cpntes_table(3);
        $this->load->view('dashboard', $data);
    }

    //Descripcion de Habilidades
    public function form_new_desc_hab() {
        $data = new stdClass();
        $data->content_view = "cpntescompe/dash_form_add_cpnte";
        $data->title = APP_NAME . "::Nuevo Componente de Competencias para Descripcion de Hablidades";
        $data->active = "cpntes";
        $data->maintitle = "Descripcion de Hablidades";
        $data->maindescription = "Nuevo Componente de Competencias para Descripcion de Hablidades";
        $data->categorias = $this->CategoriasComponentes_model->getActives();
        $data->cpnte_type = 4;
        $this->load->view('dashboard', $data);
    }

    public function list_lvl_desc_hab() {
        $data = new stdClass();
        $data->content_view = "cpntescompe/dash_list_cpnte";
        $data->title = APP_NAME . "::Lista Componente de Competencias para Descripcion de Hablidades";
        $data->active = "cpntes";
        $data->maintitle = "Componente de Competencias";
        $data->maindescription = "Lista de Componente de Competencias para Descripcion de Hablidades";
        $data->cpntes = $this->create_cpntes_table(4);
        $this->load->view('dashboard', $data);
    }

    public function add_cpnte() {
        $this->load->library('form_validation');
        $cpnte_type = $this->input->post('cpnte_type');
        if ($this->form_validation->run('dash_add_cpnte') == FALSE) {
            if ($cpnte_type == 1) {
                $this->form_new_lvl_education();
            }
            if ($cpnte_type == 2) {
                $this->form_new_lvl_formation();
            }
            if ($cpnte_type == 3) {
                $this->form_new_exp_lab();
            }
            if ($cpnte_type == 4) {
                $this->form_new_desc_hab();
            }
        } else {
            $cpnte_grado = 0;
            if ($cpnte_type == 1) {
                $cpnte_grado = $this->input->post('cpnte_grado');
            }
            $cpnte_description = $this->input->post('cpnte_description');

            $cpnte_cat = $this->input->post('cpnte_cat');
            $cpnte_status = $this->input->post('cpnte_status');
            $insert = [
                "Descripcion" => $cpnte_description,
                "TipoCatalogoID" => $cpnte_type,
                "EstadoID" => $cpnte_status,
                "CategoriaPerfilID" => $cpnte_cat,
                "tipo_grado_id" => $cpnte_grado
            ];

            $cpnte_id = $this->CpntesCompe_model->insert($insert);
            if ($cpnte_id > 0) {
                if ($cpnte_type == 1) {
                    $this->session->set_flashdata('message_success', 'El Componente de Competencias se agrego correctamente.');
                    redirect('lista-cpnte-edu');
                }
                if ($cpnte_type == 2) {
                    $this->session->set_flashdata('message_success', 'El Componente de Competencias se agrego correctamente.');
                    redirect('lista-cpnte-form');
                }
                if ($cpnte_type == 3) {
                    $this->session->set_flashdata('message_success', 'El Componente de Competencias se agrego correctamente.');
                    redirect('lista-cpnte-exp');
                }
                if ($cpnte_type == 4) {
                    $this->session->set_flashdata('message_success', 'El Componente de Competencias se agrego correctamente.');
                    redirect('lista-cpnte-hab');
                }
            }
        }
    }

    public function create_cpntes_table($id) {
        $cpnte = $this->CpntesCompe_model->get_pagination($id);

        $cpnte_table = "";

        if (count($cpnte) > 0) {
            foreach ($cpnte as $key => $value) {
                $cat = $this->CategoriasComponentes_model->get($value->CategoriaPerfilID);
                if($value->tipo_grado_id > 0){
                    $grado = $this->Grados_model->get($value->tipo_grado_id);
                }
                
                $links = "";
                if ($value->EstadoID == 1) {
                    $status = "<span class='label label-success'>Activo</span>";
                    $links .= '<a class="btn btn-warning btn-xs btn-o" href="' . base_url('desactivar-cpnte') . '/' . $value->CatalogoPerfilID . '">
                                    <i class="fa fa-ban"></i> Desactivar
                                </a>';
                }
                if ($value->EstadoID == 2) {
                    $status = "<span class='label label-warning'>Inactivo</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-cpnte') . '/' . $value->CatalogoPerfilID . '">
                                    <i class="fa fa-ban"></i> Activar
                                </a>';
                }
                if ($value->EstadoID == 3) {
                    $status = "<span class='label label-danger'>Eliminado</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-cpnte') . '/' . $value->CatalogoPerfilID . '">
                                    <i class="fa fa-check"></i> Activar
                                </a>';
                }
                $cpnte_table .= "<tr>";
                if($value->tipo_grado_id > 0){
                    $cpnte_table .= "<td>{$grado->grado_nombre}</td>";
                }
                $cpnte_table .= "<td>{$value->Descripcion}</td>";
                $cpnte_table .= "<td>{$cat->NombreCategoria}</td>";
                $cpnte_table .= "<td>{$status}</td>";
                $cpnte_table .= "<td>
                                    <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-cpnte') . "/" . $value->CatalogoPerfilID . "'>
                                        <i class='fa fa-edit'></i> Editar
                                    </a>
                                    " . $links . "
                                    <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-cpnte') . "/" . $value->CatalogoPerfilID . "'>
                                        <i class='fa fa-trash'></i> Eliminar
                                    </a>
                                </td>";
                $cpnte_table .= "</tr>";
            }
        } else {
            $cpnte_table .= "<tr>";
            $cpnte_table .= "<td colspan='2' class='info center'>Sin Informacion</td>";
            $cpnte_table .= "</tr>";
        }

        return $cpnte_table;
    }

    public function edit_cpnte($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Componente de Competencia no valido');
            redirect('dashboard');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('dashboard');
        }
        $cpnte = $this->CpntesCompe_model->get($id);
        if (!count($cpnte) == 1) {
            $this->session->set_flashdata('message_error', 'Componente de Competencia no encontrado');
            redirect('dashboard');
        }

        $data = new stdClass();
        $data->content_view = "cpntescompe/dash_form_edit_cpnte";
        $data->title = APP_NAME . "::Editar Componente de Competencia";
        $data->active = "cpntes";
        $data->categorias = $this->CategoriasComponentes_model->getActives();
        $data->maintitle = "Componente de Competencia";
        $data->maindescription = "Editar Componente de Competencia";
        $data->grados = $this->Grados_model->getActives();
        $data->cpnte = $cpnte;

        $this->load->view('dashboard', $data);
    }

    public function delete_cpnte($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Componente de Competencia no valido');
            redirect('dashboard');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('dashboard');
        }
        $cpnte = $this->CpntesCompe_model->get($id);
        if (!count($cpnte) == 1) {
            $this->session->set_flashdata('message_error', 'Componente de Competencia no encontrado');
            redirect('dashboard');
        }
        $update = [
            'EstadoID' => 3
        ];
        $id_user = $this->CpntesCompe_model->update($id, $update);
        if ($id_user > 0) {
            if ($cpnte->TipoCatalogoID == 1) {
                $this->session->set_flashdata('message_success', 'El Componente de Competencias se Elimino correctamente.');
                redirect('lista-cpnte-edu');
            }
            if ($cpnte->TipoCatalogoID == 2) {
                $this->session->set_flashdata('message_success', 'El Componente de Competencias se Elimino correctamente.');
                redirect('lista-cpnte-form');
            }
            if ($cpnte->TipoCatalogoID == 3) {
                $this->session->set_flashdata('message_success', 'El Componente de Competencias se Elimino correctamente.');
                redirect('lista-cpnte-exp');
            }
            if ($cpnte->TipoCatalogoID == 4) {
                $this->session->set_flashdata('message_success', 'El Componente de Competencias se Elimino correctamente.');
                redirect('lista-cpnte-hab');
            }
        }
    }

    public function active_cpnte($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Componente de Competencias no valido');
            redirect('dashboard');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('dashboard');
        }
        $cpnte = $this->CpntesCompe_model->get($id);
        if (!count($cpnte) == 1) {
            $this->session->set_flashdata('message_error', 'Componente de Competencias no encontrado');
            redirect('dashboard');
        }
        $update = [
            'EstadoID' => 1
        ];
        $id_user = $this->CpntesCompe_model->update($id, $update);
        if ($id_user > 0) {
            if ($cpnte->TipoCatalogoID == 1) {
                $this->session->set_flashdata('message_success', 'El Componente de Competencias se Activo correctamente.');
                redirect('lista-cpnte-edu');
            }
            if ($cpnte->TipoCatalogoID == 2) {
                $this->session->set_flashdata('message_success', 'El Componente de Competencias se Activo correctamente.');
                redirect('lista-cpnte-form');
            }
            if ($cpnte->TipoCatalogoID == 3) {
                $this->session->set_flashdata('message_success', 'El Componente de Competencias se Activo correctamente.');
                redirect('lista-cpnte-exp');
            }
            if ($cpnte->TipoCatalogoID == 4) {
                $this->session->set_flashdata('message_success', 'El Componente de Competencias se Activo correctamente.');
                redirect('lista-cpnte-hab');
            }
        }
    }

    public function unactive_cpnte() {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Componente de Competencias  no valido');
            rredirect('dashboard');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('dashboard');
        }
        $cpnte = $this->CpntesCompe_model->get($id);
        if (!count($cpnte) == 1) {
            $this->session->set_flashdata('message_error', 'Componente de Competencias  no encontrado');
            redirect('dashboard');
        }
        $update = [
            'EstadoID' => 2
        ];
        $cpnte_id = $this->CpntesCompe_model->update($id, $update);
        if ($cpnte_id > 0) {
            if ($cpnte->TipoCatalogoID == 1) {
                $this->session->set_flashdata('message_success', 'El Componente de Competencias se Desactivo correctamente.');
                redirect('lista-cpnte-edu');
            }
            if ($cpnte->TipoCatalogoID == 2) {
                $this->session->set_flashdata('message_success', 'El Componente de Competencias se Desactivo correctamente.');
                redirect('lista-cpnte-form');
            }
            if ($cpnte->TipoCatalogoID == 3) {
                $this->session->set_flashdata('message_success', 'El Componente de Competencias se Desactivo correctamente.');
                redirect('lista-cpnte-exp');
            }
            if ($cpnte->TipoCatalogoID == 4) {
                $this->session->set_flashdata('message_success', 'El Componente de Competencias se Desactivo correctamente.');
                redirect('lista-cpnte-hab');
            }
        }
    }

    public function update_cpnte() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_cpnte') == FALSE) {
            $this->edit_cpnte($id);
        } else {

            $cpnte_description = $this->input->post('cpnte_description');
            $cpnte_type = $this->input->post('cpnte_type');
            $cpnte_cat = $this->input->post('cpnte_cat');

            if ($cpnte_type == 1) {
                $cpnte_grado = $this->input->post('cpnte_grado');

                $update = [
                    "Descripcion" => $cpnte_description,
                    "CategoriaPerfilID" => $cpnte_cat,
                    "tipo_grado_id" => $cpnte_grado
                ];
            } else {
                $update = [
                    "Descripcion" => $cpnte_description,
                    "CategoriaPerfilID" => $cpnte_cat
                ];
            }

            $cpnte_id = $this->CpntesCompe_model->update($id, $update);
            if ($cpnte_id > 0) {
                if ($cpnte_type == 1) {
                    $this->session->set_flashdata('message_success', 'El Componente de Competencias se Actualizo correctamente.');
                    redirect('lista-cpnte-edu');
                }
                if ($cpnte_type == 2) {
                    $this->session->set_flashdata('message_success', 'El Componente de Competencias se Actualizo correctamente.');
                    redirect('lista-cpnte-form');
                }
                if ($cpnte_type == 3) {
                    $this->session->set_flashdata('message_success', 'El Componente de Competencias se Actualizo correctamente.');
                    redirect('lista-cpnte-exp');
                }
                if ($cpnte_type == 4) {
                    $this->session->set_flashdata('message_success', 'El Componente de Competencias se Actualizo correctamente.');
                    redirect('lista-cpnte-hab');
                }
            }
        }
    }

}
