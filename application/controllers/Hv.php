<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Hv
 *
 * @author furbox
 */
class Hv extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->library('Auth');
        $this->load->model([
            "HvHijos_model",
            "CpntesCompe_model",
            "HvInfAcad_model",
            "Departamento_model",
            "HvExpLab_model",
            "HvExpLabExpGral_model",
            "HvDescHab_model",
            "HvEstComp_model",
            "HvRef_model",
            "DatosUsuario_model",
            "Users_model",
            "TipoDocumento_model",
            "EstadoCivil_model",
            "Municipio_model",
            "HvEstExt_model"
        ]);
        $this->auth->is_session_start();
    }

    public function index() {
        $data = new stdClass();
        $data->content_view = "hv/form_hv";
        $data->title = APP_NAME . "::Hoja de Vida";
        $data->active = "cv";
        $data->maintitle = "Hoja de Vida";

        $data->maindescription = "Información de Hoja de vida";
        $childs = $this->HvHijos_model->getByUser($_SESSION['id_user']);
        $infoaca = $this->HvInfAcad_model->getByUser($_SESSION['id_user']);
        $explab = $this->HvExpLab_model->getByUser($_SESSION['id_user']);
        $nivform = $this->HvEstComp_model->getByUser($_SESSION['id_user']);
        $estcomp = $this->HvEstExt_model->getByUser($_SESSION['id_user']);
        $refpers = $this->HvRef_model->getByUser($_SESSION['id_user'], 1);
        $refprof = $this->HvRef_model->getByUser($_SESSION['id_user'], 2);
        $deschab = $this->HvDescHab_model->getByUser($_SESSION['id_user']);
        $data->childs = $this->create_table_childs($childs);
        $data->infoaca = $this->create_table_infoaca($infoaca);
        $data->explab = $this->create_table_explab($explab);
        $data->nivform = $this->create_table_estcomp($nivform);
        $data->estcomp = $this->create_table_estext($estcomp);
        $data->refpers = $this->create_table_ref($refpers);
        $data->refprof = $this->create_table_ref($refprof);
        $data->deschab = $this->create_table_deschab($deschab);
        $data->infoAcad = $this->CpntesCompe_model->getByCpnte(1);
        $data->ExpLab = $this->CpntesCompe_model->getByCpnte(3);
        $data->EstComp = $this->CpntesCompe_model->getByCpnte(2);
        $data->DescHab = $this->CpntesCompe_model->getByCpnte(4);
        $data->Departamentos = $this->Departamento_model->get_active_departments();


        $id = $_SESSION['id_user'];
        $userdata = $this->DatosUsuario_model->get($id);
        $user = $this->Users_model->get($id);
        $data->user = $user;

        $data->UsuarioID = $id;
        $data->UserData = $userdata;

        $data->tiposdocumentos = $this->TipoDocumento_model->get_active_documenttype();
        $data->departamentos = $this->Departamento_model->get_active_departments();
        $data->estadosciviles = $this->EstadoCivil_model->get_active_estado_civil();

        if ($data->UserData->MunIDExpDocumento > 0) {
            $data->MunicipiosExp = $this->Municipio_model->getMunicipiosByDepId($data->UserData->DepIdExpedicion);
        }

        if ($data->UserData->MunIDResidencia > 0) {
            $data->MunicipiosRes = $this->Municipio_model->getMunicipiosByDepId($data->UserData->DepIdResidencia);
        }



        $this->load->view('dashboard_aspirante', $data);
    }

    public function add_child() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_hv_child') == FALSE) {
            $this->index();
        } else {

            $nombre = $this->input->post('hv_child_name');
            $birthdate = $this->input->post('hv_child_birthdate');


            $insert = [
                "UsuarioID" => $_SESSION['id_user'],
                "NombreCompleto" => $nombre,
                "FechaNacimiento" => $birthdate,
            ];

            $insert_id = $this->HvHijos_model->insert($insert);
            if ($insert_id > 0) {
                $this->session->set_flashdata('message_success', 'Se agrego un hijo a la lista correctamente.');
                redirect('hoja-de-vida');
            }
        }
    }

    public function add_infaca() {
        $this->load->library('form_validation');
        $this->load->model("CpntesCompe_model");
        if ($this->form_validation->run('dash_add_hv_infaca') == FALSE) {
            $this->index();
        } else {
            if ($this->input->post('hv_ia_titulo') == 0) {
                $hv_option_ia_titulo = $this->input->post('hv_option_ia_titulo');
                $insert_titulo = [
                    "Descripcion" => $hv_option_ia_titulo,
                    "TipoCatalogoID" => 1,
                    "EstadoID" => 1,
                    "CategoriaPerfilID" => 7,
                    "tipo_grado_id" => 4
                ];
                $idcat = $this->CpntesCompe_model->insert($insert_titulo);
                if (!$idcat > 0) {
                    $this->session->set_flashdata('message_error', 'Se ocurrio un error al agregar tu Titulo obtenido. Intentalo nuevamente');
                    redirect('hoja-de-vida');
                }
            } else {
                $idcat = $this->input->post('hv_ia_titulo');
            }

            $nombre = $this->input->post('hv_name_inst');
            $fecha = $this->input->post('hv_date_grade');

            $insert = [
                "UsuarioID" => $_SESSION['id_user'],
                "CatalogoPerfilID" => $idcat,
                "NombreInstitucion" => $nombre,
                "FechaGrado" => $fecha,
                "HvInfAcad_exp" => 0
            ];

            $insert_id = $this->HvInfAcad_model->insert($insert);

            if ($insert_id > 0) {
                $this->session->set_flashdata('message_success', 'Se agrego un informacion academica a la lista correctamente.');
                redirect('hoja-de-vida');
            }
        }
    }

    public function add_desc_hab() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_desc_hab') == FALSE) {
            $this->index();
        } else {
            $desc_de_habilidades = $this->input->post('hv_desc_hab');

            $insert = [
                "UsuarioID" => $_SESSION['id_user'],
                "CatalogoPerfilID" => $desc_de_habilidades
            ];

            $insert_id = $this->HvDescHab_model->insert($insert);

            if ($insert_id > 0) {
                $this->session->set_flashdata('message_success', 'Se agrego una describcion de habilidades a la lista correctamente.');
                redirect('hoja-de-vida');
            }
        }
    }

    public function add_explab() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_hv_explab') == FALSE) {
            $this->index();
        } else {
            $iddep = $this->input->post('hv_explab_dep');
            $cargo = $this->input->post('hv_explab_cargo');
            $jefe = $this->input->post('hv_explab_jefe_name');
            $fechaini = $this->input->post('hv_explab_date_ini');
            $fechafin = $this->input->post('hv_explab_date_fin');
            $tel = $this->input->post('hv_explab_tel');
            $insert = [
                "UsuarioID" => $_SESSION['id_user'],
                "NombreCargo" => $cargo,
                "DepartamentoID" => $iddep,
                "NombreJefeInmediato" => $jefe,
                "FechaInicio" => $fechaini,
                "FechaFinalizacion" => $fechafin,
                "Telefono" => $tel
            ];

            $insert_id = $this->HvExpLab_model->insert($insert);

            if ($insert_id > 0) {
                $this->session->set_flashdata('message_success', 'Se agrego Experiencia Laboral a la lista correctamente. Puedes agregae una Actividad Desarrollada');
                redirect('hv-nueva-actividad/' . $insert_id);
            }
        }
    }

    public function add_estcomp() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_hv_estcomp') == FALSE) {
            $this->index();
        } else {
            $idcat = $this->input->post('hv_ec_titulo');
            $nombre = $this->input->post('hv_ec_inst');
            $fecha = $this->input->post('hv_ec_date_cert');
            $exp = $this->input->post('hv_ec_experiencia');

            $insert = [
                "UsuarioID" => $_SESSION['id_user'],
                "CatalogoPerfilID" => $idcat,
                "NombreInstitucion" => $nombre,
                "FechaCertificacion" => $fecha,
                "HvEstComp_exp" => $exp
            ];

            $insert_id = $this->HvEstComp_model->insert($insert);

            if ($insert_id > 0) {
                $this->session->set_flashdata('message_success', 'Se agrego Nivel de Formacion a la lista correctamente.');
                redirect('hoja-de-vida');
            }
        }
    }

    public function add_est_ext() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_hv_estext') == FALSE) {
            $this->index();
        } else {
            $hv_ex_titulo = $this->input->post('hv_ex_titulo');
            $nombre = $this->input->post('hv_ex_inst');
            $fecha = $this->input->post('hv_ex_date_cert');

            $insert = [
                "UsuarioID" => $_SESSION['id_user'],
                "EstudioExtra" => $hv_ex_titulo,
                "NombreInstitucion" => $nombre,
                "FechaCertificacion" => $fecha
            ];

            $insert_id = $this->HvEstExt_model->insert($insert);

            if ($insert_id > 0) {
                $this->session->set_flashdata('message_success', 'Se agrego Estudio Complementario a la lista correctamente.');
                redirect('hoja-de-vida');
            }
        }
    }

    public function add_ref() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_hv_ref') == FALSE) {
            $this->index();
        } else {
            $id_ref = $this->input->post('type_ref_pers');
            $nombre = $this->input->post('hv_ref_per_name');
            $profesion = $this->input->post('hv_ref_per_prof');
            $telefono = $this->input->post('hv_ref_per_tel');

            $insert = [
                "UsuarioID" => $_SESSION['id_user'],
                "NombreReferencia" => $nombre,
                "ProfesionOcupacion" => $profesion,
                "Telefono" => $telefono,
                'TipoReferenciaID' => $id_ref
            ];

            $insert_id = $this->HvRef_model->insert($insert);

            if ($insert_id > 0) {
                $this->session->set_flashdata('message_success', 'Se agrego una Referencia a la lista correctamente.');
                redirect('hoja-de-vida');
            }
        }
    }

    public function form_new_activity($id = FALSE) {

        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Modulo no valido');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }

        $data = new stdClass();
        $data->id_explab = $id;
        $data->content_view = "hv/form_new_activity";
        $data->title = APP_NAME . "::Hoja de Vida";
        $data->active = "cv";
        $data->maintitle = "Hoja de Vida";

        $data->maindescription = "Información de Hoja de vida";
        $data->ExpLab = $this->CpntesCompe_model->getByCpnte(3);
        $this->load->view('dashboard_aspirante', $data);
    }

    public function add_activity() {
        $this->load->library('form_validation');
        $id_explab = $this->input->post('id_explab');
        if ($this->form_validation->run('dash_add_hv_act') == FALSE) {
            $this->form_new_activity($id_explab);
        } else {
            $id_act = $this->input->post('hv_act_titulo');
            $hv_act_exp_a = $this->input->post('hv_act_exp_a');
            $hv_act_exp_m = $this->input->post('hv_act_exp_m');

            $meses = $hv_act_exp_a * 12;
            $mesesexp = $hv_act_exp_m + $meses;

            $insert = [
                "HvExpLabID" => $id_explab,
                "CatalogoPerfilID" => $id_act,
                "Experiencia" => $mesesexp,
            ];

            $row = $this->HvExpLabExpGral_model->getActivity($id_explab, $id_act);
            if (count($row) > 0) {
                $this->session->set_flashdata('message_error', 'Las actividades no pueden repetirse.');
                redirect('hoja-de-vida');
            }

            $insert_id = $this->HvExpLabExpGral_model->insert($insert);

            if ($insert_id > 0) {
                $this->session->set_flashdata('message_success', 'Se agrego una Actividad a la lista correctamente. Puedes agregar otra.');
                redirect('hv-nueva-actividad/' . $id_explab);
            }
        }
    }

    public function create_table_childs($data) {

        $table = "";

        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $birthdate = new DateTime($value->FechaNacimiento);
                $hoy = new DateTime(date('Y-m-d'));

                $years = date_diff($birthdate, $hoy);

                $table .= "<tr>";
                $table .= "<td>{$value->NombreCompleto}</td>";
                $table .= "<td>{$value->FechaNacimiento}</td>";
                $table .= "<td>{$years->format('%y Años')}</td>";
                $table .= "<td>
                                <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-hijo') . "/" . $value->HvHijoID . "'>
                                    <i class='fa fa-edit'></i> Editar
                                </a>
                                <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-hijo') . "/" . $value->HvHijoID . "'>
                                    <i class='fa fa-trash'></i> Eliminar
                                </a>
                                </td>";
                $table .= "</tr>";
            }
        } else {
            $table .= "<tr><td class='center'>Sin Informacion</td></tr>";
        }

        return $table;
    }

    public function create_table_infoaca($data) {
        $table = "";
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $tituloobt = $this->CpntesCompe_model->get($value->CatalogoPerfilID);
                $table .= "<tr>";
                $table .= "<td>{$value->FechaGrado}</td>";
                $table .= "<td>{$tituloobt->Descripcion}</td>";
                $table .= "<td>{$value->NombreInstitucion}</td>";
                $table .= "<td>
                                <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-info-aca') . "/" . $value->HvInfAcadID . "'>
                                    <i class='fa fa-edit'></i> Editar
                                </a>
                                <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-info-aca') . "/" . $value->HvInfAcadID . "'>
                                    <i class='fa fa-trash'></i> Eliminar
                                </a>
                                </td>";
                $table .= "</tr>";
            }
        } else {
            $table .= "<tr><td class='center'>Sin Informacion</td></tr>";
        }

        return $table;
    }

    public function create_table_deschab($data) {
        $table = "";
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $tituloobt = $this->CpntesCompe_model->get($value->CatalogoPerfilID);
                $table .= "<tr>";
                $table .= "<td>{$tituloobt->Descripcion}</td>";
                $table .= "<td>
                                <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-desc-hab') . "/" . $value->HvHab_id . "'>
                                    <i class='fa fa-edit'></i> Editar
                                </a>
                                <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-desc-hab') . "/" . $value->HvHab_id . "'>
                                    <i class='fa fa-trash'></i> Eliminar
                                </a>
                                </td>";
                $table .= "</tr>";
            }
        } else {
            $table .= "<tr><td colspan='3' class='center'>Sin Informacion</td></tr>";
        }

        return $table;
    }

    public function create_table_explab($data) {
        $table = "";
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $experiencias = "";
                $dep = $this->Departamento_model->get($value->DepartamentoID);
                $ExpGral = $this->CpntesCompe_model->getByCpnte(3);
                $expgrals = $this->HvExpLabExpGral_model->getByUser($value->HvExpLabID);
                if (count($expgrals) > 0) {
                    $experiencias .= '<ul class="list-group">';
                    foreach ($expgrals as $expg) {
                        foreach ($ExpGral as $exp) {
                            if ($exp->CatalogoPerfilID == $expg->CatalogoPerfilID) {
                                $experiencias .= '<li class="list-group-item">';
                                $experiencias .= '<h5 class="list-group-item-heading">' . $expg->Experiencia . ' Meses de Experiencia</h5>';
                                $experiencias .= '<p class="list-group-item-text">' . $exp->Descripcion . '</p>';
                                $experiencias .= "
                                                    <a class='btn btn-info btn-xs btn-o' href='" . base_url('hv-editar-actividad') . "/" . $expg->HvExpLabID . "/" . $expg->CatalogoPerfilID . "'>
                                                        <i class='fa fa-edit'></i> Editar
                                                    </a>
                                                    <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('hv-eliminar-actividad') . "/" . $expg->HvExpLabID . "/" . $expg->CatalogoPerfilID . "'>
                                                        <i class='fa fa-trash'></i> Eliminar
                                                    </a>
                                                ";
                                $experiencias .= '</li>';
                            }
                        }
                    }
                    $experiencias .= '</ul>';
                } else {
                    $experiencias .= "Sin Experiencias agregadas";
                }

                $table .= "<tr>";
                $table .= "<td>{$value->NombreCargo}</td>";
                $table .= "<td>{$dep->Nombre}</td>";
                $table .= "<td>{$value->NombreJefeInmediato}</td>";
                $table .= "<td>{$value->Telefono}</td>";
                $table .= "<td>{$value->FechaInicio}</td>";
                $table .= "<td>{$value->FechaFinalizacion}</td>";
                $table .= "<td>{$experiencias}</td>";
                $table .= "<td>
                                    <a class='btn btn-success btn-xs btn-o' href='" . base_url('hv-nueva-actividad') . "/" . $value->HvExpLabID . "'>
                                        <i class='fa fa-plus'></i> Agregar Actividades
                                    </a>
                                    <a class='btn btn-info btn-xs btn-o' href='" . base_url('hv-editar-exp-lab') . "/" . $value->HvExpLabID . "'>
                                        <i class='fa fa-edit'></i> Editar
                                    </a>
                                    <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('hv-eliminar-exp-lab') . "/" . $value->HvExpLabID . "'>
                                        <i class='fa fa-trash'></i> Eliminar
                                    </a>
                                </td>";
                $table .= "</tr>";
            }
        } else {
            $table .= "<tr><td colspan='7' class='center'>Sin Informacion</td></tr>";
        }

        return $table;
    }

    public function create_table_estcomp($data) {
        $table = "";
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $tituloobt = $this->CpntesCompe_model->get($value->CatalogoPerfilID);

                $table .= "<tr>";
                $table .= "<td>{$value->FechaCertificacion}</td>";
                $table .= "<td>{$tituloobt->Descripcion}</td>";
                $table .= "<td>{$value->NombreInstitucion}</td>";
//                $table .= "<td></td>";
                $table .= "<td>
                                <a class='btn btn-success btn-xs btn-o' href='" . base_url('editar-est-comp') . "/" . $value->HvEstCompID . "'>
                                    <i class='fa fa-edit'></i> Editar 
                                </a>
                                <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-est-comp') . "/" . $value->HvEstCompID . "'>
                                    <i class='fa fa-trash'></i> Eliminar
                                </a>
                                </td>";
                $table .= "</tr>";
            }
        } else {
            $table .= "<tr><td colspan='7' class='center'>Sin Informacion</td></tr>";
        }

        return $table;
    }

    public function create_table_estext($data) {
        $table = "";
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $table .= "<tr>";
                $table .= "<td>{$value->FechaCertificacion}</td>";
                $table .= "<td>{$value->EstudioExtra}</td>";
                $table .= "<td>{$value->NombreInstitucion}</td>";
//                $table .= "<td></td>";
                $table .= "<td>
                                <a class='btn btn-success btn-xs btn-o' href='" . base_url('editar-est-ext') . "/" . $value->HvEstExtraID . "'>
                                    <i class='fa fa-edit'></i> Editar 
                                </a>
                                <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-est-ext') . "/" . $value->HvEstExtraID . "'>
                                    <i class='fa fa-trash'></i> Eliminar
                                </a>
                                </td>";
                $table .= "</tr>";
            }
        } else {
            $table .= "<tr><td colspan='7' class='center'>Sin Informacion</td></tr>";
        }

        return $table;
    }

    public function create_table_ref($data) {
        $table = "";
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $table .= "<tr>";
                $table .= "<td>{$value->NombreReferencia}</td>";
                $table .= "<td>{$value->ProfesionOcupacion}</td>";
                $table .= "<td>{$value->Telefono}</td>";

//                $table .= "<td></td>";
                $table .= "<td>
                                <a class='btn btn-success btn-xs btn-o' href='" . base_url('editar-ref') . "/" . $value->HvRefID . "'>
                                    <i class='fa fa-edit'></i> Editar
                                </a>
                                <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-ref') . "/" . $value->HvRefID . "'>
                                    <i class='fa fa-trash'></i> Eliminar
                                </a>
                                </td>";
                $table .= "</tr>";
            }
        } else {
            $table .= "<tr><td colspan='7' class='center'>Sin Informacion</td></tr>";
        }

        return $table;
    }

    //Formulario para Editar Hijo
    public function edit_child($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Hijo no valido');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $child = $this->HvHijos_model->getByIDUser($id);
        if (!count($child) == 1) {
            $this->session->set_flashdata('message_error', 'Hijo no encontrado');
            redirect('hoja-de-vida');
        }
        $data = new stdClass();
        $data->content_view = "hv/dash_form_edit_child";
        $data->title = APP_NAME . "::Editar hijo";
        $data->active = "hv";
        $data->maintitle = "Hoja de Vida";
        $data->maindescription = "Editar Hijo";

        $data->child = $child;

        $this->load->view('dashboard_aspirante', $data);
    }

    //Actualizar Hijo
    public function update_child() {
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_hv_child') == FALSE) {
            $this->edit_child($id);
        } else {

            $child_name = $this->input->post('hv_child_name');
            $child_date = $this->input->post('hv_child_birthdate');

            $update = [
                "NombreCompleto" => $child_name,
                "FechaNacimiento" => $child_date
            ];

            $id = $this->HvHijos_model->update($id, $update);
            if ($id > 0) {
                $this->session->set_flashdata('message_success', 'El Hijo se actualizo correctamente.');
                redirect('hoja-de-vida');
            } else {
                $this->session->set_flashdata('message_error', 'No se logro actualizar.');
                redirect('hoja-de-vida');
            }
        }
    }

    //Eliminar Hijo
    public function delete_child() {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Hijo no valido');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $child = $this->HvHijos_model->getByIDUser($id);
        if (!count($child) == 1) {
            $this->session->set_flashdata('message_error', 'Hijo no encontrado');
            redirect('hoja-de-vida');
        }

        $id = $this->HvHijos_model->delete($id);
        if ($id > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Hijo se ha eliminado.');
            redirect('hoja-de-vida');
        }
    }

    //Editar Informacion Academica
    public function edit_info_acad($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Informacion Academica no valida');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $row = $this->HvInfAcad_model->getByIDUser($id);
        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Informacion Academica no encontrada');
            redirect('hoja-de-vida');
        }
        $data = new stdClass();
        $data->content_view = "hv/dash_form_edit_info_acad";
        $data->title = APP_NAME . "::Editar Informacion Academica";
        $data->active = "hv";
        $data->maintitle = "Hoja de Vida";
        $data->maindescription = "Editar Informacion Academica";
        $data->infoAcad = $this->CpntesCompe_model->getByCpnte(1);
        $data->infoacad = $row;

        $this->load->view('dashboard_aspirante', $data);
    }

    //Actualizar Informacion Academica
    public function update_info_acad() {
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_hv_infaca') == FALSE) {
            $this->edit_info_acad($id);
        } else {

            $hv_ia_titulo = $this->input->post('hv_ia_titulo');
            $hv_name_inst = $this->input->post('hv_name_inst');
            $hv_date_grade = $this->input->post('hv_date_grade');
            $hv_exp = $this->input->post('hv_ie_exp');


            $update = [
                "CatalogoPerfilID" => $hv_ia_titulo,
                "NombreInstitucion" => $hv_name_inst,
                "FechaGrado" => $hv_date_grade,
                "HvInfAcad_exp" => $hv_exp
            ];

            $id = $this->HvInfAcad_model->update($id, $update);
            if ($id > 0) {
                $this->session->set_flashdata('message_success', 'La Informacion Academica se actualizo correctamente.');
                redirect('hoja-de-vida');
            } else {
                $this->session->set_flashdata('message_error', 'No se logro actualizar.');
                redirect('hoja-de-vida');
            }
        }
    }

    //Eliminar Informacion Academica
    public function delete_info_acad() {

        $id = (int) $this->uri->segment(2);

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Informacion Academica no valido');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $row = $this->HvInfAcad_model->getByIDUser($id);
        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Informacion Academica no encontrado');
            redirect('hoja-de-vida');
        }

        $id = $this->HvInfAcad_model->delete($id);
        if ($id > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'La Informacion Academica se ha eliminado.');
            redirect('hoja-de-vida');
        }
    }

    //Editar Estudio Complementario
    public function edit_est_ext($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Estudio Complementario no valida');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $row = $this->HvEstExt_model->get($id);
        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Estudio Complementario no encontrada');
            redirect('hoja-de-vida');
        }
        $data = new stdClass();
        $data->content_view = "hv/dash_form_edit_est_ext";
        $data->title = APP_NAME . "::Editar Estudio Complementario";
        $data->active = "hv";
        $data->maintitle = "Hoja de Vida";
        $data->maindescription = "Editar Estudio Complementario";
        $data->estcomp = $row;

        $this->load->view('dashboard_aspirante', $data);
    }

    //Actualizar Estudio Complementario
    public function update_est_ext() {
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_hv_estext') == FALSE) {
            $this->edit_est_ext($id);
        } else {

            $hv_ex_titulo = $this->input->post('hv_ex_titulo');
            $hv_name_inst = $this->input->post('hv_ex_inst');
            $hv_date_grade = $this->input->post('hv_ex_date_cert');

            $update = [
                "EstudioExtra" => $hv_ex_titulo,
                "NombreInstitucion" => $hv_name_inst,
                "FechaCertificacion" => $hv_date_grade
            ];

            $id = $this->HvEstExt_model->update($id, $update);
            if ($id > 0) {
                $this->session->set_flashdata('message_success', 'El Estudio Complementario se actualizo correctamente.');
                redirect('hoja-de-vida');
            } else {
                $this->session->set_flashdata('message_error', 'No se logro actualizar.');
                redirect('hoja-de-vida');
            }
        }
    }

    //Eliminar Estudio Complementario
    public function delete_est_ext() {

        $id = (int) $this->uri->segment(2);

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Estudio Complementario no valido');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $row = $this->HvEstExt_model->get($id);
        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Estudio Complementario no encontrado');
            redirect('hoja-de-vida');
        }

        $id = $this->HvEstExt_model->delete($id);
        if ($id > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Estudio Complementario se ha eliminado.');
            redirect('hoja-de-vida');
        }
    }

    //Editar Estudio Complementario
    public function edit_est_comp($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Estudio Complementario no valida');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $row = $this->HvEstComp_model->getByIDUser($id);
        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Estudio Complementario no encontrada');
            redirect('hoja-de-vida');
        }
        $data = new stdClass();
        $data->content_view = "hv/dash_form_edit_est_comp";
        $data->title = APP_NAME . "::Editar Estudio Complementario";
        $data->active = "hv";
        $data->maintitle = "Hoja de Vida";
        $data->maindescription = "Editar Estudio Complementario";
        $data->EstComp = $this->CpntesCompe_model->getByCpnte(2);
        $data->estcomp = $row;

        $this->load->view('dashboard_aspirante', $data);
    }

    //Actualizar Estudio Complementario
    public function update_est_comp() {
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_hv_estcomp') == FALSE) {
            $this->edit_est_comp($id);
        } else {

            $hv_ia_titulo = $this->input->post('hv_ec_titulo');
            $hv_name_inst = $this->input->post('hv_ec_inst');
            $hv_date_grade = $this->input->post('hv_ec_date_cert');
            $hv_exp = $this->input->post('hv_ec_experiencia');

            $update = [
                "CatalogoPerfilID" => $hv_ia_titulo,
                "NombreInstitucion" => $hv_name_inst,
                "FechaCertificacion" => $hv_date_grade,
                "HvEstComp_exp" => $hv_exp
            ];

            $id = $this->HvEstComp_model->update($id, $update);
            if ($id > 0) {
                $this->session->set_flashdata('message_success', 'El Estudio Complementario se actualizo correctamente.');
                redirect('hoja-de-vida');
            } else {
                $this->session->set_flashdata('message_error', 'No se logro actualizar.');
                redirect('hoja-de-vida');
            }
        }
    }

    //Eliminar Estudio Complementario
    public function delete_est_comp() {

        $id = (int) $this->uri->segment(2);

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Estudio Complementario no valido');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $row = $this->HvEstComp_model->getByIDUser($id);
        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Estudio Complementario no encontrado');
            redirect('hoja-de-vida');
        }

        $id = $this->HvEstComp_model->delete($id);
        if ($id > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Estudio Complementario se ha eliminado.');
            redirect('hoja-de-vida');
        }
    }

    //Editar Referencias
    public function edit_ref($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Referencia Personal no valida');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $row = $this->HvRef_model->getByIDUser($id);
        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Referencia Personal no encontrada');
            redirect('hoja-de-vida');
        }
        $data = new stdClass();
        $data->content_view = "hv/dash_form_edit_ref";
        $data->title = APP_NAME . "::Editar Referencias";
        $data->active = "hv";
        $data->maintitle = "Hoja de Vida";
        $data->maindescription = "Editar Referencias";
        $data->ref = $row;

        $this->load->view('dashboard_aspirante', $data);
    }

    //Actualizar Referencias
    public function update_ref() {
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_hv_ref') == FALSE) {
            $this->edit_est_comp($id);
        } else {

            $nombre = $this->input->post('hv_ref_per_name');
            $ocupacion = $this->input->post('hv_ref_per_prof');
            $tel = $this->input->post('hv_ref_per_tel');

            $update = [
                "NombreReferencia" => $nombre,
                "ProfesionOcupacion" => $ocupacion,
                "Telefono" => $tel,
            ];

            $id = $this->HvRef_model->update($id, $update);
            if ($id > 0) {
                $this->session->set_flashdata('message_success', 'La Referencia actualizo correctamente.');
                redirect('hoja-de-vida');
            } else {
                $this->session->set_flashdata('message_error', 'No se logro actualizar.');
                redirect('hoja-de-vida');
            }
        }
    }

    //Eliminar Referencias
    public function delete_ref() {

        $id = (int) $this->uri->segment(2);

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Referencia no valida');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $row = $this->HvRef_model->getByIDUser($id);
        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Referencia no encontrada');
            redirect('hoja-de-vida');
        }

        $id = $this->HvRef_model->delete($id);
        if ($id > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'La Referencia se ha eliminado.');
            redirect('hoja-de-vida');
        }
    }

    //Editar Experiencia Laboral
    public function edit_exp_lab($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Experiencia Laboral no valida');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $row = $this->HvExpLab_model->getByIDUser($id);
        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Experiencia Laboral no encontrada');
            redirect('hoja-de-vida');
        }
        $data = new stdClass();
        $data->content_view = "hv/dash_form_edit_exp_lab";
        $data->title = APP_NAME . "::Editar Experiencia Laboral";
        $data->active = "hv";
        $data->maintitle = "Hoja de Vida";
        $data->maindescription = "Editar Experiencia Laboral";
        $data->Departamentos = $this->Departamento_model->get_active_departments();
        $data->explab = $row;

        $this->load->view('dashboard_aspirante', $data);
    }

    //Actualizar Experiencia Laboral
    public function update_exp_lab() {
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_hv_explab') == FALSE) {
            $this->edit_exp_lab($id);
        } else {

            $hv_explab_cargo = $this->input->post('hv_explab_cargo');
            $hv_explab_dep = $this->input->post('hv_explab_dep');
            $hv_explab_jefe_name = $this->input->post('hv_explab_jefe_name');
            $hv_explab_date_ini = $this->input->post('hv_explab_date_ini');
            $hv_explab_date_fin = $this->input->post('hv_explab_date_fin');
            $hv_explab_tel = $this->input->post('hv_explab_tel');

            $update = [
                "NombreCargo" => $hv_explab_cargo,
                "DepartamentoID" => $hv_explab_dep,
                "NombreJefeInmediato" => $hv_explab_jefe_name,
                "FechaInicio" => $hv_explab_date_ini,
                "FechaFinalizacion" => $hv_explab_date_fin,
                "Telefono" => $hv_explab_tel
            ];

            $id = $this->HvExpLab_model->update($id, $update);
            if ($id > 0) {
                $this->session->set_flashdata('message_success', 'La Experiencia Laboral se actualizo correctamente.');
                redirect('hoja-de-vida');
            } else {
                $this->session->set_flashdata('message_error', 'No se logro actualizar.');
                redirect('hoja-de-vida');
            }
        }
    }

    //Eliminar Experiencia Laboral
    public function delete_exp_lab() {

        $id = (int) $this->uri->segment(2);

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Experiencia Laboral no valido');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $row = $this->HvExpLab_model->getByIDUser($id);
        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Experiencia Laboral no encontrado');
            redirect('hoja-de-vida');
        }
        $this->db->trans_start();
        $actividades = $this->HvExpLabExpGral_model->get($id);
        if (count($actividades) > 0) {
            $id1 = $this->HvExpLabExpGral_model->delete($id);
            if ($id1 > 0) {
                $id2 = $this->HvExpLab_model->delete($id);
                if ($id2 > 0) {
                    $this->db->trans_complete();
                    $this->session->set_flashdata('message_success', 'La Experiencia Laboral se ha eliminado.');
                    redirect('hoja-de-vida');
                }
            } else {
                $this->db->trans_rollback();
                $this->session->set_flashdata('message_error', 'La Experiencia Laboral no se ha eliminado. Ocurrio un error en el proceso.');
                redirect('hoja-de-vida');
            }
        } else {
            $id2 = $this->HvExpLab_model->delete($id);
            if ($id2 > 0) {
                $this->db->trans_complete();
                $this->session->set_flashdata('message_success', 'La Experiencia Laboral se ha eliminado.');
                redirect('hoja-de-vida');
            } else {
                $this->db->trans_rollback();
                $this->session->set_flashdata('message_error', 'La Experiencia Laboral no se ha eliminado. Ocurrio un error en el proceso.');
                redirect('hoja-de-vida');
            }
        }
    }

    //Editar Actividad
    public function edit_act($id = FALSE, $id2 = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
            $id2 = (int) $this->uri->segment(3);
        } else {
            $id = (int) $id;
            $id2 = (int) $id2;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Actividad no valida');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }

        $exist = $this->HvExpLab_model->getByIDUser($id);
        if (!count($exist) == 1) {
            $this->session->set_flashdata('message_error', 'Actividad no encontrada');
            redirect('hoja-de-vida');
        }
        $row = $this->HvExpLabExpGral_model->getActivity($id, $id2);

        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Actividad no encontrada');
            redirect('hoja-de-vida');
        }
        $data = new stdClass();
        $data->content_view = "hv/dash_form_edit_act";
        $data->title = APP_NAME . "::Editar Actividad de Experiencia Laboral";
        $data->active = "hv";
        $data->maintitle = "Hoja de Vida";
        $data->maindescription = "Editar Actividad Experiencia Laboral";
        $data->explab = $row;
        $data->ExpLab = $this->CpntesCompe_model->getByCpnte(3);
        $data->expgralID = $id2;

        $this->load->view('dashboard_aspirante', $data);
    }

    //Actualizar Experiencia Laboral
    public function update_act() {
        $this->load->library('form_validation');

        $id = $this->input->post('id_explab');
        $id2 = $this->input->post('id_expgral');
        if ($this->form_validation->run('dash_update_hv_act') == FALSE) {
            $this->edit_act($id,$id2);
        } else {

            $hv_act_titulo = $this->input->post('hv_act_titulo');
            $hv_act_exp_a = $this->input->post('hv_act_exp_a');
            $hv_act_exp_m = $this->input->post('hv_act_exp_m');
            
            $meses = $hv_act_exp_a * 12;
            $mesesexp = $hv_act_exp_m + $meses;
            

            $update = [
                "CatalogoPerfilID" => $hv_act_titulo,
                "Experiencia" => $mesesexp
            ];
            $row = $this->HvExpLabExpGral_model->getActivity($id, $id2);
            if (count($row) > 1 && $id2 != $hv_act_titulo) {
                $this->session->set_flashdata('message_error', 'Las actividades no pueden repetirse.');
                redirect('hoja-de-vida');
            }
            $id = $this->HvExpLabExpGral_model->update($id, $id2, $update);
            if ($id > 0) {
                $this->session->set_flashdata('message_success', 'La Actividad de Experiencia Laboral se actualizo correctamente.');
                redirect('hoja-de-vida');
            } else {
                $this->session->set_flashdata('message_error', 'No se logro actualizar.');
                redirect('hoja-de-vida');
            }
        }
    }

    //Eliminar Experiencia Laboral
    public function delete_act() {

        $id = (int) $this->uri->segment(2);
        $id2 = (int) $this->uri->segment(3);

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Actividad de Experiencia Laboral no valido');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $exist = $this->HvExpLab_model->getByIDUser($id);
        if (!count($exist) == 1) {
            $this->session->set_flashdata('message_error', 'Actividad no encontrada');
            redirect('hoja-de-vida');
        }
        $row = $this->HvExpLabExpGral_model->getActivity($id, $id2);
        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Actividad de Experiencia Laboral no encontrado');
            redirect('hoja-de-vida');
        }

        $id1 = $this->HvExpLabExpGral_model->deleteActivity($id, $id2);
        if ($id1 > 0) {
            $this->session->set_flashdata('message_success', 'La Actividad de Experiencia Laboral se ha eliminado.');
            redirect('hoja-de-vida');
        }
    }

    //Editar Descripcion de Habilidad
    public function edit_desc_hab($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Habilidad no valida');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $row = $this->HvDescHab_model->getByIDUser($id);

        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Habilidad no encontrada');
            redirect('hoja-de-vida');
        }
        $data = new stdClass();
        $data->content_view = "hv/dash_form_edit_desc_hab";
        $data->title = APP_NAME . "::Editar Habilidad";
        $data->active = "hv";
        $data->maintitle = "Hoja de Vida";
        $data->maindescription = "Editar Habilidad";
        $data->habilidad = $row;
        $data->DescHab = $this->CpntesCompe_model->getByCpnte(4);

        $this->load->view('dashboard_aspirante', $data);
    }

    //Actualizar Descripcion de Habilidad
    public function update_desc_hab() {
        $this->load->library('form_validation');

        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_add_desc_hab') == FALSE) {
            $this->edit_exp_lab($id);
        } else {

            $hv_act_titulo = $this->input->post('hv_desc_hab');

            $update = [
                "CatalogoPerfilID" => $hv_act_titulo
            ];
            
            $id = $this->HvDescHab_model->update($id, $update);
            if ($id > 0) {
                $this->session->set_flashdata('message_success', 'La Descripcion de Habilidad se actualizo correctamente.');
                redirect('hoja-de-vida');
            } else {
                $this->session->set_flashdata('message_error', 'No se logro actualizar.');
                redirect('hoja-de-vida');
            }
        }
    }

    //Eliminar Descripcion de Habilidad
    public function delete_desc_hab() {

        $id = (int) $this->uri->segment(2);

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Actividad de Experiencia Laboral no valido');
            redirect('hoja-de-vida');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('hoja-de-vida');
        }
        $row = $this->HvDescHab_model->getByIDUser($id);
        if (!count($row) == 1) {
            $this->session->set_flashdata('message_error', 'Actividad de Experiencia Laboral no encontrado');
            redirect('hoja-de-vida');
        }

        $id1 = $this->HvDescHab_model->delete($id);
        if ($id1 > 0) {
            $this->session->set_flashdata('message_success', 'La Actividad de Experiencia Laboral se ha eliminado.');
            redirect('hoja-de-vida');
        }
    }

}
