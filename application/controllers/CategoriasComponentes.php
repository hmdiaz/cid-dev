<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoriasComponentes
 *
 * @author furbox
 */
class CategoriasComponentes extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('CategoriasComponentes_model');
        $this->load->library([
            'Auth'
        ]);
        $this->auth->is_session_start();   
    }

    public function form_new_cat_comp() {
        $data = new stdClass();
        $data->content_view = "categoriascomp/dash_form_add_cat_cpnte";
        $data->title = APP_NAME . "::Nueva Categorias de Componentes para Componente de Competencias";
        $data->active = "cpntes";
        $data->maintitle = "Categorias de Componentes";
        $data->maindescription = "Nueva Categorias de Componentes para Componente de Competencias";
        $data->cpnte_type = 1;
        $this->load->view('dashboard', $data);
    }

    public function list_cats() {
        $data = new stdClass();
        $data->content_view = "categoriascomp/dash_list_cat_cpnte";
        $data->title = APP_NAME . "::Lista Categorias para Componente de Competencias";
        $data->active = "cpntes";
        $data->maintitle = "categorias";
        $data->maindescription = "Lista Categorias para Componente de Competencias";
        $data->cats = $this->create_cat_table();
        $this->load->view('dashboard', $data);
    }

    public function add_cat() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_cat') == FALSE) {
            $this->form_new_cat_comp();
        } else {

            $cat_name = $this->input->post('cat_name');
            $cat_status = $this->input->post('cat_status');

            $insert = [
                "NombreCategoria" => $cat_name,
                "EstadoID" => $cat_status
            ];

            $insert_id = $this->CategoriasComponentes_model->insert($insert);
            if ($insert_id > 0) {
                $this->session->set_flashdata('message_success', 'La categoria se agrego correctamente.');
                redirect('lista-cate-cpnte');
            }
        }
    }

    public function create_cat_table() {
        $cats = $this->CategoriasComponentes_model->get();

        $cats_table = "";

        if (count($cats) > 0) {
            foreach ($cats as $key => $value) {
                $links = "";

                if ($value->EstadoID == 1) {
                    $status = "<span class='label label-success'>Activo</span>";
                    $links .= '<a class="btn btn-warning btn-xs btn-o" href="' . base_url('desactivar-cat') . '/' . $value->CategoriaPerfilID . '">
                                    <i class="fa fa-ban"></i> Desactivar
                                </a>';
                    $links.= "<a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-cat') . "/" . $value->CategoriaPerfilID . "'>
                                                <i class='fa fa-trash'></i> Eliminar
                                            </a>";
                }
                if ($value->EstadoID == 2) {
                    $status = "<span class='label label-warning'>Inactivo</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-cat') . '/' . $value->CategoriaPerfilID . '">
                                    <i class="fa fa-ban"></i> Activar
                                </a>';
                }
                if ($value->EstadoID == 3) {
                    $status = "<span class='label label-danger'>Eliminado</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-cat') . '/' . $value->CategoriaPerfilID . '">
                                    <i class="fa fa-check"></i> Activar
                                </a>';
                }

                $cats_table .= "<tr>";
                $cats_table .= "<td>{$value->NombreCategoria}</td>";
                $cats_table .= "<td>{$status}</td>";
                $cats_table .= "<td>
                                            <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-cat') . "/" . $value->CategoriaPerfilID . "'>
                                                <i class='fa fa-edit'></i> Editar
                                            </a>
                                            " . $links . "
                                </td>";
                $cats_table .= "</tr>";
            }
        }

        return $cats_table;
    }
    
    public function edit_cat($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Categoria no valido');
            redirect('lista-cate-cpnte');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Categoria no Valida');
            redirect('lista-cate-cpnte');
        }
        $cat = $this->CategoriasComponentes_model->get($id);
        if (!count($cat) == 1) {
            $this->session->set_flashdata('message_error', 'Categoria no encontrado');
            redirect('lista-cate-cpnte');
        }
        $data = new stdClass();
        $data->content_view = "categoriascomp/dash_form_edit_cat";
        $data->title = APP_NAME . "::Editar Categoria";
        $data->active = "cpntes";
        $data->maintitle = "Categoria de Componentes de Competencias";
        $data->maindescription = "Editar Categoria";

        $data->cat = $cat;

        $this->load->view('dashboard', $data);
    }

    public function update_cat() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        
        if ($this->form_validation->run('dash_update_cat') == FALSE) {
            $this->edit_cat($id);
        } else {

            $cat_name = $this->input->post('cat_name');

            $update = [
                "NombreCategoria" => $cat_name
            ];

            $update_id = $this->CategoriasComponentes_model->update($id, $update);
            if ($update_id > 0) {
                $this->session->set_flashdata('message_success', 'La categoria se actualizo correctamente.');
                redirect('lista-cate-cpnte');
            }
        }
    }

    public function active_cat($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Categoria no valido');
            redirect('lista-cate-cpnte');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-cate-cpnte');
        }
        $cat = $this->CategoriasComponentes_model->get($id);
        if (!count($cat) == 1) {
            $this->session->set_flashdata('message_error', 'Categoria no encontrado');
            redirect('lista-cate-cpnte');
        }
        $update = [
            'EstadoID' => 1
        ];
        $update_id = $this->CategoriasComponentes_model->update($id, $update);
        if ($update_id > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'La Categoria se ha Activado.');
            redirect('lista-cate-cpnte');
        }
    }

    public function unactive_cat() {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Categoria no valido');
            redirect('lista-cate-cpnte');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-cate-cpnte');
        }
        $cat = $this->CategoriasComponentes_model->get($id);
        if (!count($cat) == 1) {
            $this->session->set_flashdata('message_error', 'Categoria no encontrado');
            redirect('lista-cate-cpnte');
        }
        $update = [
            'EstadoID' => 2
        ];
        $update_id = $this->CategoriasComponentes_model->update($id, $update);
        if ($update_id > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'La Categoria se ha Desactivado.');
            redirect('lista-cate-cpnte');
        }
    }

    public function delete_cat($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Categoria no valida');
            redirect('lista-cate-cpnte');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-cate-cpnte');
        }
        $cat = $this->CategoriasComponentes_model->get($id);
        if (!count($cat) == 1) {
            $this->session->set_flashdata('message_error', 'Categoria no encontrada');
            redirect('lista-cate-cpnte');
        }
        $update = [
            'EstadoID' => 3
        ];
        $update_id = $this->CategoriasComponentes_model->update($id, $update);
        if ($update_id > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'La Categoria se ha eliminado.');
            redirect('lista-cate-cpnte');
        }
    }

}
