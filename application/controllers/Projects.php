<?php

class Projects extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model([
            'Projects_model',
            'SocioCooperante_model',
            'Perfil_model',
            'Municipio_model',
            'Departamento_model',
            'MunicipiosProyecto_model',
            'TipoSocio_model',
            'SocioCooperanteProyecto_model',
            'Poblacion_model',
            'PoblacionProyecto_model',
            'Ubicacion_model'
        ]);

        $this->load->library(['Auth']);
        $this->auth->is_session_start();
    }

    public function dash_new_project() {
        $data = new stdClass();
        $data->tipos_socios = $this->TipoSocio_model->get(FALSE, 1);
        $data->SocioCooperanteList = $this->getSociosCooperantes();
        $data->poblaciones = $this->Poblacion_model->get(FALSE, 1);
        $data->ubicaciones = $this->Ubicacion_model->get(FALSE, 1);
        $data->departamentos = $this->Departamento_model->get_active_departments();
        $data->content_view = "projects/dash_new_project";
        $data->title = APP_NAME . "::Nuevo Proyecto";
        $data->active = "projects";
        $data->maintitle = "Proyectos";
        $data->maindescription = "Crear Nuevo Proyecto";

        $this->load->view('dashboard', $data);
    }

    public function getSociosCooperantes() {
        return $this->SocioCooperante_model->get(FALSE, 1);
    }

    public function getByTipo($id) {
        $prjs = $this->Projects_model->getProjectsByTipo($id);
        ?>
        <option value="">Seleccione uno...</option>
        <?php
        foreach ($prjs as $item) {
            echo '<option value="' . $item->ProyectoID . '">' . $item->NombreProyecto . '</option>';
        }
        ?>
        <?php
    }

    public function getSociosByTipo($id) {
        $socios = $this->SocioCooperante_model->getSociosByTipo($id);
        ?>
        <option value="">Seleccione uno...</option>
        <?php
        foreach ($socios as $item) {
            echo '<option value="' . $item->SocioCooperanteID . '">' . $item->Nombre . '</option>';
        }
        ?>
        <?php
    }

    public function dash_create_project() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_new_project') == FALSE) {
            $this->dash_new_project();
        } else {
            if ($this->input->post('licitacion') == 1) {
                $licitacion = 3;
            } else {
                $licitacion = 1;
            }
            $fecha_firma_contrato = $this->input->post('fecha_firma_contrato');
            $fecha_firma_acta_inicio = $this->input->post('fecha_firma_acta_inicio');
            $fecha_firma_acta_inicio_cron = $this->input->post('fecha_firma_acta_inicio_cron');
            $nombre_proyecto = $this->input->post('nombre_proyecto');
            $objeto_contrato = $this->input->post('objeto_contrato');
            $codigo_contrato = $this->input->post('codigo_contrato');
            $identificacion_proyecto = $this->input->post('identificacion_proyecto');
            $fecha_inicio_contrato = $this->input->post('fecha_inicio_contrato');
            $fecha_fin_contrato = $this->input->post('fecha_fin_contrato');
            $tiempo_ejecucion_efectiva = $this->input->post('tiempo_ejecucion_efectiva');
            $contribucion_institucional = $this->input->post('contribucion_institucional');

            $insert = [
                'TipoProyectoID' => $licitacion,
                'FechaFirmaContrato' => $fecha_firma_contrato,
                'FechaFirmaActaInicioDoc' => $fecha_firma_acta_inicio,
                'FechaFirmaActaInicioCron' => $fecha_firma_acta_inicio_cron,
                'NombreProyecto' => $nombre_proyecto,
                'ObjetoContrato' => $objeto_contrato,
                'CodigoContrato' => $codigo_contrato,
                'IdentificacionProyecto' => $identificacion_proyecto,
                'FechaInicioContrato' => $fecha_inicio_contrato,
                'FechaFinContrato' => $fecha_fin_contrato,
                'TiempoEjecucionEfectiva' => $tiempo_ejecucion_efectiva,
                'ContribucionInstitucional' => $contribucion_institucional,
                'FechaCreacion' => date('Y-m-d h:i:s')
            ];

            $project_id = $this->Projects_model->insert($insert);

            /* for ($i = 0; $i < count($this->input->post('hdItemsMunicipiosId')); $i++) {
              $data = array(
              'MunicipioID' => $this->input->post('hdItemsMunicipiosId')[$i],
              'ProyectoID' => $project_id
              );

              $this->MunicipiosProyecto_model->insert($data);
              } */

            for ($j = 0; $j < count($this->input->post('hdItemsSocioId')); $j++) {
                $data = array(
                    'SocioCooperanteID' => $this->input->post('hdItemsSocioId')[$j],
                    'ProyectoID' => $project_id
                );

                $this->SocioCooperanteProyecto_model->insert($data);
            }

            for ($k = 0; $k < count($this->input->post('hdItemsPoblacionId')); $k++) {
                $data = array(
                    'PoblacionID' => $this->input->post('hdItemsPoblacionId')[$k],
                    'Cobertura' => $this->input->post('hdItemsCoberturaId')[$k],
                    'MunicipioID' => $this->input->post('hdItemsMunicipiosId')[$k],
                    'UbicacionID' => $this->input->post('hdItemsUbicacionId')[$k],
                    'ProyectoID' => $project_id
                );

                $this->MunicipiosProyecto_model->insert($data);
            }

            if ($project_id > 0) {
                $this->session->set_flashdata('message_success', 'El Proyecto se agrego correctamente.');
                redirect('lista-proyectos');
            }
        }
    }

    public function dash_list_projects($inicio, $limite) {
        $projects = $this->Projects_model->get(FALSE, 1, 1); // 1=Activos
        $projects_table = "";

        if (count($projects) > 0) {
            foreach ($projects as $key => $value) {
                $links = "";

                $projects_table .= "<tr>";
                $projects_table .= "<td>{$value->ProyectoID}</td>";
                $projects_table .= "<td>{$value->NombreProyecto}</td>";
                $projects_table .= "<td class='center'>
                                        <a class='btn btn-primary btn-xs btn-o' href='" . base_url('perfiles') . "/" . $value->ProyectoID . "'>
                                        <i class='fa fa-key'></i> Perfiles
                                        </a>
                                        <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-proyecto') . "/" . $value->ProyectoID . "'>
                                            <i class='fa fa-edit'></i> Editar
                                        </a>
                                        <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-proyecto') . "/" . $value->ProyectoID . "'>
                                            <i class='fa fa-trash'></i> Eliminar
                                        </a>
                                </td>";
                $projects_table .= "</tr>";
            }
        }

        return $projects_table;
    }

    public function dash_list_profiles_by_project($id, $inicio, $limite) {
        $profiles = $this->Perfil_model->getByProyecto($id, 1);
        $profiles_table = "";

        if (count($profiles) > 0) {
            foreach ($profiles as $key => $value) {
                $profiles_table .= "<tr>";
                $profiles_table .= "<td>{$value->MisionCargo}</td>";
                $profiles_table .= "<td>{$value->TituloCargo}</td>";
                $profiles_table .= "<td>{$value->NumeroOcupantes}</td>";
                $profiles_table .= "<td>
                                        <a class='btn btn-primary btn-xs btn-o' href='" . base_url('lista-requisiciones') . "/" . $value->PerfilID . "'>
                                            <i class='fa fa-file'></i> Requisiciones
                                        </a>
                                        <a class='btn btn-primary btn-xs btn-o' href='" . base_url('modificar-perfil') . "/" . $value->PerfilID . "'>
                                            <i class='fa fa-edit'></i> Editar
                                        </a>
                                        <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-perfil') . "/" . $value->PerfilID . "'>
                                            <i class='fa fa-trash'></i> Eliminar
                                        </a>
                                        <a class='btn btn-default btn-xs btn-o' href='" . base_url('clonar-perfil') . "/" . $value->PerfilID . "'>
                                            <i class='fa fa-files-o'></i> Clonar
                                        </a>
                                </td>";
                $profiles_table .= "</tr>";
            }
        }

        return $profiles_table;
    }

    public function edit_project($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Proyecto no valido');
            redirect('lista-proyectos');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-proyectos');
        }
        $project = $this->Projects_model->get($id);
        if (!count($project) == 1) {
            $this->session->set_flashdata('message_error', 'Proyecto no encontrado');
            redirect('lista-proyectos');
        }
        $data = new stdClass();
        $data->SocioCooperanteList = $this->getSociosCooperantes();
        $data->poblaciones = $this->Poblacion_model->get(FALSE, 1);
        $data->tipos_socios = $this->TipoSocio_model->get(FALSE, 1);
        $data->ubicaciones = $this->Ubicacion_model->get(FALSE, 1);
        $data->departamentos = $this->Departamento_model->get_active_departments();
        $data->municipios_x_proyecto = $this->MunicipiosProyecto_model->getMunicipiosProyecto($id);
        $data->socios_x_proyecto = $this->SocioCooperanteProyecto_model->getSociosCooperantesProyecto($id);
        $data->poblaciones_x_proyecto = $this->PoblacionProyecto_model->getPoblacionesProyecto($id);
        $data->content_view = "projects/dash_form_edit_project";
        $data->title = APP_NAME . "::Editar Proyecto";
        $data->active = "projects";
        $data->maintitle = "Proyectos";
        $data->maindescription = "Editar Proyecto";

        $data->project = $project;

        $this->load->view('dashboard', $data);
    }

    public function dash_delete_socio_coop_proyecto() {
        $id = $this->input->post('SocioProyectoID');
        echo $this->SocioCooperanteProyecto_model->delete($id);
    }

    public function dash_delete_municipio_proyecto() {
        $id = $this->input->post('MunicipioProyectoID');
        echo $this->MunicipiosProyecto_model->delete($id);
    }

    public function dash_delete_poblacion_proyecto() {
        $id = $this->input->post('PoblacionProyectoID');
        echo $this->PoblacionProyecto_model->delete($id);
    }

    public function dash_add_socio_coop_proyecto() {
        $SocioCooperanteID = $this->input->post('SocioCooperanteID');
        $ProyectoID = $this->input->post('ProyectoID');

        echo $this->SocioCooperanteProyecto_model->insert([
            'SocioCooperanteID' => $SocioCooperanteID,
            'ProyectoID' => $ProyectoID
        ]);
    }

    public function dash_add_municipio_proyecto() {
        $MunicipioID = $this->input->post('MunicipioID');
        $ProyectoID = $this->input->post('ProyectoID');
        $Cobertura = $this->input->post('Cobertura');
        $PoblacionID = $this->input->post('PoblacionID');
        $UbicacionID = $this->input->post('UbicacionID');

        echo $this->MunicipiosProyecto_model->insert([
            'PoblacionID' => $PoblacionID,
            'MunicipioID' => $MunicipioID,
            'Cobertura' => $Cobertura,
            'ProyectoID' => $ProyectoID,
            'UbicacionID' => $UbicacionID,
        ]);
    }

    public function dash_add_poblacion_proyecto() {
        $PoblacionID = $this->input->post('PoblacionID');
        $ProyectoID = $this->input->post('ProyectoID');
        $Cobertura = $this->input->post('Cobertura');

        echo $this->PoblacionProyecto_model->insert([
            'PoblacionID' => $PoblacionID,
            'ProyectoID' => $ProyectoID,
            'Cobertura' => $Cobertura,
        ]);
    }

    public function update_project() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_new_project') == FALSE) {
            $this->edit_project($id);
        } else {
            $id = $this->input->post('id');
            $fecha_firma_contrato = $this->input->post('fecha_firma_contrato');
            $fecha_firma_acta_inicio = $this->input->post('fecha_firma_acta_inicio');
            $fecha_firma_acta_inicio_cron = $this->input->post('fecha_firma_acta_inicio_cron');
            $nombre_proyecto = $this->input->post('nombre_proyecto');
            $objeto_contrato = $this->input->post('objeto_contrato');
            $codigo_contrato = $this->input->post('codigo_contrato');
            $identificacion_proyecto = $this->input->post('identificacion_proyecto');
            $fecha_inicio_contrato = $this->input->post('fecha_inicio_contrato');
            $fecha_fin_contrato = $this->input->post('fecha_fin_contrato');
            $tiempo_ejecucion_efectiva = $this->input->post('tiempo_ejecucion_efectiva');
            $contribucion_institucional = $this->input->post('contribucion_institucional');

            $data = [
                'FechaFirmaContrato' => $fecha_firma_contrato,
                'FechaFirmaActaInicioDoc' => $fecha_firma_acta_inicio,
                'FechaFirmaActaInicioCron' => $fecha_firma_acta_inicio_cron,
                'NombreProyecto' => $nombre_proyecto,
                'ObjetoContrato' => $objeto_contrato,
                'CodigoContrato' => $codigo_contrato,
                'IdentificacionProyecto' => $identificacion_proyecto,
                'FechaInicioContrato' => $fecha_inicio_contrato,
                'FechaFinContrato' => $fecha_fin_contrato,
                'TiempoEjecucionEfectiva' => $tiempo_ejecucion_efectiva,
                'ContribucionInstitucional' => $contribucion_institucional,
                'FechaUltimaActualizacion' => date('Y-m-d h:i:s')
            ];

            $num_rows = $this->Projects_model->update($id, $data);
            if ($num_rows > 0) {
                $this->session->set_flashdata('message_success', 'El Proyecto se actualizó correctamente.');
            }
            redirect('lista-proyectos');
        }
    }

    public function delete_project($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Proyecto no valido');
            redirect('lista-proyectos');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-proyectos');
        }
        $project = $this->Projects_model->get($id);
        if (!count($project) == 1) {
            $this->session->set_flashdata('message_error', 'Proyecto no encontrado');
            redirect('lista-proyectos');
        }

        $num_rows = $this->Projects_model->update($id, ['EstadoID' => 3]);
        if ($num_rows > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Proyecto se ha eliminado.');
            redirect('lista-proyectos');
        }
    }

    public function list_projects($pagina = FALSE) {
        $inicio = 0;
        $limite = 5;
        if ($pagina) {
            $inicio = ($pagina - 1) * $limite;
        }
        $data = new stdClass();
        $data->content_view = "projects/dash_list_projects";
        $data->title = APP_NAME . "::Lista Proyectos";
        $data->active = "projects";
        $data->maintitle = "Proyectos";
        $data->maindescription = "Lista de Proyectos";
        $data->projects = $this->dash_list_projects($inicio, $limite);
        $this->load->view('dashboard', $data);
    }

    public function list_profiles_by_proyecto($id, $pagina = FALSE) {
        $inicio = 0;
        $limite = 5;
        if ($pagina) {
            $inicio = ($pagina - 1) * $limite;
        }
        $data = new stdClass();
        $data->content_view = "projects/dash_list_profiles";
        $data->title = APP_NAME . "::Lista Perfiles del  Proyecto";
        $data->active = "projects";
        $data->maintitle = "Perfiles";
        $data->maindescription = "Lista de Perfiles";
        $data->profiles = $this->dash_list_profiles_by_project($id, $inicio, $limite);
        $this->load->view('dashboard', $data);
    }
    
    public function list_profiles(){
        
        $data = new stdClass();
        $data->content_view = "projects/dash_list_all_profiles";
        $data->title = APP_NAME . "::Lista Perfiles";
        $data->active = "projects";
        $data->maintitle = "Perfiles";
        $data->maindescription = "Lista de Perfiles";
        $data->profiles = $this->Perfil_model->getAllProfiles();
        $this->load->view('dashboard', $data);
        
    }
    
    public function list_all_requisiciones(){
        $this->load->model("Convocatoria_model");
        $this->load->model('Requisicion_model');
        $data = new stdClass();
        $data->content_view = "projects/list_all_requisiciones";
        $data->title = APP_NAME . "::Lista de Requisiciones";
        $data->active = "projects";
        $data->maintitle = "Requisiciones";
        $data->maindescription = "Lista de Requisiciones";
        $data->requisiciones = $this->Requisicion_model->getAllRequisiciones();
        $this->load->view('dashboard', $data);
    }

}
