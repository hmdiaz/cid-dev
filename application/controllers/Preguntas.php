<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Preguntas
 *
 * @author furbox
 */
class Preguntas extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model([
            "Preguntas_model",
            "Pruebas_model",
            "Status_model",
            "CategoriasComponentes_model"
        ]);
        $this->load->library([
            'Auth'
        ]);
        $this->auth->is_session_start();
    }

    //lista de pruebas
    public function list_questions() {
        $data = new stdClass();
        $data->content_view = "preguntas/dash_list_questions";
        $data->title = APP_NAME . "::Lista de Preguntas";
        $data->active = "preguntas";
        $data->maintitle = "Modulo de Preguntas";
        $data->status = $this->Status_model->get_active_status();
        $data->maindescription = "Preguntas";
        $data->questions = $this->create_questions_table();
        $data->categorias = $this->CategoriasComponentes_model->get();
        $this->load->view('dashboard', $data);
    }

    public function list_test_questions() {
        $id_test = $this->uri->segment(2);
        $test = $this->Pruebas_model->get($id_test);
        $preguntas = $this->Preguntas_model->getPreguntasByCat($test->CategoriaPerfilID);
        $questionsAct = $this->Preguntas_model->getPreguntas($id_test);
        $data = new stdClass();
        $data->content_view = "preguntas/dash_list_test_questions";
        $data->title = APP_NAME . "::Lista de Preguntas";
        $data->active = "pruebas";
        $data->maintitle = "Modulo de Preguntas";
        $data->status = $this->Status_model->get_active_status();
        $data->maindescription = "Preguntas";
        $data->questions = $this->create_test_questions_table($id_test);
        $data->preguntas = $preguntas;
        $data->preg_act = $questionsAct;
        $data->id_test = $id_test;
        $this->load->view('dashboard', $data);
    }

    public function form_new_question() {
        $data = new stdClass();
        $data->content_view = "preguntas/dash_form_new_question";
        $data->title = APP_NAME . "::Lista de Preguntas";
        $data->active = "preguntas";
        $data->maintitle = "Modulo de Preguntas";
        $data->status = $this->Status_model->get_active_status();
        $data->categorias = $this->CategoriasComponentes_model->get();
        $data->maindescription = "Preguntas";
        $this->load->view('dashboard', $data);
    }

    public function add_question() {
        $this->load->library('form_validation');
        if ($this->form_validation->run('dash_add_question') == FALSE) {
            $this->form_new_question();
        } else {

            $pregunta_tipo = $this->input->post('pregunta_tipo');
            $pregunta = $this->input->post('pregunta_title');
            $pregunta_options = $this->input->post('pregunta_options');
            $pregunta_resp = $this->input->post('pregunta_resp');
            $pregunta_estatus = $this->input->post('pregunta_status');
            $pregunta_cat = $this->input->post('pregunta_cat');

            $insert = [
                "pregunta_tipo" => $pregunta_tipo,
                "pregunta_title" => $pregunta,
                "pregunta_options" => $pregunta_options,
                "pregunta_resp" => $pregunta_resp,
                "pregunta_status" => $pregunta_estatus,
                "CategoriaPerfilID" => $pregunta_cat
            ];

            $id = $this->Preguntas_model->insert($insert);
            if ($id > 0) {
                $this->session->set_flashdata('message_success', 'La Pregunta se agrego correctamente.');
                redirect('lista-preguntas');
            }
        }
    }

    public function add_questions() {
        $id_test = $this->input->post('id');
        $cont = 0;
        $this->Preguntas_model->deleteTestQuestions($id_test);
        foreach ($_POST as $key => $value) {
            if ($cont > 0) {
                $id_preg = intval(preg_replace('/[^0-9]+/', '', $key), 10);
                $this->setPreg($id_preg, $this->input->post());
            }
            $cont++;
        }
        $this->session->set_flashdata('message_success', 'La Pregunta se agrego correctamente.');
        redirect(base_url('listar-preguntas/' . $id_test));
    }

    public function setPreg($id_preg) {
        $id_test = $this->input->post('id');
        $preg = $this->Preguntas_model->checkQuestion($id_test, $id_preg);
        if (count($preg) == 0) {
            if ($this->input->post('check_' . $id_preg) == 1) {
                $insert = [
                    "prueba_id" => $id_test,
                    "pregunta_id" => $id_preg,
                    "valor" => $this->input->post('preg_' . $id_preg)
                ];
                $this->Preguntas_model->insertTestQuestion($insert);
            }
        }
    }

    public function create_questions_table() {
        $questions = $this->Preguntas_model->get();
        $questions_table = "";

        if (count($questions) > 0) {
            foreach ($questions as $key => $value) {
                $links = "";

                if ($value->pregunta_status == 1) {
                    $status = "<span class='label label-success'>Activo</span>";
                    $links .= '<a class="btn btn-warning btn-xs btn-o" href="' . base_url('desactivar-pregunta') . '/' . $value->pregunta_id . '">
                                    <i class="fa fa-ban"></i> Desactivar
                                </a>';
                    $links .="<a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-pregunta') . "/" . $value->pregunta_id . "'>
                                        <i class='fa fa-trash'></i> Eliminar
                                    </a>";
                }
                if ($value->pregunta_status == 2) {
                    $status = "<span class='label label-warning'>Inactivo</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-pregunta') . '/' . $value->pregunta_id . '">
                                    <i class="fa fa-ban"></i> Activar
                                </a>';
                }
                if ($value->pregunta_status == 3) {
                    $status = "<span class='label label-danger'>Eliminado</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-pregunta') . '/' . $value->pregunta_id . '">
                                    <i class="fa fa-check"></i> Activar
                                </a>';
                }

                $questions_table .= "<tr>";
                $questions_table .= "<td>{$value->pregunta_title}</td>";
                $questions_table .= "<td>{$value->pregunta_tipo}</td>";
                $questions_table .= "<td>{$value->pregunta_options}</td>";
                $questions_table .= "<td>{$value->pregunta_resp}</td>";
                $questions_table .= "<td>{$value->NombreCategoria}</td>";
                $questions_table .= "<td>{$status}</td>";
                $questions_table .= "<td>
                                    <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-pregunta') . "/" . $value->pregunta_id . "'>
                                        <i class='fa fa-edit'></i> Editar
                                    </a>
                                    " . $links . "
                                </td>";
                $questions_table .= "</tr>";
            }
        }

        return $questions_table;
    }

    public function create_test_questions_table($id_test) {
        $questions = $this->Preguntas_model->getPreguntas($id_test);
        $questions_table = "";

        if (count($questions) > 0) {
            foreach ($questions as $key => $value) {
                $links = "";

                $questions_table .= "<tr>";
                $questions_table .= "<td>{$value->pregunta_title}</td>";
                $questions_table .= "<td>{$value->pregunta_tipo}</td>";
                $questions_table .= "<td>{$value->pregunta_options}</td>";
                $questions_table .= "<td>{$value->pregunta_resp}</td>";
                $questions_table .= "<td>{$value->valor}</td>";
                $questions_table .= "</tr>";
            }
        }

        return $questions_table;
    }

    public function edit_question($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        $question = $this->Preguntas_model->get($id);

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Pregunta no valida');
            redirect('lista-preguntas/' . $question->prueba_id);
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-preguntas/' . $question->prueba_id);
        }

        if (!count($question) == 1) {
            $this->session->set_flashdata('message_error', 'Pregunta no encontrada');
            redirect('lista-preguntas/' . $question->prueba_id);
        }

        $data = new stdClass();
        $data->content_view = "preguntas/dash_form_edit_question";
        $data->title = APP_NAME . "::Editar Pregunta";
        $data->active = "pruebas";
        $data->maintitle = "Modulo de Pruebas";
        $data->maindescription = "Editar Pregunta";
        $data->status = $data->status = $this->Status_model->get_active_status();
        $data->categorias = $this->CategoriasComponentes_model->get();
        $data->pregunta = $question;

        $this->load->view('dashboard', $data);
    }

    public function update_question() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        $question = $this->Preguntas_model->get($id);

        if ($this->form_validation->run('dash_add_question') == FALSE) {
            $this->edit_question($id);
        } else {

            $quest_name = $this->input->post('pregunta_title');
            $quest_tipo = $this->input->post('pregunta_tipo');
            $quest_option = $this->input->post('pregunta_options');
            $quest_resp = $this->input->post('pregunta_resp');
            $pregunta_cat = $this->input->post('pregunta_cat');

            $update = [
                "pregunta_tipo" => $quest_tipo,
                "pregunta_title" => $quest_name,
                "pregunta_options" => $quest_option,
                "pregunta_resp" => $quest_resp,
                "CategoriaPerfilID" => $pregunta_cat
            ];

            $question_id = $this->Preguntas_model->update($id, $update);
            if ($question_id > 0) {
                $this->session->set_flashdata('message_success', 'La Pregunta se actualizo correctamente.');
                redirect('lista-preguntas/' . $question->prueba_id);
            }
        }
    }

    public function active_question($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }
        $question = $this->Preguntas_model->get($id);

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Pregunta no valida');
            redirect('lista-preguntas/' . $question->prueba_id);
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-preguntas/' . $question->prueba_id);
        }

        if (!count($question) == 1) {
            $this->session->set_flashdata('message_error', 'Pregunta no encontrada');
            redirect('lista-preguntas/' . $question->prueba_id);
        }
        $update = [
            'pregunta_status' => 1
        ];
        $id_question = $this->Preguntas_model->update($id, $update);
        if ($id_question > 0) {
            $this->session->set_flashdata('message_success', 'La Pregunta se ha Activado.');
            redirect('lista-preguntas/' . $question->prueba_id);
        }
    }

    public function unactive_question($id = FALSE) {

        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        $question = $this->Preguntas_model->get($id);

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Pregunta no valida');
            redirect('lista-preguntas/' . $question->prueba_id);
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-preguntas/' . $question->prueba_id);
        }

        if (!count($question) == 1) {
            $this->session->set_flashdata('message_error', 'Pregunta no encontrada');
            redirect('lista-preguntas/' . $question->prueba_id);
        }
        $update = [
            'pregunta_status' => 2
        ];
        $id_question = $this->Preguntas_model->update($id, $update);
        if ($id_question > 0) {
            $this->session->set_flashdata('message_success', 'La Pregunta se ha Desactivado.');
            redirect('lista-preguntas/' . $question->prueba_id);
        }
    }

    public function delete_question($id = FALSE) {

        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        $question = $this->Preguntas_model->get($id);

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Pregunta no valida');
            redirect('lista-preguntas/' . $question->prueba_id);
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-preguntas/' . $question->prueba_id);
        }

        if (!count($question) == 1) {
            $this->session->set_flashdata('message_error', 'Pregunta no encontrada');
            redirect('lista-preguntas/' . $question->prueba_id);
        }
        $update = [
            'pregunta_status' => 3
        ];
        $id_question = $this->Preguntas_model->update($id, $update);
        if ($id_question > 0) {
            $this->session->set_flashdata('message_success', 'La Pregunta se ha Eliminado.');
            redirect('lista-preguntas/' . $question->prueba_id);
        }
    }

}
