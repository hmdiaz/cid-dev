<?php
class TipoResponsabilidad extends CI_Controller
{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model([
            "TipoResponsabilidad_model"
        ]);

        $this->load->library(['Auth']);
        $this->auth->is_session_start();
    }

    public function list_tipo_responsabilidad() {
        $data = new stdClass();
        $data->content_view = "tipo_responsabilidad/dash_tipo_responsabilidad";
        $data->title = APP_NAME . "::Lista Tipos de Responsabilidad";
        $data->active = "cpntes";
        $data->maintitle = "Tipos de Responsabilidad";
        $data->maindescription = "Lista Tipos de Responsabilidad";
        $data->tipos_responsabilidad = $this->TipoResponsabilidad_model->get(FALSE, 1);
        $this->load->view('dashboard', $data);
    }

    public function form_new_tipo_responsabilidad() {
        $data = new stdClass();
        $data->content_view = "tipo_responsabilidad/dash_form_add_tipo_responsabilidad";
        $data->title = APP_NAME . "::Nuevo Tipo de Responsabilidad";
        $data->active = "cpntes";
        $data->maintitle = "Tipos de Responsabilidad";
        $data->maindescription = "Crear Nuevo Tipo de Responsabilidad";

        $this->load->view('dashboard', $data);
    }

    public function add_tipo_responsabilidad() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_tipo_responsabilidad') == FALSE) {
            $this->form_new_tipo_responsabilidad();
        } else {

            $tipo_responsabilidad_name = $this->input->post('tipo_responsabilidad_name');
            $insert = [
                "Descripcion" => $tipo_responsabilidad_name
            ];

            $tipo_responsabilidad_id = $this->TipoResponsabilidad_model->insert($insert);
            if ($tipo_responsabilidad_id > 0) {
                $this->session->set_flashdata('message_success', 'El Tipo de Responsabilidad  se agrego correctamente.');
            }
            redirect('lista-tipo-responsabilidad');
        }
    }

    public function edit_tipo_responsabilidad($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Tipo de Responsabilidad no valido');
            redirect('lista-tipo-responsabilidad');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-tipo-responsabilidad');
        }
        $tipo_responsabilidad = $this->TipoResponsabilidad_model->get($id);
        if (!count($tipo_responsabilidad) == 1) {
            $this->session->set_flashdata('message_error', 'Tipo de Responsabilidad no encontrado');
            redirect('lista-tipo-responsabilidad');
        }
        $data = new stdClass();
        $data->content_view = "tipo_responsabilidad/dash_form_edit_tipo_responsabilidad";
        $data->title = APP_NAME . "::Editar Tipo Responsabilidad";
        $data->active = "cpntes";
        $data->maintitle = "Tipos de Responsabilidades";
        $data->maindescription = "Editar Tipo de Responsabilidad";

        $data->tipo_responsabilidad = $tipo_responsabilidad;

        $this->load->view('dashboard', $data);
    }

    public function update_tipo_responsabilidad() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_tipo_responsabilidad') == FALSE) {
            $this->edit_tipo_responsabilidad($id);
        } else {
            $tipo_responsabilidad_name = $this->input->post('tipo_responsabilidad_name');

            $update = [
                "Descripcion" => $tipo_responsabilidad_name
            ];

            $responsabilidad_id = $this->TipoResponsabilidad_model->update($id, $update);
            if ($responsabilidad_id > 0) {
                $this->session->set_flashdata('message_success', 'El Tipo de Responsabilidad se actualizo correctamente.');
            }
            redirect('lista-tipo-responsabilidad');
        }
    }

    public function delete_tipo_responsabilidad($id = FALSE)
    {
        $perfil = $this->TipoResponsabilidad_model->get($id);
        if (!$id) {
            $id = (int)$this->uri->segment(2);
        } else {
            $id = (int)$id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Tipo de Responsabilidad no valido');
            redirect('lista-tipo-responsabilidad');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-tipo-responsabilidad');
        }

        if (!count($perfil) == 1) {
            $this->session->set_flashdata('message_error', 'Tipo de Responsabilidad no encontrado');
            redirect('lista-tipo-responsabilidad');
        }

        $num_rows = $this->TipoResponsabilidad_model->update($id, ['EstadoID' => 3]);
        if ($num_rows > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Tipo de Responsabilidad se ha eliminado.');
            redirect('lista-tipo-responsabilidad');
        }
    }
}