<?php
class Etapa extends CI_Controller
{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model([
            "Etapa_model",
            "Pruebas_model",
            "Pruebasconfig_model",
            "EtapaConvocatoria_model"
        ]);

        $this->load->library(['Auth']);
        $this->auth->is_session_start();
    }

    public function list_etapas() {
        $data = new stdClass();
        $data->content_view = "etapa/dash_list_etapas";
        $data->title = APP_NAME . "::Lista Etapas";
        $data->active = "cpntes";
        $data->maintitle = "Etapas";
        $data->maindescription = "Lista de Etapas";
        $data->etapas = $this->Etapa_model->get(FALSE, 1);
        $this->load->view('dashboard', $data);
    }

    public function form_new_etapa() {
        $data = new stdClass();
        $data->content_view = "etapa/dash_form_add_etapa";
        $data->title = APP_NAME . "::Nueva Etapa";
        $data->active = "cpntes";
        $data->maintitle = "Etapas";
        $data->maindescription = "Crear Nueva Etapa";

        $this->load->view('dashboard', $data);
    }

    public function add_etapa() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_etapa') == FALSE) {
            $this->form_new_etapa();
        } else {

            $etapa_name = $this->input->post('etapa_name');
            $etapa_desc = $this->input->post('etapa_desc');

            $insert = [
                "Descripcion" => $etapa_desc,
                "Nombre" => $etapa_name
            ];

            $etapa_id = $this->Etapa_model->insert($insert);
            if ($etapa_id > 0) {
                $this->session->set_flashdata('message_success', 'La Etapa se agrego correctamente.');
            }
            redirect('lista-etapas');
        }
    }

    public function edit_etapa($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Etapa no valido');
            redirect('lista-etapas');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-etapas');
        }
        $etapa = $this->Etapa_model->get($id);
        if (!count($etapa) == 1) {
            $this->session->set_flashdata('message_error', 'Etapa no encontrado');
            redirect('lista-etapas');
        }
        $data = new stdClass();
        $data->content_view = "etapa/dash_form_edit_etapa";
        $data->title = APP_NAME . "::Editar Etapa";
        $data->active = "cpntes";
        $data->maintitle = "Etapa";
        $data->maindescription = "Editar Etapa";

        $data->etapa = $etapa;

        $this->load->view('dashboard', $data);
    }

    public function update_etapa() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_etapa') == FALSE) {
            $this->edit_etapa($id);
        } else {
            $etapa_name = $this->input->post('etapa_name');
            $etapa_desc = $this->input->post('etapa_desc');

            $update = [
                "Descripcion" => $etapa_desc,
                "Nombre" => $etapa_name
            ];

            $socio_id = $this->Etapa_model->update($id, $update);
            if ($socio_id > 0) {
                $this->session->set_flashdata('message_success', 'La Etapa se actualizo correctamente.');
            }
            redirect('lista-etapas');
        }
    }

    public function delete_etapa($id = FALSE)
    {
        $perfil = $this->Etapa_model->get($id);
        if (!$id) {
            $id = (int)$this->uri->segment(2);
        } else {
            $id = (int)$id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Etapa no valido');
            redirect('lista-etapas');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-etapas');
        }

        if (!count($perfil) == 1) {
            $this->session->set_flashdata('message_error', 'Etapa no encontrado');
            redirect('lista-etapas');
        }

        $num_rows = $this->Etapa_model->update($id, ['EstadoID' => 3]);
        if ($num_rows > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'La Etapa se ha eliminado.');
            redirect('lista-etapas');
        }
    }
    
    public function configurar_etapa(){
        $id_etapa_convocatoria = (int)$this->uri->segment(2);
        $etapa = $this->EtapaConvocatoria_model->get($id_etapa_convocatoria);
        $data = new stdClass();
        $data->content_view = "etapa/dash_form_config_etapa";
        $data->title = APP_NAME . "::Configurar Etapa";
        $data->active = "convocatorias";
        $data->maintitle = "Etapa";
        $data->maindescription = "Configurar Etapa";
        $data->etapa = $etapa;
        $data->tests = $this->Pruebas_model->get_active();
        $data->id_etapa_convocatoria = $id_etapa_convocatoria;
        $this->load->view('dashboard', $data);
    }
    
    public function add_config(){
        $id_etapa_convocatoria = $this->input->post('id');
        $etapa = $this->EtapaConvocatoria_model->get($id_etapa_convocatoria);
        $id_test = $this->input->post('etapa_test');
        $time = $this->input->post('etapa_time');
        
        $insert =[
            "EtapaConvocatoriaID" => $id_etapa_convocatoria,
            "pruebasconfig_tiempo" => $time,
            "prueba_id" => $id_test
        ];
        
        $success = $this->Pruebasconfig_model->insert($insert);
        if ($success > 0) {
            $this->session->set_flashdata('message_success', 'Configuracion Exitosa.');
            redirect('etapas/'.$etapa->ConvocatoriaID);
        }
    }
}