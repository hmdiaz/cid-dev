<?php
class Area extends CI_Controller
{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model([
            "Area_model"
        ]);
        $this->load->library([
            'Auth'
        ]);

        $this->auth->is_session_start();
    }

    public function list_areas() {
        $data = new stdClass();
        $data->content_view = "area/list_areas";
        $data->title = APP_NAME . "::Lista Áreas";
        $data->active = "cpntes";
        $data->maintitle = "Áreas";
        $data->maindescription = "Lista de Áreas";
        $data->areas = $this->Area_model->get(FALSE, 1);
        $this->load->view('dashboard', $data);
    }

    public function form_new_area() {
        $data = new stdClass();
        $data->content_view = "area/dash_form_add_area";
        $data->title = APP_NAME . "::Nueva Área";
        $data->active = "cpntes";
        $data->maintitle = "Áreas";
        $data->maindescription = "Crear Nueva Área";

        $this->load->view('dashboard', $data);
    }

    public function add_area() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_area') == FALSE) {
            $this->form_new_area();
        } else {

            $project_name = $this->input->post('area_name');
            $apruebaReq = $this->input->post('chkApruebaReq');

            $insert = [
                "NombreProyecto" => $project_name,
                "TipoProyectoID" => 2,
                "ApruebaRequisicion" => isset($apruebaReq)
            ];

            $socio_id = $this->Area_model->insert($insert);
            if ($socio_id > 0) {
                $this->session->set_flashdata('message_success', 'El Área se agrego correctamente.');
            }
            redirect('lista-areas');
        }
    }

    public function edit_area($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Área no valido');
            redirect('lista-areas');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-areas');
        }
        $area = $this->Area_model->get($id);
        if (!count($area) == 1) {
            $this->session->set_flashdata('message_error', 'Área no encontrado');
            redirect('lista-areas');
        }
        $data = new stdClass();
        $data->content_view = "area/dash_form_edit_area";
        $data->title = APP_NAME . "::Editar Área";
        $data->active = "cpntes";
        $data->maintitle = "Áreas";
        $data->maindescription = "Editar Área";
        $data->tipos_socios = $this->Area_model->get(FALSE, 1);
        $data->area = $area;

        $this->load->view('dashboard', $data);
    }

    public function update_area() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_area') == FALSE) {
            $this->edit_area($id);
        } else {
            $project_name = $this->input->post('area_name');
            $apruebaReq = $this->input->post('chkApruebaReq');

            $update = [
                "NombreProyecto" => $project_name,
                "TipoProyectoID" => 2,
                "ApruebaRequisicion" => isset($apruebaReq)
            ];

            $socio_id = $this->Area_model->update($id, $update);
            if ($socio_id > 0) {
                $this->session->set_flashdata('message_success', 'El Área se actualizo correctamente.');
            }
            redirect('lista-areas');
        }
    }

    public function delete_area($id = FALSE)
    {
        $perfil = $this->Area_model->get($id);
        if (!$id) {
            $id = (int)$this->uri->segment(2);
        } else {
            $id = (int)$id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Área no valido');
            redirect('lista-areas');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-areas');
        }

        if (!count($perfil) == 1) {
            $this->session->set_flashdata('message_error', 'Área no encontrado');
            redirect('lista-areas');
        }

        $num_rows = $this->Area_model->update($id, ['EstadoID' => 3]);
        if ($num_rows > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Área se ha eliminado.');
            redirect('lista-areas');
        }
    }
}