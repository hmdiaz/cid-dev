<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users
 *
 * @author furbox
 */
class Users extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->library([
            'Auth',
            'Utilities',
            'Sendmail'
        ]);

        $this->load->model([
            'Roles_model',
            'Users_model',
            'Status_model',
            'TipoDocumento_model',
            'DatosUsuario_model',
            'Cargos_model'
        ]);

//        $modulos = [
//            'users'
//        ];
//        $this->auth->is_access($modulos);
    }

    //crea un nuevo usuario desde el dash
    public function dash_form_add_user() {
        $this->auth->is_session_start();
        $data = new stdClass();
        $data->content_view = "users/dash_form_add_user";
        $data->title = APP_NAME . "::Nuevo Usuario";
        $data->active = "users";
        $data->maintitle = "Usuarios";

        $data->roles = $this->generate_select_roles();
        $data->status = $this->generate_select_status();
        $data->cargos = $this->generate_select_cargos();

        $data->maindescription = "Crear Nuevo Usuario";

        $this->load->view('dashboard', $data);
    }

    public function form_register() {
        $data = new stdClass();
        $data->title = APP_NAME . "::Nueva Cuenta de Usuario";
        $data->active = "users";
        $data->maintitle = "Usuarios";

        $data->cargos = $this->generate_select_cargos();

        $data->maindescription = "Registro de Usuario";

        $this->load->view('form_register', $data);
    }

    public function ext_register_new_user() {

        $data = new stdClass();
        $data->title = APP_NAME . "::Nuevo Usuario";
        $data->active = "users";
        $data->maintitle = "Usuarios";
        $data->maindescription = "Crear Nuevo Usuario";
        $this->load->view('users/ext_register_user', $data);
    }

    //lista usuarios en el dash
    public function dash_list_user() {
        $this->auth->is_session_start();
        $data = new stdClass();
        $data->content_view = "users/dash_list_user";
        $data->title = APP_NAME . "::Lista de Usuarios";
        $data->active = "users";
        $data->maintitle = "Usuarios";
        $data->maindescription = "Lista de Usuarios";
        $data->list_users = $this->create_users_table();
        $this->load->view('dashboard', $data);
    }

    public function dash_perfil_user() {
        $data = new stdClass();
        $id = $_SESSION['id_user'];
        $user = $this->Users_model->get($id);
        $userDatos = $this->DatosUsuario_model->getByUsuarioID($user->UsuarioID);
        $data->cargos = $this->Cargos_model->get_active();
        $data->user = $user;
        $data->userdatos = $userDatos;
        $data->content_view = "users/dash_perfil_edit_user";
        $data->title = APP_NAME . "::Editar de Usuario";
        $data->active = "users";
        $data->maintitle = "Usuarios";

        $data->maindescription = "Edicion de Usuario";

        $this->load->view('dashboard', $data);
    }

    public function dash_insert_new_user() {

        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_user') == FALSE) {
            if ($this->input->post('form_register') == "register") {
                $this->form_register();
            } else {
                $this->dash_form_add_user();
            }
        } else {

            $EmailUsuario = $this->input->post('user_email');
            $NombresUsuario = $this->input->post('user_nombres');
            $ApellidosUsuario = $this->input->post('user_apellidos');
            $Clave = $this->input->post('user_pass');
            $cargo = $this->input->post('user_cargo');
            $Salt = $this->utilities->random_salt();
            $user_pass_hash = $this->utilities->hash_passwd($Clave, $Salt);
            $date_created = date('Y-m-d H:i:s');
            $activation_code = sha1($date_created);

            if ($_FILES['userfile']['name'] != NULL) {
                $ram1 = $this->utilities->randomString(4);
                $ram2 = $this->utilities->randomString(8);
                $file_name = $_FILES['userfile']['name'];
                $tmp = explode('.', $file_name);
                $extension_img = end($tmp);

                $user_img_profile = $ram1 . '_' . $ram2 . '.' . $extension_img;

                $config['upload_path'] = './assets/images/users_img/';
                //'allowed_types' => "gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp",
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size'] = '5000000';
                $config['quality'] = '90%';
                $config['width'] = 150;
                $config['height'] = 180;
                $config['file_name'] = $user_img_profile;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    if ($this->input->post('form_register') == "register") {
                        $this->session->set_flashdata('message_error', $this->upload->display_errors());
                        $this->form_register();
                    } else {
                        $this->session->set_flashdata('message_error', $this->upload->display_errors());
                        $this->dash_form_add_user();
                    }
                }
            } else {
                $user_img_profile = 'default-user.png';
            }
            if ($this->input->post('user_rol')) {
                $roleid = $this->input->post('user_rol');
            } else {
                $roleid = 1;
            }

            if ($this->input->post('user_status')) {
                $estadoID = $this->input->post('user_status');
            } else {
                $estadoID = 2;
            }

            $insert = [
                'EmailUsuario' => $EmailUsuario,
                'Clave' => $user_pass_hash,
                'Salt' => $Salt,
                'FechaUltimaSesion' => "",
                'FechaCreacion' => $date_created,
                'FechaActualizacion' => $date_created,
                'CodigoActivacion' => $activation_code,
                'FotoUsuario' => $user_img_profile,
                'RolID' => $roleid,
                'EstadoID' => $estadoID
            ];

            $this->db->trans_start();
            $id_user = $this->Users_model->insert($insert);

            if ($id_user > 0) {
                $insert2 = [
                    'UsuarioID' => $id_user,
                    'Apellidos' => $ApellidosUsuario,
                    'Nombres' => $NombresUsuario,
                    'CargoID' => $cargo
                ];
                $id_user2 = $this->DatosUsuario_model->insert($insert2);

                if ($id_user2 > 0) {
                    $this->db->trans_complete();
                    //$this->authentication->send_validation_email($user_email,$user_activation_code);
                    if ($this->input->post('form_register') == "register") {
                        $this->session->set_flashdata('message_success', 'Cuenta creada satisfactoriamente Espera que un administrador la active');
                        redirect('signin');
                    } else {
                        $this->session->set_flashdata('message_success', 'Cuenta creada satisfactoriamente');
                        redirect('lista-usuarios');
                    }
                } else {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message_error', 'Ocurrio un error al crear el usuario');
                    redirect('lista-usuarios');
                }
            } else {
                $this->db->trans_rollback();
                $this->session->set_flashdata('message_error', 'Ocurrio un error al crear el usuario');
                redirect('lista-usuarios');
            }
        }
    }

    public function ext_register_user() {

        $this->load->library('form_validation');

        if ($this->form_validation->run('ext_register_user') == FALSE) {
            $this->ext_register_new_user();
        } else {

            $EmailUsuario = $this->input->post('user_email');
            $Clave = $this->input->post('user_pass');
            $Salt = $this->utilities->random_salt();
            $user_pass_hash = $this->utilities->hash_passwd($Clave, $Salt);
            $date_created = date('Y-m-d H:i:s');
            $activation_code = sha1($date_created);

            $roleid = 6;
            $estadoID = 2;

            $insert = [
                'EmailUsuario' => $EmailUsuario,
                'Clave' => $user_pass_hash,
                'Salt' => $Salt,
                'FechaUltimaSesion' => "",
                'FechaCreacion' => $date_created,
                'FechaActualizacion' => $date_created,
                'CodigoActivacion' => $activation_code,
                'RolID' => $roleid,
                'EstadoID' => $estadoID
            ];
            $id_user = $this->Users_model->insert($insert);
            $this->DatosUsuario_model->insert(['UsuarioID' => $id_user]);
            if ($id_user > 0) {
                //$this->authentication->send_validation_email($user_email,$user_activation_code);
                $subject = 'Validacion de Correo eletronico de '.APP_NAME;
                $message = '<a href="' . base_url('verification') . '/' . $activation_code . '">Validar Email</a>';
                $this->sendmail->sendMail($EmailUsuario, $subject, $message);
                $this->session->set_flashdata('message_success', 'Gracias por registrarte con nosotros. Te enviamos un codigo de confirmacion a tu correo para la activacion de tu cuenta.');
                redirect('login-ext');
            }
        }
    }

    //edita un usuario desde el dash
    public function dash_form_edit_user() {
        $this->auth->is_session_start();
        $data = new stdClass();
        if (!$this->uri->segment(2)) {
            $this->session->set_flashdata('message_error', 'Usuario no valido');
            redirect('lista-usuarios');
        }
        $id = (int) $this->uri->segment(2);
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-usuarios');
        }
        $user = $this->Users_model->get($id);
        $userDatos = $this->DatosUsuario_model->getByUsuarioID($user->UsuarioID);
        if (!count($user) == 1) {
            $this->session->set_flashdata('message_error', 'Ususario no encontrado');
            redirect('lista-usuarios');
        }
        $data->user = $user;
        $data->userdatos = $userDatos;
        $data->roles = $this->Roles_model->get_active_roles();
        $data->status = $this->Status_model->get_active_status();
        $data->cargos = $this->Cargos_model->get_active();
        $data->content_view = "users/dash_edit_user";
        $data->title = APP_NAME . "::Editar de Usuario";
        $data->active = "users";
        $data->maintitle = "Usuarios";

        $data->maindescription = "Edicion de Usuario";

        $this->load->view('dashboard', $data);
    }

    public function check_password_strength($pass) {
        return $this->auth->check_password_strength($pass);
    }

    public function generate_select_roles() {
        $roles = $this->Roles_model->get_active_roles();
        $options = "";
        if (count($roles)) {
            foreach ($roles as $key => $value) {
                $options.= "<option value ='{$value->RolID}'>{$value->Nombre}</options>";
            }
        }

        return $options;
    }

    public function generate_select_status() {
        $status = $this->Status_model->get_active_status();
        $options = "";
        if (count($status)) {
            foreach ($status as $key => $value) {
                $options.= "<option value ='{$value->EstadoID}'>{$value->Nombre}</options>";
            }
        }

        return $options;
    }

    public function generate_select_cargos() {
        $cargos = $this->Cargos_model->get_active();
        $options = "";
        if (count($cargos)) {
            foreach ($cargos as $key => $value) {
                $options.= "<option value ='{$value->cargoID}'>{$value->nombreCargo}</options>";
            }
        }

        return $options;
    }

    public function create_users_table() {

        $users = $this->Users_model->getInternos();


        $users_table = "";

        if (count($users) > 0) {
            foreach ($users as $key => $value) {
                $links = "";
                if ($value->EstadoID == 1) {
                    $status = "<span class='label label-success'>Activo</span>";
                    $links .= '<a class="btn btn-danger btn-xs btn-o delete" href="' . base_url('eliminar-usuario') . '/' . $value->UsuarioID . '">
                                    <i class="fa fa-ban"></i> Eliminar
                                </a>';
                }
                if ($value->EstadoID == 2) {
                    $status = "<span class='label label-warning'>Inactivo</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-usuario') . '/' . $value->UsuarioID . '">
                                    <i class="fa fa-check"></i> Activar
                                </a>';
                }
                if ($value->EstadoID == 3) {
                    $status = "<span class='label label-danger'>Eliminado</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-usuario') . '/' . $value->UsuarioID . '">
                                    <i class="fa fa-check"></i> Activar
                                </a>';
                }

                $users_table .= "<tr>";
                $users_table .= "<td>{$value->Nombres} {$value->Apellidos}</td>";
                $users_table .= "<td>{$value->EmailUsuario}</td>";
                $users_table .= "<td>{$status}</td>";
                $users_table .= "<td>
                    
                                            <a class='btn btn-primary  btn-xs btn-o' href='" . base_url('editar-usuario') . "/" . $value->UsuarioID . "'>
                                                <i class='fa fa-edit'></i> Editar
                                            </a>
                                            
                                        " . $links . "                                        

                                            <a class='btn btn-warning btn-xs btn-o' href='" . base_url('cambiar-clave-usuario') . "/" . $value->UsuarioID . "'>
                                                <i class='fa fa-key'></i> Cambiar Contraseña
                                            </a>
                                </td>";
                $users_table .= "</tr>";
            }
        }

        return $users_table;
    }

    public function ext_login_form() {
        $data = new stdClass();
        $data->title = APP_NAME . "::Ingreso de Usuario";
        return $this->load->view('users/ext_form_login', $data);
    }

    public function ext_register_form_user() {
        $data = new stdClass();
        $data->title = APP_NAME . "::Registro de Usuario";
        //$data->list_users = $this->create_users_table();
        $this->load->view('users/ext_register_user',$data);
    }

    public function dash_update_user() {

        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_update_user') == FALSE) {
            $this->dash_form_edit_user();
        } else {
            $id = $this->input->post('id');
            $user = $this->Users_model->get($id);
            $user2 = $this->DatosUsuario_model->getByUsuarioID($id);
            $NombresUsuario = $this->input->post('user_nombres');
            $ApellidosUsuario = $this->input->post('user_apellidos');
            $cargo = $this->input->post('user_cargo');
            $roleid = $this->input->post('user_rol');
            $estadoID = $this->input->post('user_status');
            $date_update = date('Y-m-d H:i:s');
            if ($_FILES['userfile']['name'] != NULL) {
                $ram1 = $this->utilities->randomString(4);
                $ram2 = $this->utilities->randomString(8);
                $file_name = $_FILES['userfile']['name'];
                $tmp = explode('.', $file_name);
                $extension_img = end($tmp);
                $user_img_profile = $ram1 . '_' . $ram2 . '.' . $extension_img;
                $config['upload_path'] = './assets/images/users_img/';
                //'allowed_types' => "gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp",
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size'] = '5000000';
                $config['quality'] = '90%';
                $config['width'] = 150;
                $config['height'] = 180;
                $config['file_name'] = $user_img_profile;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->session->set_flashdata('message_error', $this->upload->display_errors());
                    $this->dash_form_add_user();
                }
            } else {
                $user_img_profile = $user->FotoUsuario;
            }
            $update = [
                'FechaActualizacion' => $date_update,
                'FotoUsuario' => $user_img_profile,
                'RolID' => $roleid,
                'EstadoID' => $estadoID
            ];

            if ($user2->Nombres != $NombresUsuario || $user2->Apellidos != $ApellidosUsuario || $user2->CargoID != $cargo) {
                $update2 = [
                    "Apellidos" => $ApellidosUsuario,
                    "Nombres" => $NombresUsuario,
                    "CargoID" => $cargo
                ];
            }

            $this->db->trans_start();
            $id_user = $this->Users_model->update($id, $update);


            if ($id_user > 0) {

                if ($user2->Nombres != $NombresUsuario || $user2->Apellidos != $ApellidosUsuario || $user2->CargoID != $cargo) {
                    $id_user2 = $this->DatosUsuario_model->update($id, $update2);
                    if ($id_user2 > 0) {
                        $this->db->trans_complete();
                        //$this->authentication->send_validation_email($user_email,$user_activation_code);
                        $this->session->set_flashdata('message_success', 'Los Datos se Actualizaron Correctamente.');
                        redirect('lista-usuarios');
                    } else {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('message_error', 'Error al actualizar Datos');
                        redirect('lista-usuarios');
                    }
                } else {
                    $this->db->trans_complete();
                    //$this->authentication->send_validation_email($user_email,$user_activation_code);
                    $this->session->set_flashdata('message_success', 'Los Datos se Actualizaron Correctamente.');
                    redirect('lista-usuarios');
                }
            } else {
                $this->db->trans_rollback();
                $this->session->set_flashdata('message_error', 'Error al actualizar Datos');
                redirect('lista-usuarios');
            }
        }
    }

    public function dash_update_perfil_user() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_update_user') == FALSE) {
            $this->dash_form_edit_user();
        } else {
            $id = $this->input->post('id');
            $user = $this->Users_model->get($id);
            $user2 = $this->DatosUsuario_model->getByUsuarioID($id);
            $NombresUsuario = $this->input->post('user_nombres');
            $ApellidosUsuario = $this->input->post('user_apellidos');
            $cargo = $this->input->post('user_cargo');

            $date_update = date('Y-m-d H:i:s');
            if ($_FILES['userfile']['name'] != NULL) {
                $ram1 = $this->utilities->randomString(4);
                $ram2 = $this->utilities->randomString(8);
                $file_name = $_FILES['userfile']['name'];
                $tmp = explode('.', $file_name);
                $extension_img = end($tmp);

                $user_img_profile = $ram1 . '_' . $ram2 . '.' . $extension_img;
                $config['upload_path'] = './assets/images/users_img/';
                //'allowed_types' => "gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp",
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size'] = '5000000';
                $config['quality'] = '90%';
                $config['width'] = 150;
                $config['height'] = 180;
                $config['file_name'] = $user_img_profile;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->session->set_flashdata('message_error', $this->upload->display_errors());
                    $this->dash_form_edit_user();
                }
            } else {
                $user_img_profile = $user->FotoUsuario;
            }

            $update = [
                'FechaActualizacion' => $date_update,
                'FotoUsuario' => $user_img_profile
            ];


            if ($user2->Nombres != $NombresUsuario || $user2->Apellidos != $ApellidosUsuario || $user2->CargoID != $cargo) {

                $update2 = [
                    "Apellidos" => $ApellidosUsuario,
                    "Nombres" => $NombresUsuario,
                    'CargoID' => $cargo
                ];
            }

            $this->db->trans_start();

            $id_user = $this->Users_model->update($id, $update);
            if ($id_user > 0) {
                if ($user2->Nombres != $NombresUsuario || $user2->Apellidos != $ApellidosUsuario || $user2->CargoID != $cargo) {
                    $id_user2 = $this->DatosUsuario_model->update($id, $update2);

                    if ($id_user2 > 0) {
                        $this->db->trans_complete();
                        //$this->authentication->send_validation_email($user_email,$user_activation_code);
                        $this->session->set_flashdata('message_success', 'Los Datos se Actualizaron Correctamente.');
                        redirect('perfil');
                    } else {
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('message_error', 'Error al actualizar Datos');
                        redirect('perfil');
                    }
                } else {
                    $this->db->trans_complete();
                    //$this->authentication->send_validation_email($user_email,$user_activation_code);
                    $this->session->set_flashdata('message_success', 'Los Datos se Actualizaron Correctamente.');
                    redirect('perfil');
                }
            } else {
                $this->db->trans_rollback();
                $this->session->set_flashdata('message_error', 'Error al actualizar Datos');
                redirect('perfil');
            }
        }
    }

    //activa usuarios desde el dash
    public function active_user() {
        $this->auth->is_session_start();
        if (!$this->uri->segment(2)) {
            $this->session->set_flashdata('message_error', 'Usuario no valido');
            redirect('lista-usuarios');
        }
        $id = (int) $this->uri->segment(2);
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-usuarios');
        }
        $user = $this->Users_model->get($id);
        if (!count($user) == 1) {
            $this->session->set_flashdata('message_error', 'Ususario no encontrado');
            redirect('lista-usuarios');
        }
        $update = [
            'EstadoID' => 1
        ];
        $id_user = $this->Users_model->update($id, $update);
        if ($id_user > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El usuario se ha activado.');
            redirect('lista-usuarios');
        }
    }

    //elimina usuarios desde el dash
    public function delete_user($id = FALSE) {
        $this->auth->is_session_start();   
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Usuario no valido');
            redirect('lista-usuarios');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-usuarios');
        }
        $user = $this->Users_model->get($id);
        if (!count($user) == 1) {
            $this->session->set_flashdata('message_error', 'Usuario no encontrado');
            redirect('lista-usuarios');
        }
        $update = [
            'EstadoID' => 3
        ];
        $id_user = $this->Users_model->update($id, $update);
        if ($id_user > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El usuario se ha eliminado.');
            redirect('lista-usuarios');
        }
    }
    //formulario para cambiar la contraseña de un usuario
    public function form_change_password($id = FALSE) {
        $this->auth->is_session_start();   
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }
        if (!$id) {
            $this->session->set_flashdata('message_error', 'Usuario no valido');
            redirect('lista-usuarios');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-usuarios');
        }
        $user = $this->Users_model->get($id);
        if (!count($user) == 1) {
            $this->session->set_flashdata('message_error', 'Usuario no encontrado');
            redirect('lista-usuarios');
        }
        $data = new stdClass();
        $data->id = $id;
        $data->content_view = "users/dash_form_change_pass_user";
        $data->title = APP_NAME . "::Cambio de contraseña de Usuario";
        $data->active = "users";
        $data->maintitle = "Usuarios";

        $data->maindescription = "Cambio de contraseña de Usuario";
        //$data->list_users = $this->create_users_table();
        $this->load->view('dashboard', $data);
    }

    public function update_password_user() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_password_user') == FALSE) {
            $this->form_change_password($id);
        } else {
            $id = $this->input->post('id');
            $user = $this->Users_model->get($id);

            $Clave = $this->input->post('user_pass');
            $user_pass_hash = $this->utilities->hash_passwd($Clave, $user->Salt);
            $date_update = date('Y-m-d H:i:s');

            $update = [
                'Clave' => $user_pass_hash,
                'FechaActualizacion' => $date_update,
            ];
            $id_user = $this->Users_model->update($id, $update);
            if ($id_user > 0) {
                //$this->authentication->send_validation_email($user_email,$user_activation_code);
                $this->session->set_flashdata('message_success', 'La Contraseña se actualizo Correctamente.');
                redirect('lista-usuarios');
            }
        }
    }

}
