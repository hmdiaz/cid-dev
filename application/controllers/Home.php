<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 * @author furbox
 */
class Home extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model("Convocatoria_model");
        $this->load->model("PostulantesConvocatorias_model");
        $this->load->model("ComponenteCompetencia_model");
        $data = new stdClass();
        $data->convocatorias = $this->Convocatoria_model->getListaConvocatorias();
        $data->content_view = "home/index";
        $data->title = APP_NAME . ":: Convocatorias CID";
        $data->active = "home";
        $data->maintitle = "Convocatorias CID";
        $data->maindescription = "Lista de ofertas de trabajo";
        $this->load->view('home', $data);
    }

    public function send_email_users() {
        $this->load->model("Users_model");
        $this->load->library("Sendmail");
        $users = $this->Users_model->getByEstatus();
        foreach ($users as $user) {
            $subject = 'Actualizacion de Perfil ' . APP_NAME;
            $message = 'Te invitamos a realizar una actualización de tus datos, igualmente a que actualices tu hoja de vida para tener mejores oportunidades. <a href="' . base_url('login-ext') . '">Iniciar Sesión</a>';
            $this->sendmail->sendMail($user->EmailUsuario, $subject, $message);
        }
        $this->session->set_flashdata('message_success', 'Listo.');
        redirect('home');
    }

}
