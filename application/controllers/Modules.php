<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Modules
 *
 * @author furbox
 */
class Modules extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->library([
            'Auth'
        ]);
        $this->load->model("Modules_model");
        
    }

    public function add_module() {
        $this->auth->is_session_start();   
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_module') == FALSE) {
            $this->form_new_module();
        } else {

            $module_nick = $this->input->post('module_nick');
            $module_name = $this->input->post('module_name');
            $module_submodule = $this->input->post('module_submodule');

            $insert = [
                "Nombre" => $module_nick,
                "modulo" => $module_name,
                "submodulo" => $module_submodule
            ];

            $module_id = $this->Modules_model->insert($insert);
            if ($module_id > 0) {
                $this->session->set_flashdata('message_success', 'El Modulo se agrego correctamente.');
                redirect('lista-modulos');
            }
        }
    }

    public function form_new_module() {
        $this->auth->is_session_start();   
        $data = new stdClass();
        $data->content_view = "modules/dash_form_add_module";
        $data->title = APP_NAME . "::Lista Modulos del sistema";
        $data->active = "modules";
        $data->maintitle = "Modulos";
        $data->maindescription = "Lista de Modulos";
        //$data->modules = $this->create_modules_table();
        $this->load->view('dashboard', $data);
    }

    public function list_modules() {
        $this->auth->is_session_start();   
        $data = new stdClass();
        $data->content_view = "modules/dash_list_modules";
        $data->title = APP_NAME . "::Lista Modulos del sistema";
        $data->active = "modules";
        $data->maintitle = "Modulos";
        $data->maindescription = "Lista de Modulos";
        $data->modules = $this->create_modules_table();
        $this->load->view('dashboard', $data);
    }

    public function create_modules_table() {
        $modules = $this->Modules_model->get_pagination();        
        
        $modules_table = "";

        if (count($modules) > 0) {
            foreach ($modules as $key => $value) {

                $modules_table .= "<tr>";
                $modules_table .= "<td>{$value->Nombre}</td>";
                $modules_table .= "<td>{$value->modulo}</td>";
                $modules_table .= "<td>{$value->submodulo}</td>";
                $modules_table .= "<td>
                                            <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-modulo') . "/" . $value->ModuloID . "'>
                                                <i class='fa fa-edit'></i> Editar
                                            </a>
                                </td>";
                $modules_table .= "</tr>";
            }
        }

        return $modules_table;
    }

    public function edit_module($id = FALSE) {
        $this->auth->is_session_start();   
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Modulo no valido');
            redirect('lista-modulos');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-modulos');
        }
        $module = $this->Modules_model->get($id);
        if (!count($module) == 1) {
            $this->session->set_flashdata('message_error', 'Modulo no encontrado');
            redirect('lista-modulos');
        }
        $data = new stdClass();
        $data->content_view = "modules/dash_form_edit_modulo";
        $data->title = APP_NAME . "::Editar Modulo";
        $data->active = "modules";
        $data->maintitle = "Modulos";
        $data->maindescription = "Editar Modulo";

        $data->module = $module;

        $this->load->view('dashboard', $data);
    }

    public function update_module() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_module') == FALSE) {
            $this->edit_module($id);
        } else {

            $module_nick = $this->input->post('module_nick');
            $module_name = $this->input->post('module_name');
            $module_submodule = $this->input->post('module_submodule');

            $update = [
                "Nombre" => $module_nick,
                "modulo" => $module_name,
                "submodulo" => $module_submodule
            ];

            $module_id = $this->Modules_model->update($id, $update);
            if ($module_id > 0) {
                $this->session->set_flashdata('message_success', 'El Modulo se actualizo correctamente.');
                redirect('lista-modulos');
            }
        }
    }

}
