<?php

class Aprobaciones extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model([
            'Projects_model',
            'CatalogoPerfil_model',
            'Perfil_model',
            'ComponenteCompetencia_model',
            'Responsabilidad_model',
            'MunicipiosProyecto_model',
            'MunicipiosPerfil_model',
            'Aprobaciones_model',
            'Requisicion_model',
            'TipoContrato_model',
            'Motivo_model',
            'DatosUsuario_model',
            'MunicipiosRequisicion_model',
            'AprobacionesHistorial_model',
            'Cargos_model',
            'Flujos_model',
            'Users_model'
        ]);

        $this->load->library(['Auth', 'Sendmail']);

        $this->auth->is_session_start();
    }

    public function list_aprobaciones() {
        $data = new stdClass();
        $data->content_view = "aprobaciones/dash_list_aprobaciones";
        $data->title = APP_NAME . "::Lista de Requisiciones";
        $data->active = "aprobaciones";
        $data->maintitle = "Requisiciones";
        $data->maindescription = "Requisiciones";
        $data->aprobaciones_pendientes = $this->dash_list_aprobaciones(6);
        $data->aprobaciones_aprobadas = $this->dash_list_aprobaciones(4);
        $data->aprobaciones_rechazadas = $this->dash_list_aprobaciones(5);
        $this->load->view('dashboard', $data);
    }

    public function getAprobacionesByUsuario($idUsuario) {
        $requisiciones = $this->Requisicion_model->getRequisiciones_x_Usuario($idUsuario);
        $requisiciones_table = "";

        setlocale(LC_MONETARY, 'es_ES');
        if (count($requisiciones) > 0) {
            foreach ($requisiciones as $key => $value) {
                $requisiciones_table .= "<tr>";
                $requisiciones_table .= "<td>{$value->NombreCargo}</td>";
                $requisiciones_table .= "<td>$ " . number_format($value->SalarioAsignado) . "</td>";
                $requisiciones_table .= "<td>{$value->CantidadPersonasSolicitadas}</td>";
                $requisiciones_table .= "<td>{$value->FechaSolicitud}</td>";
                $requisiciones_table .= "<td>
                                        <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-requisicion') . "/" . $value->RequisicionID . "/" . $value->PerfilID . "'>
                                            <i class='fa fa-edit'></i> Editar
                                        </a>
                                        <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-requisicion') . "/" . $value->RequisicionID . "'>
                                            <i class='fa fa-trash'></i> Eliminar
                                        </a>
                                </td>";
                $requisiciones_table .= "</tr>";
            }
        }

        return $requisiciones_table;
    }

    public function dash_list_aprobaciones($status) {
        $aprobaciones = $this->Aprobaciones_model->getAprobaciones($_SESSION['du_id'], $status);
        $aprobaciones_table = "";

        setlocale(LC_MONETARY, 'es_ES');
        if (count($aprobaciones) > 0) {
            foreach ($aprobaciones as $key => $value) {
                $aprobaciones_table .= "<tr>";
                $aprobaciones_table .= "<td>{$value->NombreCargo}</td>";
                $aprobaciones_table .= "<td>$ " . number_format($value->SalarioAsignado) . "</td>";
                $aprobaciones_table .= "<td>{$value->CantidadPersonasSolicitadas}</td>";
                $aprobaciones_table .= "<td>{$value->FechaSolicitud}</td>";
                $aprobaciones_table .= "<td>
                                        <a class='btn btn-primary btn-xs btn-o' href='" . base_url('preview-requisicion') . "/" . $value->RequisicionID . "'>
                                            <i class='fa fa-info'></i> Ver
                                        </a>
                                </td>";
                $aprobaciones_table .= "</tr>";
            }
        }

        return $aprobaciones_table;
    }

    public function dash_requisicion($idReq, $idProy) {
        $data = new stdClass();
        $req = $this->Requisicion_model->get($idReq, FALSE);
        $idPerfil = $req->PerfilID;
        $data->idPerfil = $idPerfil;
        $data->req = $req;
        $data->ProyectoID = $idProy;
        $data->tipos_contrato = $this->TipoContrato_model->get(FALSE, 1);
        $data->motivos = $this->Motivo_model->get(FALSE, 1);
        $data->project = $this->Projects_model->get($idProy, FALSE);
        $data->datos_usuario = $this->DatosUsuario_model->get($_SESSION['id_user']);
        $data->departamentos = $this->MunicipiosRequisicion_model->getGetDepartamentoPerfil($idPerfil);
        $data->perfil = $this->Perfil_model->getPerfil($idPerfil);
        $data->muns = $this->MunicipiosRequisicion_model->getMunicipiosRequisicion($idReq);
        $data->content_view = "aprobaciones/view_requisicion";
        $data->title = APP_NAME . "::Aprobación de Requisiciones";
        $data->active = "projects";
        $data->maintitle = "Aprobación de Requisiciones";
        $data->maindescription = "Aprobación de Requisiciones";
        $this->load->view('dashboard', $data);
    }

    public function dash_accion_aprobacion() {
        $aceptado = $this->input->post('btnAceptar');
        $rechazado = $this->input->post('btnRechazar');
        $proyId = $this->input->post('hdProyectoID');
        $reqId = $this->input->post('hdRequisicionID');
        $just_rech = $this->input->post('justificacion_rechazo');

        if (isset($aceptado)) {
            $this->aprobar_requisicion($proyId, $reqId);
        } else {
            $this->rechazar_requisicion($proyId, $reqId, $just_rech);
        }
    }

    public function aprobar_requisicion($ProyectoID, $RequisicionID) {
        $update = [
            "EstadoID" => 4,
            'FechaActualizacion' => date('Y-m-d h:i:s')
        ];

        $id = $this->Aprobaciones_model->aprobarRequisicion($ProyectoID, $RequisicionID, $update);

        if ($id > 0) {
            $aprobacion = $this->Aprobaciones_model->getAprovacion($RequisicionID, $ProyectoID);
            $historial = [
                "aprobaciones_historial_id_aprobacion" => $aprobacion->AprobacionID,
                "aprobaciones_historials_obsevaciones" => "Aprobacion de Requisicion",
                "aprobaciones_historial_update" => date('Y-m-d H:i:s'),
                "aprobaciones_historial_status" => 4
            ];
            $this->AprobacionesHistorial_model->insert($historial);
            $this->enviar_notificacion($RequisicionID);
            $this->session->set_flashdata('message_success', 'Se acepto la solicitud correctamente.');
        }
        redirect('lista-aprobaciones');
    }

    public function rechazar_requisicion($ProyectoID, $RequisicionID, $Justificacion) {
        $this->load->library('Sendmail');
        $update = [
            "EstadoID" => 5,
            "Observaciones" => $Justificacion,
            'FechaActualizacion' => date('Y-m-d h:i:s')
        ];

        $id = $this->Aprobaciones_model->aprobarRequisicion($ProyectoID, $RequisicionID, $update);
        if ($id > 0) {
            $aprobacion = $this->Aprobaciones_model->getAprovacion($RequisicionID, $ProyectoID);
            $historial = [
                "aprobaciones_historial_id_aprobacion" => $aprobacion->AprobacionID,
                "aprobaciones_historials_obsevaciones" => $Justificacion,
                "aprobaciones_historial_update" => date('Y-m-d H:i:s'),
                "aprobaciones_historial_status" => 5
            ];
            $this->AprobacionesHistorial_model->insert($historial);
            $requisicion = $this->Requisicion_model->getRequisicion($RequisicionID);
            $user = $this->Users_model->getUserByDatosUsuarioID($aprobacion->DatosUsuarioID);
            $to = $user->EmailUsuario;
            $subject = "Rechazo de Requisicion";
            $html = "Tu solicitud ha sido rechazada";
            $html .= "<br>Requision: " . $requisicion->NombreCargo;
            $html .= "<br>Motivo: ".$Justificacion;
            $this->sendmail->sendMail($to, $subject, $html);
            $this->session->set_flashdata('message_success', 'Se rechazó la solicitud correctamente.');
        }
        redirect('lista-aprobaciones');
    }

    public function enviar_notificacion($req_id) {
        $this->load->library('Sendmail');
        $envio = TRUE;
        $requisicion = $this->Requisicion_model->getRequisicion($req_id);
        $flujos = $this->Flujos_model->getFlujo(1);
        $aprobaciones = $this->Aprobaciones_model->getXRequisicion($req_id);
        foreach ($flujos as $flujo) {
            $cargo = $this->Cargos_model->get($flujo->flujo_id_cargo);
            foreach ($aprobaciones as $a) {
                if ($cargo->AreaID == $a->ProyectoID) {
                    if ($a->EstadoID == 6 && $a->mostrar_notificaciones == 0) {
                        if ($envio) {
                            $users = $this->Users_model->getUserByCargo($flujo->flujo_id_cargo);
                            foreach ($users as $u) {
                                $to = $u->EmailUsuario;
                                $subject = "Aprobacion de Requisicion";
                                $html = "Tienes una requisicion pendiente para aprobar";
                                $html .= "<br>Requision: " . $requisicion->NombreCargo;
                                $this->sendmail->sendMail($to, $subject, $html);

                                $update = [
                                    "mostrar_notificaciones" => 1
                                ];

                                $this->Aprobaciones_model->aprobarRequisicion($u->AreaID, $req_id, $update);
                            }
                        }
                        $envio = FALSE;
                    }
                }
            }
        }
    }

}
