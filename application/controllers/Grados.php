<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Grados
 *
 * @author furbox
 */
class Grados extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('Grados_model');
        $this->load->library([
            'Auth'
        ]);
    }

    public function form_new_grado() {
        $data = new stdClass();
        $data->content_view = "grados/dash_form_add_grado";
        $data->title = APP_NAME . "::Nuevo Grado Para Nivel Educativo";
        $data->active = "cpntes";
        $data->maintitle = "Grados para Niveles Educativos";
        $data->maindescription = "Nuevo Grado Para Nivel Educativo.";
        $this->load->view('dashboard', $data);
    }

    public function list_grados() {
        $data = new stdClass();
        $data->content_view = "grados/dash_list_grados";
        $data->title = APP_NAME . "::Lista Grados para Niveles Educativos";
        $data->active = "cpntes";
        $data->maintitle = "Grados para Niveles Educativos";
        $data->maindescription = "Lista Grados para Niveles Educativos";
        $data->grados = $this->create_grados_table();
        $this->load->view('dashboard', $data);
    }

    public function add_grado() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_grado') == FALSE) {
            $this->form_new_grado();
        } else {

            $grado_name= $this->input->post('grado_name');
            $grado_status = $this->input->post('grado_status');

            $insert = [
                "grado_nombre" => $grado_name,
                "EstadoID" => $grado_status
            ];

            $insert_id = $this->Grados_model->insert($insert);
            if ($insert_id > 0) {
                $this->session->set_flashdata('message_success', 'El Grado se agrego correctamente.');
                redirect('lista-grados');
            }
        }
    }

    public function create_grados_table() {
        $grados = $this->Grados_model->get();

        $grados_table = "";

        if (count($grados) > 0) {
            foreach ($grados as $key => $value) {
                $links = "";

                if ($value->EstadoID == 1) {
                    $status = "<span class='label label-success'>Activo</span>";
                    $links .= '<a class="btn btn-warning btn-xs btn-o" href="' . base_url('desactivar-grado') . '/' . $value->grado_id . '">
                                    <i class="fa fa-ban"></i> Desactivar
                                </a>';
                    $links.= "<a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-grado') . "/" . $value->grado_id . "'>
                                                <i class='fa fa-trash'></i> Eliminar
                                            </a>";
                }
                if ($value->EstadoID == 2) {
                    $status = "<span class='label label-warning'>Inactivo</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-grado') . '/' . $value->grado_id . '">
                                    <i class="fa fa-ban"></i> Activar
                                </a>';
                }
                if ($value->EstadoID == 3) {
                    $status = "<span class='label label-danger'>Eliminado</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-grado') . '/' . $value->grado_id . '">
                                    <i class="fa fa-check"></i> Activar
                                </a>';
                }

                $grados_table .= "<tr>";
                $grados_table .= "<td>{$value->grado_nombre}</td>";
                $grados_table .= "<td>{$status}</td>";
                $grados_table .= "<td>
                                            <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-grado') . "/" . $value->grado_id . "'>
                                                <i class='fa fa-edit'></i> Editar
                                            </a>
                                            " . $links . "
                                </td>";
                $grados_table .= "</tr>";
            }
        }else{
            $grados_table = "<tr><td colspan='3'>Sin Grados</td></tr>";
        }

        return $grados_table;
    }

    public function edit_grado($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Grado no valido');
            redirect('lista-grados');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Grado no Valido');
            redirect('lista-grados');
        }
        $grado = $this->Grados_model->get($id);
        if (!count($grado) == 1) {
            $this->session->set_flashdata('message_error', 'Grado no encontrado');
            redirect('lista-grados');
        }
        $data = new stdClass();
        $data->content_view = "grados/dash_form_edit_grado";
        $data->title = APP_NAME . "::Editar Grado";
        $data->active = "cpntes";
        $data->maintitle = "Grados para Niveles Educativos";
        $data->maindescription = "Editar Grado";

        $data->grado = $grado;

        $this->load->view('dashboard', $data);
    }

    public function update_grado() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');

        if ($this->form_validation->run('dash_update_grado') == FALSE) {
            $this->edit_grado($id);
        } else {

            $grado_name = $this->input->post('grado_name');

            $update = [
                "grado_nombre" => $grado_name
            ];

            $update_id = $this->Grados_model->update($id, $update);
            if ($update_id > 0) {
                $this->session->set_flashdata('message_success', 'El Grado se actualizo correctamente.');
                redirect('lista-grados');
            }
        }
    }

    public function active_grado($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Grado no valido');
            redirect('lista-grados');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-grados');
        }
        $grado = $this->Grados_model->get($id);
        if (!count($grado) == 1) {
            $this->session->set_flashdata('message_error', 'Grado no encontrado');
            redirect('lista-grados');
        }
        $update = [
            'EstadoID' => 1
        ];
        $update_id = $this->Grados_model->update($id, $update);
        if ($update_id > 0) {
            $this->session->set_flashdata('message_success', 'El Grado se ha Activado.');
            redirect('lista-grados');
        }
    }

    public function unactive_grado() {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Grado no valido');
            redirect('lista-grados');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-grados');
        }
        $grado = $this->Grados_model->get($id);
        if (!count($grado) == 1) {
            $this->session->set_flashdata('message_error', 'Grado no encontrado');
            redirect('lista-grados');
        }
        $update = [
            'EstadoID' => 2
        ];
        $update_id = $this->Grados_model->update($id, $update);
        if ($update_id > 0) {
            $this->session->set_flashdata('message_success', 'El Grado se ha Desactivado.');
            redirect('lista-grados');
        }
    }

    public function delete_grado($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Grado no valido');
            redirect('lista-grados');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-grados');
        }
        $grado = $this->Grados_model->get($id);
        if (!count($grado) == 1) {
            $this->session->set_flashdata('message_error', 'Grado no encontrado');
            redirect('lista-grados');
        }
        $update = [
            'EstadoID' => 3
        ];
        $update_id = $this->Grados_model->update($id, $update);
        if ($update_id > 0) {
            $this->session->set_flashdata('message_success', 'El Grado se ha eliminado.');
            redirect('lista-grados');
        }
    }

}
