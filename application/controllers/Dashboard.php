<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dashboard
 *
 * @author furbox
 */
class Dashboard extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->library(['Auth']);
        $this->load->model([
            'Aprobaciones_model',
            'DatosUsuario_model'
        ]);
        
        $this->auth->is_session_start();        
    }
    
    public function index(){
        $du = $this->DatosUsuario_model->getByUsuarioID($_SESSION['id_user']);
        $_SESSION['du_id'] = $du->DatosUsuarioID;

        $module = [
            "dashboard"
        ];     
        $this->auth->is_access($module);
        $data = new stdClass();
        $data->content_view = "dashboard/index";
        $data->title = APP_NAME."::Home";
        $data->active = "home";
        $data->maintitle = "Dashboard";
        $data->maindescription = "Area de trabajo";
        $this->load->view('dashboard',$data);
    }

    public function aspirante()
    {
        $data = new stdClass();
        $data->content_view = "dashboard/index";
        $data->title = APP_NAME."::Home";
        $data->active = "home";
        $data->maintitle = "Dashboard";
        $data->maindescription = "Area de trabajo";
        $this->load->view('dashboard_aspirante',$data);
    }
}
