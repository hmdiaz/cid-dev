<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Roles
 *
 * @author furbox
 */
class Roles extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model([
            "Roles_model",
            "Modules_model",
            "PermisosModulo_model"
            ]);
        $this->load->library([
            'Auth'
        ]);
    }

    //Lista los Roles existentes
    public function list_roles() {
        $this->auth->is_session_start();   
        $data = new stdClass();
        $data->content_view = "roles/dash_list_role";
        $data->title = APP_NAME . "::Lista Roles";
        $data->active = "roles";
        $data->maintitle = "Roles Y Permisos";
        $data->maindescription = "Lista de Roles";
        $data->roles = $this->create_roles_table();
        $this->load->view('dashboard', $data);
    }

    //formulario para agregar un nuevo rol
    public function form_new_role() {
        $this->auth->is_session_start();
        $data = new stdClass();
        $data->content_view = "roles/dash_form_add_role";
        $data->title = APP_NAME . "::Nuevo Rol";
        $data->active = "roles";
        $data->maintitle = "Roles Y Permisos";
        $data->maindescription = "Crear Nuevo Rol";

        $this->load->view('dashboard', $data);
    }

    public function add_role() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_role') == FALSE) {
            $this->form_new_role();
        } else {

            $rol_name = $this->input->post('rol_name');
            $rol_status = $this->input->post('rol_status');

            $insert = [
                "Nombre" => $rol_name,
                "EstadoID" => $rol_status
            ];

            $rol_id = $this->Roles_model->insert($insert);
            if ($rol_id > 0) {
                $this->session->set_flashdata('message_success', 'El Rol se agrego correctamente.');
                redirect('lista-roles');
            }
        }
    }

    public function create_roles_table() {
        $roles = $this->Roles_model->get_pagination();
        
        $roles_table = "";

        if (count($roles) > 0) {
            foreach ($roles as $key => $value) {
                $links = "";

                if ($value->EstadoID == 1) {
                    $status = "<span class='label label-success'>Activo</span>";
                    $links .= '<a class="btn btn-warning btn-xs btn-o" href="' . base_url('desactivar-rol') . '/' . $value->RolID . '">
                                    <i class="fa fa-ban"></i> Desactivar
                                </a>';
                }
                if ($value->EstadoID == 2) {
                    $status = "<span class='label label-warning'>Inactivo</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-rol') . '/' . $value->RolID . '">
                                    <i class="fa fa-check"></i> Activar
                                </a>';
                }
                if ($value->EstadoID == 3) {
                    $status = "<span class='label label-danger'>Eliminado</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-rol') . '/' . $value->RolID . '">
                                    <i class="fa fa-check"></i> Activar
                                </a>';
                }

                $roles_table .= "<tr>";
                $roles_table .= "<td>{$value->Nombre}</td>";
                $roles_table .= "<td>{$status}</td>";
                $roles_table .= "<td>
                                    <a class='btn btn-primary btn-xs btn-o' href='" . base_url('permisos-rol') . "/" . $value->RolID . "'>
                                        <i class='fa fa-key'></i> Permisos de Rol
                                    </a>
                                    <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-rol') . "/" . $value->RolID . "'>
                                        <i class='fa fa-edit'></i> Editar
                                    </a>
                                    " . $links . "                                        

                                    <a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-rol') . "/" . $value->RolID . "'>
                                        <i class='fa fa-trash'></i> Eliminar
                                    </a>
                                </td>";
                $roles_table .= "</tr>";
            }
        }

        return $roles_table;
    }

    //formulario para editar un rol
    public function edit_rol($id = FALSE) {
        $this->auth->is_session_start();   
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Rol no valido');
            redirect('lista-roles');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-roles');
        }
        $rol = $this->Roles_model->get($id);
        if (!count($rol) == 1) {
            $this->session->set_flashdata('message_error', 'Rol no encontrado');
            redirect('lista-roles');
        }
        $data = new stdClass();
        $data->content_view = "roles/dash_form_edit_role";
        $data->title = APP_NAME . "::Editar Rol";
        $data->active = "roles";
        $data->maintitle = "Roles Y Permisos";
        $data->maindescription = "Editar Rol";

        $data->rol = $rol;

        $this->load->view('dashboard', $data);
    }

    public function update_role() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_role') == FALSE) {
            $this->edit_rol($id);
        } else {

            $rol_name = $this->input->post('rol_name');

            $update = [
                "Nombre" => $rol_name
            ];

            $rol_id = $this->Roles_model->update($id, $update);
            if ($rol_id > 0) {
                $this->session->set_flashdata('message_success', 'El Rol se actualizo correctamente.');
                redirect('lista-roles');
            }
        }
    }

    //activa un rol desde el dash
    public function active_rol($id = FALSE) {
        $this->auth->is_session_start();   
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Rol no valido');
            redirect('lista-roles');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-roles');
        }
        $rol = $this->Roles_model->get($id);
        if (!count($rol) == 1) {
            $this->session->set_flashdata('message_error', 'Rol no encontrado');
            redirect('lista-roles');
        }
        $update = [
            'EstadoID' => 1
        ];
        $id_user = $this->Roles_model->update($id, $update);
        if ($id_user > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Rol se ha Activado.');
            redirect('lista-roles');
        }
    }

    //desactiva un rol desde el dash
    public function unactive_rol() {
        $this->auth->is_session_start();   
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Rol no valido');
            redirect('lista-roles');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-roles');
        }
        $rol = $this->Roles_model->get($id);
        if (!count($rol) == 1) {
            $this->session->set_flashdata('message_error', 'Rol no encontrado');
            redirect('lista-roles');
        }
        $update = [
            'EstadoID' => 2
        ];
        $id_user = $this->Roles_model->update($id, $update);
        if ($id_user > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Rol se ha Desactivado.');
            redirect('lista-roles');
        }
    }

    //elimina un rol desde el dash
    public function delete_rol($id = FALSE) {
        $this->auth->is_session_start();   
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Rol no valido');
            redirect('lista-roles');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-roles');
        }
        $rol = $this->Roles_model->get($id);
        if (!count($rol) == 1) {
            $this->session->set_flashdata('message_error', 'Rol no encontrado');
            redirect('lista-roles');
        }
        $update = [
            'EstadoID' => 3
        ];
        $id_user = $this->Roles_model->update($id, $update);
        if ($id_user > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Rol se ha eliminado.');
            redirect('lista-roles');
        }
    }

    //formulario que modifica los permisos del rol
    public function form_permisions_rol($id = FALSE) {
        $this->auth->is_session_start();   
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Rol no valido');
            redirect('lista-roles');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-roles');
        }
        $rol = $this->Roles_model->getByIdAllModules($id);
        $data = new stdClass();
        $data->content_view = "roles/dash_form_permissions_role";
        $data->title = APP_NAME . "::Nuevo Rol";
        $data->active = "roles";
        $data->maintitle = "Roles Y Permisos";
        $data->maindescription = "Editar Rol";
        $data->modules = $this->Modules_model->getModules();
        $data->allmodules = $this->Modules_model->get();
        $data->rols = $rol;
        $data->rolID = $id;

        $this->load->view('dashboard', $data);
    }

    public function update_permissions() {
        $idrol = $this->input->post('idrol');
        $permission = $this->input->post('permission');

        $permissionsroles = $this->PermisosModulo_model->getByIdRol($idrol);
        $modules = $this->Modules_model->get();

        $perms = [];
        foreach ($permission as $key => $value) {
            $perms[] = explode("_", $value);
        }
        $band = FALSE;
        foreach ($modules as $mod) {
            foreach ($perms as $perm) {
                if ($perm[0] == $mod->ModuloID) {
                    $band = TRUE;
                    $p = $this->PermisosModulo_model->getwhere($idrol, $mod->ModuloID);
                    if (count($p) > 0) {
                        //update
                        $where = [
                            "RolID" => $idrol,
                            "ModuloID" => $perm[0]
                        ];
                        $update = [
                            "EstadoID" => $perm[1]
                        ];

                        $this->PermisosModulo_model->update($where, $update);
                    } else {
                        //insert
                        $insert = [
                            "RolID" => $idrol,
                            "ModuloID" => $perm[0],
                            "EstadoID" => $perm[1]
                        ];
                        $this->PermisosModulo_model->insert($insert);
                    }
                }
            }
            if (!$band) {
                $p = $this->PermisosModulo_model->getwhere($idrol, $mod->ModuloID);
                if (count($p) > 0) {
                    //update
                    $where = [
                        "RolID" => $idrol,
                        "ModuloID" => $perm[0]
                    ];
                    $update = [
                        "EstadoID" => 2
                    ];

                    $this->PermisosModulo_model->update($where, $update);
                } else {
                    //insert
                    $insert = [
                        "RolID" => $idrol,
                        "ModuloID" => $perm[0],
                        "EstadoID" => 2
                    ];
                    $affected = $this->PermisosModulo_model->insert($insert);
                }
            }
            $band = FALSE;
        }   
        $this->form_permisions_rol($idrol);
    }

}
