<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pruebas
 *
 * @author furbox
 */
class Pruebas extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model([
            "Pruebas_model",
            "Preguntas_model",
            "CategoriasComponentes_model",
            "PruebasSeleccion_model",
            "PruebasSeleccionResult_model"
        ]);
        $this->load->library([
            'Auth'
        ]);
        $this->auth->is_session_start();
    }

    //lista de pruebas
    public function list_test() {
        $data = new stdClass();
        $data->content_view = "pruebas/dash_list_test";
        $data->title = APP_NAME . "::Lista de Pruebas";
        $data->active = "pruebas";
        $data->maintitle = "Modulo de Pruebas";
        $data->maindescription = "Lista de Pruebas";
        $data->tests = $this->create_test_table();
        $this->load->view('dashboard', $data);
    }

    //formulario para agregar una nuevo prueba
    public function form_new_test() {
        $data = new stdClass();
        $data->content_view = "pruebas/dash_form_add_test";
        $data->title = APP_NAME . "::Nueva Prueba";
        $data->active = "pruebas";
        $data->maintitle = "Modulo de Pruebas";
        $data->maindescription = "Crear Nueva Prueba";

        $data->categorias = $this->CategoriasComponentes_model->getActives();
        $this->load->view('dashboard', $data);
    }

    public function add_test() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_test') == FALSE) {
            $this->form_new_test();
        } else {

            $test_name = $this->input->post('test_name');
            $test_cat = $this->input->post('test_categoria');
            $test_date = date('Y-m-d H:i:s');
            $test_observaciones = $this->input->post('test_observaciones');

            $insert = [
                "prueba_name" => $test_name,
                "prueba_date" => $test_date,
                "prueba_estatus" => 1,
                "CategoriaPerfilID" => $test_cat,
                "prueba_observaciones" => $test_observaciones
            ];

            $id = $this->Pruebas_model->insert($insert);
            if ($id > 0) {
                $this->session->set_flashdata('message_success', 'La Prueba se agrego correctamente.');
                redirect('lista-pruebas');
            }
        }
    }

    public function create_test_table() {
        $tests = $this->Pruebas_model->get_pagination();

        $test_table = "";

        if (count($tests) > 0) {
            foreach ($tests as $key => $value) {
                $links = "";
                //$preguntas = $this->Preguntas_model->getPreguntas($value->prueba_id);
                //$num_preg = count($preguntas);
                if ($value->prueba_estatus == 1) {
                    $status = "<span class='label label-success'>Activo</span>";
                    $links .= '<a class="btn btn-warning btn-xs btn-o" href="' . base_url('desactivar-prueba') . '/' . $value->prueba_id . '">
                                    <i class="fa fa-ban"></i> Desactivar
                                </a>';
                    $links .="<a class='btn btn-danger btn-xs btn-o delete' href='" . base_url('eliminar-prueba') . "/" . $value->prueba_id . "'>
                                        <i class='fa fa-trash'></i> Eliminar
                                    </a>";
                }
                if ($value->prueba_estatus == 2) {
                    $status = "<span class='label label-warning'>Inactivo</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-prueba') . '/' . $value->prueba_id . '">
                                    <i class="fa fa-ban"></i> Activar
                                </a>';
                }
                if ($value->prueba_estatus == 3) {
                    $status = "<span class='label label-danger'>Eliminado</span>";
                    $links .= '<a class="btn btn-success btn-xs btn-o" href="' . base_url('activar-prueba') . '/' . $value->prueba_id . '">
                                    <i class="fa fa-check"></i> Activar
                                </a>';
                }
//                if ($num_preg > 0) {
//                    $links .= '<a class="btn btn-info btn-xs btn-o" href="' . base_url('ver-prueba') . '/' . $value->prueba_id . '">
//                                    <i class="fa fa-eye"></i> ver prueba
//                                </a>';
//                    $links .= "<a class='btn btn-default btn-xs btn-o' href='" . base_url('duplicar-prueba') . "/" . $value->prueba_id . "'>
//                                        <i class='fa fa-files-o'></i> Duplicar
//                                    </a>";
//                }

                $test_table .= "<tr>";
                $test_table .= "<td>{$value->prueba_name}</td>";
                $test_table .= "<td>{$value->prueba_date}</td>";
                $test_table .= "<td>{$status}</td>";
                $test_table .= "<td>
                                    <a class='btn btn-success btn-xs btn-o' href='" . base_url('listar-preguntas') . "/" . $value->prueba_id . "'>
                                        <i class='fa fa-question'></i> Preguntas
                                    </a>
                                    <a class='btn btn-primary btn-xs btn-o' href='" . base_url('editar-prueba') . "/" . $value->prueba_id . "'>
                                        <i class='fa fa-edit'></i> Editar
                                    </a>                                    
                                    " . $links . "
                                </td>";
                $test_table .= "</tr>";
            }
        }

        return $test_table;
    }

    public function edit_test($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Prueba no valida');
            redirect('lista-pruebas');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-pruebas');
        }
        $prueba = $this->Pruebas_model->get($id);
        if (!count($prueba) == 1) {
            $this->session->set_flashdata('message_error', 'Prueba no encontrada');
            redirect('lista-pruebas');
        }
        $data = new stdClass();
        $data->content_view = "pruebas/dash_form_edit_test";
        $data->title = APP_NAME . "::Editar Prueba";
        $data->active = "pruebas";
        $data->maintitle = "Modulo de Pruebas";
        $data->maindescription = "Editar Prueba";
        $data->categorias = $this->CategoriasComponentes_model->getActives();
        $data->prueba = $prueba;

        $this->load->view('dashboard', $data);
    }

    public function update_test() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_add_test') == FALSE) {
            $this->edit_test($id);
        } else {

            $test_name = $this->input->post('test_name');
            $test_cat = $this->input->post('test_categoria');
            $test_observaciones = $this->input->post('test_observaciones');
            $update = [
                "prueba_name" => $test_name,
                "CategoriaPerfilID" => $test_cat,
                "prueba_observaciones" => $test_observaciones
            ];

            $prueba_id = $this->Pruebas_model->update($id, $update);
            if ($prueba_id > 0) {
                $this->session->set_flashdata('message_success', 'La Prueba se actualizo correctamente.');
                redirect('lista-pruebas');
            }
        }
    }

    public function active_test($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Prueba no valida');
            redirect('lista-pruebas');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-pruebas');
        }
        $test = $this->Pruebas_model->get($id);
        if (!count($test) == 1) {
            $this->session->set_flashdata('message_error', 'Prueba no encontrada');
            redirect('lista-pruebas');
        }
        $update = [
            'prueba_estatus' => 1
        ];
        $id = $this->Pruebas_model->update($id, $update);
        if ($id > 0) {
            $this->session->set_flashdata('message_success', 'La Prueba se ha Activado.');
            redirect('lista-pruebas');
        }
    }

    public function unactive_test($id = FALSE) {

        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Prueba no valida');
            redirect('lista-pruebas');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-pruebas');
        }
        $test = $this->Pruebas_model->get($id);
        if (!count($test) == 1) {
            $this->session->set_flashdata('message_error', 'Prueba no encontrada');
            redirect('lista-pruebas');
        }
        $update = [
            'prueba_estatus' => 2
        ];
        $id_test = $this->Pruebas_model->update($id, $update);
        if ($id_test > 0) {
            $this->session->set_flashdata('message_success', 'La Prueba se ha Desactivado.');
            redirect('lista-pruebas');
        }
    }

    public function delete_test($id = FALSE) {

        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Prueba no valida');
            redirect('lista-pruebas');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-pruebas');
        }
        $test = $this->Pruebas_model->get($id);
        if (!count($test) == 1) {
            $this->session->set_flashdata('message_error', 'Prueba no encontrada');
            redirect('lista-pruebas');
        }
        $update = [
            'prueba_estatus' => 3
        ];
        $id_test = $this->Pruebas_model->update($id, $update);
        if ($id_test > 0) {
            $this->session->set_flashdata('message_success', 'La Prueba se ha Eliminado.');
            redirect('lista-pruebas');
        }
    }

    public function view_test() {

        $id_prueba = $this->uri->segment(2);
        $prueba = $this->Pruebas_model->get($id_prueba);
        $preguntas = $this->Preguntas_model->getPreguntasActive($id_prueba);
        $data = new stdClass();
        $data->content_view = "pruebas/dash_form_view_test";
        $data->title = APP_NAME . "::Prueba";
        $data->active = "pruebas";
        $data->maintitle = "Modulo de Pruebas";
        $data->maindescription = "Prueba: " . $prueba->prueba_name;
        $data->preguntas = $preguntas;

        $this->load->view('dashboard', $data);
    }

    public function duplicar_test($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Prueba no valida');
            redirect('lista-pruebas');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-pruebas');
        }
        $test = $this->Pruebas_model->get($id);
        if (!count($test) == 1) {
            $this->session->set_flashdata('message_error', 'Prueba no encontrada');
            redirect('lista-pruebas');
        }
        $date = date('Y-m-d H:i:s');
        $insert = [
            'prueba_name' => $test->prueba_name,
            'prueba_date' => $date,
            'prueba_estatus' => $test->prueba_estatus
        ];
        $this->db->trans_begin();
        $id_test = $this->Pruebas_model->insert($insert);
        if ($id_test > 0) {
            $questions = $this->Preguntas_model->getPreguntas($id);
            foreach ($questions as $quest) {
                $insert_quest = [
                    "prueba_id" => $id_test,
                    "pregunta_tipo" => $quest->pregunta_tipo,
                    "pregunta" => $quest->pregunta,
                    "pregunta_opciones" => $quest->pregunta_opciones,
                    "pregunta_respuesta" => $quest->pregunta_respuesta,
                    "pregunta_valor" => $quest->pregunta_valor,
                    "pregunta_status" => $quest->pregunta_status
                ];

                $id_quest = $this->Preguntas_model->insert($insert_quest);
                if ($id_quest > 0) {
                    continue;
                } else {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message_error', 'Las Preguntas no pudieron duplicarse.');
                    redirect('lista-pruebas');
                }
            }
            $this->db->trans_commit();
            $this->session->set_flashdata('message_success', 'La Prueba se ha Duplicado.');
            redirect('lista-pruebas');
        } else {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message_error', 'La Prueba no pudo duplicarse.');
            redirect('lista-pruebas');
        }
    }

    public function test() {
        $id_prueba_seleccion = (int) $this->uri->segment(2);

        $prueba_seleccion = $this->PruebasSeleccion_model->getByUser($id_prueba_seleccion, $_SESSION['id_user']);
        if (count($prueba_seleccion) == 1) {
            $exist = $this->PruebasSeleccionResult_model->getbyPruebaSeleccionID($prueba_seleccion->pruebasseleccion_id);
            if (@$exist->pruebaseleccionresult_result != NULL) {
                $this->session->set_flashdata('message_error', 'Ya has terminado tu prueba. Gracias');
                redirect('dashboard-aspirante');
            }
            $prueba = $this->Pruebas_model->get($prueba_seleccion->prueba_id);
            $preguntas = $this->Preguntas_model->getPreguntasPrueba($prueba_seleccion->prueba_id);
            if (count($exist) === 0) {
                $data = new stdClass();
                $data->content_view = "pruebas/view_test";
                $data->title = APP_NAME . "::Prueba";
                $data->active = "pruebas";
                $data->maintitle = "Modulo de Pruebas";
                $data->maindescription = "Prueba: " . $prueba->prueba_name;
                $data->seleccion = $prueba_seleccion;
                $data->preguntas = $preguntas;

                $this->load->view('dashboard_aspirante', $data);
            }
            if (count($exist) === 1) {
                $data = new stdClass();
                $data->content_view = "pruebas/view_test";
                $data->title = APP_NAME . "::Prueba";
                $data->active = "pruebas";
                $data->maintitle = "Modulo de Pruebas";
                $data->maindescription = "Prueba: " . $prueba->prueba_name;
                $data->seleccion = $prueba_seleccion;
                $data->preguntas = $preguntas;
                $data->exist = $exist;

                $this->load->view('dashboard_aspirante', $data);
            }
        } else {
            $this->session->set_flashdata('message_error', 'No Existe Prueba');
            redirect('dashboard-aspirante');
        }
    }

    public function start_test() {
        if ($this->input->post('id')) {
            $id_prueba_seleccion = $this->input->post('id');
            $prueba_seleccion = $this->PruebasSeleccion_model->get($id_prueba_seleccion);
            $exist = $this->PruebasSeleccionResult_model->getbyPruebaSeleccionID($id_prueba_seleccion);
            $date = date('Y-m-d H:i:s');
            if (count($exist) == 0) {
                $date_ini = $date;
                $date_fin = strtotime('+' . $prueba_seleccion->pruebasconfig_tiempo . ' minute', strtotime($date_ini));
                $date_fin = date('Y-m-d H:i:s', $date_fin);
                $insert = [
                    "pruebasseleccion_id" => $id_prueba_seleccion,
                    "pruebaseleccionresult_start" => $date_ini,
                    "pruebaseleccionresult_end" => $date_fin
                ];
                $this->PruebasSeleccionResult_model->insert($insert);
            } elseif (count($exist) == 1) {
                if ($date > $exist->pruebaseleccionresult_end) {
                    $this->session->set_flashdata('message_error', 'El tiempo de la prueba ya termino');
                    redirect('dashboard-aspirante');
                }
            }
            $exist = $this->PruebasSeleccionResult_model->getbyPruebaSeleccionID($id_prueba_seleccion);
            $prueba = $this->Pruebas_model->get($prueba_seleccion->prueba_id);
            $preguntas = $this->Preguntas_model->getPreguntasPrueba($prueba_seleccion->prueba_id);
            if (count($exist) === 1) {
                $data = new stdClass();
                $data->content_view = "pruebas/view_start_test";
                $data->title = APP_NAME . "::Prueba";
                $data->active = "pruebas";
                $data->maintitle = "Modulo de Pruebas";
                $data->maindescription = "Prueba: " . $prueba->prueba_name;
                $data->prueba = $prueba;
                $data->seleccion = $prueba_seleccion;
                $data->preguntas = $preguntas;
                $data->exist = $exist;
                $this->load->view('dashboard_aspirante', $data);
            }
        } else {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('dashboard-aspirante');
        }
    }

    public function set_test() {
        $pruebaseleccionresult_id = $this->input->post('id');
        $datos = [];
        $cont = 0;
        foreach ($this->input->post() as $key => $value) {
            if ($cont > 0) {
                $datos[$key] = $value;
            }
            $cont++;
        }
        $update = [
            "pruebaseleccionresult_result" => json_encode($datos)
        ];

        $success = $this->PruebasSeleccionResult_model->update($pruebaseleccionresult_id, $update);
        if (count($success) > 0) {
            $this->session->set_flashdata('message_success', 'La Prueba se ha Enviado. <strong>El Resultado sera enviado a tu correo.</strong><br>Gracias por postularte');
            redirect('dashboard-aspirante');
        } else {
            $this->session->set_flashdata('message_error', 'La prueba no se guardo de manera correcta');
            redirect('dashboard-aspirante');
        }
    }

    public function test_result() {
        $this->load->model("Pruebasconfig_model");
        $this->load->model("EtapaConvocatoria_model");
        $EtapaConvocatoriaID = $this->uri->segment(2);
        $etapa = $this->EtapaConvocatoria_model->get($EtapaConvocatoriaID);
        $ConvocatoriaID = $etapa->ConvocatoriaID;
        $seleccion = $this->PruebasSeleccion_model->getbyUsuariosEtapaConvcatoria($EtapaConvocatoriaID);

        $pruebas = $this->Pruebasconfig_model->getPruebaByConvocatoria($EtapaConvocatoriaID);

        $test = $this->PruebasSeleccion_model->getbyTestEtapaConvcatoria($EtapaConvocatoriaID);
        $match = $this->match_test($seleccion, $test, $pruebas);

        $data = new stdClass();
        $data->content_view = "pruebas/view_resp";
        $data->title = APP_NAME . "::Prueba";
        $data->active = "convocatorias";
        $data->maintitle = "Modulo de Convocatorias";
        $data->maindescription = "Resultatos y Seleccion";
        $data->etapa = $etapa;
        $data->matchs = $match;
        $data->postulantes = $seleccion;
        $data->ConvocatoriaID = $ConvocatoriaID;
        $this->load->view('dashboard', $data);
    }

    public function match_test($seleccion, $test, $pruebas) {
        $match = [];
        foreach ($seleccion as $s) {
            $band_match = FALSE;
            $match_users = [];
            foreach ($test as $t) {
                if ($s->UsuarioID == $t->UsuarioID) {
                    $band_match = TRUE;
                    $cal_tot = 0;
                    $resp = json_decode($t->pruebaseleccionresult_result);
                    if ($resp != NULL) {
                        $preg_abiertas = FALSE;
                        foreach ($pruebas as $prueba) {
                            foreach ($resp as $key => $value) {
                                if ($prueba->pregunta_id == $key) {
                                    $pregunta_id = $prueba->pregunta_id;
                                    $pregunta = $prueba->pregunta_title;
                                    $valor = $prueba->valor;
                                    $respuesta = $prueba->pregunta_resp;

                                    if ($respuesta == "" || $respuesta == NULL) {
                                        $preg_abiertas = TRUE;
                                    }
                                    $respuesta_user = $value;
                                    if ($prueba->pregunta_tipo == "text") {
                                        if ($respuesta != "") {
                                            if ($respuesta == $respuesta_user) {
                                                $calificacion = $valor;
                                            } else {
                                                $calificacion = 0;
                                            }
                                        } else {
                                            $calificacion = $valor;
                                        }
                                    }
                                    if ($prueba->pregunta_tipo == "textarea") {
                                        if ($respuesta != "") {
                                            if ($respuesta == $respuesta_user) {
                                                $calificacion = $valor;
                                            } else {
                                                $calificacion = 0;
                                            }
                                        } else {
                                            $calificacion = $valor;
                                        }
                                    }
                                    if ($prueba->pregunta_tipo == "select") {
                                        if ($respuesta != "") {
                                            if ($respuesta == $respuesta_user) {
                                                $calificacion = $valor;
                                            } else {
                                                $calificacion = 0;
                                            }
                                        } else {
                                            $calificacion = $valor;
                                        }
                                    }
                                    if ($prueba->pregunta_tipo == "checkbox") {
                                        $respuestas = explode(',', $respuesta);
                                        if ($respuestas != "") {
                                            if (is_array($respuestas)) {
                                                $cont = count($respuestas);
                                                $aciertos = 0;
                                                for ($i = 0; $i < $cont; $i++) {
                                                    if ($respuesta_user[$i]) {
                                                        if ($respuestas[$i] == $respuesta_user[$i]) {
                                                            $aciertos+=1;
                                                        }
                                                    }
                                                }
                                                if ($aciertos == $cont) {
                                                    $calificacion = $valor;
                                                } else {
                                                    $calculo = $aciertos / $cont;
                                                    $calificacion = $calculo * $valor;
                                                }
                                            }
                                        } else {
                                            $calificacion = $valor;
                                        }
                                    }
                                    if ($prueba->pregunta_tipo == "radio") {
                                        if ($respuesta != "") {
                                            if ($respuesta == $respuesta_user) {
                                                $calificacion = $valor;
                                            } else {
                                                $calificacion = 0;
                                            }
                                        } else {
                                            $calificacion = $valor;
                                        }
                                    }
                                    $cal_tot +=$calificacion;
                                    $match_users[] = [
                                        "pregunta_id" => $pregunta_id,
                                        "pregunta" => $pregunta,
                                        "respuesta_pregunta" => $respuesta,
                                        "valor_pregunta" => $valor,
                                        "respuesta_user" => $respuesta_user,
                                        "calificacion" => $calificacion,
                                        "calificacion_manual" => 0,
                                    ];
                                }
                            }
                        }
                        $match [] = [
                            "pruebaseleccionresult_id" => $t->pruebaseleccionresult_id,
                            "user_id" => $s->UsuarioID,
                            "puntos" => $cal_tot,
                            "preg_abiertas" => $preg_abiertas,
                            "match_resp" => json_encode($match_users),
                        ];
                        $save = [
                            "pruebaseleccionresult_id" => $t->pruebaseleccionresult_id,
                            "user_id" => $s->UsuarioID,
                            "puntos" => $cal_tot,
                            "preg_abiertas" => $preg_abiertas,
                            "match_resp" => json_encode($match_users),
                        ];
                        $update = [
                            "pruebaseleccionresult_match" => json_encode($save),
                        ];
                        $this->PruebasSeleccionResult_model->update($t->pruebaseleccionresult_id, $update);
                    }
                }
            }
        }
        return $match;
    }

    public function calificar() {
        $pruebaseleccionresult_id = $this->uri->segment(2);
        $select = $this->PruebasSeleccionResult_model->get($pruebaseleccionresult_id);

        $pregutas = json_decode($select->pruebaseleccionresult_match);
        $etapa = $this->PruebasSeleccion_model->getEtapa($select->pruebasseleccion_id);
        $data = new stdClass();
        $data->content_view = "pruebas/form_calificacion_manual";
        $data->title = APP_NAME . "::Prueba";
        $data->active = "convocatorias";
        $data->maintitle = "Modulo de Convocatorias";
        $data->maindescription = "Resultatos y Seleccion";
        $data->preguntas = $pregutas;
        $data->etapa = $etapa;
        $this->load->view('dashboard', $data);
    }

    public function update_calificacion() {
        $pruebaseleccionresult_id = $this->input->post('pruebaseleccionresult_id');
        $select = $this->PruebasSeleccionResult_model->get($pruebaseleccionresult_id);
        $pruebasseleccion = $this->PruebasSeleccion_model->get($select->pruebasseleccion_id);
        $etapa = $this->PruebasSeleccion_model->getEtapa($select->pruebasseleccion_id);
        $preg = json_decode($select->pruebaseleccionresult_match);
        $match_resp = json_decode($preg->match_resp);
        $preg_match = [];
        $calificacion = 0;
        $archivo = "";
        if ($etapa->EtapaID == 2) {
            $this->load->library('Utilities');
            if ($_FILES['userfile']['name']) {
                $ram1 = $this->utilities->randomString(4);
                $ram2 = $this->utilities->randomString(8);
                $file_name = $_FILES['userfile']['name'];
                $tmp = explode('.', $file_name);
                $extension_img = end($tmp);

                $archivo = $ram1 . '_' . $ram2 . '.' . $extension_img;

                $config['upload_path'] = './assets/files/';
                //'allowed_types' => "gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp",
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf|doc|docx|PDF';
                $config['max_size'] = '9000000';
                $config['quality'] = '90%';
                $config['file_name'] = $archivo;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->session->set_flashdata('message_error', $this->upload->display_errors());
                    redirect();
                }
            } else {
                $archivo = '';
            }
        }
        foreach ($match_resp as $mr) {
            if ($this->input->post($mr->pregunta_id)) {
                $preg_match[] = [
                    "pregunta_id" => $mr->pregunta_id,
                    "pregunta" => $mr->pregunta,
                    "respuesta_pregunta" => $mr->respuesta_pregunta,
                    "valor_pregunta" => $mr->valor_pregunta,
                    "respuesta_user" => $mr->respuesta_user,
                    "calificacion" => $mr->calificacion,
                    "calificacion_manual" => $this->input->post($mr->pregunta_id),
                ];
                $calificacion += $this->input->post($mr->pregunta_id);
            } else {
                $preg_match[] = [
                    "pregunta_id" => $mr->pregunta_id,
                    "pregunta" => $mr->pregunta,
                    "respuesta_pregunta" => $mr->respuesta_pregunta,
                    "valor_pregunta" => $mr->valor_pregunta,
                    "respuesta_user" => $mr->respuesta_user,
                    "calificacion" => $mr->calificacion,
                    "calificacion_manual" => 0,
                ];
                $calificacion += $mr->calificacion;
            }
        }
        $match = [
            "pruebaseleccionresult_id" => $preg->pruebaseleccionresult_id,
            "user_id" => $preg->user_id,
            "puntos" => $calificacion,
            "preg_abiertas" => FALSE,
            "match_resp" => json_encode($preg_match),
        ];
        $update = [
            "pruebaseleccionresult_match" => json_encode($match),
            "pruebaseleccionresult_file" => $archivo
        ];
        $success = $this->PruebasSeleccionResult_model->update($pruebaseleccionresult_id, $update);
        if (count($success) > 0) {
            $this->session->set_flashdata('message_success', 'La Calificacion se asigno correctamente.');
            redirect('resultados/' . $pruebasseleccion->EtapaConvocatoriaID);
        }
    }

    public function generar_archivo() {
        $id_convocatoria = $this->uri->segment(2);
        $respuestas_etapa_2 = $this->PruebasSeleccionResult_model->getResultadosEtapa2($id_convocatoria);
        $fecha = date('U');
        $this->load->library("PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("CORPORACIÓN INFANCIA Y DESARROLL");
        $objPHPExcel->getProperties()->setLastModifiedBy("CORPORACIÓN INFANCIA Y DESARROLL");
        $objPHPExcel->getProperties()->setTitle("Resultados Etapa 2");
        $objPHPExcel->getProperties()->setSubject("Prueba Psicotecnica");
        $objPHPExcel->getProperties()->setDescription("Resultados de la prueba Psicotecnica");

        $cont = 0;
        foreach ($respuestas_etapa_2 as $re2) {
            $nombre_usuario = $re2->Nombres . ' ' . $re2->Apellidos;
            $respuestas = json_decode($re2->pruebaseleccionresult_result);
            if ($cont == 0) {
                $objPHPExcel->setActiveSheetIndex($cont);
                $objPHPExcel->getActiveSheet()->setTitle($nombre_usuario);
            } else {
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($cont);
                $objPHPExcel->getActiveSheet()->setTitle($nombre_usuario);
            }
            $cont_preg = 1;
            foreach ($respuestas as $key => $value) {
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $cont_preg, $value);
                $cont_preg++;
            }

            $cont++;
        }
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save('./assets/temp/' . $fecha . '.xlsx');
        redirect(base_url('assets/temp/' . $fecha . '.xlsx'));
    }

}
