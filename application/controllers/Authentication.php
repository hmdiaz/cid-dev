<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Authentication
 *
 * @author furbox
 */
class Authentication extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->library([
            'Auth',
            'Utilities',
            'Sendmail'
        ]);
        $this->load->model([
            'Users_model',
            'DatosUsuario_model'
        ]);
    }

    public function form_login() {
        if (!@$_SESSION['is_logged_in']) {
            $data = new stdClass();
            $data->title = APP_NAME . "::Ingreso de Usuario";

            $this->load->view('form_login', $data);
        } else {
            redirect('dashboard');
        }
    }

    public function signin() {
        $this->load->library('form_validation');
        if ($this->form_validation->run('signin') == FALSE) {
            $this->form_login();
        } else {

            $user_email = $this->security->xss_clean(addslashes(strip_tags($this->input->post('user_email', TRUE))));
            $user_pass = $this->security->xss_clean(addslashes(strip_tags($this->input->post('user_pass', TRUE))));

            $user = $this->Users_model->getUserLogin($user_email);

            if (!$user) {
                $this->session->set_flashdata('message_error', 'Datos inválidos');
                redirect('signin');
            }

            $check_passwd = $this->utilities->check_passwd($user->Clave, $user->Salt, $user_pass);

            if (!$check_passwd) {
                $this->session->set_flashdata('message_error', 'Datos inválidos');
                redirect('signin');
            }

            if ($user->RolID == 6) {
                $this->session->set_flashdata('message_error', 'No tienes acceso a esta area');
                redirect('login-ext');
            }

            $this->auth->login($user);
            $this->session->set_flashdata('message_success', 'Bienvenido ' . $_SESSION['username']);
            redirect('dashboard');
        }
    }

    public function signin_ext() {
        $this->load->library('form_validation');
        if ($this->form_validation->run('signin') == FALSE) {
            $this->form_login();
        } else {
            $user_email = $this->security->xss_clean(addslashes(strip_tags($this->input->post('user_email', TRUE))));
            $user_pass = $this->security->xss_clean(addslashes(strip_tags($this->input->post('user_pass', TRUE))));

            $user = $this->Users_model->getUserLogin($user_email);

            if (!$user) {
                $this->session->set_flashdata('message_error', 'Datos inválidos');
                redirect('login-ext');
            }

            $check_passwd = $this->utilities->check_passwd($user->Clave, $user->Salt, $user_pass);

            if (!$check_passwd) {
                $this->session->set_flashdata('message_error', 'Datos inválidos');
                redirect('login-ext');
            }
            
            if ($user->RolID == 6) {
                $this->auth->login($user);
                session_start();
                $_SESSION['id_user'] = $user->UsuarioID;
                $_SESSION['email'] = $user_email;
                $_SESSION['firstName'] = $user->Nombres;
                $_SESSION['lastName'] = $user->Apellidos;
                $_SESSION['user_role_id'] = 6;
                $_SESSION['is_logged_in'] = TRUE;
                $this->session->set_flashdata('message_success', 'Bienvenido ' . $_SESSION['username']);
                redirect('dashboard-aspirante');
            } else {
                $this->session->set_flashdata('message_error', 'Este usuario no tiene los permisos requeridos para ingresar en esta sección');
                redirect('login-ext');
            }
        }
    }

    public function signin_linkedin() {
        $user_email = $this->input->post('user_email');
        $linkedin_id = $this->input->post('linkedin_id');
        $url_foto = $this->input->post('url_foto');
        $firstname = $this->input->post('firstName');
        $lastname = $this->input->post('lastName');

        $date_created = date('Y-m-d H:i:s');
        $activation_code = sha1($date_created);

        $Clave = 'ClaveLinkedInFake';
        $Salt = $this->utilities->random_salt();
        $user_pass_hash = $this->utilities->hash_passwd($Clave, $Salt);

        $roleid = 6;
        $estadoID = 1;

        $data = array
            (
            'EmailUsuario' => $user_email,
            'LinkedInID' => $linkedin_id,
            'FotoUsuario' => $url_foto,
            'CodigoActivacion' => $activation_code,
            'Clave' => $user_pass_hash,
            'Salt' => $Salt,
            'RolID' => $roleid,
            'EstadoID' => $estadoID
        );

        $user_find = $this->Users_model->getUserData($user_email);
        if (count($user_find) == 0) {
            $userid = $this->Users_model->insert($data);
        } else {
            $userid = $user_find->UsuarioID;
        }

        $this->Users_model->update($userid, ['LinkedInID' => $linkedin_id]);

        $datosusuario = $this->DatosUsuario_model->getByUsuarioID($userid);
        if (count($datosusuario) == 0) {
            $datosusuarioID = $this->DatosUsuario_model->insert([
                'UsuarioID' => $userid,
                'Nombres' => $firstname,
                'Apellidos' => $lastname
            ]);
        }

        //$this->auth->login($user);
        session_start();
        $_SESSION['id_user'] = $userid;
        $_SESSION['email'] = $user_email;
        $_SESSION['firstName'] = $firstname;
        $_SESSION['lastName'] = $lastname;
        $_SESSION['img_profile'] = $url_foto;
        $_SESSION['user_role_id'] = $roleid;
        $_SESSION['is_logged_in'] = TRUE;
        $_SESSION['firstname'] = $firstname;
        $_SESSION['lastname'] = $lastname;

        $this->session->set_flashdata('message_success', 'Bienvenido ' . $_SESSION['firstname']);

        if ($_SESSION['user_role_id'] == 6) {
            redirect('dashboard-aspirante');
        } else {
            redirect('dashboard');
        }
    }

    public function signout() {
        if (isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] === TRUE) {

            // remove session datas
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }
            // user logout ok
            redirect();
        } else {
            // there user was not logged in, we cannot logged him out,
            // redirect him to site root
            redirect();
        }
    }

    public function signout_ext() {
        if (isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] === TRUE) {

            // remove session datas
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }
            // user logout ok
            redirect('login-ext');
        } else {
            // there user was not logged in, we cannot logged him out,
            // redirect him to site root
            redirect('login-ext');
        }
    }

    public function check_password_strength($pass) {
        return $this->auth->check_password_strength($pass);
    }

    public function form_lost_pass() {
        $data = new stdClass();
        $data->title = APP_NAME . "::Recuperacion de Contraseña de Usuario";

        $this->load->view('authentication/form_lost_pass_user', $data);
    }

    public function send_pass_user() {
        $this->load->helper('string');
        $this->load->library('form_validation');
        if ($this->form_validation->run('forgot_pass') === FALSE) {
            $this->form_lost_pass();
        } else {
            $email = $this->security->xss_clean(strip_tags($this->input->post('user_email', TRUE)));
            $user = $this->Users_model->getUserByEmail($email);
            if (count($user) == 1) {

                $ramdon = random_string('alnum', 8);
                $new_passwd = $this->utilities->hash_passwd($ramdon, $user->Salt);
                
                $to = $user->EmailUsuario;
                $subject = "Recuperacion de contraseña";
                $message = "Aqui hemos enviado su nueva contraseña : " . $ramdon;

                if ($this->sendmail->sendMail($to, $subject, $message)) {
                    $update = [
                        "Clave" => $new_passwd
                    ];
                    $actualiza = $this->Users_model->update($user->UsuarioID, $update);
                    if ($actualiza > 0) {
                        $this->session->set_flashdata('message_success', 'Hemos enviado tu nueva contraseña por correo puedes verificarla e ingresar de nuevo a la pagina.');
                        redirect('lost_pass');
                    } else {
                        $this->session->set_flashdata('message_error', 'Ocurrio un error al actualizar tu password en la pagina. Intentalo de nuevo Gracias.');
                        redirect('lost_pass');
                    }
                } else {
                    $this->session->set_flashdata('message_error', 'Hubo un error a la hora de enviar tu clave. Intentalo de Nuevo mas tarde.');
                    redirect('lost_pass');
                }
            } else {
                $this->session->set_flashdata('message_error', 'No encontramos tu cuenta de correo. Puedes registrarte.');
                redirect('signin');
            }
        }
    }
    
    public function code_verification() {
        $code = $this->uri->segment(2);
        $user = $this->Users_model->getUserByCode($code);
        $user_status = $user->EstadoID;
        if (count($user) == 1 && $user_status == 2) {
            $user_modified = date('Y-m-d H:i:s');

            $update = [
                'FechaActualizacion' => $user_modified,
                'EstadoID' => 1,
            ];
            $success = $this->Users_model->update($user->UsuarioID, $update);
            if ($success) {
                $this->session->set_flashdata('message_success', 'Tu cuenta ya esta activada. puedes iniciar sesion.');
                redirect('login-ext');
            }
        } else {
            $this->session->set_flashdata('message_error', 'Ocurrio un error al activar la cuenta.');
            redirect('login-ext');
        }
    }

}
