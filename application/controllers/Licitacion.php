<?php
class Licitacion extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model([
            "Licitacion_model",
            "Projects_model"
        ]);
        $this->load->library([
            'Auth'
        ]);

        $this->auth->is_session_start();
    }

    public function list_licitaciones() {
        $data = new stdClass();
        $data->content_view = "licitacion/list_licitaciones";
        $data->title = APP_NAME . "::Lista de Licitaciones";
        $data->active = "cpntes";
        $data->maintitle = "Licitaciones";
        $data->maindescription = "Lista de Licitaciones";
        $data->licitaciones = $this->Licitacion_model->get(FALSE, 1);
        $this->load->view('dashboard', $data);
    }

    public function form_new_licitacion() {
        $data = new stdClass();
        $data->content_view = "licitacion/dash_form_add_licitacion";
        $data->title = APP_NAME . "::Nueva Licitación";
        $data->active = "cpntes";
        $data->maintitle = "Licitaciones";
        $data->maindescription = "Crear Nueva Licitación";

        $this->load->view('dashboard', $data);
    }

    public function add_licitacion() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_licitacion') == FALSE) {
            $this->form_new_licitacion();
        } else {

            $project_name = $this->input->post('licitacion_name');
            $insert = [
                "NombreProyecto" => $project_name,
                "TipoProyectoID" => 3
            ];

            $socio_id = $this->Licitacion_model->insert($insert);
            if ($socio_id > 0) {
                $this->session->set_flashdata('message_success', 'La Licitación se agrego correctamente.');
            }
            redirect('lista-licitaciones');
        }
    }

    public function edit_licitacion($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Licitación no valido');
            redirect('lista-licitaciones');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-licitaciones');
        }
        $licitacion = $this->Licitacion_model->get($id);
        if (!count($licitacion) == 1) {
            $this->session->set_flashdata('message_error', 'Licitación no encontrado');
            redirect('lista-licitaciones');
        }
        $data = new stdClass();
        $data->content_view = "licitacion/dash_form_edit_licitacion";
        $data->title = APP_NAME . "::Editar Área";
        $data->active = "cpntes";
        $data->maintitle = "Licitación";
        $data->maindescription = "Editar Licitación";
        $data->licitacion = $licitacion;

        $this->load->view('dashboard', $data);
    }

    public function update_licitacion() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_licitacion') == FALSE) {
            $this->edit_licitacion($id);
        } else {
            $project_name = $this->input->post('licitacion_name');
            $update = [
                "NombreProyecto" => $project_name,
                "TipoProyectoID" => 3
            ];

            $socio_id = $this->Licitacion_model->update($id, $update);
            if ($socio_id > 0) {
                $this->session->set_flashdata('message_success', 'La Licitación se actualizo correctamente.');
            }
            redirect('lista-licitaciones');
        }
    }

    public function delete_licitacion($id = FALSE)
    {
        $perfil = $this->Licitacion_model->get($id);
        if (!$id) {
            $id = (int)$this->uri->segment(2);
        } else {
            $id = (int)$id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Licitación no valido');
            redirect('lista-licitaciones');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-licitaciones');
        }

        if (!count($perfil) == 1) {
            $this->session->set_flashdata('message_error', 'Licitación no encontrado');
            redirect('lista-licitaciones');
        }

        $num_rows = $this->Licitacion_model->update($id, ['EstadoID' => 3]);
        if ($num_rows > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'La Licitación se ha eliminado.');
            redirect('lista-licitaciones');
        }
    }

    public function convert_to_project($id)
    {
        $data = [
            "TipoProyectoID" => 1
        ];
        $num_rows = $this->Projects_model->update($id, $data);

        if($num_rows > 0)
        {
            $this->session->set_flashdata('message_success', 'La Licitación se ha convertido a proyecto de manera satisfactoria.');
            redirect('lista-licitaciones');
        }
    }
}