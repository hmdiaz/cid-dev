<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Mario Diaz
 * Date: 10/04/2016
 * Time: 11:03 AM
 */
class DatosUsuario extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library([
            'Auth',
            'Utilities'
        ]);
        $this->load->model([
            'Roles_model',
            'Users_model',
            'Status_model',
            'TipoDocumento_model',
            'DatosUsuario_model',
            'Departamento_model',
            'Municipio_model',
            'EstadoCivil_model'
        ]);
    }

    public function ext_form_edit_user() {
        $this->auth->is_session_start();
        $id = $_SESSION['id_user'];
        $data = new stdClass();

        $userdata = $this->DatosUsuario_model->get($id);
        $user = $this->Users_model->get($id);
        $data->user = $user;

        $data->UsuarioID = $id;
        $data->UserData = $userdata;

        $data->tiposdocumentos = $this->TipoDocumento_model->get_active_documenttype();
        $data->departamentos = $this->Departamento_model->get_active_departments();
        $data->estadosciviles = $this->EstadoCivil_model->get_active_estado_civil();

        if ($data->UserData->MunIDExpDocumento > 0) {
            $data->MunicipiosExp = $this->Municipio_model->getMunicipiosByDepId($data->UserData->DepIdExpedicion);
        }

        if ($data->UserData->MunIDResidencia > 0) {
            $data->MunicipiosRes = $this->Municipio_model->getMunicipiosByDepId($data->UserData->DepIdResidencia);
        }

        $data->content_view = "users/ext_edit_profile";
        $data->title = APP_NAME . "::Editar de Usuario";
        $data->active = "users";
        $data->maintitle = "Editar Perfil";

        $data->maindescription = "Edición de Datos de Usuario";

        $this->load->view('dashboard_aspirante', $data);
    }

    public function ext_update_profile() {

        $this->load->library('form_validation');

        if ($this->form_validation->run('ext_update_profile') == FALSE) {
            $this->ext_form_edit_user();
        } else {
            $id = $_SESSION['id_user'];
            $user = $this->DatosUsuario_model->get($id);
            $TipoDocumentoID = $this->input->post('tipo_documento');
            $DocumentoIdentidad = $this->input->post('documento_identidad');
            $Nombres = $this->input->post('nombres');
            $Apellidos = $this->input->post('apellidos');
            $MunIDExpDocumento = $this->input->post('municipio_expedicion');
            $FechaNacimiento = $this->input->post('fecha_nacimiento');
            $Telefono = $this->input->post('telefono');
            $Celular = $this->input->post('celular');
            $Direccion = $this->input->post('direccion');
            $MunIDResidencia = $this->input->post('municipio_residencia');
            $NumeroTarjetaProfesional = $this->input->post('tarjeta_profesional');
            $FechaExpTarjetaProfesional = $this->input->post('fecha_exp_tarjeta_profesional');
            $Genero = $this->input->post('genero');
            $EstadoCivilID = $this->input->post('estado_civil');

            //$date_update = date('Y-m-d H:i:s');

            if (($_FILES['userfile']['name']) != NULL) {
                $ram1 = $this->utilities->randomString(4);
                $ram2 = $this->utilities->randomString(8);
                $file_name = $_FILES['userfile']['name'];
                $tmp = explode('.', $file_name);
                $extension_img = end($tmp);

                $user_img_profile = $ram1 . '_' . $ram2 . '.' . $extension_img;
                $config['upload_path'] = './assets/images/users_img/';
                //'allowed_types' => "gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp",
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size'] = '5000000';
                $config['quality'] = '90%';
                $config['width'] = 150;
                $config['height'] = 180;
                $config['file_name'] = $user_img_profile;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->session->set_flashdata('message_error', $this->upload->display_errors());
                    $this->ext_form_edit_user();
                }
            } else {
                $user_img_profile = $user->FotoUsuario;
            }

            $update = [
                'TipoDocumentoID' => $TipoDocumentoID,
                'DocumentoIdentidad' => $DocumentoIdentidad,
                'Nombres' => $Nombres,
                'Apellidos' => $Apellidos,
                'MunIDExpDocumento' => $MunIDExpDocumento,
                'FechaNacimiento' => $FechaNacimiento,
                'Telefono' => $Telefono,
                'Celular' => $Celular,
                'Direccion' => $Direccion,
                'MunIDResidencia' => $MunIDResidencia,
                'NumeroTarjetaProfesional' => $NumeroTarjetaProfesional,
                'FechaExpTarjetaProfesional' => $FechaExpTarjetaProfesional,
                'Genero' => $Genero,
                'EstadoCivilID' => $EstadoCivilID
            ];
            if ($user->TipoDocumentoID != $TipoDocumentoID || $user->DocumentoIdentidad != $DocumentoIdentidad || $user->Nombres != $Nombres || $user->Apellidos != $Apellidos || $user->MunIDExpDocumento != $MunIDExpDocumento || $user->FechaNacimiento != $FechaNacimiento || $user->Telefono != $Telefono || $user->Celular != $Celular || $user->Direccion != $Direccion || $user->MunIDResidencia != $MunIDResidencia || $user->NumeroTarjetaProfesional != $NumeroTarjetaProfesional || $user->FechaExpTarjetaProfesional != $FechaExpTarjetaProfesional || $user->Genero != $Genero || $user->EstadoCivilID != EstadoCivilID) {
                $id_user = $this->DatosUsuario_model->update($id, $update);
            }
            if ($user->FotoUsuario != $user_img_profile) {
                $fecha = date('Y-m-d H:i:s');
                $update_user = [
                    "FechaActualizacion" => $fecha,
                    "FotoUsuario" => $user_img_profile
                ];
                $id_user2 = $this->Users_model->update($id, $update_user);
            }

            if ($id_user > 0 || $id_user2 > 0) {
                if ($this->input->post('hojavida') == 'perfil') {
                    $this->session->set_flashdata('message_success', 'Los Datos se Actualizaron Correctamente.');
                    redirect('hoja-de-vida');
                } else {
                    $this->session->set_flashdata('message_success', 'Los Datos se Actualizaron Correctamente.');
                    redirect('dashboard-aspirante');
                }
            } else {
                if ($this->input->post('hojavida') == 'perfil') {
                    $this->session->set_flashdata('message_error', 'Los Datos se No Actualizaron.');
                    redirect('hoja-de-vida');
                } else {
                    $this->session->set_flashdata('message_error', 'Los Datos se No Actualizaron.');
                    redirect('dashboard-aspirante');
                }
            }
        }
    }

    public function ext_form_update_profile() {
        $data = new stdClass();

        $data->content_view = "datosusuario/ext_edit_user";
        $data->title = APP_NAME . "::Editar de Usuario";
        $data->maintitle = "Perfil de Usuario";

        $data->maindescription = "Edicion de Usuario";

        $this->load->view('dashboard-aspirante', $data);
    }

    public function getMunicipiosByDepId($depId) {
        $muns = $this->Municipio_model->getMunicipiosByDepId($depId);
        echo '<option value="">Seleccione uno...</option>';
        foreach ($muns as $mun) {
            echo '<option value="' . $mun->MunicipioID . '">' . $mun->Nombre . '</option>';
        }
    }

}
