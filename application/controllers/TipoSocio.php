<?php
class TipoSocio extends CI_Controller
{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model([
            "TipoSocio_model"
        ]);

        $this->load->library(['Auth']);
        $this->auth->is_session_start();
    }

    public function list_tipo_socio() {
        $data = new stdClass();
        $data->content_view = "socios_cooperantes/dash_tipo_socio";
        $data->title = APP_NAME . "::Lista Poblaciones";
        $data->active = "cpntes";
        $data->maintitle = "Tipos de Socios";
        $data->maindescription = "Lista Tipos de Socios";
        $data->tipos_socios = $this->TipoSocio_model->get(FALSE, 1);
        $this->load->view('dashboard', $data);
    }

    public function form_new_tipo_socio() {
        $data = new stdClass();
        $data->content_view = "socios_cooperantes/dash_form_add_tipo_socio";
        $data->title = APP_NAME . "::Nuevo Tipo de Socio";
        $data->active = "cpntes";
        $data->maintitle = "Tipos de Socios";
        $data->maindescription = "Crear Nuevo Tipo de Socio";

        $this->load->view('dashboard', $data);
    }

    public function add_tipo_socio() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_tipo_socio') == FALSE) {
            $this->form_new_tipo_socio();
        } else {

            $tipo_socio_name = $this->input->post('tipo_socio_name');
            $insert = [
                "Nombre" => $tipo_socio_name
            ];

            $tipo_socio_id = $this->TipoSocio_model->insert($insert);
            if ($tipo_socio_id > 0) {
                $this->session->set_flashdata('message_success', 'El Tipo de Socio  se agrego correctamente.');
            }
            redirect('lista-tipo-socio');
        }
    }

    public function edit_tipo_socio($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Tipo de Socio no valido');
            redirect('lista-tipo-socio');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-tipo-socio');
        }
        $tipo_socio = $this->TipoSocio_model->get($id);
        if (!count($tipo_socio) == 1) {
            $this->session->set_flashdata('message_error', 'Tipo de Socio no encontrado');
            redirect('lista-tipo-socio');
        }
        $data = new stdClass();
        $data->content_view = "socios_cooperantes/dash_form_edit_tipo_socio";
        $data->title = APP_NAME . "::Editar Tipo Socio";
        $data->active = "cpntes";
        $data->maintitle = "Tipos de Socios";
        $data->maindescription = "Editar Tipo de Socio";

        $data->tipo_socio = $tipo_socio;

        $this->load->view('dashboard', $data);
    }

    public function update_tipo_socio() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_tipo_socio') == FALSE) {
            $this->edit_tipo_socio($id);
        } else {
            $tipo_socio_name = $this->input->post('tipo_socio_name');

            $update = [
                "Nombre" => $tipo_socio_name
            ];

            $socio_id = $this->TipoSocio_model->update($id, $update);
            if ($socio_id > 0) {
                $this->session->set_flashdata('message_success', 'El Tipo de Socio se actualizo correctamente.');
            }
            redirect('lista-tipo-socio');
        }
    }

    public function delete_tipo_socio($id = FALSE) {
        $perfil = $this->TipoSocio_model->get($id);
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Tipo de Socio no valido');
            redirect('lista-tipo-socio');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-tipo-socio');
        }

        if (!count($perfil) == 1) {
            $this->session->set_flashdata('message_error', 'Tipo de Socio no encontrado');
            redirect('lista-tipo-socio');
        }

        $num_rows = $this->TipoSocio_model->update($id, ['EstadoID' => 3]);
        if ($num_rows > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Tipo de Socio se ha eliminado.');
            redirect('lista-tipo-socio');
        }
    }
}