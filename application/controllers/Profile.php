<?php

class Profile extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model([
            'Projects_model',
            'CatalogoPerfil_model',
            'Perfil_model',
            'ComponenteCompetencia_model',
            'Responsabilidad_model',
            'MunicipiosProyecto_model',
            'MunicipiosPerfil_model',
            'TipoProyecto_model',
            'ResponsabilidadPerfil_model',
            'TipoResponsabilidad_model',
            'Grados_model'
        ]);

        $this->load->library(['Auth']);

        $this->auth->is_session_start();
    }

    public function dash_new_profile() {
        $data = new stdClass();
        $data->grados = $this->Grados_model->getActives();
        $data->nivel_educativo = $this->CatalogoPerfil_model->getByCatalogoPerfilID(1);
        $data->nivel_formacion = $this->CatalogoPerfil_model->getByCatalogoPerfilID(2);
        $data->experiencia_general = $this->CatalogoPerfil_model->getByCatalogoPerfilID(3);
        $data->habilidades = $this->CatalogoPerfil_model->getByCatalogoPerfilID(4);
        $data->tipos_responsabilidad = $this->TipoResponsabilidad_model->get(FALSE, 1);
        $data->responsabilidades = $this->Responsabilidad_model->get(FALSE, FALSE);
        $data->tipos_proyectos = $this->TipoProyecto_model->get(FALSE, 1);
        $data->Projects = $this->Projects_model->get(FALSE, 1);
        $data->content_view = "profiles/dash_new_profile";
        $data->title = APP_NAME . "::Nuevo Perfil";
        $data->active = "projects";
        $data->maintitle = "Perfiles";
        $data->maindescription = "Crear Nuevo Perfil";

        $this->load->view('dashboard', $data);
    }

    public function dash_edit_profile($id) {
        $data = new stdClass();
        $Perfil = $this->Perfil_model->getPerfil($id);
        $prj = $this->Projects_model->get($Perfil->ProyectoID);
        $data->grados = $this->Grados_model->getActives();
        $data->nivel_educativo = $this->CatalogoPerfil_model->getByCatalogoPerfilID(1);
        $data->nivel_formacion = $this->CatalogoPerfil_model->getByCatalogoPerfilID(2);
        $data->experiencia_general = $this->CatalogoPerfil_model->getByCatalogoPerfilID(3);
        $data->habilidades = $this->CatalogoPerfil_model->getByCatalogoPerfilID(4);
        $data->Projects = $this->Projects_model->getProjectsByTipo($prj->TipoProyectoID);
        $data->responsabilidades = $this->Responsabilidad_model->get(FALSE, FALSE);
        $data->tipos_responsabilidad = $this->TipoResponsabilidad_model->get(FALSE, 1);
        $data->responsabilidades_x_perfil = $this->ResponsabilidadPerfil_model->getResponsabilidadesPerfil($id);
        $data->tipos_proyectos = $this->TipoProyecto_model->get(FALSE, 1);
        $data->tipo_proyecto_id = $prj->TipoProyectoID;
        $data->items = $this->Perfil_model->getItemsCatalogoPerfil($id);
        $data->profileData = $Perfil;
        $data->departamentos = $this->MunicipiosProyecto_model->getGetDepartamentoProyecto($Perfil->ProyectoID);
        $data->munsPerfil = $this->MunicipiosPerfil_model->getMunicipiosPerfil($id);
        //$data->responsabilidades = $this->Perfil_model->getResponsabilidadesPerfil($id);
        $data->content_view = "profiles/dash_edit_profile";
        $data->title = APP_NAME . "::Editar Perfil";
        $data->active = "projects";
        $data->maintitle = "Perfiles";
        $data->maindescription = "Editar Perfil";

        $this->load->view('dashboard', $data);
    }

    public function dash_update_responsability() {
        $idResp = $this->input->post('ResponsabilidadID');
        $descripcion = $this->input->post('Descripcion');

        echo $this->Responsabilidad_model->update($idResp, ['Descripcion' => $descripcion]);
    }

    public function dash_delete_responsability() {
        $id = $this->input->post('ResponsabilidadPerfilID');
        echo $this->ResponsabilidadPerfil_model->delete($id);
    }

    public function dash_delete_item_catalogo() {
        $id = $this->input->post('ComponenteCompetenciaID');
        echo $this->ComponenteCompetencia_model->delete($id);
    }

    public function dash_add_item_catalogo() {
        $CatalogoPerfilID = $this->input->post('CatalogoPerfilID');
        $PerfilID = $this->input->post('PerfilID');
        $Peso = $this->input->post('Peso');
        $TiempoExperiencia = $this->input->post('TiempoExperiencia');

        echo $this->ComponenteCompetencia_model->insert([
            'CatalogoPerfilID' => $CatalogoPerfilID,
            'PerfilID' => $PerfilID,
            'Peso' => $Peso,
            'TiempoExperiencia' => $TiempoExperiencia,
        ]);
    }

    public function dash_add_responsabilidad() {
        $ResponsabilidadID = $this->input->post('ResponsabilidadID');
        $PerfilID = $this->input->post('PerfilID');

        echo $this->ResponsabilidadPerfil_model->insert([
            'ResponsabilidadID' => $ResponsabilidadID,
            'PerfilID' => $PerfilID
        ]);
    }

    public function getDepartamentos_x_proyecto($id) {
        $deptos = $this->MunicipiosProyecto_model->getGetDepartamentoProyecto($id);
        echo '<option value="">Seleccione uno...</option>';
        foreach ($deptos as $dpto) {
            echo '<option value="' . $dpto->DepartamentoID . '">' . $dpto->Nombre . '</option>';
            //echo $dto->DepartamentoID  . '  ' .$dto->Nombre . '<br />';
        }
    }

    public function getMunicipiosDepartamentos_x_proyecto($idProyecto, $idDepto) {

        $muns = $this->MunicipiosProyecto_model->getMunicipiosDepartamentoProyecto($idProyecto, $idDepto);
        echo '<option value="">Seleccione uno...</option>';
        foreach ($muns as $mun) {
            echo '<option value="' . $mun->MunicipioID . '">' . $mun->Nombre . '</option>';
            //echo $dto->DepartamentoID  . '  ' .$dto->Nombre . '<br />';
        }
    }

    public function dash_add_municipio_perfil() {
        $PerfilID = $this->input->post('PerfilID');
        $MunicipioID = $this->input->post('MunicipioID');
        $vacantes = $this->input->post('Vacantes');

        echo $this->MunicipiosPerfil_model->insert([
            'PerfilID' => $PerfilID,
            'MunicipioID' => $MunicipioID,
            'municipios_x_perfil_vacantes' => $vacantes
        ]);
    }

    public function dash_delete_municipio_perfil() {
        $id = $this->input->post('MunicipioPerfilID');
        echo $this->MunicipiosPerfil_model->delete($id);
    }

    public function getVacantesProfile() {
        $id_perfil = $this->input->post('PerfilID');
        $id_municipio = $this->input->post('MunicipioID');
        $data = $this->MunicipiosPerfil_model->getVacantesMunicipio($id_perfil, $id_municipio);
        echo $data->municipios_x_perfil_vacantes;
    }

    public function delete_municipio_perfil() {
        $idmun = $this->input->post('MunicipioPerfilID');
        $id_perfil = $this->input->post('PerfilID');
        echo $this->MunicipiosPerfil_model->deleteMun($idmun, $id_perfil);
    }

    public function dash_update_profile() {
        $this->load->library('form_validation');

        $area = $this->input->post('area');
        $mision_cargo = $this->input->post('mision_cargo');
        $titulo_cargo = $this->input->post('titulo_cargo');
        $numero_ocupantes = $this->input->post('numero_ocupantes');
        $perfil_id = $this->input->post('PerfilID');

        if ($this->form_validation->run('dash_new_profile') == FALSE) {
            $this->dash_edit_profile($perfil_id);
        } else {
            $num_rows = $this->Perfil_model->update($perfil_id, [
                'ProyectoID' => $area,
                'MisionCargo' => $mision_cargo,
                'TituloCargo' => $titulo_cargo,
                'NumeroOcupantes' => $numero_ocupantes,
                'FechaUltimaActualizacion' => date('Y-m-d h:i:s')
            ]);

            $this->session->set_flashdata('message_success', 'El Perfil se actualizo correctamente.');
            redirect('perfiles/' . $area);
        }
    }

    public function dash_create_profile() {
        $this->load->library('form_validation');
        if ($this->form_validation->run('dash_new_profile') == FALSE) {
            $this->dash_new_profile();
        } else {
            $proyecto_id = $this->input->post('area');
            $data = array(
                'ProyectoID' => $this->input->post('area'),
                'MisionCargo' => $this->input->post('mision_cargo'),
                'TituloCargo' => $this->input->post('titulo_cargo'),
                'NumeroOcupantes' => $this->input->post('numero_ocupantes'),
                'FechaCreacion' => date('Y-m-d h:i:s')
            );

            $perfil_id = $this->Perfil_model->insert($data);

            for ($i = 0; $i < count($this->input->post('hdItemsId')); $i++) {
                if ($this->input->post('hdExpGeeral')[$i]) {
                    $exp = $this->input->post('hdExpGeneral')[$i];
                } else {
                    $exp = 0;
                }

                $dataComponentes = array(
                    'CatalogoPerfilID' => $this->input->post('hdItemsId')[$i],
                    'PerfilID' => $perfil_id,
                    'Peso' => $this->input->post('hdPeso')[$i],
                    'TiempoExperiencia' => $exp
                );

                $this->ComponenteCompetencia_model->insert($dataComponentes);
            }

            for ($i = 0; $i < count($this->input->post('hdResponsabilidadIDs')); $i++) {
                $dataResponsabilidades = array(
                    'ResponsabilidadID' => $this->input->post('hdResponsabilidadIDs')[$i],
                    'PerfilID' => $perfil_id
                );
                $this->ResponsabilidadPerfil_model->insert($dataResponsabilidades);
            }

            for ($i = 0; $i < count($this->input->post('hdItemsMunicipiosId')); $i++) {
                $dataMuns = array(
                    'MunicipioID' => $this->input->post('hdItemsMunicipiosId')[$i],
                    'PerfilID' => $perfil_id,
                    'municipios_x_perfil_vacantes' => $this->input->post('vacantesxmun')[$i]
                );
                $this->MunicipiosPerfil_model->insert($dataMuns);
            }

            if ($perfil_id > 0) {
                $this->session->set_flashdata('message_success', 'El Perfil se agrego correctamente.');
                redirect('perfiles/' . $proyecto_id);
            }
        }
    }

    public function check_total_ocupantes_x_municipio() {
        $suma_vacantes_x_mun = 0;
        for ($i = 0; $i < count($this->input->post('hdItemsMunicipiosId')); $i++) {
            $suma_vacantes_x_mun += $this->input->post('vacantesxmun')[$i];
        }
        if ($suma_vacantes_x_mun == $this->input->post('numero_ocupantes')) {
            return TRUE;
        }
        $this->form_validation->set_message(
                'check_total_ocupantes_x_municipio', 'El numero de ocupantes debe ser igual a la suma de vacantes por cada municipio'
        );
        return FALSE;
    }

    public function delete_profile($id = FALSE) {
        $perfil = $this->Perfil_model->getPerfil($id);
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Perfil no valido');
            redirect('perfiles/' . $perfil->ProyectoID);
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('perfiles/' . $perfil->ProyectoID);
        }

        if (!count($perfil) == 1) {
            $this->session->set_flashdata('message_error', 'Perfil no encontrado');
            redirect('perfiles/' . $perfil->ProyectoID);
        }

        $num_rows = $this->Perfil_model->update($id, ['EstadoID' => 3]);
        if ($num_rows > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Perfil se ha eliminado.');
            redirect('perfiles/' . $perfil->ProyectoID);
        }
    }

    public function get_responsabilidades($id) {
        $resp = $this->Responsabilidad_model->getResponsabilidadesXTipo($id);
        echo '<option value="">Seleccione uno...</option>';
        foreach ($resp as $item) {
            echo '<option value="' . $item->ResponsabilidadID . '">' . $item->Descripcion . '</option>';
        }
    }

    public function getNivelesEducativosByGrado($grado_id) {
        $nes = $this->CatalogoPerfil_model->getNivelesEducativosByGrado($grado_id);
        echo '<option value="">Seleccione uno...</option>';
        foreach ($nes as $ne) {
            echo '<option value="' . $ne->CatalogoPerfilID . '">' . $ne->Descripcion . '</option>';
            //echo $dto->DepartamentoID  . '  ' .$dto->Nombre . '<br />';
        }
    }

    public function dash_update_componente_competencia()
    {
        $catalogo_perfil_id = $this->input->post('CatalogoPerfilID');
        $id = $this->input->post('idCompCompetencia');
        $peso = $this->input->post('Peso');

        $data = array(
            'CatalogoPerfilID' => $catalogo_perfil_id,
            'Peso' => $peso,
        );

        echo $this->ComponenteCompetencia_model->update($id, $data);
    }

    public function form_clonar_profile($id_perfil = FALSE) {
        if ($this->uri->segment(2)) {
            $id_profile = $this->uri->segment(2);
        } else {
            $id_profile = $id_perfil;
        }

        $data = new stdClass();
        $data->content_view = "profiles/dash_clonar_profile";
        $data->title = APP_NAME . "::Clonar Perfil";
        $data->active = "projects";
        $data->maintitle = "Perfiles";
        $data->maindescription = "Clonar Perfil";
        $data->id_profile = $id_profile;
        $data->Projects = $this->Projects_model->get(FALSE, 1);
        $data->tipos_proyectos = $this->TipoProyecto_model->get(FALSE, 1);
        $this->load->view('dashboard', $data);
    }

    public function clonar_profile() {
        $id_perfil = $this->input->post('id_perfil');
        $id_proyecto = $this->input->post('area');
        $perfil = $this->Perfil_model->getPerfil($id_perfil);
        $fecha = date('Y-m-d H:i:s');
        $insert_new_profile = [
            "ProyectoID" => $id_proyecto,
            "MisionCargo" => $perfil->MisionCargo,
            "TituloCargo" => $perfil->TituloCargo,
            "EstadoID" => $perfil->EstadoID,
            "FechaCreacion" => $fecha,
            "FechaUltimaActualizacion" => $fecha
        ];
        $this->db->trans_start();
        $new_id_profile = $this->Perfil_model->insert($insert_new_profile);
        if ($new_id_profile > 0) {
            $componentes = $this->Perfil_model->getItemsCatalogoPerfil($id_perfil);
            foreach ($componentes as $comp) {
                $insert_comp = [
                    "CatalogoPerfilID" => $comp->CatalogoPerfilID,
                    "PerfilID" => $new_id_profile,
                    "Peso" => $comp->Peso,
                    "TiempoExperiencia" => $comp->TiempoExperiencia
                ];
                $success = $this->ComponenteCompetencia_model->insert($insert_comp);
                if (!$success > 0) {
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('message_error', 'Ocurrio un error al clonar el perfil');
                    redirect('clonar-perfil/' . $id_perfil);
                }
            }
            $this->db->trans_complete();
            $this->session->set_flashdata('message_success', 'Cuenta creada satisfactoriamente');
            redirect('perfiles/' . $id_proyecto);
        } else {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message_error', 'Ocurrio un error al clonar el perfil');
            redirect('clonar-perfil/' . $id_perfil);
        }
    }

}
