<?php
class Ubicacion extends CI_Controller
{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model([
            "Ubicacion_model"
        ]);

        $this->load->library(['Auth']);
        $this->auth->is_session_start();
    }

    public function list_ubicaciones() {
        $data = new stdClass();
        $data->content_view = "ubicacion/dash_list_ubicaciones";
        $data->title = APP_NAME . "::Lista Ubicaciones";
        $data->active = "cpntes";
        $data->maintitle = "Ubicaciones";
        $data->maindescription = "Lista de Ubicaciones";
        $data->ubicaciones = $this->Ubicacion_model->get(FALSE, 1);
        $this->load->view('dashboard', $data);
    }

    public function form_new_ubicacion() {
        $data = new stdClass();
        $data->content_view = "ubicacion/dash_form_add_ubicacion";
        $data->title = APP_NAME . "::Nueva Ubicación";
        $data->active = "cpntes";
        $data->maintitle = "Ubicaciones";
        $data->maindescription = "Crear Nueva Ubicación";

        $this->load->view('dashboard', $data);
    }

    public function add_ubicacion() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_ubicacion') == FALSE) {
            $this->form_new_ubicacion();
        } else {

            $ubicacion_name = $this->input->post('ubicacion_name');
            $insert = [
                "Descripcion" => $ubicacion_name
            ];

            $ubicacion_id = $this->Ubicacion_model->insert($insert);
            if ($ubicacion_id > 0) {
                $this->session->set_flashdata('message_success', 'La Ubicación se agrego correctamente.');
            }
            redirect('lista-ubicaciones');
        }
    }

    public function edit_ubicacion($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Ubicación no valido');
            redirect('lista-ubicaciones');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-ubicaciones');
        }
        $ubicacion = $this->Ubicacion_model->get($id);
        if (!count($ubicacion) == 1) {
            $this->session->set_flashdata('message_error', 'Ubicación no encontrado');
            redirect('lista-ubicaciones');
        }
        $data = new stdClass();
        $data->content_view = "ubicacion/dash_form_edit_ubicacion";
        $data->title = APP_NAME . "::Editar Ubicación";
        $data->active = "cpntes";
        $data->maintitle = "Ubicación";
        $data->maindescription = "Editar Ubicación";

        $data->ubicacion = $ubicacion;

        $this->load->view('dashboard', $data);
    }

    public function update_ubicacion() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_ubicacion') == FALSE) {
            $this->edit_ubicacion($id);
        } else {
            $ubicacion_name = $this->input->post('ubicacion_name');

            $update = [
                "Descripcion" => $ubicacion_name
            ];

            $socio_id = $this->Ubicacion_model->update($id, $update);
            if ($socio_id > 0) {
                $this->session->set_flashdata('message_success', 'La Ubicación se actualizo correctamente.');
            }
            redirect('lista-ubicaciones');
        }
    }

    public function delete_ubicacion($id = FALSE)
    {
        $perfil = $this->Ubicacion_model->get($id);
        if (!$id) {
            $id = (int)$this->uri->segment(2);
        } else {
            $id = (int)$id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Ubicación no valido');
            redirect('lista-ubicaciones');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-ubicaciones');
        }

        if (!count($perfil) == 1) {
            $this->session->set_flashdata('message_error', 'Ubicación no encontrado');
            redirect('lista-ubicaciones');
        }

        $num_rows = $this->Ubicacion_model->update($id, ['EstadoID' => 3]);
        if ($num_rows > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'La Ubicación se ha eliminado.');
            redirect('lista-ubicaciones');
        }
    }
}