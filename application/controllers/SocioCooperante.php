<?php
class SocioCooperante extends CI_Controller
{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model([
            "SocioCooperante_model",
            "TipoSocio_model"
        ]);
        $this->load->library([
            'Auth'
        ]);
        $this->auth->is_session_start();
    }

    public function list_socios() {
        $data = new stdClass();
        $data->content_view = "socios_cooperantes/dash_list_socios";
        $data->title = APP_NAME . "::Lista Socios Cooperantes";
        $data->active = "cpntes";
        $data->maintitle = "Socios Cooperantes";
        $data->maindescription = "Lista de Socios Cooperantes";
        $data->socios = $this->SocioCooperante_model->get(FALSE, 1);
        $this->load->view('dashboard', $data);
    }

    public function form_new_socio() {
        $data = new stdClass();
        $data->content_view = "socios_cooperantes/dash_form_add_socio";
        $data->title = APP_NAME . "::Nuevo Socio Cooperante";
        $data->active = "cpntes";
        $data->maintitle = "Socios Cooperantes";
        $data->maindescription = "Crear Nuevo Socio Cooperante";
        $data->tipos_socios = $this->TipoSocio_model->get(FALSE, 1);

        $this->load->view('dashboard', $data);
    }

    public function add_socio() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_socio') == FALSE) {
            $this->form_new_socio();
        } else {

            $socio_name = $this->input->post('socio_name');
            $tipo_socio_id = $this->input->post('tipo_socio');
            $insert = [
                "Nombre" => $socio_name,
                "TipoSocioID" => $tipo_socio_id
            ];

            $socio_id = $this->SocioCooperante_model->insert($insert);
            if ($socio_id > 0) {
                $this->session->set_flashdata('message_success', 'El Socio Cooperante se agrego correctamente.');
            }
            redirect('lista-socios');
        }
    }

    public function edit_socio($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Socio Cooperante no valido');
            redirect('lista-socios');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-socios');
        }
        $socio = $this->SocioCooperante_model->get($id);
        if (!count($socio) == 1) {
            $this->session->set_flashdata('message_error', 'Socio Cooperante no encontrado');
            redirect('lista-socios');
        }
        $data = new stdClass();
        $data->content_view = "socios_cooperantes/dash_form_edit_socio";
        $data->title = APP_NAME . "::Editar Socio Cooperante";
        $data->active = "cpntes";
        $data->maintitle = "Socios Cooperantes";
        $data->maindescription = "Editar Rol";
        $data->tipos_socios = $this->TipoSocio_model->get(FALSE, 1);
        $data->socio = $socio;

        $this->load->view('dashboard', $data);
    }

    public function update_socio() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_socio') == FALSE) {
            $this->edit_socio($id);
        } else {
            $socio_name = $this->input->post('socio_name');
            $tipo_socio_id = $this->input->post('tipo_socio');
            $update = [
                "Nombre" => $socio_name,
                "TipoSocioID" => $tipo_socio_id
            ];

            $socio_id = $this->SocioCooperante_model->update($id, $update);
            if ($socio_id > 0) {
                $this->session->set_flashdata('message_success', 'El Socio Cooperante se actualizo correctamente.');
            }
            redirect('lista-socios');
        }
    }

    public function delete_socio($id = FALSE) {
        $perfil = $this->SocioCooperante_model->get($id);
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Socio Cooperante no valido');
            redirect('lista-socios');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-socios');
        }

        if (!count($perfil) == 1) {
            $this->session->set_flashdata('message_error', 'Socio Cooperante no encontrado');
            redirect('lista-socios');
        }

        $num_rows = $this->SocioCooperante_model->update($id, ['EstadoID' => 3]);
        if ($num_rows > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Socio Cooperante se ha eliminado.');
            redirect('lista-socios');
        }
    }
}