<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Flujos
 *
 * @author furbox
 */
class Flujos extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model([
            "Flujos_model",
            "Cargos_model"
        ]);
    }

    public function list_flujo_aprobaciones() {
        $data = new stdClass();
        $data->content_view = "flujos/dash_list_flujos";
        $data->title = APP_NAME . "::Lista de Flujos de Aprobaciones";
        $data->active = "cpntes";
        $data->maintitle = "Flujos";
        $data->maindescription = "Lista de Flujos de Aprobaciones";
        $data->flujos = $this->Flujos_model->get(FALSE, 1);
        $this->load->view('dashboard', $data);
    }

    public function update_orden_flujo_aprobarciones() {

        $flujos = $this->Flujos_model->get(FALSE, 1);
        $this->db->trans_start();
        foreach ($flujos as $f) {
            $update = [
                "flujo_orden" => $this->input->post('flujo_' . $f->flujo_id)
            ];
            $success = $this->Flujos_model->update($f->flujo_id, $update);
            if (!$success > 0) {
                $this->db->trans_rollback();
                $this->session->set_flashdata('message_error', 'Los Flujos no se actualizaron correctamente.');
                redirect('lista-flujo-aprobaciones');
            }
        }
        $this->db->trans_complete();
        $this->session->set_flashdata('message_success', 'Los flujos se actualizacon correctamente');
        redirect('lista-flujo-aprobaciones');
    }

    public function form_edit_flujo_aprobaciones($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Flujo no valido');
            redirect('lista-flujo-aprobaciones');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-flujo-aprobaciones');
        }
        $flujo = $this->Flujos_model->get($id);
        if (!count($flujo) == 1) {
            $this->session->set_flashdata('message_error', 'Flujo no encontrado');
            redirect('lista-flujo-aprobaciones');
        }
        $cargos = $this->Cargos_model->get_active();
        $data = new stdClass();
        $data->content_view = "flujos/form_edit_flujo";
        $data->title = APP_NAME . "::Editar Flujo";
        $data->active = "cpntes";
        $data->maintitle = "Flujos";
        $data->maindescription = "Editar Flujo";
        $data->cargos = $cargos;
        $data->flujo = $flujo;

        $this->load->view('dashboard', $data);
    }

    public function update_flujo() {
        $this->load->library('form_validation');

        $id_flujo = $this->input->post('id');
//        $total_flujos = count($this->Flujos_model->get(FALSE, 1));
//        $cont = 1 + (int) $total_flujos;

        $flujo_name = $this->input->post('flujo_name');
        $flujo_cargo = $this->input->post('flujo_cargo');

        $update = [
            "flujo_nombre_flujo" => $flujo_name,
            "flujo_id_cargo" => $flujo_cargo,
        ];

        $success = $this->Flujos_model->update($id_flujo, $update);
        if ($success > 0) {
            $this->session->set_flashdata('message_success', 'El Flujo se actualizo correctamente.');
            redirect('lista-flujo-aprobaciones');
        }
    }

    public function form_new_flujo() {
        $data = new stdClass();
        $data->content_view = "flujos/dash_form_new_flujo";
        $data->title = APP_NAME . "::Nuevo Flujo";
        $data->active = "cpntes";
        $data->maintitle = "Flujos";
        $data->maindescription = "Nuevo Flujo";
        $data->cargos = $this->Cargos_model->get_active();
        $this->load->view('dashboard', $data);
    }

    public function add_flujo() {
        $nombre_flujo = $this->input->post("flujo_name");
        $cargo_flujo = $this->input->post("flujo_cargo");
        $total_flujos = count($this->Flujos_model->get(FALSE, 1));
        $cont = 1 + (int) $total_flujos;
        $insert = [
            "flujo_nombre_flujo" => $nombre_flujo,
            "flujo_id_cargo" => $cargo_flujo,
            "flujo_orden" => $cont,
            "flujo_tipo" => 1
        ];
        $success = $this->Flujos_model->insert($insert);
        if ($success > 0) {
            $this->session->set_flashdata('message_success', 'El Flujo se agrego correctamente.');
            redirect('lista-flujo-aprobaciones');
        }
    }

    public function delete_flujo() {
        $id = (int) $this->uri->segment(2);
        
        if (!$id) {
            $this->session->set_flashdata('message_error', 'Flujo no valido');
            redirect('lista-flujo-aprobaciones');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-flujo-aprobaciones');
        }
        $flujo = $this->Flujos_model->get($id);
        if (!count($flujo) == 1) {
            $this->session->set_flashdata('message_error', 'Flujo no encontrado');
            redirect('lista-flujo-aprobaciones');
        }

        $num_rows = $this->Flujos_model->delete($id);
        if ($num_rows > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'El Flujo se ha eliminado.');
            redirect('lista-flujo-aprobaciones');
        }
    }

}
