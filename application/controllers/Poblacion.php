<?php
class Poblacion extends CI_Controller
{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model([
            "Poblacion_model"
        ]);
        $this->load->library([
            'Auth'
        ]);

        $this->auth->is_session_start();
    }

    public function list_poblaciones() {
        $data = new stdClass();
        $data->content_view = "poblacion/dash_list_poblaciones";
        $data->title = APP_NAME . "::Lista Poblaciones";
        $data->active = "cpntes";
        $data->maintitle = "Poblaciones";
        $data->maindescription = "Lista de Poblaciones";
        $data->poblaciones = $this->Poblacion_model->get(FALSE, 1);
        $this->load->view('dashboard', $data);
    }

    public function form_new_poblacion() {
        $data = new stdClass();
        $data->content_view = "poblacion/dash_form_add_poblacion";
        $data->title = APP_NAME . "::Nueva Población";
        $data->active = "cpntes";
        $data->maintitle = "Poblaciones";
        $data->maindescription = "Crear Nueva Población";

        $this->load->view('dashboard', $data);
    }

    public function add_poblacion() {
        $this->load->library('form_validation');

        if ($this->form_validation->run('dash_add_poblacion') == FALSE) {
            $this->form_new_poblacion();
        } else {

            $poblacion_name = $this->input->post('poblacion_name');
            $insert = [
                "Nombre" => $poblacion_name
            ];

            $poblacion_id = $this->Poblacion_model->insert($insert);
            if ($poblacion_id > 0) {
                $this->session->set_flashdata('message_success', 'El Población se agrego correctamente.');
            }
            redirect('lista-poblaciones');
        }
    }

    public function edit_poblacion($id = FALSE) {
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Población no valido');
            redirect('lista-poblaciones');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-poblaciones');
        }
        $poblacion = $this->Poblacion_model->get($id);
        if (!count($poblacion) == 1) {
            $this->session->set_flashdata('message_error', 'Población no encontrado');
            redirect('lista-poblaciones');
        }
        $data = new stdClass();
        $data->content_view = "poblacion/dash_form_edit_poblacion";
        $data->title = APP_NAME . "::Editar Población";
        $data->active = "cpntes";
        $data->maintitle = "Población";
        $data->maindescription = "Editar Población";

        $data->poblacion = $poblacion;

        $this->load->view('dashboard', $data);
    }

    public function update_poblacion() {
        $this->load->library('form_validation');
        $id = $this->input->post('id');
        if ($this->form_validation->run('dash_update_poblacion') == FALSE) {
            $this->edit_poblacion($id);
        } else {
            $poblacion_name = $this->input->post('poblacion_name');

            $update = [
                "Nombre" => $poblacion_name
            ];

            $rol_id = $this->Poblacion_model->update($id, $update);
            if ($rol_id > 0) {
                $this->session->set_flashdata('message_success', 'La Población se actualizo correctamente.');
            }
            redirect('lista-poblaciones');
        }
    }

    public function delete_poblacion($id = FALSE) {
        $perfil = $this->Poblacion_model->get($id);
        if (!$id) {
            $id = (int) $this->uri->segment(2);
        } else {
            $id = (int) $id;
        }

        if (!$id) {
            $this->session->set_flashdata('message_error', 'Población no valida');
            redirect('lista-poblaciones');
        }
        if (!is_int($id)) {
            $this->session->set_flashdata('message_error', 'Informacion no Valida');
            redirect('lista-poblaciones');
        }

        if (!count($perfil) == 1) {
            $this->session->set_flashdata('message_error', 'Población no encontrada');
            redirect('lista-poblaciones');
        }

        $num_rows = $this->Poblacion_model->update($id, ['EstadoID' => 3]);
        if ($num_rows > 0) {
            //$this->authentication->send_validation_email($user_email,$user_activation_code);
            $this->session->set_flashdata('message_success', 'La Población se ha eliminado.');
            redirect('lista-poblaciones');
        }
    }
}