<?php
class ComponenteCompetencia_model extends CI_Model{
    private $table = "componentescompetencias";
    private $id = "ComponenteCompetenciaID";
    public function __construct() {
        parent::__construct();
    }
    
    public function getbyPerfil($id_perfil){
        $this->db->select('cc.*,cp.*,tc.Descripcion AS tipocat');
        $this->db->from($this->table.' as cc');
        $this->db->join('catalogoperfil as cp','cp.CatalogoPerfilID = cc.CatalogoPerfilID');
        $this->db->join('tipocatalogo as tc','tc.TipoCatalogoID = cp.TipoCatalogoID');
        $this->db->where('PerfilID = '.$id_perfil);
        $data = $this->db->get();
        return $data->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }
}
