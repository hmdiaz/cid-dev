<?php
class MunicipiosProyecto_model extends CI_Model
{
    //put your code here
    private $table = "municipios_x_proyecto";
    private $id = "MunicipioProyectoID";

    public function __construct() {
        parent::__construct();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function getMunicipiosProyecto($id)
    {
        $this->db->select('p.ProyectoID, m.MunicipioID, m.Nombre AS NombreMunicipio, d.Nombre AS NombreDepartamento, pb.Nombre AS NombrePoblacion, u.Descripcion AS NombreUbicacion, mp.Cobertura');
        $this->db->from('proyecto p');
        $this->db->join('municipios_x_proyecto mp', 'p.ProyectoID = mp.ProyectoID');
        $this->db->join('municipio m', 'mp.MunicipioID = m.MunicipioID');
        $this->db->join('poblacion pb', 'mp.PoblacionID = pb.PoblacionID');
        $this->db->join('ubicacion u', 'mp.UbicacionID = u.UbicacionID');
        $this->db->join('departamento d', 'd.DepartamentoID = m.DepartamentoID');
        $this->db->where('p.ProyectoID', $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function getGetDepartamentoProyecto($id)
    {
        $this->db->select('d.DepartamentoID, d.Nombre');
        $this->db->from('proyecto p');
        $this->db->join('municipios_x_proyecto mp', 'p.ProyectoID = mp.ProyectoID');
        $this->db->join('municipio m', 'mp.MunicipioID = m.MunicipioID');
        $this->db->join('departamento d', 'd.DepartamentoID = m.DepartamentoID');
        $this->db->where('p.ProyectoID', $id);
        $this->db->distinct();
        $query = $this->db->get();
        return $query->result();
    }

    public function getMunicipiosDepartamentoProyecto($idProyecto, $idDepto){
        $this->db->select('m.MunicipioID, m.Nombre');
        $this->db->from('proyecto p');
        $this->db->join('municipios_x_proyecto mp', 'p.ProyectoID = mp.ProyectoID');
        $this->db->join('municipio m', 'mp.MunicipioID = m.MunicipioID');
        $this->db->join('departamento d', 'd.DepartamentoID = m.DepartamentoID');
        $this->db->where(['p.ProyectoID' => $idProyecto, 'd.DepartamentoID' => $idDepto]);
        $query = $this->db->get();
        return $query->result();
    }
}