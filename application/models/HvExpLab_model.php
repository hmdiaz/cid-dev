<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HvExpLab_model
 *
 * @author furbox
 */
class HvExpLab_model extends CI_Model {

    //put your code here
    private $table = "HvExpLab";
    private $id = "HvExpLabID";

    public function __construct() {
        parent::__construct();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }
    
    public function getByIDUser($id) {
        $query = $this->db->get_where($this->table, [$this->id => $id,'UsuarioID' => $_SESSION['id_user']]);
        return $query->row();
    }

    public function getByUser($id) {
        $query = $this->db->get_where($this->table, ['UsuarioID' => $id]);
        return $query->result();
    }
    
    public function getExpByUser($id){
        $this->db->select();
        $this->db->from($this->table.' el');
        $this->db->join('HvExpLabExpGral eleg', 'eleg.HvExpLabID = el.HvExpLabID');
        $this->db->join('catalogoperfil cp','cp.CatalogoPerfilID = eleg.CatalogoPerfilID');
        $this->db->where('el.UsuarioID = '.$id);
        $data = $this->db->get();
        return $data->result();
    }
    
    public function getExpGralByUser($id){
        $this->db->select("eleg.CatalogoPerfilID,MAX(eleg.Experiencia) AS Experiencia");
        $this->db->from($this->table.' el');
        $this->db->join('HvExpLabExpGral eleg', 'eleg.HvExpLabID = el.HvExpLabID');
        $this->db->join('catalogoperfil cp','cp.CatalogoPerfilID = eleg.CatalogoPerfilID');
        $this->db->where('el.UsuarioID = '.$id);
        $this->db->group_by("eleg.CatalogoPerfilID");
        $this->db->order_by("Experiencia","desc");
        $data = $this->db->get();
        return $data->result();
    }
    
    public function getExpUser($id){
        $this->db->select();
        $this->db->from($this->table.' el');
        $this->db->join('departamento d', 'd.DepartamentoID = el.DepartamentoID');
        $this->db->where('el.UsuarioID = '.$id);
        $data = $this->db->get();
        return $data->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

}
