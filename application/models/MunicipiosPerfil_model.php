<?php
class MunicipiosPerfil_model extends CI_Model
{
    //put your code here
    private $table = "municipios_x_perfil";
    private $id = "MunicipiosPerfilID";

    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function delete($id)
    {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }
    
    public function deleteMun($idmun,$id_perfil)
    {
        $this->db->delete($this->table, array("PerfilID" => $id_perfil, "MunicipioID" => $idmun));
        return $this->db->affected_rows();
    }

    public function getMunicipiosPerfil($id)
    {
        $this->db->select('mp.municipios_x_perfil_vacantes,mp.MunicipiosPerfilID, p.PerfilID, m.MunicipioID, m.Nombre AS NombreMunicipio, d.Nombre AS NombreDepartamento');
        $this->db->from('perfil p');
        $this->db->join('municipios_x_perfil mp', 'p.PerfilID = mp.PerfilID');
        $this->db->join('municipio m', 'mp.MunicipioID = m.MunicipioID');
        $this->db->join('departamento d', 'd.DepartamentoID = m.DepartamentoID');
        $this->db->where('p.PerfilID', $id);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getVacantesMunicipio($id_perfil,$id_municipio){
        $this->db->select('municipios_x_perfil_vacantes');
        $this->db->from($this->table);
        $this->db->where(['PerfilID' => $id_perfil,'MunicipioID' => $id_municipio]);
        $query = $this->db->get();
        return $query->row();
    }
}