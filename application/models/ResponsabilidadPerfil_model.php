<?php
class ResponsabilidadPerfil_model extends CI_Model
{
    private $table = "responsabilidad_x_perfil";
    private $id = "ResponsabilidadPerfilID";

    public function __construct() {
        parent::__construct();
    }

    public function get($id = FALSE, $status = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        if(!$status)
        {
            $query = $this->db->get($this->table);
            return $query->result();
        }
        $query = $this->db->get_where($this->table, ['EstadoID' => $status]);
        return $query->result();
    }

    public function getResponsabilidadesPerfil($perfilId)
    {

        $this->db->select('r.ResponsabilidadID, r.Descripcion, r.TipoResponsabilidadID, tr.Descripcion AS TipoRespDesc, rp.ResponsabilidadPerfilID, rp. PerfilID');
        $this->db->from('responsabilidad_x_perfil rp');
        $this->db->join('responsabilidad r', 'r.ResponsabilidadID = rp.ResponsabilidadID');
        $this->db->join('tiporesponsabilidad tr', 'tr.TipoResponsabilidadID = r.TipoResponsabilidadID');
        $this->db->where('rp.PerfilID', $perfilId);
        $query = $this->db->get();
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where([$this->id => $id]);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }
}