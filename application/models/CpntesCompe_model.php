<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CpntesCompe_model
 *
 * @author furbox
 */
class CpntesCompe_model extends CI_Model{
    //put your code here
    //put your code here
    private $table = "catalogoperfil";
    private $id = "CatalogoPerfilID";

    public function __construct() {
        parent::__construct();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }
    
    public function getByCpnte($id){
        $this->db->where('TipoCatalogoID',$id);
        if($id == 1){
            $this->db->where(['CategoriaPerfilID !=' => 7,'tipo_grado_id !=' => 4]);
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }
    
    public function get_pagination($id,$inicio = FALSE, $limite = FALSE){ 
        $this->db->where('TipoCatalogoID',$id);
        if($inicio !== FALSE && $limite !== FALSE){
            $this->db->limit($limite,$inicio);
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }
    
}
