<?php
/**
 * Created by PhpStorm.
 * User: Mario Díaz
 * Date: 10/04/2016
 * Time: 12:03 PM
 */

class Municipio_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function getMunicipiosByDepId($depId)
    {
        $query = $this->db->get_where('municipio', ['DepartamentoID' => $depId]);
        return $query->result();
    }
}