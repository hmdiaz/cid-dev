<?php

class Aprobaciones_model extends CI_Model {

    //put your code here
    private $table = "aprobacion";
    private $id = "AprobacionID";

    public function __construct() {
        parent::__construct();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getAprovacion($id_req, $id_proyecto) {
        $query = $this->db->get_where($this->table, ['AprobarID' => $id_req, 'ProyectoID' => $id_proyecto, 'tipo_aprobacion' => 1]);
        return $query->row();
    }

    public function getXRequisicion($idReq) {
        $query = $this->db->get_where($this->table, ['AprobarID' => $idReq, 'tipo_aprobacion' => 1]);
        return $query->result();
    }

    public function get_active() {
        $query = $this->db->get_where($this->table, ['EstadoID' => 1]);
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function aprobarRequisicion($idProyecto, $idRequisicion, $data) {
        $this->db->where(['ProyectoID' => $idProyecto, 'AprobarID' => $idRequisicion]);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function getRequisiciones_x_Usuario($idUsuario) {
        $this->db->select('r.RequisicionID, r.TipoContratoID, r.DatosUsuarioSolitanteID, r.FechaSolicitud, r.CantidadPersonasSolicitadas, r.Justificacion, r.SalarioAsignado, r.NombreCargo, r.Observaciones, r.FechaInicioLabores, r.FechaFinLabores, r.MotivoID, r.PerfilID, r.<EstadoID');
        $this->db->from('requisicion r');
        $this->db->join('aprobacion a', 'a.AprobarID = r.RequisicionID');
        $this->db->where(['a.DatosUsuarioID' => $idUsuario,]);
        $query = $this->db->get();
        return $query->result();
    }

    public function getAprobacionesPendientes($datosUsuarioID) {
        $this->db->select('r.RequisicionID, r.TipoContratoID, r.DatosUsuarioSolitanteID, r.FechaSolicitud, r.CantidadPersonasSolicitadas, r.Justificacion, r.SalarioAsignado, r.NombreCargo, r.Observaciones, r.FechaInicioLabores, r.FechaFinLabores, r.MotivoID, r.PerfilID, r.EstadoID');
        $this->db->from('requisicion r');
        $this->db->join('perfil pf', 'pf.PerfilID = r.PerfilID');
        $this->db->join('proyecto p', 'p.ProyectoID = pf.ProyectoID');
        $this->db->join('aprobacion a', 'a.AprobarID = r.RequisicionID');
        $this->db->join('proyecto pj', 'pj.ProyectoID = a.ProyectoID');
        $this->db->join('datosusuario du', 'du.DatosUsuarioID = pj.DatosUsuarioApruebaID');
        $this->db->where(['du.DatosUsuarioID' => $datosUsuarioID, 'a.EstadoID' => 6]);

        $query = $this->db->get();
        return $query->result();
    }

    public function getAprobaciones($datosUsuarioID, $status = FALSE) {
        $this->db->select('a.ProyectoID, a.AprobacionID, tc.Nombre AS NombreTipoContrato, r.RequisicionID, r.TipoContratoID, r.DatosUsuarioSolitanteID, r.FechaSolicitud, r.CantidadPersonasSolicitadas, r.Justificacion, r.SalarioAsignado, r.NombreCargo, r.Observaciones, r.FechaInicioLabores, r.FechaFinLabores, r.MotivoID, r.PerfilID, r.EstadoID');
        $this->db->from('requisicion r');
        $this->db->join('perfil pf', 'pf.PerfilID = r.PerfilID');
        $this->db->join('proyecto p', 'p.ProyectoID = pf.ProyectoID');
        $this->db->join('aprobacion a', 'a.AprobarID = r.RequisicionID');
        $this->db->join('proyecto pj', 'pj.ProyectoID = a.ProyectoID');
        $this->db->join('tipocontrato tc', 'tc.TipoContratoID = r.TipoContratoID');
        $this->db->join('datosusuario du', 'du.DatosUsuarioID = pj.DatosUsuarioApruebaID');
        $this->db->where(['a.mostrar_notificaciones' => 1]);
        if ($status)
            $this->db->where(['du.DatosUsuarioID' => $datosUsuarioID, 'a.EstadoID' => $status]);
        else
            $this->db->where(['du.DatosUsuarioID' => $datosUsuarioID]);

        $query = $this->db->get();
        return $query->result();
    }

    public function VerificarRequisicionesAprobadas($id) {
        $aprobados = 0;
        $req_aprobada = 'No';
        $query = $this->db->get_where($this->table, ['AprobarID' => $id]);
        $result = $query->result();
        foreach ($result as $item) {
            if ($item->EstadoID == 4)
                $aprobados++;
        }

        $total = $query->num_rows();
        if (($aprobados == $total) && $total > 0)
            $req_aprobada = 'Si';

        $data['RequisicionAprobada'] = $req_aprobada;
        $data['CantRequisicionesAprobadas'] = $aprobados;
        $data['Total'] = $total;

        return $data;
    }

    public function getListadoAprobacionesXRequisicion($id) {
        $this->db->select('eah.Nombre AS status_name,ah.*,pj.NombreProyecto, e.Nombre AS Estado, a.Observaciones,a.FechaActualizacion, CONCAT(du.Nombres, " ", du.Apellidos) AS Nombre');
        $this->db->from('requisicion r');
        $this->db->join('aprobacion a', 'a.AprobarID = r.RequisicionID');
        $this->db->join('aprobaciones_historial ah', 'ah.aprobaciones_historial_id_aprobacion = a.AprobacionID');
        $this->db->join('estado eah', 'eah.EstadoID = ah.aprobaciones_historial_status');
        $this->db->join('estado e', 'e.EstadoID = a.EstadoID');
        $this->db->join('proyecto pj', 'pj.ProyectoID = a.ProyectoID');
        $this->db->join('datosusuario du', 'du.DatosUsuarioID = pj.DatosUsuarioApruebaID', 'left');
        $this->db->where(['r.RequisicionID' => $id]);

        $query = $this->db->get();
        return $query->result();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

}
