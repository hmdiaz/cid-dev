<?php
class Etapa_model extends CI_Model
{
    private $table = "etapa";
    private $id = "EtapaID";

    public function __construct() {
        parent::__construct();
    }

    public function get($id = FALSE, $status = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        if(!$status)
        {
            $this->db->order_by("Orden", "asc");
            $query = $this->db->get($this->table);
            return $query->result();
        }
        $this->db->order_by("Orden", "asc");
        $query = $this->db->get_where($this->table, ['EstadoID' => $status]);
        return $query->result();
    }
    public function getActives(){
        $query = $this->db->get_where($this->table, ['EstadoID' => 1]);
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }
}
