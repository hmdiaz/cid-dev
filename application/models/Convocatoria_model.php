<?php

class Convocatoria_model extends CI_Model {

    //put your code here
    private $table = "convocatoria";
    private $id = "ConvocatoriaID";

    public function __construct() {
        parent::__construct();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getConvocatoriaByRequisicion($id_req) {
        $query = $this->db->get_where($this->table, ['RequisicionID' => $id_req]);
        return $query->row();
    }

    public function getDatosConvocatoria($idReq) {
        $this->db->select('tc.Nombre AS NombreTipoContrato, r.RequisicionID, r.TipoContratoID, r.DatosUsuarioSolitanteID, r.FechaSolicitud, r.CantidadPersonasSolicitadas, pf.MisionCargo, r.Justificacion, r.SalarioAsignado, r.NombreCargo, r.Observaciones, r.FechaInicioLabores, r.FechaFinLabores, DATEDIFF(r.FechaFinLabores, r.FechaInicioLabores) AS DuracionContrato, r.MotivoID, m.Descripcion AS DescripcionMotivo, r.PerfilID, r.EstadoID');
        $this->db->from('requisicion r');
        $this->db->join('perfil pf', 'pf.PerfilID = r.PerfilID');
        $this->db->join('proyecto p', 'p.ProyectoID = pf.ProyectoID');
        $this->db->join('motivo m', 'm.MotivoID = r.MotivoID');
        $this->db->join('tipocontrato tc', 'tc.TipoContratoID = r.TipoContratoID');
        $this->db->where(['r.RequisicionID' => $idReq]);

        $query = $this->db->get();
        return $query->row();
    }

    public function getListaConvocatorias_x_Requisicion($idReq) {
        $this->db->select('c.ConvocatoriaID, tc.Nombre AS NombreTipoContrato, r.RequisicionID, r.TipoContratoID, r.DatosUsuarioSolitanteID, r.FechaSolicitud, r.CantidadPersonasSolicitadas, pf.MisionCargo, r.Justificacion, r.SalarioAsignado, r.NombreCargo, r.Observaciones, r.FechaInicioLabores, r.FechaFinLabores, DATEDIFF(r.FechaFinLabores, r.FechaInicioLabores) AS DuracionContrato, r.MotivoID, m.Descripcion AS DescripcionMotivo, r.PerfilID, r.EstadoID');
        $this->db->from('requisicion r');
        $this->db->join('perfil pf', 'pf.PerfilID = r.PerfilID');
        $this->db->join('proyecto p', 'p.ProyectoID = pf.ProyectoID');
        $this->db->join('motivo m', 'm.MotivoID = r.MotivoID');
        $this->db->join('tipocontrato tc', 'tc.TipoContratoID = r.TipoContratoID');
        $this->db->join('convocatoria c', 'c.RequisicionID = r.RequisicionID');
        $this->db->where(['r.RequisicionID' => $idReq]);

        $query = $this->db->get();
        return $query->result();
    }

    public function getListaConvocatorias() {
        $this->db->select('p.*, c.*, c.EstadoID AS status, tc.*, r.*, pf.*, DATEDIFF(r.FechaFinLabores, r.FechaInicioLabores) AS DuracionContrato, m.Descripcion AS DescripcionMotivo');
        $this->db->from('requisicion r');
        $this->db->join('perfil pf', 'pf.PerfilID = r.PerfilID');
        $this->db->join('proyecto p', 'p.ProyectoID = pf.ProyectoID');
        $this->db->join('motivo m', 'm.MotivoID = r.MotivoID');
        $this->db->join('tipocontrato tc', 'tc.TipoContratoID = r.TipoContratoID');
        $this->db->join('convocatoria c', 'c.RequisicionID = r.RequisicionID');
        $query = $this->db->get();
        return $query->result();
    }

    public function getConvocatoria($id) {
        $this->db->select('c.ConvocatoriaID, tc.Nombre AS NombreTipoContrato, r.RequisicionID, r.TipoContratoID, r.DatosUsuarioSolitanteID, r.FechaSolicitud, r.CantidadPersonasSolicitadas, pf.MisionCargo, r.Justificacion, r.SalarioAsignado, r.NombreCargo, r.Observaciones, r.FechaInicioLabores, r.FechaFinLabores, DATEDIFF(r.FechaFinLabores, r.FechaInicioLabores) AS DuracionContrato, r.MotivoID, m.Descripcion AS DescripcionMotivo, r.PerfilID, r.EstadoID');
        $this->db->from('requisicion r');
        $this->db->join('perfil pf', 'pf.PerfilID = r.PerfilID');
        $this->db->join('proyecto p', 'p.ProyectoID = pf.ProyectoID');
        $this->db->join('motivo m', 'm.MotivoID = r.MotivoID');
        $this->db->join('tipocontrato tc', 'tc.TipoContratoID = r.TipoContratoID');
        $this->db->join('convocatoria c', 'c.RequisicionID = r.RequisicionID');
        $this->db->where(['c.ConvocatoriaID' => $id]);

        $query = $this->db->get();
        return $query->row();
    }
    
    public function getConvocatoriaEtapa4($id) {
        $this->db->select('exc.*,c.ConvocatoriaID, tc.Nombre AS NombreTipoContrato, r.RequisicionID, r.TipoContratoID, r.DatosUsuarioSolitanteID, r.FechaSolicitud, r.CantidadPersonasSolicitadas, pf.MisionCargo, r.Justificacion, r.SalarioAsignado, r.NombreCargo, r.Observaciones, r.FechaInicioLabores, r.FechaFinLabores, DATEDIFF(r.FechaFinLabores, r.FechaInicioLabores) AS DuracionContrato, r.MotivoID, m.Descripcion AS DescripcionMotivo, r.PerfilID, r.EstadoID');
        $this->db->from('requisicion r');
        $this->db->join('perfil pf', 'pf.PerfilID = r.PerfilID');
        $this->db->join('proyecto p', 'p.ProyectoID = pf.ProyectoID');
        $this->db->join('motivo m', 'm.MotivoID = r.MotivoID');
        $this->db->join('tipocontrato tc', 'tc.TipoContratoID = r.TipoContratoID');
        $this->db->join('convocatoria c', 'c.RequisicionID = r.RequisicionID');
        $this->db->join('etapa_x_convocatoria exc', 'exc.ConvocatoriaID = c.ConvocatoriaID');
        $this->db->where(['c.ConvocatoriaID' => $id,'exc.EtapaID' => 4]);

        $query = $this->db->get();
        return $query->row();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

}
