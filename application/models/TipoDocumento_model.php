<?php
class TipoDocumento_model extends CI_Model {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function get_active_documenttype() {
        $query = $this->db->get('tipodocumento');
        return $query->result();
    }

}
?>