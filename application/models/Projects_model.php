<?php

/**
 * Created by PhpStorm.
 * User: nancy rincones
 * Date: 13/04/2016
 * Time: 23:12
 */
class Projects_model extends CI_Model {

    private $table = "proyecto";
    private $id = "ProyectoID";

    public function __construct() {
        parent::__construct();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function getProjects() {
        $query = $this->db->get_where($this->table, ['EstadoID' => 0]);
        return $query->result();
    }

    public function getProyectosAprueba() {
        $query = $this->db->get_where($this->table, ['ApruebaRequisicion' => 1]);
        return $query->result();
    }

    public function getProjectsByTipo($id) {
        $query = $this->db->get_where($this->table, ['TipoProyectoID' => $id]);
        return $query->result();
    }

    public function getAllProjects() {
        $query = $this->db->get_where($this->table, ['PadreID >' => 0]);
        return $query->result();
    }

    public function get($id = FALSE, $type = FALSE, $status = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        if ($type && $status) {
            $query = $this->db->get_where($this->table, ['TipoProyectoID' => $type, 'EstadoID' => $status]);
            return $query->result();
        }
        if ($type && !$status) {
            $query = $this->db->get_where($this->table, ['TipoProyectoID' => $type]);
            return $query->result();
        }
        if (!$status) {
            $query = $this->db->get($this->table);
            return $query->result();
        }

        $query = $this->db->get_where($this->table, ['EstadoID' => $status]);
        return $query->result();
    }

    public function get_pagination($inicio = FALSE, $limite = FALSE) {
        if ($inicio !== FALSE && $limite !== FALSE) {
            $this->db->limit($limite, $inicio);
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

}
