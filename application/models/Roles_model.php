<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Roles_model
 *
 * @author furbox
 */
class Roles_model extends CI_Model {

    //put your code here
    private $table = "rol";
    private $id = "RolID";

    public function __construct() {
        parent::__construct();
    }

    public function get_active_roles() {
        $this->db->where('EstadoID', 1);
        $query = $this->db->get('rol');

        return $query->result();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }
    
    public function get_pagination($inicio = FALSE, $limite = FALSE){        
        if($inicio !== FALSE && $limite !== FALSE){
            $this->db->limit($limite,$inicio);
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getByIdAllModules($id) {
        $this->db->select('rol.*, m.ModuloID as moduloid, m.Nombre as module_name, m.modulo, m.submodulo ,pm.RolID as id_rol,pm.ModuloID as id_modulo,pm.EstadoID as status');
        $this->db->from($this->table);
        $this->db->join('permisosmodulo pm', 'pm.RolID = rol.RolID');
        $this->db->join('modulo m','m.ModuloID = pm.ModuloID');        
        $this->db->where('rol.RolID', $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

}
