<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Modules_model
 *
 * @author furbox
 */
class Modules_model extends CI_Model{
   //put your code here
    private $table = "modulo";
    private $id = "ModuloID";

    public function __construct() {
        parent::__construct();
    }
    
    public function getModules(){
        $this->db->select('modulo');
        $this->db->distinct();
        $query = $this->db->get($this->table);
        return $query->result();
    }
    
    public function getBySub($sub){
        $query = $this->db->get_where($this->table, ['submodulo' => $sub]);
        return $query->row();
    }

    public function get_active_roles() {
        $this->db->where('EstadoID', 1);
        $query = $this->db->get($this->table);

        return $query->result();
    }
    
    public function get_pagination($inicio = FALSE, $limite = FALSE){        
        if($inicio !== FALSE && $limite !== FALSE){
            $this->db->limit($limite,$inicio);
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $this->db->order_by('modulo');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }
    
}
