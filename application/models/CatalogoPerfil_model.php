<?php

class CatalogoPerfil_model extends CI_Model {

    private $table = "catalogoperfil";
    private $id = "CatalogoPerfilID";

    public function __construct() {
        parent::__construct();
    }

    public function getByCatalogoPerfilID($id = FALSE) {
        $this->db->select();
        $this->db->from($this->table . ' cp');
        if ($id == 1) {
            $this->db->join('grados g', 'g.grado_id = cp.tipo_grado_id');
        }
        $this->db->where(['cp.TipoCatalogoID' => $id]);
        $query = $this->db->get();
        return $query->result();
    }

    public function getNivelesEducativosByGrado($grado_id = FALSE) {
        if ($grado_id) {
            $this->db->select();
            $this->db->from($this->table);
            $this->db->where(['tipo_grado_id' => $grado_id]);
            $query = $this->db->get();
            return $query->result();
        }
    }

}
