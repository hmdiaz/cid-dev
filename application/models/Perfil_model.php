<?php

class Perfil_model extends CI_Model {

//put your code here
    private $table = "perfil";
    private $id = "PerfilID";

    public function __construct() {
        parent::__construct();
    }

    function getPerfilesByProyectoID($id) {
        $query = $this->db->get_where($this->table, ['ProyectoID' => $id]);
        return $query->result();
    }

    public function getAllProfiles() {
        $this->db->select();
        $this->db->from($this->table.' pe');
        $this->db->join('proyecto pro','pro.ProyectoID = pe.ProyectoID');
        $this->db->order_by('pro.ProyectoID','asc');
        $query = $this->db->get();
        return $query->result();
        
    }

    function getItemsCatalogoPerfil($id) {
        $this->db->select('cc.ComponenteCompetenciaID, cc.CatalogoPerfilID, cp.tipo_grado_id, cp.Descripcion, cp.TipoCatalogoID, cc.Peso, cc.TiempoExperiencia');
        $this->db->from('catalogoperfil cp');
        // $this->db->join('grados g', 'g.grado_id = cp.grado_id');
        $this->db->join('componentescompetencias cc', 'cc.CatalogoPerfilID = cp.CatalogoPerfilID');
        $this->db->where('cc.PerfilID', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getResponsabilidadesPerfil($id) {
        //$query = $this->db->get_where('responsabilidad', ['PerfilID' => $id]);
        //return $query->result();
    }

    function getPerfil($id) {
        $query = $this->db->get_where($this->table, ['PerfilID' => $id]);
        return $query->row();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function get_pagination($id, $inicio = FALSE, $limite = FALSE) {
        if ($inicio !== FALSE && $limite !== FALSE) {
            $this->db->limit($limite, $inicio);
            $this->db->where(['ProyectoID' => $id]);
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getByProyecto($ProyectoID, $status = FALSE) {
        $query = $this->db->get_where($this->table, ['ProyectoID' => $ProyectoID, 'EstadoID' => $status]);
        return $query->result();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function get($id = FALSE, $status = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        if (!$status) {
            $query = $this->db->get($this->table);
            return $query->result();
        }
        $query = $this->db->get_where($this->table, ['EstadoID' => $status]);
        return $query->result();
    }

}
