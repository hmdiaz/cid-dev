<?php
class PoblacionProyecto_model extends CI_Model
{
    //put your code here
    private $table = "poblacion_x_proyecto";
    private $id = "PoblacionProyectoID";

    public function __construct() {
        parent::__construct();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function getPoblacionesProyecto($id)
    {
        $this->db->select('p.ProyectoID, pp.Cobertura, pb.PoblacionID, pb.Nombre');
        $this->db->from('proyecto p');
        $this->db->join('poblacion_x_proyecto pp', 'p.ProyectoID = pp.ProyectoID');
        $this->db->join('poblacion pb', 'pb.PoblacionID = pp.PoblacionID');
        $this->db->where('p.ProyectoID', $id);
        $query = $this->db->get();
        return $query->result();
    }
}