<?php

class EtapaConvocatoria_model extends CI_Model {

    private $table = "etapa_x_convocatoria";
    private $id = "EtapaConvocatoriaID";

    public function __construct() {
        parent::__construct();
    }

    public function getByIdConvocatoriaAndIdEtapa($id_etapa,$id_convocatoria) {
        $query = $this->db->get_where($this->table, ['EtapaID' => $id_etapa, 'ConvocatoriaID'=>$id_convocatoria]);
        return $query->row();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function getEtapas_x_Convocatoria($id) {
        $this->db->select('e.Orden,e.EtapaID, e.Nombre, e.Descripcion, ec.EtapaConvocatoriaID, ec.ConvocatoriaID, ec.FechaInicio, ec.FechaFin');
        $this->db->from('etapa e');
        $this->db->join('etapa_x_convocatoria ec', 'e.EtapaID = ec.EtapaID AND ec.ConvocatoriaID = ' . $id . '', 'left');
        $this->db->order_by('e.Orden','asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function getEtapas_Existentes_x_Convocatoria($id) {
        $this->db->select('e.EtapaID, e.Nombre, e.Descripcion, ec.EtapaConvocatoriaID, ec.ConvocatoriaID, ec.FechaInicio, ec.FechaFin');
        $this->db->from('etapa e');
        $this->db->join('etapa_x_convocatoria ec', 'e.EtapaID = ec.EtapaID');
        $this->db->where(['ec.ConvocatoriaID' => $id]);
        $query = $this->db->get();
        return $query->result();
    }

    public function postulantes_finales($id) {
        $this->db->select();
        $this->db->from('etapa_x_convocatoria ec');
        $this->db->join('etapa e', 'e.EtapaID = ec.EtapaID');
        $this->db->join('pruebasseleccion ps', 'ps.EtapaConvocatoriaID = ec.EtapaConvocatoriaID');
        $this->db->join('pruebaseleccionresult psr', 'psr.pruebasseleccion_id = ps.pruebasseleccion_id');
        $this->db->join('usuario u', 'u.UsuarioID = ps.UsuarioID');
        $this->db->join('datosusuario du', 'du.UsuarioID = u.UsuarioID');
        $this->db->where(['ec.ConvocatoriaID' => $id]);
        $query = $this->db->get();
        return $query->result();
    }

}
