<?php
/**
 * Created by PhpStorm.
 * User: Mario Díaz
 * Date: 10/04/2016
 * Time: 1:13 PM
 */

class EstadoCivil_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function get_active_estado_civil() {
        $query = $this->db->get('estadocivil');
        return $query->result();
    }
}