<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HvExpLabExpGral_model
 *
 * @author furbox
 */
class HvExpLabExpGral_model extends CI_Model {

    //put your code here
    private $table = "HvExpLabExpGral";
    private $id = "HvExpLabID";

    public function __construct() {
        parent::__construct();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }
    public function deleteActivity($id,$id2) {
        $this->db->delete($this->table, array($this->id => $id,'CatalogoPerfilID'=> $id2));
        return $this->db->affected_rows();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->result();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }
    
    public function getByIDUser($id) {
        $query = $this->db->get_where($this->table, [$this->id => $id,'UsuarioID' => $_SESSION['id_user']]);
        return $query->row();
    }

    public function getByUser($id) {
        $this->db->select();
        $this->db->from($this->table.' eg');
        $this->db->join('catalogoperfil cp','cp.CatalogoPerfilID = eg.CatalogoPerfilID');
        $this->db->where(['HvExpLabID' => $id]);
        $query = $this->db->get();
        return $query->result();
    }
    public function getActivity($id, $id2) {
        $query = $this->db->get_where($this->table, ['HvExpLabID' => $id, 'CatalogoPerfilID' => $id2]);
        return $query->row();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->affected_rows();
    }

    public function update($id,$id2, $data) {
        $this->db->update($this->table, $data,[$this->id=>$id,'CatalogoPerfilID' => $id2]);
        return $this->db->affected_rows();
    }

}
