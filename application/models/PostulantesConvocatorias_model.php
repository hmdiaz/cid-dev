<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PostulantesConvocatorias_model
 *
 * @author furbox
 */
class PostulantesConvocatorias_model extends CI_Model {

    //put your code here
    private $table = "convocariasPostulantes";
    private $id = "convocariasPostulantesID";

    public function __construct() {
        parent::__construct();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }
    
    public function getPostulantesbyConvocatoria($id_convocartoria){
        $this->db->select();
        $this->db->from($this->table);
        $this->db->join('usuario u','u.UsuarioID = convocariasPostulantes.PostulanteID');
        $this->db->join('datosusuario du' ,'u.UsuarioID = du.UsuarioID');
        $this->db->where('convocatoriaID', $id_convocartoria);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getConvocatoriasPostuladas($id_user){
        $this->db->select();
        $this->db->from($this->table);
        $this->db->join('usuario u','u.UsuarioID = convocariasPostulantes.PostulanteID');
        $this->db->join('datosusuario du' ,'u.UsuarioID = du.UsuarioID');
        $this->db->where('PostulanteID', $id_user);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getPostulada($id_user,$id_convocatoria){
        $query = $this->db->get_where($this->table,['PostulanteID' => $id_user, 'convocatoriaID' => $id_convocatoria]);
        return $query->row();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

}
