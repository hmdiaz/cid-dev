<?php
class SocioCooperanteProyecto_model extends CI_Model
{
    //put your code here
    private $table = "sociocooperante_x_proyecto";
    private $id = "SocioProyectoID";

    public function __construct() {
        parent::__construct();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function get_pagination($inicio = FALSE, $limite = FALSE){
        if($inicio !== FALSE && $limite !== FALSE){
            $this->db->limit($limite,$inicio);
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function getSociosCooperantesProyecto($id)
    {
        $this->db->select('p.ProyectoID, sc.SocioCooperanteID, sp.SocioProyectoID, sc.Nombre');
        $this->db->from('proyecto p');
        $this->db->join('sociocooperante_x_proyecto sp', 'p.ProyectoID = sp.ProyectoID');
        $this->db->join('sociocooperante sc', 'sc.SocioCooperanteID = sp.SocioCooperanteID');
        $this->db->where('p.ProyectoID', $id);
        $query = $this->db->get();
        return $query->result();
    }
}