<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Preguntas_model
 *
 * @author furbox
 */
class Preguntas_model extends CI_Model {

    //put your code here
    private $table = "preguntas";
    private $id = "pregunta_id";

    public function __construct() {
        parent::__construct();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function deleteTestQuestions($id_test) {
        $this->db->delete('pruebaspreguntas', array('prueba_id' => $id_test));
        return $this->db->affected_rows();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $this->db->select();
        $this->db->from($this->table . ' as p');
        $this->db->join('CategoriaPerfil as cp', 'cp.CategoriaPerfilID = p.CategoriaPerfilID');
        $query = $this->db->get();
        return $query->result();
    }

    public function getPreguntas($prueba_id) {
        $this->db->select();
        $this->db->from('pruebaspreguntas' . ' as pp');
        $this->db->join('preguntas as p', 'p.pregunta_id = pp.pregunta_id');
        $this->db->where(['prueba_id' => $prueba_id]);
        $query = $this->db->get();
        return $query->result();
    }

    public function getPreguntasByCat($cat) {
        $query = $this->db->get_where($this->table, ['CategoriaPerfilID' => $cat]);
        return $query->result();
    }

    public function checkQuestion($id_test, $id_preg) {
        $query = $this->db->get_where('pruebaspreguntas', ['prueba_id' => $id_test, 'pregunta_id' => $id_preg]);
        return $query->row();
    }

    public function getPreguntasPrueba($prueba_id) {
        $this->db->select();
        $this->db->from('pruebaspreguntas pp');
        $this->db->join('preguntas p', 'p.pregunta_id = pp.pregunta_id');
        $this->db->where(['pp.prueba_id' => $prueba_id]);
        $query = $this->db->get();
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function insertTestQuestion($data) {
        $this->db->insert('pruebaspreguntas', $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

}
