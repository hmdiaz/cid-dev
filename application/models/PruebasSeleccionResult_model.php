<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PruebasSeleccionResult
 *
 * @author furbox
 */
class PruebasSeleccionResult_model extends CI_Model {

    //put your code here
    private $table = "pruebaseleccionresult";
    private $id = "pruebaseleccionresult_id";

    public function __construct() {
        parent::__construct();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function getResultadosEtapa2($id_convocatoria) {
        $this->db->select();
        $this->db->from($this->table . " psr");
        $this->db->join('pruebasseleccion ps', 'ps.pruebasseleccion_id = psr.pruebasseleccion_id');
        $this->db->join('usuario u', 'u.UsuarioID = ps.UsuarioID');
        $this->db->join('datosusuario du', 'du.UsuarioID = u.UsuarioID');
        $this->db->join('etapa_x_convocatoria exc', 'exc.EtapaConvocatoriaID = ps.EtapaConvocatoriaID');
        $this->db->where(['exc.EtapaID' => 2,'exc.ConvocatoriaID' => $id_convocatoria]);
        $query = $this->db->get();
        return $query->result();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getbyPruebaSeleccionID($id) {
        $query = $this->db->get_where($this->table, ['pruebasseleccion_id' => $id]);
        return $query->row();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

}
