<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HvDescHab
 *
 * @author furbox
 */
class HvDescHab_model extends CI_Model {

    //put your code here
    private $table = "HvHab";
    private $id = "HvHab_id";

    public function __construct() {
        parent::__construct();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getByIDUser($id) {
        $query = $this->db->get_where($this->table, [$this->id => $id,'UsuarioID' => $_SESSION['id_user']]);
        return $query->row();
    }

    public function getByUser($id) {
        $this->db->select();
        $this->db->from($this->table . ' dh');
        $this->db->join('catalogoperfil cp', 'cp.CatalogoPerfilID = dh.CatalogoPerfilID');
        $this->db->where('dh.UsuarioID = ' . $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function getDescHabByUser($id) {
        $this->db->select("dh.CatalogoPerfilID");
        $this->db->from($this->table . ' dh');
        $this->db->join('catalogoperfil cp', 'cp.CatalogoPerfilID = dh.CatalogoPerfilID');
        $this->db->where('dh.UsuarioID = ' . $id);
        $this->db->group_by("dh.CatalogoPerfilID");
        $query = $this->db->get();
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

}
