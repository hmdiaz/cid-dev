<?php
/**
 * Created by PhpStorm.
 * User: Mario Diaz
 * Date: 10/04/2016
 * Time: 11:32 AM
 */

class Departamento_model extends CI_Model {

    //put your code here
    private $table = "departamento";
    private $id = "DepartamentoID";

    public function __construct() {
        parent::__construct();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }
    
    public function get_active_departments() {
        $query = $this->db->get('departamento');
        return $query->result();
    }

}