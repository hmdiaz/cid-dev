<?php

class DatosUsuario_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get($id = FALSE) {
        if ($id) {

            $this->db->select('u.FotoUsuario,
                               u.Clave,u.Salt,
                               u.UsuarioID,
                               u.EmailUsuario,
                               e.Nombre as NombreStatus,
                               du.TipoDocumentoID,
                               du.DatosUsuarioID,
                               du.DocumentoIdentidad,
                               du.MunIDExpDocumento,
                               du.Apellidos,
                               du.Nombres,
                               du.FechaNacimiento,
                               du.Telefono,
                               du.Celular,
                               du.Direccion,
                               du.MunIDResidencia,
                               du.NumeroTarjetaProfesional,
                               du.FechaExpTarjetaProfesional,
                               du.CargoID,
                               du.Genero,
                               du.EstadoCivilID,
                               du.MunIDExpDocumento,
                               mexp.DepartamentoID AS DepIdExpedicion,
                               du.MunIDResidencia,
                               mres.DepartamentoID AS DepIdResidencia');
            $this->db->from('usuario u');
            $this->db->join('datosusuario du', 'du.UsuarioID = u.UsuarioID');
            $this->db->join('estado e', 'e.EstadoID = u.EstadoID');
            $this->db->join('municipio mexp', 'mexp.MunicipioID = du.MunIDExpDocumento', 'left');
            $this->db->join('municipio mres', 'mres.MunicipioID = du.MunIDResidencia', 'left');
            $this->db->join('departamento dexp', 'dexp.DepartamentoID = mexp.DepartamentoID', 'left');
            $this->db->join('departamento dres', 'dres.DepartamentoID = mres.DepartamentoID', 'left');
            $this->db->where('u.UsuarioID', $id);
            $query = $this->db->get();
            return $query->row();

            $query = $this->db->get_where('datosusuario', ['UsuarioID' => $id]);
            return $query->row();
        }
        $query = $this->db->get('datosusuario');
        return $query->result();
    }

    public function getByUsuarioID($id = FALSE) {
        $query = $this->db->get_where('datosusuario', ['UsuarioID' => $id]);
        return $query->row();
    }

    public function insert($data) {
        $this->db->insert('datosusuario', $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where('UsuarioID', $id);
        $this->db->update('datosusuario', $data);
        return $this->db->affected_rows();
    }
}