<?php
class TipoResponsabilidad_model extends CI_Model
{
    private $table = "tiporesponsabilidad";
    private $id = "TipoResponsabilidadID";

    public function __construct() {
        parent::__construct();
    }

    public function get($id = FALSE, $status = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        if(!$status)
        {
            $query = $this->db->get($this->table);
            return $query->result();
        }
        $query = $this->db->get_where($this->table, ['EstadoID' => $status]);
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }
}