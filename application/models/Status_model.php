<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Status_model
 *
 * @author furbox
 */
class Status_model extends CI_Model {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function get_active_status() {
        $query = $this->db->get_where('estado',['Grupo' => 1]);
        return $query->result();
    }

}
