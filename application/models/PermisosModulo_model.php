<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PermisosModulo_model
 *
 * @author furbox
 */
class PermisosModulo_model extends CI_Model {

    //put your code here
    private $table = "permisosmodulo";
    private $id = "RolID";

    public function __construct() {
        parent::__construct();
    }

    public function getByIdRol($id) {
        $this->db->where($this->id, $id);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getwhere($idrol, $idmodule) {
        $query = $this->db->get_where($this->table, ["RolID" => $idrol, "ModuloID" => $idmodule]);
        return $query->row();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data) {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

}
