<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PruebasSeleccion_model
 *
 * @author furbox
 */
class PruebasSeleccion_model extends CI_Model {

    //put your code here
    private $table = "pruebasseleccion";
    private $id = "pruebasseleccion_id";

    public function __construct() {
        parent::__construct();
    }

    public function get($id = FALSE) {
        if ($id) {
            $this->db->select();
            $this->db->from($this->table . " ps");
            $this->db->join('etapa_x_convocatoria exc', 'exc.EtapaConvocatoriaID = ps.EtapaConvocatoriaID');
            $this->db->join('pruebasconfig pc', 'pc.EtapaConvocatoriaID = ps.EtapaConvocatoriaID');
//            $this->db->join('');
//            $this->db->join('');
            $this->db->where(['ps.' . $this->id => $id]);
            $query = $this->db->get();
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }
    
    public function getByUser($id,$user_id) {
        if ($id) {
            $this->db->select();
            $this->db->from($this->table . " ps");
            $this->db->join('etapa_x_convocatoria exc', 'exc.EtapaConvocatoriaID = ps.EtapaConvocatoriaID');
            $this->db->join('pruebasconfig pc', 'pc.EtapaConvocatoriaID = ps.EtapaConvocatoriaID');
            $this->db->where(['ps.' . $this->id => $id,'ps.UsuarioID' => $user_id]);
            $query = $this->db->get();
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getEtapa($pruebasseleccion_id) {
        $this->db->select();
        $this->db->from($this->table . " ps");
        $this->db->join('etapa_x_convocatoria exc','exc.EtapaConvocatoriaID = ps.EtapaConvocatoriaID');
        $this->db->where(['ps.' . $this->id => $pruebasseleccion_id]);
        $query = $this->db->get();
        return $query->row();
    }

    public function getbyEtapaConvcatoria($EtapaConvocatoriaID) {
        $query = $this->db->get_where($this->table, ['EtapaConvocatoriaID' => $EtapaConvocatoriaID]);
        return $query->result();
    }

    public function getbyUsuariosEtapaConvcatoria($EtapaConvocatoriaID) {
        $this->db->select();
        $this->db->from($this->table . " ps");
//        $this->db->join('pruebaseleccionresult psr','psr.pruebasseleccion_id = ps.pruebasseleccion_id');
        $this->db->join('usuario u', 'u.UsuarioID = ps.UsuarioID');
        $this->db->join('datosusuario du', 'du.UsuarioID = u.UsuarioID');
        $this->db->where(['EtapaConvocatoriaID' => $EtapaConvocatoriaID]);
        $query = $this->db->get();
        return $query->result();
    }

    public function getbyTestEtapaConvcatoria($EtapaConvocatoriaID) {
        $this->db->select();
        $this->db->from($this->table . " ps");
        $this->db->join('pruebaseleccionresult psr', 'psr.pruebasseleccion_id = ps.pruebasseleccion_id');
        $this->db->join('usuario u', 'u.UsuarioID = ps.UsuarioID');
        $this->db->join('datosusuario du', 'du.UsuarioID = u.UsuarioID');
        $this->db->where(['EtapaConvocatoriaID' => $EtapaConvocatoriaID]);
        $query = $this->db->get();
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

}
