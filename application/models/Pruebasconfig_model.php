<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PruebasConfig_model
 *
 * @author furbox
 */
class Pruebasconfig_model extends CI_Model {

    //put your code here
    private $table = "pruebasconfig";
    private $id = "pruebasconfig_id";

    public function __construct() {
        parent::__construct();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }
    
    public function getPruebaByConvocatoria($EtapaConvocatoriaID){
        $this->db->select();
        $this->db->from($this->table . " pc");
        $this->db->join('pruebas p','p.prueba_id = pc.prueba_id');
        $this->db->join('pruebaspreguntas pp','pp.prueba_id = pc.prueba_id');
        $this->db->join('preguntas pre','pre.pregunta_id = pp.pregunta_id');
        $this->db->where(['pc.EtapaConvocatoriaID' => $EtapaConvocatoriaID]);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getConfigPruebaByConvocatoria($EtapaConvocatoriaID){
        $this->db->select();
        $this->db->from($this->table . " pc");
        $this->db->join('pruebas p','p.prueba_id = pc.prueba_id');
        $this->db->join('etapa_x_convocatoria exc','exc.EtapaConvocatoriaID = pc.EtapaConvocatoriaID');
        $this->db->where(['pc.EtapaConvocatoriaID' => $EtapaConvocatoriaID]);
        $query = $this->db->get();
        return $query->row();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

}
