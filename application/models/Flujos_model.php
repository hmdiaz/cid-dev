<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Flujos_model
 *
 * @author furbox
 */
class Flujos_model extends CI_Model {

    //put your code here
    private $table = "flujos";
    private $id = "flujo_id";

    public function __construct() {
        parent::__construct();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function getFlujo($tipo) {
        $this->db->select();
        $this->db->from($this->table);
        $this->db->where(['flujo_tipo' => $tipo]);
        $this->db->order_by('flujo_orden','ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function get($id = FALSE,$tipo = FALSE) {
        if ($id) {
            
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        $this->db->select();
        $this->db->from($this->table.' f');
        $this->db->join('cargos c','c.CargoID = f.flujo_id_cargo');
        $this->db->where(['f.flujo_tipo' => $tipo]);
        $this->db->order_by('f.flujo_orden','asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

}
