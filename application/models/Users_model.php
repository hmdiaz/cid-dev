<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: furbox
 * Date: 3/04/16
 * Time: 02:00 PM
 */
class Users_model extends CI_Model {

    /**
     * user_model constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getUserByCode($code) {
        $query = $this->db->get_where('usuario', ['CodigoActivacion' => $code]);
        return $query->row();
    }

    public function getByEstatus() {
        $query = $this->db->get_where('usuario', ['RolID' => 6]);
        return $query->result();
    }

    public function getUsuarioPostulante($id) {
        $query = $this->db->get_where('usuario', ['UsuarioID' => $id]);
        return $query->result();
    }

    public function get($id = FALSE) {
        if ($id) {
            $query = $this->db->get_where('usuario', ['UsuarioID' => $id]);
            return $query->row();
        }
        $query = $this->db->get('usuario');
        return $query->result();
    }

    public function getUserByDatosUsuarioID($datos_user_id) {
        $this->db->select();
        $this->db->from('usuario u');
        $this->db->join('datosusuario du', 'du.UsuarioID = u.UsuarioID');
        $this->db->where('du.DatosUsuarioID', $datos_user_id);
        $query = $this->db->get();
        return $query->row();
    }

    public function getInternos($inicio = FALSE, $limite = FALSE) {
        $this->db->select('usuario.FotoUsuario,usuario.EstadoID,usuario.Clave,usuario.Salt,usuario.UsuarioID,usuario.EmailUsuario,datosusuario.Apellidos,,datosusuario.Nombres,estado.Nombre as NombreStatus,rol.Nombre as NombreRol');
        $this->db->from('usuario');
        $this->db->join('rol', 'rol.RolID = usuario.RolID');
        $this->db->join('estado', 'estado.EstadoID = usuario.EstadoID');
        $this->db->join('datosusuario', 'datosusuario.UsuarioID = usuario.UsuarioID');
        $this->db->where('usuario.RolID !=', 6);
        if ($inicio !== FALSE && $limite !== FALSE) {
            $this->db->limit($limite, $inicio);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function getUserByEmail($email) {
        $query = $this->db->get_where('usuario', ['EmailUsuario' => $email]);
        return $query->row();
    }

    public function getUserData($email) {
        $query = $this->db->get_where('usuario', ['EmailUsuario' => $email]);
        return $query->row();
    }

    public function getUserLogin($email) {
        $this->db->select('usuario.RolID,usuario.FotoUsuario,usuario.Clave,usuario.Salt,usuario.UsuarioID,usuario.EmailUsuario,datosusuario.Nombres,datosusuario.Apellidos,estado.Nombre as NombreStatus,rol.Nombre as NombreRol');
        $this->db->from('usuario');
        $this->db->join('rol', 'rol.RolID = usuario.RolID');
        $this->db->join('estado', 'estado.EstadoID = usuario.EstadoID');
        $this->db->join('datosusuario', 'datosusuario.UsuarioID = usuario.UsuarioID');
        $this->db->where('usuario.EmailUsuario', $email);
        $query = $this->db->get();
        return $query->row();
    }

    public function getUserByCargo($id_cargo) {
        $this->db->select();
        $this->db->from('usuario u');
        $this->db->join('datosusuario du', 'du.UsuarioID = u.UsuarioID');
        $this->db->join('cargos c', 'c.CargoID = du.CargoID');
        $this->db->where('c.CargoID', $id_cargo);
        $query = $this->db->get();
        return $query->result();
    }

    public function getUserInfo($id) {
        $this->db->select('u.*,du.*,td.*,ec.Nombre as NombreEstCivil,mun.Nombre as NombreMunDoc, munr.Nombre as NombreMunRes, dep.Nombre as NombreDepRes');
        $this->db->from('usuario u');
        $this->db->join('datosusuario du', 'du.UsuarioID = u.UsuarioID');
        $this->db->join('tipodocumento td', 'td.TipoDocumentoID = du.TipoDocumentoID');
        $this->db->join('municipio mun', 'mun.MunicipioID = du.MunIDExpDocumento');
        $this->db->join('municipio munr', 'munr.MunicipioID = du.MunIDResidencia');
        $this->db->join('departamento dep', 'dep.DepartamentoID = munr.DepartamentoID');
        $this->db->join('estadocivil ec', 'ec.EstadoCivilID = du.EstadoCivilID');
        $this->db->where('u.UsuarioID', $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function insert($data) {
        $this->db->insert('usuario', $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where('UsuarioID', $id);
        $this->db->update('usuario', $data);
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->delete('usuario', array('UsuarioID' => $id));
        return $this->db->affected_rows();
    }

}
