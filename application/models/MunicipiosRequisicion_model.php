<?php
class MunicipiosRequisicion_model extends CI_Model
{
    //put your code here
    private $table = "municipios_x_requisicion";
    private $id = "MunicipioRequisicionID";

    public function __construct() {
        parent::__construct();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function getMunicipiosRequisicion($id)
    {
        $this->db->select('mr.municipios_x_requisicion_vacantes,mr.MunicipioRequisicionID, m.MunicipioID, m.Nombre AS NombreMunicipio, d.Nombre AS NombreDepartamento');
        $this->db->from('requisicion r');
        $this->db->join('municipios_x_requisicion mr', 'r.RequisicionID = mr.RequisicionID');
        $this->db->join('municipio m', 'mr.MunicipioID = m.MunicipioID');
        $this->db->join('departamento d', 'd.DepartamentoID = m.DepartamentoID');
        $this->db->where('r.RequisicionID', $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function getGetDepartamentoPerfil($id)
    {
        $this->db->select('d.DepartamentoID, d.Nombre');
        $this->db->from('perfil p');
        $this->db->join('municipios_x_perfil mp', 'p.PerfilID = mp.PerfilID');
        $this->db->join('municipio m', 'mp.MunicipioID = m.MunicipioID');
        $this->db->join('departamento d', 'd.DepartamentoID = m.DepartamentoID');
        $this->db->where('p.PerfilID', $id);
        $this->db->distinct();
        $query = $this->db->get();
        return $query->result();
    }

    public function getMunicipiosDepartamentoPerfil($idPerfil, $idDepto){
        $this->db->select('m.MunicipioID, m.Nombre');
        $this->db->from('perfil p');
        $this->db->join('municipios_x_proyecto mp', 'p.ProyectoID = mp.ProyectoID');
        $this->db->join('municipio m', 'mp.MunicipioID = m.MunicipioID');
        $this->db->join('departamento d', 'd.DepartamentoID = m.DepartamentoID');
        $this->db->where(['p.PerfilID' => $idPerfil, 'd.DepartamentoID' => $idDepto]);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getSumaVacatesReqxPerfil($id_perfil,$id_municipio){
        $this->db->select('sum(mxr.municipios_x_requisicion_vacantes) AS suma');
        $this->db->from($this->table. ' mxr');
        $this->db->join('requisicion r', 'r.RequisicionID = mxr.RequisicionID');
        $this->db->join('perfil p', 'p.PerfilID = r.PerfilID');
        $this->db->where(['p.PerfilID' => $id_perfil, 'mxr.MunicipioID' => $id_municipio, 'r.EstadoID' => 1]);
        $query = $this->db->get();
        return $query->row();
    }
}