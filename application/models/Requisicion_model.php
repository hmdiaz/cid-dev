<?php

class Requisicion_model extends CI_Model {

    private $table = "requisicion";
    private $id = "RequisicionID";

    public function __construct() {
        parent::__construct();
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->id => $id));
        return $this->db->affected_rows();
    }

    public function getCantidadPersonasSocilitadas($id_perfil) {
        $this->db->select('sum(CantidadPersonasSolicitadas) as total');
        $this->db->from($this->table);
        $this->db->where(['PerfilID' => $id_perfil]);
        $query = $this->db->get();
        return $query->row();
    }

    public function getRequisicion($id) {
        $query = $this->db->get_where($this->table, [$this->id => $id]);
        return $query->row();
    }

    public function get($id = FALSE, $status = FALSE) {
        if ($id) {
            $query = $this->db->get_where($this->table, [$this->id => $id]);
            return $query->row();
        }
        if (!$status) {
            $query = $this->db->get($this->table);
            return $query->result();
        }
        $query = $this->db->get_where($this->table, ['EstadoID' => $status]);
        return $query->result();
    }

    public function getRequisiciones_x_Perfil($id, $status) {
        $query = $this->db->get_where($this->table, ['PerfilID' => $id, 'EstadoID' => $status]);
        return $query->result();
    }

    public function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function getAllRequisiciones() {
        $this->db->select();
        $this->db->from($this->table . ' r');
        $this->db->join('perfil pe', 'pe.PerfilID = r.PerfilID');
        $this->db->join('proyecto pro', 'pro.ProyectoID = pe.ProyectoID');
        $this->db->order_by('pro.ProyectoID', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

}
