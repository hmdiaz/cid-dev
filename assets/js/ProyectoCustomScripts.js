var baseUrl = $('#baseUrl').val();
var idProyecto = $("#id").val();
$(document).ready(function(){
    $('#delRowsMunicipiosProyectoEditProject').attr('disabled', 'disabled');
    $('#delRowsSociosCooperantesEditProject').attr('disabled', 'disabled');
    $('#delRowsPoblacionesEditProject').attr('disabled', 'disabled');
    $('#delRowsPoblacionNewProject').attr('disabled', 'disabled');
    $('#delRowsMunicipiosNewProject').attr('disabled', 'disabled');
    $('#delRowsSociosNewProject').attr('disabled', 'disabled');

    /*********************** Validaciones **********************/

    var form = $('#frmProyecto');
    var errorHandler1 = $('#errorContainer');

    errorHandler1.hide();
    
    form.validate({
        errorLabelContainer: "#errorContainer",
        wrapper: "span",
        errorElement: "span", // contain the error msg in a span tag
        errorClass: 'help-block',
        rules: {
            nombre_proyecto: {
                required: true,
                minlength: 2,
                maxlength: 255
            },
            codigo_contrato: {
                required: true,
                minlength: 2,
                maxlength: 255
            },
            identificacion_proyecto: {
                required: true,
                minlength: 2,
                maxlength: 255
            },
            fecha_inicio_contrato: {
                required: true
            },
            tiempo_ejecucion_efectiva: {
                number: true
            }
        },
        messages: {
            nombre_proyecto: {
                required: " - El nombre del proyecto es requerido <br />",
                minlength: "- El Nombre del Proyecto debe tener por lo menos dos (2) caracteres. <br />",
                maxlength: "- El Nombre del Proyecto debe tener maximo (255) caracteres. <br />"

            },
            codigo_contrato: {
                required: " - El Código de Contrato es requerido <br />",
                minlength: "- El Código de Contrato debe tener por lo menos dos (2) caracteres. <br />",
                maxlength: "- El Código de Contrato debe tener maximo (255) caracteres. <br />"
            },
            identificacion_proyecto: {
                required: " - La Identificación del proyecto es requerido <br />",
                minlength: "- El Identificación del proyecto debe tener por lo menos dos (2) caracteres. <br />",
                maxlength: "- El Identificación del proyecto debe tener maximo (125550) caracteres. <br />"
            },
            fecha_inicio_contrato: {
                required: "- La Fecha de Inicio de Contrato es requerida. <br />"
            },
            tiempo_ejecucion_efectiva: {
                number: "- El Tiempo de Ejecucion Efectiva debe ser numerico. <br />"
            }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit
            // successHandler1.hide();
            errorHandler1.show();
        },
        highlight: function (element, errorClass) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
        unhighlight: function (element, errorClass) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        },
        submitHandler: function () {
            $.blockUI({ message: $('#loadMessage') }); 
            document.getElementById("frmProyecto").submit();      
        }
    });

    /*********************** Validaciones **********************/
    var oTableProyecto = $('#sample-table-1').DataTable({
        "aoColumnDefs": [{
            "aTargets": [0],
            "visible" : false
        }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'desc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"]],
        "iDisplayLength": 5,
        "bFilter": true,
        "bLengthChange": true
    });
    var oTableMunicipiosProyecto = $('#tbl_municipios_proyecto').DataTable({
        "columns": [
            {"visible": false},
            null,
            null
        ],
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sLengthMenu": "Show _MENU_ Rows",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "bFilter": false,
        "bLengthChange": false,
        "iDisplayLength": 10
    });
    var oTableSociosCooperantes = $('#tbl_socios_cooperantes').DataTable({
        "columns": [
            {"visible": false},
            null
        ],
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sLengthMenu": "Show _MENU_ Rows",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "bFilter": false,
        "bLengthChange": false,
        "iDisplayLength": 10
    });
    var oTablePoblaciones = $('#tbl_poblaciones').DataTable({
        "columns": [
            {"visible": false},
            null,
            null,
            null,
            null,
            null
        ],
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sLengthMenu": "Show _MENU_ Rows",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "bFilter": false,
        "bLengthChange": false,
        "iDisplayLength": 10
    });

    $('#sample-table-1_wrapper .dataTables_length select').select2({
        theme: "classic"
    });
    $('#sample-table-1_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    $('#sample-table-1_wrapper .dataTables_length select').addClass("m-wrap small");

    $('#tbl_municipios_proyecto tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        if (oTableMunicipiosProyecto.row('.selected').length == 0)
        {
            $('#delRowsMunicipiosProyectoEditProject').attr('disabled', 'disabled');
            $('#delRowsMunicipiosNewProject').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsMunicipiosProyectoEditProject').removeAttr('disabled');
            $('#delRowsMunicipiosNewProject').removeAttr('disabled');
        }
    });

    $('#tbl_socios_cooperantes tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        if (oTableSociosCooperantes.row('.selected').length == 0)
        {
            $('#delRowsSociosCooperantesEditProject').attr('disabled', 'disabled');
            $('#delRowsSociosNewProject').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsSociosCooperantesEditProject').removeAttr('disabled');
            $('#delRowsSociosNewProject').removeAttr('disabled');
        }
    });

    $('#tbl_poblaciones tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        if (oTablePoblaciones.row('.selected').length == 0)
        {
            $('#delRowsPoblacionesEditProject').attr('disabled', 'disabled');
            $('#delRowsPoblacionNewProject').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsPoblacionesEditProject').removeAttr('disabled');
            $('#delRowsPoblacionNewProject').removeAttr('disabled');
        }
    });

    $('#addItemMunicipio').on('click', function(e){
        e.preventDefault();
        var municipioId = $('#municipio').val();
        var municipioText = $("#municipio").find("option:selected").text();
        var departamentoText = $("#departamento").find("option:selected").text();

        if(municipioId != '')
        {
            var existe = false;
            $('input[name^="hdItemsMunicipiosId"]').each(function () {
                if ($(this).val() == municipioId)
                    existe = true;
            });

            if (existe) {
                swal({
                    title: "Elemento existente",
                    text: "El elemento que intenta agregar ya existe en la lista!",
                    type: "warning",
                    confirmButtonColor: "#007AFF"
                });
            }
            else {
                var itemToAdd = "";
                itemToAdd += "<input type='hidden' id='hdItemsMunicipiosId' name='hdItemsMunicipiosId[]' value='" + municipioId + "' />";
                oTableMunicipiosProyecto.row.add([municipioId, departamentoText, municipioText + itemToAdd]).draw(false);
                swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
            }
        }
        else
        {
            swal("Requerido!", "Debe seleccionar un registro para agregar", "warning");
        }


    });

    $('#addItemSocioCooperante').on('click', function(e){
        e.preventDefault();
        var socio_cooperanteId = $('#socio_cooperante').val();
        var socio_cooperanteText = $("#socio_cooperante").find("option:selected").text();

        if(socio_cooperanteId != '')
        {
            var existe = false;
            $('input[name^="hdItemsSocioId"]').each(function () {
                if ($(this).val() == socio_cooperanteId)
                    existe = true;
            });

            if (existe) {
                swal({
                    title: "Elemento existente",
                    text: "El elemento que intenta agregar ya existe en la lista!",
                    type: "warning",
                    confirmButtonColor: "#007AFF"
                });
            }
            else {
                var itemToAdd = "";
                itemToAdd += "<input type='hidden' id='hdItemsSocioId' name='hdItemsSocioId[]' value='" + socio_cooperanteId + "' />";
                oTableSociosCooperantes.row.add([socio_cooperanteId, socio_cooperanteText + itemToAdd]).draw(false);
                swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
            }
        }
        else
        {
            swal("Requerido!", "Debe seleccionar un registro para agregar", "warning");
        }

    });

    $('#addItemPoblacion').on('click', function(e){
        e.preventDefault();
        var ubicacionId = $('#ubicacion').val();
        var ubicacionText = $("#ubicacion").find("option:selected").text();
        var poblacionId = $('#poblacion').val();
        var poblacionText = $("#poblacion").find("option:selected").text();
        var municipioId = $('#municipio').val();
        var municipioText = $("#municipio").find("option:selected").text();
        var departamentoText = $("#departamento").find("option:selected").text();
        var Cobertura = $("#cobertura").val();

        if(poblacionId != '')
        {
            if(isNumeric(Cobertura))
            {
                var existe = false;
                $('input[name^="hdItemsMunicipiosId"]').each(function () {
                    if ($(this).val() == municipioId)
                        existe = true;
                });

                if (existe) {
                    swal({
                        title: "Elemento existente",
                        text: "El elemento que intenta agregar ya existe en la lista!",
                        type: "warning",
                        confirmButtonColor: "#007AFF"
                    });
                }
                else {
                    var itemToAdd = "";
                    itemToAdd += "<input type='hidden' id='hdItemsPoblacionId' name='hdItemsPoblacionId[]' value='" + poblacionId + "' />";
                    itemToAdd += "<input type='hidden' id='hdItemsMunicipiosId' name='hdItemsMunicipiosId[]' value='" + municipioId + "' />";
                    itemToAdd += "<input type='hidden' id='hdItemsCoberturaId' name='hdItemsCoberturaId[]' value='" + Cobertura + "' />";
                    itemToAdd += "<input type='hidden' id='hdItemsUbicacionId' name='hdItemsUbicacionId[]' value='" + ubicacionId + "' />";
                    oTablePoblaciones.row.add([poblacionId, departamentoText, municipioText + itemToAdd, poblacionText, ubicacionText, Cobertura]).draw(false);
                    swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
                }
            }
            else {
                swal("Formato Incorrecto!", "Debe escribir un valor numérico correcto para Cobertura", "warning");
            }
        }
        else
        {
            swal("Requerido!", "Debe seleccionar un registro para agregar", "warning");
        }

    });

    $('#addItemMunicipioEditProject').on('click', function(e){
        e.preventDefault();
        var municipioId = $('#municipio').val();
        var municipioText = $("#municipio").find("option:selected").text();
        var departamentoText = $("#departamento").find("option:selected").text();

        if(municipioId != '')
        {
            var existe = false;
            $('input[name^="hdItemsMunicipiosId"]').each(function () {
                if ($(this).val() == municipioId)
                    existe = true;
            });

            if (existe) {
                swal({
                    title: "Elemento existente",
                    text: "El elemento que intenta agregar ya existe en la lista!",
                    type: "warning",
                    confirmButtonColor: "#007AFF"
                });
            }
            else {
                addItemMunicipioProyecto(municipioId, municipioText, departamentoText, oTableMunicipiosProyecto);
            }
        }
        else
        {
            swal("Requerido!", "Debe seleccionar un registro para agregar", "warning");
        }
    });

    $('#addItemSocioCooperanteEditProject').on('click', function(e){
        e.preventDefault();
        var socio_cooperanteId = $('#socio_cooperante').val();
        var socio_cooperanteText = $("#socio_cooperante").find("option:selected").text();

        if(socio_cooperanteId != '')
        {
            var existe = false;
            $('input[name^="hdItemsSocioId"]').each(function () {
                if ($(this).val() == socio_cooperanteId)
                    existe = true;
            });

            if (existe) {
                swal({
                    title: "Elemento existente",
                    text: "El elemento que intenta agregar ya existe en la lista!",
                    type: "warning",
                    confirmButtonColor: "#007AFF"
                });
            }
            else {
                addItemSocioCooperante(socio_cooperanteId, socio_cooperanteText, oTableSociosCooperantes);
            }
        }
        else
        {
            swal("Requerido!", "Debe seleccionar un registro para agregar", "warning");
        }

    });

    $('#addItemPoblacionEditProject').on('click', function(e){
        e.preventDefault();
        var ubicacionId = $('#ubicacion').val();
        var ubicacionText = $("#ubicacion").find("option:selected").text();
        var poblacionId = $('#poblacion').val();
        var poblacionText = $("#poblacion").find("option:selected").text();
        var municipioId = $('#municipio').val();
        var municipioText = $("#municipio").find("option:selected").text();
        var departamentoText = $("#departamento").find("option:selected").text();
        var Cobertura = $("#cobertura").val();

        if(poblacionId != '')
        {
            if(isNumeric(Cobertura))
            {
                var existe = false;
                $('input[name^="hdItemsMunicipiosId"]').each(function () {
                    if ($(this).val() == municipioId)
                        existe = true;
                });

                if (existe) {
                    swal({
                        title: "Elemento existente",
                        text: "El elemento que intenta agregar ya existe en la lista!",
                        type: "warning",
                        confirmButtonColor: "#007AFF"
                    });
                }
                else {
                    addItemPoblacion(poblacionId, departamentoText, municipioId, municipioText, poblacionText, ubicacionId, ubicacionText, Cobertura, oTablePoblaciones);
                }
            }
            else {
                swal("Formato Incorrecto!", "Debe escribir un valor numérico correcto para Cobertura", "warning");
            }
        }
        else
        {
            swal("Requerido!", "Debe seleccionar un registro para agregar", "warning");
        }

    });

    $('#delRowsMunicipiosProyectoEditProject').on('click', function () {
        var id = oTableMunicipiosProyecto.rows('.selected').data()[0][0];
        if (oTableMunicipiosProyecto.column(0).data().length == 0)
        {
            $('#delRowsMunicipiosProyectoEditProject').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsMunicipiosProyectoEditProject').removeAttr('disabled');
            deleteItem("delete-mun-pro", "MunicipioProyectoID", id, oTableMunicipiosProyecto)
        }
    });

    $('#delRowsSociosCooperantesEditProject').on('click', function () {
        var id = oTableSociosCooperantes.rows('.selected').data()[0][0];
        if (oTableSociosCooperantes.column(0).data().length == 0)
        {
            $('#delRowsSociosCooperantesEditProject').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsSociosCooperantesEditProject').removeAttr('disabled');
            deleteItem("delete-socio-coop-pro", "SocioProyectoID", id, oTableSociosCooperantes)
        }
    });

    $('#delRowsPoblacionesEditProject').on('click', function () {
        var id = oTablePoblaciones.rows('.selected').data()[0][0];
        if (oTablePoblaciones.column(0).data().length == 0)
        {
            $('#delRowsPoblacionesEditProject').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsPoblacionesEditProject').removeAttr('disabled');
            deleteItem("delete-poblacion-pro", "PoblacionProyectoID", id, oTablePoblaciones)
        }
    });

    $('#delRowsPoblacionNewProject').on('click', function(e){
        oTablePoblaciones.row('.selected').remove().draw(false);
        if (oTableRespCargo.column(0).data().length == 0)
            $('#delRowsPoblacionNewProject').attr('disabled', 'disabled');
        else
            $('#delRowsPoblacionNewProject').removeAttr('disabled');
    });

    $('#delRowsMunicipiosNewProject').on('click', function(e){
        oTableMunicipiosProyecto.row('.selected').remove().draw(false);
        if (oTableRespCargo.column(0).data().length == 0)
            $('#delRowsMunicipiosNewProject').attr('disabled', 'disabled');
        else
            $('#delRowsMunicipiosNewProject').removeAttr('disabled');
    });

    $('#delRowsSociosNewProject').on('click', function(e){
        oTableSociosCooperantes.row('.selected').remove().draw(false);
        if (oTableRespCargo.column(0).data().length == 0)
            $('#delRowsSociosNewProject').attr('disabled', 'disabled');
        else
            $('#delRowsSociosNewProject').removeAttr('disabled');
    });

    $('#area').on('change', function(e) {
        var id = $('#area').val();
        $.ajax({
            url: 'departamento-proyecto/' + id,
            type: "POST",
            success: function (response) {
                $('#departamento_new_profile').html(response);
            }
        })
    });

    $('#tipo_socio').on('change', function(e) {
        var id = $('#tipo_socio').val();
        $.ajax({
            url: baseUrl + 'get-socios-by-tipo/' + id,
            type: "POST",
            success: function (response) {
                $('#socio_cooperante').html(response);
            }
        })
    });



    $('#socio_cooperante').select2();
    $('#tipo_socio').select2();
    $('#departamento').select2();
    $('#municipio').select2();
    $('#poblacion').select2();
    $('#ubicacion').select2();
});

function deleteItem(url, campo, id, oTable) {
    swal({
        title: "Estás seguro?",
        text: "Vas a eliminar un registro que no podrás recuperar!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007AFF",
        confirmButtonText: "Si, borrar!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            url: baseUrl + "" + url,
            data: campo + "=" + id,
            type: "POST",
            success: function (response) {
                oTable.row('.selected').remove().draw(false);
                swal("Eliminado!", "Tu registro ha sido eliminado satisfactoriamente", "success");
            }
        })
    });
}

function addItemMunicipioProyecto(MunicipioID, NombreMunicipio, NombreDepartamento, oTable) {
    $.ajax({
        url: baseUrl + 'add-mun-pro',
        data: "MunicipioID=" + MunicipioID + "&ProyectoID=" + idProyecto,
        type: "POST",
        success: function (response) {
            var itemToAdd = "";
            itemToAdd += "<input type='hidden' id='hdItemsMunicipiosId' name='hdItemsMunicipiosId[]' value='" + MunicipioID + "' />";

            oTable.row.add([MunicipioID, NombreMunicipio + itemToAdd, NombreDepartamento]).draw(false);
            swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
        }
    })
}

function addItemSocioCooperante(SocioCooperanteID, NombreSocio, oTable) {
    $.ajax({
        url: baseUrl + 'add-socio-coop-pro',
        data: "SocioCooperanteID=" + SocioCooperanteID + "&ProyectoID=" + idProyecto,
        type: "POST",
        success: function (response) {
            var itemToAdd = "";
            itemToAdd += "<input type='hidden' id='hdItemsSocioId' name='hdItemsSocioId[]' value='" + SocioCooperanteID + "' />";

            oTable.row.add([SocioCooperanteID, NombreSocio + itemToAdd]).draw(false);
            swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
        }
    })
}

function addItemPoblacion(PoblacionID, NombreDepto, MunId, NombreMun, NombrePoblacion, UbicacionID, UbicacionDesc, Cobertura, oTable) {
    $.ajax({
        url: baseUrl + 'add-mun-pro',
        data: "PoblacionID=" + PoblacionID + "&ProyectoID=" + idProyecto + "&Cobertura=" + Cobertura + "&MunicipioID=" + MunId + "&UbicacionID=" + UbicacionID,
        type: "POST",
        success: function (response) {
            var itemToAdd = "";
            itemToAdd += "<input type='hidden' id='hdItemsMunicipiosId' name='hdItemsMunicipiosId[]' value='" + MunId + "' />";

            oTable.row.add([PoblacionID, NombreDepto, NombreMun + itemToAdd, NombrePoblacion, UbicacionDesc, Cobertura]).draw(false);
            swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
        }
    })
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}