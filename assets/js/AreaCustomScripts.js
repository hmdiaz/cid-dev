$(document).ready(function(){
    var oTable = $('#tbl_areas').DataTable({
        "columns": [
            {"width": "47%"},
            {"width": "28%"},
            {"width": "23%"},
        ],
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"]],
        "iDisplayLength": 5,
        "bFilter": true,
        "bLengthChange": true
    });

    $('#tbl_etapas_wrapper .dataTables_length select').select2({
        theme: "classic"
    });
});