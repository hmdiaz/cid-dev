$(document).ready(function () {
    var oMuns= $('#tbl_municipios_new_req1').DataTable({
        "columns": [
            null,
            null,
            {"": "5%"}
        ],
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"]],
        "iDisplayLength": 5,
        "bFilter": true,
        "bLengthChange": true
    });
    var oTablePendientes = $('#tbl_requisiciones_pendientes').DataTable({
        "columns": [
            null,
            null,
            {"width": "5%"},
            null,
            null
        ],
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"]],
        "iDisplayLength": 5,
        "bFilter": true,
        "bLengthChange": true
    });

    var oTableAceptadas = $('#tbl_requisiciones_aceptadas').DataTable({
        "columns": [
            null,
            null,
            {"width": "5%"},
            null,
            null
        ],
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"]],
        "iDisplayLength": 5,
        "bFilter": true,
        "bLengthChange": true
    });

    var oTableRechazadas = $('#tbl_requisiciones_rechazadas').DataTable({
        "columns": [
            null,
            null,
            {"width": "5%"},
            null,
            null
        ],
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"]],
        "iDisplayLength": 5,
        "bFilter": true,
        "bLengthChange": true
    });

    var oTableAprob = $('#tbl_aprob_requisiciones').DataTable({
        
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[3, 'desc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });
});