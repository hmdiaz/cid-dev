var idReq = $('#idReq').val();
var baseUrl = $('#baseUrl').val();
var PerfilID = $("#PerfilID").val();

$(document).ready(function () {
    /*$("#frm_new_req").validate({
     errorLabelContainer: "#messageBox",
     wrapper: "li",
     errorClass: "invalid",
     rules: {
     tipo_contrato: { required: true },
     fecha_inicio_labores: { required: true },
     justificacion: { required: true },
     salario_asignado: { required: true },
     motivos: { required: true },
     cant_solicitadas: { required: true }
     },
     messages: {
     tipo_contrato: { required: "El Tipo de Contrato es requerido" },
     fecha_inicio_labores: { required: "La Fecha de Inicio de Labores es requerida" },
     justificacion: { required: "La Justificación es requerida" },
     salario_asignado: { required: "El Salario Asignado es requerido" },
     motivos: { required: "El Motivo es requerido" },
     cant_solicitadas: { required: "La Cantidad de Personas Solicitadas es requerida" }
     },
     highlight: function (element) {
     $(element).closest('.help-block').removeClass('valid');
     // display OK icon
     $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
     // add the Bootstrap error class to the control group
     },
     unhighlight: function (element) { // revert the change done by hightlight
     $(element).closest('.form-group').removeClass('has-error');
     // set error class to the control group
     },
     success: function (label, element) {
     label.addClass('help-block valid');
     // mark the current input as valid and display OK icon
     $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
     },
     submitHandler: function(form) {
     $('#frm_new_req').submit();
     }
     });*/
    $('#delRowsMunicipiosEditReq').attr('disabled', 'disabled');
    $('#delRowsMunicipiosNewReq').attr('disabled', 'disabled');

var form = $('#frmReq');
    var errorHandler1 = $('#errorContainer');

    errorHandler1.hide();
    
    form.validate({
        errorLabelContainer: "#errorContainer",
        wrapper: "span",
        errorElement: "span", // contain the error msg in a span tag
        errorClass: 'help-block',
        rules: {
            tipo_contrato: {
                required: true
            },
            fecha_inicio_labores: {
                required: true,
                date: true
            },
            justificacion: {
                required: true
            },
            salario_asignado: {
                required: true,
                number: true
            },
            motivos: {
                required: true,
                number: true
            }
        },
        messages: {
            tipo_contrato: " - El campo Tipo de Contrato es obligatorio. <br />",
            fecha_inicio_labores: {
                required: "- El campo Fecha de Inicio de Labores es obligatorio. <br />",
                date: "- El campo Fecha de Inicio de Labores debe tener un formato correcto (YYYY-MM-DD)<br />"
            },
            justificacion: " - El campo Justificación es obligatorio. <br />",
            salario_asignado: " - El campo Salario Asignado es obligatorio. <br />",
            motivos: " - El campo Motivo es obligatorio. <br />"
        },
        invalidHandler: function (event, validator) { //display error alert on form submit
            // successHandler1.hide();
            errorHandler1.show();
        },
        highlight: function (element, errorClass) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group

            var elem = $(element);
            if (elem.hasClass("select2-offscreen")) {
                $("#s2id_" + elem.attr("id") + " ul").addClass(errorClass);
            } else {
                elem.addClass(errorClass);
            }
        },
        unhighlight: function (element, errorClass) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group

            var elem = $(element);
            if (elem.hasClass("select2-offscreen")) {
                $("#s2id_" + elem.attr("id") + " ul").removeClass(errorClass);
            } else {
                elem.removeClass(errorClass);
            }
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        },
        submitHandler: function () {
            $.blockUI({ message: $('#loadMessage') }); 
            document.getElementById("frmPerfil").submit();      
        }
    });

    var oTable = $('#tbl_requisiciones').DataTable({
        "columns": [
            null,
            null,
            {"width": "5%"},
            null,
            null
        ],
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"]],
        "iDisplayLength": 5,
        "bFilter": true,
        "bLengthChange": true
    });
    var oTableMunicipiosAddReq = $('#tbl_municipios_add_req').DataTable({        
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": " Ver _MENU_ registros",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "bFilter": false,
        "bLengthChange": false,
        "iDisplayLength": 10
    });
    var oTableMunicipiosReq = $('#tbl_municipios_new_req').DataTable({        
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": " Ver _MENU_ registros",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "bFilter": false,
        "bLengthChange": false,
        "iDisplayLength": 10
    });
    $('#tbl_municipios_new_req tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        var rowData = oTableMunicipiosReq.rows('.selected').data();
        if (rowData.length == 0) {
            $('#delRowsMunicipiosEditReq').attr('disabled', 'disabled');
        } else if (rowData.length == 1) {
            $('#delRowsMunicipiosEditReq').removeAttr('disabled');
        } else if (rowData.length > 1) {
            $('#delRowsMunicipiosEditReq').attr('disabled', 'disabled');
        }
    });
    $('#tbl_municipios_add_req tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        var rowData = oTableMunicipiosAddReq.rows('.selected').data();
        if (rowData.length == 0) {
            $('#delRowsMunicipiosNewReq').attr('disabled', 'disabled');
        } else if (rowData.length == 1) {
            $('#delRowsMunicipiosNewReq').removeAttr('disabled');
        } else if (rowData.length > 1) {
            $('#delRowsMunicipiosNewReq').attr('disabled', 'disabled');
        }
    });
    
    $('#tbl_requisiciones_wrapper .dataTables_length select').select2({
        theme: "classic"
    });

    $('#tbl_aprob_requisiciones_wrapper .dataTables_length select').select2({
        theme: "classic"
    });

    $('#departamento_req, #municipio_req').select2({
        theme: "classic"
    });

    $('#tbl_requisiciones_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    $('#tbl_requisiciones_wrapper .dataTables_length select').addClass("m-wrap small");

    $('#fecha_inicio_labores').datepicker({
        format: "yyyy-mm-d",
        todayHighlight: true
    });

    $('#fecha_fin_labores').datepicker({
        format: "yyyy-mm-d",
        todayHighlight: true
    });

    $('#departamento_req').on('change', function (e) {
        var idDepto = $('#departamento_req').val();
        var idPerfil = $('#idPerfil').val();
        $.ajax({
            url: baseUrl + 'municipios-perfil/' + idPerfil + '/' + idDepto,
            type: "POST",
            success: function (response) {
                $('#municipio_req').html(response);
            }
        })
    });

    $('#addItemMunicipio_new_req').on('click', function (e) {
        e.preventDefault();
        var municipioId = $('#municipio_req').val();
        var municipioText = $("#municipio_req").find("option:selected").text();
        var departamentoText = $("#departamento_req").find("option:selected").text();
        var vacantes_x_municipio = parseInt($('#vacantes_x_municipio').val());
        var PerfilID = parseInt($('#idPerfil').val());
        if (municipioId != '')
        {
            var existe = false;
            $('input[name^="hdItemsMunicipiosId"]').each(function () {
                if ($(this).val() == municipioId)
                    existe = true;
            });

            if (existe) {
                swal({
                    title: "Elemento existente",
                    text: "El elemento que intenta agregar ya existe en la lista!",
                    type: "warning",
                    confirmButtonColor: "#007AFF"
                });
            } else {
                $.ajax({
                    url: baseUrl + 'get-vacantes-perfil',
                    data: "MunicipioID=" + municipioId + "&PerfilID=" + PerfilID,
                    type: "POST",
                    success: function (response) {
                        var cantidad_vacantes_perfil = parseInt(response);
                        $.ajax({
                            url: baseUrl + 'get-suma-vacantes-perfil',
                            data: "MunicipioID=" + municipioId + "&PerfilID=" + PerfilID,
                            type: "POST",
                            success: function (response2) {
                                var suma_vacantes_perfil = parseInt(response2);
                                var suma_total = (vacantes_x_municipio + suma_vacantes_perfil);
                                if(suma_total <= cantidad_vacantes_perfil){
                                    var itemToAdd = "";
                                    itemToAdd += "<input type='hidden' id='hdItemsMunicipiosId' name='hdItemsMunicipiosId[]' value='" + municipioId + "' />";
                                    itemToAdd += "<input type='hidden' id='vacxmun' name='vacxmun[]' value='" + vacantes_x_municipio + "' />";
                                    oTableMunicipiosAddReq.row.add([municipioId, departamentoText, municipioText + itemToAdd, vacantes_x_municipio]).draw(false);
                                    swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
                                }else{
                                    swal("Error!", "El valor de vacantes por municipio ha excedido el limite", "warning");
                                }                                
                            }
                        })
                    }
                })
            }
        } else
        {
            swal("Requerido!", "Debe seleccionar un registro para agregar", "warning");
        }
    });

    $('#addItemMunicipio_edit_req').on('click', function (e) {
        e.preventDefault();
        var municipioId = $('#municipio_req').val();
        var municipioText = $("#municipio_req").find("option:selected").text();
        var departamentoText = $("#departamento_req").find("option:selected").text();
        var vacantes_x_municipio = parseInt($('#vacantes_x_municipio').val());
        var PerfilID = parseInt($('#idPerfil').val());
        if (municipioId != '')
        {
            var existe = false;
            $('input[name^="hdItemsMunicipiosId"]').each(function () {
                if ($(this).val() == municipioId)
                    existe = true;
            });

            if (existe) {
                swal({
                    title: "Elemento existente",
                    text: "El elemento que intenta agregar ya existe en la lista!",
                    type: "warning",
                    confirmButtonColor: "#007AFF"
                });
            } else {
                $.ajax({
                    url: baseUrl + 'get-vacantes-perfil',
                    data: "MunicipioID=" + municipioId + "&PerfilID=" + PerfilID,
                    type: "POST",
                    success: function (response) {
                        var cantidad_vacantes_perfil = parseInt(response);
                        $.ajax({
                            url: baseUrl + 'get-suma-vacantes-perfil',
                            data: "MunicipioID=" + municipioId + "&PerfilID=" + PerfilID,
                            type: "POST",
                            success: function (response2) {
                                var suma_vacantes_perfil = parseInt(response2);
                                var suma_total = (vacantes_x_municipio + suma_vacantes_perfil);
                                if(suma_total <= cantidad_vacantes_perfil){
                                    addItemMunicipioRequisicion(municipioId, municipioText, departamentoText,vacantes_x_municipio, oTableMunicipiosReq);
                                }else{
                                    swal("Error!", "El valor de vacantes por municipio ha excedido el limite", "warning");
                                }                                
                            }
                        })
                    }
                })
            }
        } else
        {
            swal("Requerido!", "Debe seleccionar un registro para agregar", "warning");
        }
    });

    $('#delRowsMunicipiosEditReq').on('click', function (e) {
        var id = oTableMunicipiosReq.rows('.selected').data()[0][0];
        if (oTableMunicipiosReq.column(0).data().length == 0)
        {
            $('#delRowsMunicipiosEditReq').attr('disabled', 'disabled');
        } else
        {
            $('#delRowsMunicipiosEditReq').attr('disabled', 'disabled');
            deleteItem("delete-mun-requisicion", "MunicipioRequisicionID", id, oTableMunicipiosReq)
        }
    });
    
    $('#delRowsMunicipiosNewReq').on('click', function (e) {
        var id = oTableMunicipiosAddReq.rows('.selected').data()[0][0];
        if (oTableMunicipiosAddReq.column(0).data().length == 0)
        {
            $('#delRowsMunicipiosNewReq').attr('disabled', 'disabled');
        } else
        {
            $('#delRowsMunicipiosNewReq').attr('disabled', 'disabled');
            oTableMunicipiosAddReq.row('.selected').remove().draw(false);
            swal("Eliminado!", "Tu registro ha sido eliminado satisfactoriamente", "success");
        }
    });
});



function addItemMunicipioRequisicion(MunicipioID, NombreMunicipio, NombreDepartamento,vacantes_x_municipio, oTable) {
    $.ajax({
        url: baseUrl + 'add-mun-requisicion',
        data: "MunicipioID=" + MunicipioID + "&RequisicionID=" + idReq,
        type: "POST",
        success: function (response) {
            var itemToAdd = "";
            itemToAdd += "<input type='hidden' id='hdItemsMunicipiosId' name='hdItemsMunicipiosId[]' value='" + MunicipioID + "' />";
            itemToAdd += "<input type='hidden' id='vacxmun' name='vacxmun[]' value='" + vacantes_x_municipio + "' />";
            oTable.row.add([MunicipioID, NombreMunicipio + itemToAdd, NombreDepartamento,vacantes_x_municipio]).draw(false);
            swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
        }
    })
}

function deleteItem(url, campo, id, oTable) {
    swal({
        title: "Estás seguro?",
        text: "Vas a eliminar un registro que no podrás recuperar!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007AFF",
        confirmButtonText: "Si, borrar!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            url: baseUrl + "" + url,
            data: campo + "=" + id,
            type: "POST",
            success: function (response) {
                oTable.row('.selected').remove().draw(false);
                swal("Eliminado!", "Tu registro ha sido eliminado satisfactoriamente", "success");
            }
        })
    });
}