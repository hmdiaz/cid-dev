$(document).ready(function() {
    var oTable = $('#tbl_socios_cooperantes_mto').DataTable({
        "columns": [
            null,
            {"width": "25%"}
        ],
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"]],
        "iDisplayLength": 5,
        "bFilter": true,
        "bLengthChange": true
    });

    $('#tbl_socios_cooperantes_mto_wrapper .dataTables_length select').select2({
        theme: "classic"
    });
    $('#tbl_socios_cooperantes_mto_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    $('#tbl_socios_cooperantes_mto_wrapper .dataTables_length select').addClass("m-wrap small");
});