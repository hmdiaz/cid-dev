var baseUrl = $('#baseUrl').val();
var PerfilID = $("#PerfilID").val();
$(document).ready(function () {

    $('#fecha_firma_contrato').datepicker({
        format: "yyyy-mm-d",
        todayHighlight: true
    });

    $('#fecha_firma_acta_inicio').datepicker({
        format: "yyyy-mm-d",
        todayHighlight: true
    });

    $('#fecha_firma_acta_inicio_cron').datepicker({
        format: "yyyy-mm-d",
        todayHighlight: true
    });

    $('#fecha_inicio_contrato').datepicker({
        format: "yyyy-mm-d",
        todayHighlight: true
    });

    $('#fecha_fin_contrato').datepicker({
        format: "yyyy-mm-d",
        todayHighlight: true
    });
    //Inicio Tabla seleccion de convocatorias
    var Seleccion = $('#seleccion').dataTable({
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[3, 'desc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": -1,
    });

    $('#seleccion_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    // modify table search input
    $('#seleccion_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $('#seleccion_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $('#seleccion_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = Seleccion.fnSettings().aoColumns[iCol].bVisible;
        Seleccion.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    //Inicio Tabla etapas de convocatorias
    var EtapasConvocatorias = $('#tbl_etapas_convocatorias').dataTable({
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    $('#tbl_etapas_convocatorias_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    // modify table search input
    $('#tbl_etapas_convocatorias_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $('#tbl_etapas_convocatorias_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $('#tbl_etapas_convocatorias_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = EtapasConvocatorias.fnSettings().aoColumns[iCol].bVisible;
        EtapasConvocatorias.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    //Inicio Tabla requisiciones
    var AllRequisiciones = $('#tbl_all_requisiciones').dataTable({
        "aoColumnDefs": [{
                "aTargets": [0],
                "visible": false
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'desc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    $('#tbl_all_requisiciones_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    // modify table search input
    $('#tbl_all_requisiciones_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $('#tbl_all_requisiciones_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $('#tbl_all_requisiciones_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = AllRequisiciones.fnSettings().aoColumns[iCol].bVisible;
        AllRequisiciones.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    //Fin Tabla coincidencias
    //Inicio Tabla perfiles
    var AllProfiles = $('#tbl_all_profiles').dataTable({
        "aoColumnDefs": [{                
                "aTargets": [0],
                "visible": false
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'desc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    $('#tbl_all_profiles_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    // modify table search input
    $('#tbl_all_profiles_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $('#tbl_all_profiles_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $('#tbl_all_profiles_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = AllProfiles.fnSettings().aoColumns[iCol].bVisible;
        AllProfiles.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    //Fin Tabla coincidencias
    
    //Inicio Tabla coincidencias
    var Coincidencias = $('#tbl_coincidencias').dataTable({
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[3,'desc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    $('#tbl_coincidencias_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    // modify table search input
    $('#tbl_coincidencias_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $('#tbl_coincidencias_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $('#tbl_coincidencias_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = Coincidencias.fnSettings().aoColumns[iCol].bVisible;
        Coincidencias.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    //Fin Tabla coincidencias

    //Inicio Tabla postulantes
    var Postulantes = $('#tbl_postulantes').dataTable({
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[2, 'desc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    $('#tbl_postulantes_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    // modify table search input
    $('#tbl_postulantes_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $('#tbl_postulantes_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $('#tbl_postulantes_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = Postulantes.fnSettings().aoColumns[iCol].bVisible;
        Postulantes.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    //Fin Tabla postulantes

    //Inicio Tabla Requisiciones
    var tbl_requisiciones_wrapper = $('#tbl_requisicion').dataTable({
        "aoColumnDefs": [{
                "aTargets": [0],
                "visible":false
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    $('#tbl_requisicion_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    // modify table search input
    $('#tbl_requisicion_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $('#tbl_requisicion_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $('#tbl_requisicion_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = tbl_requisiciones_wrapper.fnSettings().aoColumns[iCol].bVisible;
        tbl_requisiciones_wrapper.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    //Fin Tabla Requisiciones

    //Inicio Tabla Usuarios
    var listUsers = $('#list_users').dataTable({
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    $('#list_users_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    // modify table search input
    $('#list_users_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $('#list_users_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $('#list_users_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = listUsers.fnSettings().aoColumns[iCol].bVisible;
        listUsers.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    //Fin Tabla Usuarios

    //Inicio Tabla roles
    var listRoles = $('#list_roles').dataTable({
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    $('#list_roles_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    // modify table search input
    $('#list_roles_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $('#list_users_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $('#list_roles_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = listRoles.fnSettings().aoColumns[iCol].bVisible;
        listRoles.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    //Fin Tabla roles


    //Inicio Tabla modulos
    var listModules = $('#list_modules').dataTable({
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    $('#list_modules_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    // modify table search input
    $('#list_modules_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $('#list_modules_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $('#list_modules_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = listModules.fnSettings().aoColumns[iCol].bVisible;
        listModules.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    //Fin Tabla modulos

    //Inicio Tabla Competencias
    var listCats = $('#list_compe').dataTable({
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    $('#list_compe_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    // modify table search input
    $('#list_compe_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $('#list_compe_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $('#list_compe_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = listCats.fnSettings().aoColumns[iCol].bVisible;
        listCats.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    //Fin Tabla Categorias

    //Inicio Tabla Categorias
    var listCompe = $('#list_cats').dataTable({
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    $('#list_cats_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    // modify table search input
    $('#list_cats_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $('#list_cats_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $('#list_cats_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = listCompe.fnSettings().aoColumns[iCol].bVisible;
        listCompe.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    //Fin Tabla Competencias

    //Inicio Tabla Cargos
    var listCargos = $('#list_cargos').dataTable({
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    $('#list_cargos_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    // modify table search input
    $('#list_cargos_wrapper .dataTables_length select').addClass("m-wrap small");
    // modify table per page dropdown
    $('#list_cargos_wrapper .dataTables_length select').select2();
    // initialzie select2 dropdown
    $('#list_cargos_column_toggler input[type="checkbox"]').change(function () {
        /* Get the DataTables object again - this is not a recreation, just a get of the object */
        var iCol = parseInt($(this).attr("data-column"));
        var bVis = listCargos.fnSettings().aoColumns[iCol].bVisible;
        listCargos.fnSetColumnVis(iCol, (bVis ? false : true));
    });
    //Fin Tabla Cargos


    //grados
    $(".grados_select_status").select2();
    //selects users
    $(".users_select_rol").select2();
    $(".users_select_status").select2();
    $(".users_select_edit_rol").select2();
    $(".users_select_edit_status").select2();
    //selects roles
    $(".roles_select_add_rol").select2();
    //categorias de componentes
    $(".cat_select_status").select2();
    $(".cpnte_select_add_cat").select2();
    $(".cpnte_select_add_status").select2();
    $(".cpnte_select_add_grado").select2();
    //Cargos
    $(".select_add_cargo").select2();
    $(".users_select_cargo").select2();
    $(".select_add_cargo_area").select2();
    $(".select_tipo_pregunta").select2();
    

    $(".delete").on("click", function (e) {
        e.preventDefault();
        var url = this.href;
        swal({
            title: "Estás seguro?",
            text: "Vas a eliminar un registro!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#007AFF",
            confirmButtonText: "Si, borrar!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm)
            {
                window.location = url;
            }
        });
    });

    $(".convertir").on("click", function (e) {
        e.preventDefault();
        var url = this.href;
        swal({
            title: "Estás seguro?",
            text: "Vas a convertir esta Licitación en un Proyecto!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#007AFF",
            confirmButtonText: "Si, convertir!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm)
            {
                window.location = url;
            }
        });
    });

//    $("select").select2({
//        theme: "classic"
//    });
});

function deleteResponsabilidad(idResp, oTableResp) {
    swal({
        title: "Estás seguro?",
        text: "Vas a eliminar un registro que no podrás recuperar!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007AFF",
        confirmButtonText: "Si, borrar!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            url: baseUrl + 'delete-responsabilidad',
            data: "ResponsabilidadPerfilID=" + idResp,
            type: "POST",
            success: function (response) {
                oTableResp.row('.selected').remove().draw(false);
                swal("Eliminado!", "Tu registro ha sido eliminado satisfactoriamente", "success");
            }
        })
    });
}

function deleteItemCompCompetencia(id, oTableResp) {
    swal({
        title: "Estás seguro?",
        text: "Vas a eliminar un registro que no podrás recuperar!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007AFF",
        confirmButtonText: "Si, borrar!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            url: baseUrl + 'delete-comp-competencia',
            data: "ComponenteCompetenciaID=" + id,
            type: "POST",
            success: function (response) {
                oTableResp.row('.selected').remove().draw(false);
                swal("Eliminado!", "Tu registro ha sido eliminado satisfactoriamente", "success");
            }
        })
    });
}

function getMunicipios(url, id, idorigen, iddestino) {
    var depId = $(idorigen).val();
    $.ajax({
        url: url + '/' + depId,
        type: 'GET',
        success: function (response) {
            $(iddestino).html(response);
        }
    })
}
