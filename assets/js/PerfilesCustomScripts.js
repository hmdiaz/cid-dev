var baseUrl = $('#baseUrl').val();
var PerfilID = $("#PerfilID").val();

$(document).ready(function(){
    $('#delRowsNivelEducativo').attr('disabled', 'disabled');
    $('#delRowsNivelFormacion').attr('disabled', 'disabled');
    $('#delRowsExperienciaGeneral').attr('disabled', 'disabled');
    $('#delRowsHabilidad').attr('disabled', 'disabled');
    $('#delRowsMunicipiosEditProfile').attr('disabled', 'disabled');
    $('#delRowsMunicipiosNewProfile').attr('disabled', 'disabled');
    $('#delRowsNivelEducativoEditProfile').attr('disabled', 'disabled');
    $('#delRowsNivelFormacionEditProfile').attr('disabled', 'disabled');
    $('#delRowsExperienciaGeneralEditProfile').attr('disabled', 'disabled');
    $('#delRowsHabilidadEditProfile').attr('disabled', 'disabled');

    $('#delRowsResponsabilidadesCargo').attr('disabled', 'disabled');
    $('#delRowsResponsabilidadesCalidad').attr('disabled', 'disabled');
    $('#delRowsResponsabilidadesSeguridad').attr('disabled', 'disabled');

    $('#delRowsResponsabilidadesCargoEditProfile').attr('disabled', 'disabled');
    $('#delRowsResponsabilidadesCalidadEditProfile').attr('disabled', 'disabled');
    $('#delRowsResponsabilidadesSeguridadEditProfile').attr('disabled', 'disabled');

    $('#editRowsNivelEducativo').attr('disabled', 'disabled');
    $('#editRowsResponsabilidadesCalidad').attr('disabled', 'disabled');
    $('#editRowsResponsabilidadesSeguridad').attr('disabled', 'disabled');
    $('#updateItemResponsabilidadCargo').attr('disabled', 'disabled');
    $('#updateItemResponsabilidadCalidad').attr('disabled', 'disabled');
    $('#updateItemResponsabilidadSeguridad').attr('disabled', 'disabled');

    var form = $('#frmPerfil');
    var errorHandler1 = $('#errorContainer');

    errorHandler1.hide();
    
    form.validate({
        errorLabelContainer: "#errorContainer",
        wrapper: "span",
        errorElement: "span", // contain the error msg in a span tag
        errorClass: 'help-block',
        rules: {
            area: {
                required: true
            },
            mision_cargo: {
                required: true
            },
            titulo_cargo: {
                required: true
            },
            numero_ocupantes: {
                required: true,
                number: true
            },
        },
        messages: {
            area: " - El Proyecto es requerido <br />",
            mision_cargo: " - La Misi&oacute;n del Cargo es requerida <br />",
            titulo_cargo: " - El T&iacute;tulo del Cargo es requerido <br />",
            numero_ocupantes: {
                required: "- El N&uacute;mero de Ocupantes es requerido <br />",
                number: "- El N&uacute;mero de Ocupantes debe ser un valor num&eacute;rico<br />"
            },
        },
        invalidHandler: function (event, validator) { //display error alert on form submit
            // successHandler1.hide();
            errorHandler1.show();
        },
        highlight: function (element, errorClass) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
        unhighlight: function (element, errorClass) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        },
        submitHandler: function () {
            $.blockUI({ message: $('#loadMessage') }); 
            document.getElementById("frmPerfil").submit();      
        }
    });


    var oTablePerfiles = $('#tbl_profiles').DataTable({
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    var oTableNE = $('#tbl_nivel_educativo').DataTable({
        "columns": [
            {"visible": false},
            {"visible": false},
            {"visible": false},
            null,
            null,
            null
        ],
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    var oTableNF = $('#tbl_nivel_formacion').DataTable({
        "columns": [
            {"visible": false},
            null,
            null
        ],
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    var oTableEG = $('#tbl_exp_general').DataTable({
        "columns": [
            {"visible": false},
            null,
            null,
            null
        ],
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    var oTableHAB = $('#tbl_habilidades').DataTable({
        "columns": [
            {"visible": false},
            null,
            null
        ],
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    var oTableRespCalidad = $('#tbl_responsabilidadesCalidad').DataTable({
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    var oTableRespSeguridad = $('#tbl_responsabilidadesSeguridad').DataTable({
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    var oTableRespCargo = $('#tbl_responsabilidadesCargo').DataTable({
        "columns": [
            {"visible": false},
            null,
            null
        ],
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    var oTableMunicipiosPerfil = $('#tbl_municipios_new_profile').DataTable({
        "columns": [
            {"visible": false},
            null,
            null,
            null
        ],
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    var oTableMunicipiosEditPerfil = $('#tbl_municipios_edit_profile').DataTable({
        "columns": [
            {"visible": false},
            null,
            null,
            null
        ],
        "aoColumnDefs": [{
                "aTargets": [0]
            }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 5,
    });

    $('#tbl_profiles_wrapper .dataTables_length select').select2({
        theme: "classic"
    });
    $('#tbl_profiles_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Buscar");
    $('#tbl_profiles_wrapper .dataTables_length select').addClass("m-wrap small");

    $('#tbl_responsabilidadesCargo tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        var rowData = oTableRespCargo.rows('.selected').data();
        if (rowData.length == 0) {
            $('#delRowsResponsabilidadesCargoEditProfile').attr('disabled', 'disabled');
            $('#delRowsResponsabilidadesCargo').attr('disabled', 'disabled');
            $('#editRowsResponsabilidadesCargo').attr('disabled', 'disabled');
            $('#hdResponsabilidadCargoEditar').val('');
        }
        else if (rowData.length == 1) {
            $('#delRowsResponsabilidadesCargoEditProfile').removeAttr('disabled');
            $('#delRowsResponsabilidadesCargo').removeAttr('disabled');
            $('#editRowsResponsabilidadesCargo').removeAttr('disabled');
            $('#hdResponsabilidadCargoEditar').val(rowData[0][1]);
            $('#hdResponsabilidadCargoIdEditar').val(rowData[0][0]);
        }
        else if (rowData.length > 1) {
            $('#editRowsResponsabilidadesCargo').attr('disabled', 'disabled');
            $('#delRowsResponsabilidadesCargoEditProfile').removeAttr('disabled');
            $('#delRowsResponsabilidadesCargo').removeAttr('disabled');
            $('#hdResponsabilidadCargoEditar').val('');
        }
    });

    var idGrado;
    var IdCatPerfil;
    var idCompCompet;

    $('#tbl_nivel_educativo tbody').on('click', 'tr', function () {
        var module = $("#module").val();
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            oTableNE.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        if (oTableNE.rows('.selected')[0].length == 0 || oTableNE.rows('.selected')[0].length > 1)
        {
            $('#delRowsNivelEducativoEditProfile').attr('disabled', 'disabled');
            $('#delRowsNivelEducativo').attr('disabled', 'disabled');
            $('#editRowsNivelEducativo').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsNivelEducativoEditProfile').removeAttr('disabled');
            $('#delRowsNivelEducativo').removeAttr('disabled');
            $('#editRowsNivelEducativo').removeAttr('disabled');

            if(module != "new")
            {
                $.blockUI({ message: $('#loadMessage') }); 

                $("#pesoNE").val(parseInt(oTableNE.rows('.selected').data()[0][5]));
                idCompCompet = parseInt(oTableNE.rows('.selected').data()[0][0]);
                idGrado = parseInt(oTableNE.rows('.selected').data()[0][1]);
                IdCatPerfil = parseInt(oTableNE.rows('.selected').data()[0][2]);

                $("#grado").select2().val([idGrado]).trigger("change");
                $.ajax({
                    url: baseUrl + 'get-niveles-educativos/' + idGrado,
                    type: "POST",
                    success: function (response) {
                        $('#nivel_educativo').html(response);
                        $("#nivel_educativo").select2().val([IdCatPerfil]).trigger("change");
                        $.unblockUI(); 
                    }
                })
            }            
        }
    });

    $('#tbl_nivel_formacion tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        if (oTableNF.row('.selected').length == 0) {
            $('#delRowsNivelFormacionEditProfile').attr('disabled', 'disabled');
            $('#delRowsNivelFormacion').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsNivelFormacionEditProfile').removeAttr('disabled');
            $('#delRowsNivelFormacion').removeAttr('disabled');
        }
    });

    $('#tbl_exp_general tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        if (oTableEG.row('.selected').length == 0)
        {
            $('#delRowsExperienciaGeneralEditProfile').attr('disabled', 'disabled');
            $('#delRowsExperienciaGeneral').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsExperienciaGeneralEditProfile').removeAttr('disabled');
            $('#delRowsExperienciaGeneral').removeAttr('disabled');
        }

    });

    $('#tbl_habilidades tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        if (oTableHAB.row('.selected').length == 0)
        {
            $('#delRowsHabilidadEditProfile').attr('disabled', 'disabled');
            $('#delRowsHabilidad').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsHabilidadEditProfile').removeAttr('disabled');
            $('#delRowsHabilidad').removeAttr('disabled');
        }
    });

    $('#tbl_responsabilidadesCalidad tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        var rowData = oTableRespCalidad.rows('.selected').data();
        if (rowData.length == 0) {
            $('#delRowsResponsabilidadesCalidadEditProfile').attr('disabled', 'disabled');
            $('#delRowsResponsabilidadesCalidad').attr('disabled', 'disabled');
            $('#editRowsResponsabilidadesCalidad').attr('disabled', 'disabled');
            $('#hdResponsabilidadCalidadEditar').val('');
        }
        else if (rowData.length == 1) {
            $('#delRowsResponsabilidadesCalidadEditProfile').removeAttr('disabled');
            $('#delRowsResponsabilidadesCalidad').removeAttr('disabled');
            $('#editRowsResponsabilidadesCalidad').removeAttr('disabled');
            $('#hdResponsabilidadCalidadEditar').val(rowData[0][1]);
            $('#hdResponsabilidadCalidadIdEditar').val(rowData[0][0]);
        }
        else if (rowData.length > 1) {
            $('#editRowsResponsabilidadesCalidad').attr('disabled', 'disabled');
            $('#delRowsResponsabilidadesCalidadEditProfile').removeAttr('disabled');
            $('#delRowsResponsabilidadesCalidad').removeAttr('disabled');
            $('#hdResponsabilidadCalidadEditar').val('');
        }
    });

    $('#tbl_responsabilidadesSeguridad tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        var rowData = oTableRespSeguridad.rows('.selected').data();
        if (rowData.length == 0) {
            $('#delRowsResponsabilidadesSeguridadEditProfile').attr('disabled', 'disabled');
            $('#delRowsResponsabilidadesSeguridad').attr('disabled', 'disabled');
            $('#editRowsResponsabilidadesSeguridad').attr('disabled', 'disabled');
            $('#hdResponsabilidadSeguridadEditar').val('');
        }
        else if (rowData.length == 1) {
            $('#delRowsResponsabilidadesSeguridadEditProfile').removeAttr('disabled');
            $('#delRowsResponsabilidadesSeguridad').removeAttr('disabled');
            $('#editRowsResponsabilidadesSeguridad').removeAttr('disabled');
            $('#hdResponsabilidadSeguridadEditar').val(rowData[0][1]);
            $('#hdResponsabilidadSeguridadIdEditar').val(rowData[0][0]);
        }
        else if (rowData.length > 1) {
            $('#editRowsResponsabilidadesSeguridad').attr('disabled', 'disabled');
            $('#delRowsResponsabilidadesSeguridadEditProfile').removeAttr('disabled');
            $('#delRowsResponsabilidadesSeguridad').removeAttr('disabled');
            $('#hdResponsabilidadSeguridadEditar').val('');
        }
    });

    $('#tbl_municipios_edit_profile tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        if (oTableMunicipiosEditPerfil.row('.selected').length == 0)
        {
            $('#delRowsMunicipiosEditProfile').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsMunicipiosEditProfile').removeAttr('disabled');
        }
    });
    
    $('#tbl_municipios_new_profile tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        if (oTableMunicipiosPerfil.row('.selected').length == 0)
        {
            $('#delRowsMunicipiosNewProfile').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsMunicipiosNewProfile').removeAttr('disabled');
        }
    });

    $('#editRowsNivelEducativo').on('click', function(e){
         var nivelEducativoId = $('#nivel_educativo').val();
        var nivelEducativoText = $("#nivel_educativo").find("option:selected").text();
        var gradoText = $("#grado").find("option:selected").text();//$("#nivel_educativo option[value='" + nivelEducativoId + "']").text();
        var peso = $('#pesoNE').val() == '' ? 0 : $('#pesoNE').val();
        var anios = $('#anios_ne').val() == '' ? 0 : $('#anios_ne').val();
        var meses = $('#meses_ne').val() == '' ? 0 : $('#meses_ne').val();

        var exp = parseInt(anios) * 12 + parseInt(meses);

        var exp_tabla = anios + ' años ' + meses + ' meses';

        var existe = false;
        $('input[name^="hdItemsId"]').each(function () {
            if ($(this).val() == nivelEducativoId)
                existe = true;
        });

        idCompCompet = parseInt(oTableNE.rows('.selected').data()[0][0]);
        idGrado = parseInt(oTableNE.rows('.selected').data()[0][1]);
        IdCatPerfil = $("#nivel_educativo").val();

        var itemToAdd = "";
        itemToAdd += "<input type='hidden' id='hdItemsId' name='hdItemsId[]' value='" + nivelEducativoId + "' />";
        itemToAdd += "<input type='hidden' id='hdPeso' name='hdPeso[]' value='" + peso + "' />";

        oTableNE.row('.selected').remove().draw(false);
        oTableNE.row.add([idCompCompet, idGrado, nivelEducativoId, gradoText, nivelEducativoText + itemToAdd, peso, exp_tabla]).draw(false);
        
        $.ajax({
            url: baseUrl + 'update-comp-competencia',
            type: "POST",
            data: 'CatalogoPerfilID=' + nivelEducativoId + '&idCompCompetencia=' + idCompCompet + '&Peso=' + peso,
            success: function (response) {
                $.unblockUI(); 
            }
        })
        swal("Actualizado!", "Tu registro ha sido actualizado satisfactoriamente", "success");
        
    });

    $('#addItemNivelEducativo').on('click', function (e) {
        e.preventDefault();
        var nivelEducativoId = $('#nivel_educativo').val();
        var nivelEducativoText = $("#nivel_educativo").find("option:selected").text();
        var gradoText = $("#grado").find("option:selected").text();//$("#nivel_educativo option[value='" + nivelEducativoId + "']").text();
        var peso = $('#pesoNE').val() == '' ? 0 : $('#pesoNE').val();
        var anios = $('#anios_ne').val() == '' ? 0 : $('#anios_ne').val();
        var meses = $('#meses_ne').val() == '' ? 0 : $('#meses_ne').val();

        var exp = parseInt(anios) * 12 + parseInt(meses);

        var exp_tabla = anios + ' años ' + meses + ' meses';

        var existe = false;
        $('input[name^="hdItemsId"]').each(function () {
            if ($(this).val() == nivelEducativoId)
                existe = true;
        });

        if (existe) {
            swal({
                title: "Elemento existente",
                text: "El elemento que intenta agregar ya existe en la lista!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {
            var itemToAdd = "";
            itemToAdd += "<input type='hidden' id='hdItemsId' name='hdItemsId[]' value='" + nivelEducativoId + "' />";
            itemToAdd += "<input type='hidden' id='hdPeso' name='hdPeso[]' value='" + peso + "' />";

            oTableNE.row.add([null, null, nivelEducativoId, gradoText, nivelEducativoText + itemToAdd, peso, exp_tabla]).draw(false);
            swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
        }
    });

    $('#addItemNivelFormacion').on('click', function (e) {
        e.preventDefault();
        var nivelFormacionId = $('#nivel_formacion').val();
        var nivelFormacionText = $("#nivel_formacion").find("option:selected").text(); //$("#nivel_educativo option[value='" + nivelEducativoId + "']").text();
        var peso = $('#pesoNF').val() == '' ? 0 : $('#pesoNF').val();
        var anios = $('#anios_nf').val() == '' ? 0 : $('#anios_nf').val();
        var meses = $('#meses_nf').val() == '' ? 0 : $('#meses_nf').val();

        var exp = parseInt(anios) * 12 + parseInt(meses);

        var exp_tabla = anios + ' años ' + meses + ' meses';

        var existe = false;
        $('input[name^="hdItemsId"]').each(function () {
            if ($(this).val() == nivelFormacionId)
                existe = true;
        });

        if (existe) {
            swal({
                title: "Elemento existente",
                text: "El elemento que intenta agregar ya existe en la lista!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {
            var itemToAdd = "";
            itemToAdd += "<input type='hidden' id='hdItemsId' name='hdItemsId[]' value='" + nivelFormacionId + "' />";
            itemToAdd += "<input type='hidden' id='hdPeso' name='hdPeso[]' value='" + peso + "' />";

            oTableNF.row.add([nivelFormacionId, nivelFormacionText + itemToAdd, peso, exp_tabla]).draw(false);
            swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
        }
    });

    $('#addItemExperienciaGeneral').on('click', function (e) {
        e.preventDefault();
        var experienciaGeneralId = $('#experiencia_general').val();
        var experienciaGeneralText = $("#experiencia_general").find("option:selected").text(); //$("#nivel_educativo option[value='" + nivelEducativoId + "']").text();
        var peso = $('#pesoEG').val() == '' ? 0 : $('#pesoEG').val();
        var anios = $('#anios_ef').val() == '' ? 0 : $('#anios_eg').val();
        var meses = $('#meses_eg').val() == '' ? 0 : $('#meses_eg').val();

        var exp = parseInt(anios) * 12 + parseInt(meses);

        var exp_tabla = anios + ' años ' + meses + ' meses';

        var existe = false;
        $('input[name^="hdItemsId"]').each(function () {
            if ($(this).val() == experienciaGeneralId)
                existe = true;
        });

        if (existe) {
            swal({
                title: "Elemento existente",
                text: "El elemento que intenta agregar ya existe en la lista!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {
            var itemToAdd = "";
            itemToAdd += "<input type='hidden' id='hdItemsId' name='hdItemsId[]' value='" + experienciaGeneralId + "' />";
            itemToAdd += "<input type='hidden' id='hdPeso' name='hdPeso[]' value='" + peso + "' />";
            itemToAdd += "<input type='hidden' id='hdExpGeneral' name='hdExpGeneral[]' value='" + exp + "' />";

            oTableEG.row.add([experienciaGeneralId, experienciaGeneralText + itemToAdd, peso, exp_tabla]).draw(false);
            swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
        }
    });

    $('#addItemHabilidad').on('click', function (e) {
        e.preventDefault();
        var habilidadId = $('#habilidades').val();
        var habilidadText = $("#habilidades").find("option:selected").text(); //$("#nivel_educativo option[value='" + nivelEducativoId + "']").text();
        var peso = $('#pesoHAB').val() == '' ? 0 : $('#pesoHAB').val();
        var anios = $('#anios_hab').val() == '' ? 0 : $('#anios_hab').val();
        var meses = $('#meses_hab').val() == '' ? 0 : $('#meses_hab').val();

        var exp = parseInt(anios) * 12 + parseInt(meses);

        var exp_tabla = anios + ' años ' + meses + ' meses'

        var existe = false;
        $('input[name^="hdItemsId"]').each(function () {
            if ($(this).val() == habilidadId)
                existe = true;
        });

        if (existe) {
            swal({
                title: "Elemento existente",
                text: "El elemento que intenta agregar ya existe en la lista!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {
            var itemToAdd = "";
            itemToAdd += "<input type='hidden' id='hdItemsId' name='hdItemsId[]' value='" + habilidadId + "' />";
            itemToAdd += "<input type='hidden' id='hdPeso' name='hdPeso[]' value='" + peso + "' />";

            oTableHAB.row.add([habilidadId, habilidadText + itemToAdd, peso, exp_tabla]).draw(false);
            swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
        }
    });

    $('#addItemNivelEducativoEditProfile').on('click', function (e) {
        $.blockUI({ message: $('#loadMessage') }); 
        e.preventDefault();
        var nivelEducativoId = $('#nivel_educativo').val();
        var nivelEducativoText = $("#nivel_educativo").find("option:selected").text(); //$("#nivel_educativo option[value='" + nivelEducativoId + "']").text();
        var gradoText = $("#grado").find("option:selected").text();
        var peso = $('#pesoNE').val() == '' ? 0 : $('#pesoNE').val();
        var anios = $('#anios_ne').val() == '' ? 0 : $('#anios_ne').val();
        var meses = $('#meses_ne').val() == '' ? 0 : $('#meses_ne').val();

        var exp = parseInt(anios) * 12 + parseInt(meses);

        var exp_tabla = anios + ' años ' + meses + ' meses';

        var existe = false;
        $('input[name^="hdItemsId"]').each(function () {
            if ($(this).val() == nivelEducativoId)
                existe = true;
        });

        if (existe) {
            swal({
                title: "Elemento existente",
                text: "El elemento que intenta agregar ya existe en la lista!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {

            $.ajax({
                url: baseUrl + 'add-comp-competencia',
                data: "CatalogoPerfilID=" + nivelEducativoId + "&PerfilID=" + PerfilID + "&Peso=" + peso,
                type: "POST",
                success: function (response) {
                    var itemToAdd = "";
                    itemToAdd += "<input type='hidden' id='hdItemsId' name='hdItemsId[]' value='" + nivelEducativoId + "' />";
                    itemToAdd += "<input type='hidden' id='hdPeso' name='hdPeso[]' value='" + peso + "' />";
                    $.unblockUI();
                    oTableNE.row.add([nivelEducativoId, gradoText, nivelEducativoText + itemToAdd, peso]).draw(false);
                    swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
                }
            })
        }
    });

    $('#addItemNivelFormacionEditProfile').on('click', function (e) {
        e.preventDefault();
        var nivelFormacionId = $('#nivel_formacion').val();
        var nivelFormacionText = $("#nivel_formacion").find("option:selected").text(); //$("#nivel_educativo option[value='" + nivelEducativoId + "']").text();
        var peso = $('#pesoNF').val() == '' ? 0 : $('#pesoNF').val();
        var anios = $('#anios_nf').val() == '' ? 0 : $('#anios_nf').val();
        var meses = $('#meses_nf').val() == '' ? 0 : $('#meses_nf').val();

        var exp = parseInt(anios) * 12 + parseInt(meses);

        var exp_tabla = anios + ' años ' + meses + ' meses';

        var existe = false;
        $('input[name^="hdItemsId"]').each(function () {
            if ($(this).val() == nivelFormacionId)
                existe = true;
        });

        if (existe) {
            swal({
                title: "Elemento existente",
                text: "El elemento que intenta agregar ya existe en la lista!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {
            addItemCompCompetencia(nivelFormacionId, PerfilID, peso, exp, exp_tabla, nivelFormacionText, oTableNF);
        }
    });

    $('#addItemExperienciaGeneralEditProfile').on('click', function (e) {
        e.preventDefault();
        var experienciaGeneralId = $('#experiencia_general').val();
        var experienciaGeneralText = $("#experiencia_general").find("option:selected").text(); //$("#nivel_educativo option[value='" + nivelEducativoId + "']").text();
        var peso = $('#pesoEG').val() == '' ? 0 : $('#pesoEG').val();
        var anios = $('#anios_eg').val() == '' ? 0 : $('#anios_eg').val();
        var meses = $('#meses_eg').val() == '' ? 0 : $('#meses_eg').val();

        var exp = parseInt(anios) * 12 + parseInt(meses);

        var exp_tabla = anios + ' años ' + meses + ' meses';

        var existe = false;
        $('input[name^="hdItemsId"]').each(function () {
            if ($(this).val() == experienciaGeneralId)
                existe = true;
        });

        if (existe) {
            swal({
                title: "Elemento existente",
                text: "El elemento que intenta agregar ya existe en la lista!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {
            addItemCompCompetencia(experienciaGeneralId, PerfilID, peso, exp, exp_tabla, experienciaGeneralText, oTableEG);
        }
    });

    $('#addItemHabilidadEditProfile').on('click', function (e) {
        e.preventDefault();
        var habilidadId = $('#habilidades').val();
        var habilidadText = $("#habilidades").find("option:selected").text(); //$("#nivel_educativo option[value='" + nivelEducativoId + "']").text();
        var peso = $('#pesoHAB').val() == '' ? 0 : $('#pesoHAB').val();
        var anios = $('#anios_hab').val() == '' ? 0 : $('#anios_hab').val();
        var meses = $('#meses_hab').val() == '' ? 0 : $('#meses_hab').val();

        var exp = parseInt(anios) * 12 + parseInt(meses);

        var exp_tabla = anios + ' años ' + meses + ' meses';

        var existe = false;
        $('input[name^="hdItemsId"]').each(function () {
            if ($(this).val() == habilidadId)
                existe = true;
        });

        if (existe) {
            swal({
                title: "Elemento existente",
                text: "El elemento que intenta agregar ya existe en la lista!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {
            addItemCompCompetencia(habilidadId, PerfilID, peso, exp, exp_tabla, habilidadText, oTableHAB);
        }
    });

    $('#addItemResponsabilidad').on('click', function (e) {
        e.preventDefault();
        var id = $('#responsabilidades').val();
        var tipo_resp = $('#tipo_responsabilidad').find("option:selected").text();
        var descripcion = $('#responsabilidades').find("option:selected").text();

        var existe = false;

        if (id == '') {
            swal({
                title: "Elemento vacio",
                text: "El elemento que intenta agregar está vacio!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {
            $('input[name^="hdResponsabilidadIDs"]').each(function () {
                if ($(this).val() == id)
                    existe = true;
            });

            if (existe) {
                swal({
                    title: "Elemento existente",
                    text: "El elemento que intenta agregar ya existe en la lista!",
                    type: "warning",
                    confirmButtonColor: "#007AFF"
                });
            }
            else
            {
                var itemToAdd = "";
                itemToAdd += "<input type='hidden' id='hdResponsabilidadIDs' name='hdResponsabilidadIDs[]' value='" + id + "' />";
                oTableRespCargo.row.add([id, descripcion + itemToAdd]).draw(false);
            }
        }
    });

    $("#addItemResponsabilidadCargoEditProfile").on('click', function(e){
        e.preventDefault();
        var responsabilidadId = $('#responsabilidades').val();
        var responsabilidadText = $("#responsabilidades").find("option:selected").text();
        var existe = false;
        $('input[name^="hdItemsResponsabilidadId"]').each(function () {
            if ($(this).val() == responsabilidadId)
                existe = true;
        });

        if (existe) {
            swal({
                title: "Elemento existente",
                text: "El elemento que intenta agregar ya existe en la lista!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {
            addNewResponsabilidad(responsabilidadId, responsabilidadText, oTableRespCargo);
        }
    });

    $('#addItemResponsabilidadCalidad').on('click', function (e) {
        e.preventDefault();
        var id = $('#responsabilidades_calidad').val();
        var descripcion = $('#responsabilidades_calidad').find("option:selected").text();

        var existe = false;

        if (id == '') {
            swal({
                title: "Elemento vacio",
                text: "El elemento que intenta agregar está vacio!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {
            $('input[name^="hdResponsabilidadIDs"]').each(function () {
                if ($(this).val() == id)
                    existe = true;
            });

            if (existe) {
                swal({
                    title: "Elemento existente",
                    text: "El elemento que intenta agregar ya existe en la lista!",
                    type: "warning",
                    confirmButtonColor: "#007AFF"
                });
            }
            else
            {
                var itemToAdd = "";
                itemToAdd += "<input type='hidden' id='hdResponsabilidadIDs' name='hdResponsabilidadIDs[]' value='" + id + "' />";
                oTableRespCalidad.row.add([0, descripcion + itemToAdd]).draw(false);
            }
        }
    });

    $("#addItemResponsabilidadCalidadEditProfile").on('click', function(e){
        e.preventDefault();
        var responsabilidadId = $('#responsabilidades_calidad').val();
        var responsabilidadText = $("#responsabilidades_calidad").find("option:selected").text();

        var existe = false;
        $('input[name^="hdItemsResponsabilidadId"]').each(function () {
            if ($(this).val() == responsabilidadId)
                existe = true;
        });

        if (existe) {
            swal({
                title: "Elemento existente",
                text: "El elemento que intenta agregar ya existe en la lista!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {
            addNewResponsabilidad(responsabilidadId, responsabilidadText, oTableRespCalidad);
        }
    });

    $('#addItemResponsabilidadSeguridad').on('click', function (e) {
        e.preventDefault();
        var id = $('#responsabilidades_seguridad').val();
        var descripcion = $('#responsabilidades_seguridad').find("option:selected").text();

        var existe = false;

        if (id == '') {
            swal({
                title: "Elemento vacio",
                text: "El elemento que intenta agregar está vacio!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {
            $('input[name^="hdResponsabilidadIDs"]').each(function () {
                if ($(this).val() == id)
                    existe = true;
            });

            if (existe) {
                swal({
                    title: "Elemento existente",
                    text: "El elemento que intenta agregar ya existe en la lista!",
                    type: "warning",
                    confirmButtonColor: "#007AFF"
                });
            }
            else
            {
                var itemToAdd = "";
                itemToAdd += "<input type='hidden' id='hdResponsabilidadIDs' name='hdResponsabilidadIDs[]' value='" + id + "' />";
                oTableRespSeguridad.row.add([0, descripcion + itemToAdd]).draw(false);
            }
        }
    });

    $("#addItemResponsabilidadSeguridadEditProfile").on('click', function(e){
        e.preventDefault();
        var responsabilidadId = $('#responsabilidades_seguridad').val();
        var responsabilidadText = $("#responsabilidades_seguridad").find("option:selected").text();

        var existe = false;
        $('input[name^="hdItemsResponsabilidadId"]').each(function () {
            if ($(this).val() == responsabilidadId)
                existe = true;
        });

        if (existe) {
            swal({
                title: "Elemento existente",
                text: "El elemento que intenta agregar ya existe en la lista!",
                type: "warning",
                confirmButtonColor: "#007AFF"
            });
        }
        else {
            addNewResponsabilidad(responsabilidadId, responsabilidadText, oTableRespSeguridad);
        }
    });

    $('#addItemMunicipio_new_profile').on('click', function(e){
        e.preventDefault();
        var municipioId = $('#municipio_new_profile').val();
        var municipioText = $("#municipio_new_profile").find("option:selected").text();
        var departamentoText = $("#departamento_new_profile").find("option:selected").text();
        var vacantes_x_municipio = parseInt($("#vacantes_x_municipio").val());
        var num_ocupantes = parseInt($("#numero_ocupantes").val());
        var suma_vacantes = 0;
        if(municipioId != '')
        {
            var existe = false;
            $('input[name^="hdItemsMunicipiosId"]').each(function () {
                if ($(this).val() == municipioId)
                    existe = true;
            });

            if (existe) {
                swal({
                    title: "Elemento existente",
                    text: "El elemento que intenta agregar ya existe en la lista!",
                    type: "warning",
                    confirmButtonColor: "#007AFF"
                });
            }
            else {
                suma_vacantes = vacantes_x_municipio;
                $('input[name^="vacantesxmun"]').each(function () {
                    suma_vacantes +=  parseInt($(this).val());
                });
                if(suma_vacantes <= num_ocupantes){
                    var itemToAdd = "";
                    itemToAdd += "<input type='hidden' id='hdItemsMunicipiosId' name='hdItemsMunicipiosId[]' value='" + municipioId + "' />";
                    itemToAdd += "<input type='hidden' id='vacantesxmun' name='vacantesxmun[]' value='" + vacantes_x_municipio + "' />";
                    oTableMunicipiosPerfil.row.add([municipioId, departamentoText, municipioText + itemToAdd,vacantes_x_municipio]).draw(false);
                    swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
                }else{
                    swal({
                        title: "Diferencia de Vacantes",
                        text: "La suma de ocupantes debe ser igual al total de la suma de vacantes de cada municipio!",
                        type: "warning",
                        confirmButtonColor: "#007AFF"
                    });
                }
                
            }
        }
        else
        {
            swal("Requerido!", "Debe seleccionar un registro para agregar", "warning");
        }
    });

    $('#addItemMunicipio_edit_profile').on('click', function(e){
        e.preventDefault();
        var municipioId = $('#municipio_edit_profile').val();
        var municipioText = $("#municipio_edit_profile").find("option:selected").text();
        var departamentoText = $("#departamento_edit_profile").find("option:selected").text();
        var vacantes_x_municipio = parseInt($("#vacantes_x_municipio").val());
        var num_ocupantes = parseInt($("#numero_ocupantes").val());
        var suma_vacantes = 0;
        if(municipioId != '')
        {
            var existe = false;
            $('input[name^="hdItemsMunicipiosId"]').each(function () {
                if ($(this).val() == municipioId)
                    existe = true;
            });

            if (existe) {
                swal({
                    title: "Elemento existente",
                    text: "El elemento que intenta agregar ya existe en la lista!",
                    type: "warning",
                    confirmButtonColor: "#007AFF"
                });
            }
            else {      
                suma_vacantes = vacantes_x_municipio;
                $('input[name^="vacantesxmun"]').each(function () {
                    suma_vacantes +=  parseInt($(this).val());
                });
                if(suma_vacantes <= num_ocupantes){
                    addItemMunicipioPerfil(municipioId, departamentoText, municipioText, vacantes_x_municipio, oTableMunicipiosEditPerfil);
                }else{
                    swal({
                        title: "Diferencia de Vacantes",
                        text: "La suma de ocupantes debe ser igual al total de la suma de vacantes de cada municipio!",
                        type: "warning",
                        confirmButtonColor: "#007AFF"
                    });
                }
                
            }
        }
        else
        {
            swal("Requerido!", "Debe seleccionar un registro para agregar", "warning");
        }
    });


    $('#delRowsNivelEducativo').on('click', function () {
        oTableNE.row('.selected').remove().draw(false);
        if (oTableNE.column(0).data().length == 0)
            $('#delRowsNivelEducativo').attr('disabled', 'disabled');
        else
            $('#delRowsNivelEducativo').removeAttr('disabled');
    });

    $('#delRowsNivelFormacion').on('click', function () {
        oTableNF.row('.selected').remove().draw(false);
        if (oTableNF.column(0).data().length == 0)
            $('#delRowsNivelFormacion').attr('disabled', 'disabled');
        else
            $('#delRowsNivelFormacion').removeAttr('disabled');
    });

    $('#delRowsExperienciaGeneral').on('click', function () {
        oTableEG.row('.selected').remove().draw(false);
        if (oTableEG.column(0).data().length == 0)
            $('#delRowsExperienciaGeneral').attr('disabled', 'disabled');
        else
            $('#delRowsExperienciaGeneral').removeAttr('disabled');
    });

    $('#delRowsHabilidad').on('click', function () {
        oTableHAB.row('.selected').remove().draw(false);
        if (oTableHAB.column(0).data().length == 0)
            $('#delRowsHabilidad').attr('disabled', 'disabled');
        else
            $('#delRowsHabilidad').removeAttr('disabled');
    });


    $('#delRowsNivelEducativoEditProfile').on('click', function () {
        var id = oTableNE.rows('.selected').data()[0][0];
        if (oTableNE.column(0).data().length == 0)
        {
            $('#delRowsNivelEducativoEditProfile').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsNivelEducativoEditProfile').removeAttr('disabled');
            deleteItemCompCompetencia(id, oTableNE);
        }
    });

    $('#delRowsNivelFormacionEditProfile').on('click', function () {
        var id = oTableNF.rows('.selected').data()[0][0];
        if (oTableNF.column(0).data().length == 0)
        {
            $('#delRowsNivelFormacionEditProfile').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsNivelFormacionEditProfile').removeAttr('disabled')
            deleteItemCompCompetencia(id, oTableNF);
        }
    });

    $('#delRowsExperienciaGeneralEditProfile').on('click', function () {
        var id = oTableEG.rows('.selected').data()[0][0];
        if (oTableEG.column(0).data().length == 0)
        {
            $('#delRowsExperienciaGeneralEditProfile').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsExperienciaGeneralEditProfile').removeAttr('disabled')
            deleteItemCompCompetencia(id, oTableEG);
        }
    });

    $('#delRowsHabilidadEditProfile').on('click', function () {
        var id = oTableHAB.rows('.selected').data()[0][0];
        if (oTableHAB.column(0).data().length == 0)
        {
            $('#delRowsHabilidadEditProfile').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsHabilidadEditProfile').removeAttr('disabled');
            deleteItemCompCompetencia(id, oTableHAB);
        }
    });

    $('#delRowsResponsabilidadesCalidad').on('click', function () {
        oTableRespCalidad.row('.selected').remove().draw(false);
        if (oTableRespCalidad.column(0).data().length == 0)
            $('#delRowsResponsabilidadesCalidad').attr('disabled', 'disabled');
        else
            $('#delRowsResponsabilidadesCalidad').removeAttr('disabled');
    });

    $('#delRowsResponsabilidadesSeguridad').on('click', function () {
        oTableRespSeguridad.row('.selected').remove().draw(false);
        if (oTableRespSeguridad.column(0).data().length == 0)
            $('#delRowsResponsabilidadesSeguridad').attr('disabled', 'disabled');
        else
            $('#delRowsResponsabilidadesSeguridad').removeAttr('disabled');
    });

    $('#delRowsResponsabilidadesCargo').on('click', function () {
        oTableRespCargo.row('.selected').remove().draw(false);
        if (oTableRespCargo.column(0).data().length == 0)
            $('#delRowsResponsabilidadesCargo').attr('disabled', 'disabled');
        else
            $('#delRowsResponsabilidadesCargo').removeAttr('disabled');
    });

    $('#delRowsResponsabilidadesCargoEditProfile').on('click', function () {
        var id = oTableRespCargo.rows('.selected').data()[0][0];
        if (oTableRespCargo.column(0).data().length == 0) {
            $('#delRowsResponsabilidadesCargoEditProfile').attr('disabled', 'disabled');
        }
        else {
            $('#delRowsResponsabilidadesCargoEditProfile').attr('disabled', 'disabled');
            deleteResponsabilidad(id, oTableRespCargo);
        }
    });

    $('#delRowsResponsabilidadesCalidadEditProfile').on('click', function () {
        var id = oTableRespCalidad.rows('.selected').data()[0][0];
        if (oTableRespCalidad.column(0).data().length == 0) {
            $('#delRowsResponsabilidadesCalidadEditProfile').attr('disabled', 'disabled');
        }
        else {
            $('#delRowsResponsabilidadesCalidadEditProfile').attr('disabled', 'disabled');
            deleteResponsabilidad(id, oTableRespCalidad);
        }
    });

    $('#delRowsResponsabilidadesSeguridadEditProfile').on('click', function () {
        if (oTableRespSeguridad.column(0).data().length == 0) {
            $('#delRowsResponsabilidadesSeguridadEditProfile').attr('disabled', 'disabled');
        }
        else {
            $('#delRowsResponsabilidadesSeguridadEditProfile').attr('disabled', 'disabled');
            var id = oTableRespSeguridad.rows('.selected').data()[0][0];
            deleteResponsabilidad(id, oTableRespSeguridad);
        }

    });

    $('#delRowsMunicipiosEditProfile').on('click', function(e){
        var id = oTableMunicipiosEditPerfil.rows('.selected').data()[0][0];
        if (oTableMunicipiosEditPerfil.column(0).data().length == 0)
        {
            $('#delRowsMunicipiosEditProfile').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsMunicipiosEditProfile').attr('disabled', 'disabled');
            deleteItem("delete-mun-perfil", "MunicipioPerfilID", id, oTableMunicipiosEditPerfil);
            
        }
    });
    
    $('#delRowsMunicipiosNewProfile').on('click', function(e){
        var id = oTableMunicipiosPerfil.rows('.selected').data()[0][0];
        if (oTableMunicipiosPerfil.column(0).data().length == 0)
        {
            $('#delRowsMunicipiosNewProfile').attr('disabled', 'disabled');
        }
        else
        {
            $('#delRowsMunicipiosNewProfile').attr('disabled', 'disabled');
            oTableMunicipiosPerfil.row('.selected').remove().draw(false);
            swal("Eliminado!", "Tu registro ha sido eliminado satisfactoriamente", "success");
            
        }
    });

    $('#area').on('change', function(e) {
        $.blockUI({ message: $('#loadMessage') }); 
        var id = $('#area').val();
        $.ajax({
            url: baseUrl + 'departamento-proyecto/' + id,
            type: "POST",
            success: function (response) {
                $('#departamento_new_profile').html(response);
                $.unblockUI();
            }
        })
    });

    $('#grado').on('change', function(e) {
        $.blockUI({ message: $('#loadMessage') }); 
        var id = $('#grado').val();
        $.ajax({
            url: baseUrl + 'get-niveles-educativos/' + id,
            type: "POST",
            success: function (response) {
                $('#nivel_educativo').html(response);
                $.unblockUI();
            }
        })
    });

    $('#area_edit').on('change', function(e) {
        $.blockUI({ message: $('#loadMessage') }); 
        var id = $('#area_edit').val();
        $.ajax({
            url: baseUrl + 'departamento-proyecto/' + id,
            type: "POST",
            success: function (response) {
                $('#departamento_edit_profile').html(response);
                $.unblockUI();
            }
        })
    });

    $('#tipo_proyecto').on('change', function(e) {
        $.blockUI({ message: $('#loadMessage') }); 
        var id = $('#tipo_proyecto').val();
        $.ajax({
            url: baseUrl + 'get-projects-by-tipo/' + id,
            type: "POST",
            success: function (response) {
                $('#area').html(response);
                $.unblockUI();
            }
        })
    });

    $('#departamento_new_profile').on('change', function(e) {
        $.blockUI({ message: $('#loadMessage') }); 
        var idDepto = $('#departamento_new_profile').val();
        var idProyecto = $('#area').val();
        $.ajax({
            url: baseUrl + 'municipios-proyecto/' + idProyecto + '/' + idDepto,
            type: "POST",
            success: function (response) {
                $('#municipio_new_profile').html(response);
                $.unblockUI();
            }
        })
    });

    $('#departamento_edit_profile').on('change', function(e) {
        $.blockUI({ message: $('#loadMessage') }); 
        var idDepto = $('#departamento_edit_profile').val();
        var idProyecto = $('#area_edit').val();
        $.ajax({
            url: baseUrl + 'municipios-proyecto/' + idProyecto + '/' + idDepto,
            type: "POST",
            success: function (response) {
                $('#municipio_edit_profile').html(response);
                $.unblockUI();
            }
        })
    });

    $('#tipo_responsabilidad').on('change', function(e) {
        $.blockUI({ message: $('#loadMessage') }); 
        var idTipo = $('#tipo_responsabilidad').val();
        $.ajax({
            url: baseUrl + 'responsabilidades-x-tipo/' + idTipo,
            type: "POST",
            success: function (response) {
                $('#responsabilidades').html(response);
                $.unblockUI();
            }
        })
    });

    $('#responsabilidades').select2({
        theme: 'classic'
    });



    $('.only-numeric').on('keydown', function(e){
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    })
    $('#responsabilidades_calidad').select2();
    $('#grado').select2();
    $('#departamento_new_profile').select2();
    $('#departamento_edit_profile').select2();
    $('#municipio_new_profile').select2();
    $('#municipio_edit_profile').select2();
    $('#nivel_educativo').select2();
    $('#nivel_formacion').select2();
    $('#experiencia_general').select2();
    $('#habilidades').select2();
    // $('#area').select2();
    // $('#area_edit').select2();
    $('#tipo_proyecto').select2();
    $('#responsabilidades_seguridad').select2();

});

function addNewResponsabilidad(ResponsabilidadID, descripcion, oTable) {
    $.blockUI({ message: $('#loadMessage') }); 
    $.ajax({
        url: baseUrl + 'add-responsabilidad',
        data: "ResponsabilidadID=" + ResponsabilidadID + "&PerfilID=" + PerfilID,
        type: "POST",
        success: function (response) {
            oTable.row.add([response, descripcion]).draw(false);
            $.unblockUI();
            swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
        }
    })
}

function addItemMunicipioPerfil(MunicipioID, NombreMunicipio, NombreDepartamento, vacantes, oTable) {
    $.blockUI({ message: $('#loadMessage') }); 
    $.ajax({
        url: baseUrl + 'add-mun-perfil',
        data: "MunicipioID=" + MunicipioID + "&PerfilID=" + PerfilID + "&Vacantes=" +vacantes,
        type: "POST",
        success: function (response) {
            var itemToAdd = "";
            itemToAdd += "<input type='hidden' id='hdItemsMunicipiosId' name='hdItemsMunicipiosId[]' value='" + MunicipioID + "' />";
            itemToAdd += "<input type='hidden' id='vacantesxmun' name='vacantesxmun[]' value='" + vacantes + "' />";
            oTable.row.add([response,NombreMunicipio + itemToAdd, NombreDepartamento, vacantes]).draw(false);
            $.unblockUI(); 
            swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
        }
    })
}

function deleteItem(url, campo, id, oTable) {
    swal({
        title: "Estás seguro?",
        text: "Vas a eliminar un registro que no podrás recuperar!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007AFF",
        confirmButtonText: "Si, borrar!",
        closeOnConfirm: false
    }, function () {
        $.blockUI({ message: $('#loadMessage') }); 
        $.ajax({
            url: baseUrl + "" + url,
            data: campo + "=" + id,
            type: "POST",
            success: function (response) {
                oTable.row('.selected').remove().draw(false);
                $.unblockUI(); 
                swal("Eliminado!", "Tu registro ha sido eliminado satisfactoriamente", "success");
            }
        })
    });
}

function addItemCompCompetencia(idCatalogoPerfil, idPerfil, peso, tiempoExp, tabla_exp, descripcion, oTable) {
    $.blockUI({ message: $('#loadMessage') }); 
    $.ajax({
        url: baseUrl + 'add-comp-competencia',
        data: "CatalogoPerfilID=" + idCatalogoPerfil + "&PerfilID=" + idPerfil + "&Peso=" + peso + "&TiempoExperiencia=" + tiempoExp,
        type: "POST",
        success: function (response) {
            var itemToAdd = "";
            itemToAdd += "<input type='hidden' id='hdItemsId' name='hdItemsId[]' value='" + idCatalogoPerfil + "' />";
            itemToAdd += "<input type='hidden' id='hdPeso' name='hdPeso[]' value='" + peso + "' />";
            itemToAdd += "<input type='hidden' id='hdExpGeneral' name='hdExpGeneral[]' value='" + tiempoExp + "' />";

            oTable.row.add([idCatalogoPerfil, descripcion + itemToAdd, peso, tabla_exp]).draw(false);
            $.unblockUI(); 
            swal("Agregado!", "Tu registro ha sido agregado satisfactoriamente", "success");
        }
    })
}