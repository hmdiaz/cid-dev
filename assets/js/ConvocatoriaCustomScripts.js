$(document).ready(function(){
    $('.dtFechaInicio').datepicker({
        format: "yyyy-mm-d",
        todayHighlight: true
    });

    $('.dtFechaFin').datepicker({
        format: "yyyy-mm-d",
        todayHighlight: true
    });

    $('.checkbox').on('click', function () {

    });

    var oTableTR = $('#tbl_responsabilidades').DataTable({
        "columns": [
            {"width": "40%"},
            {"width": "60%"},
        ],
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[0, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"]],
        "iDisplayLength": 5,
        "bFilter": true,
        "bLengthChange": true
    });

    $('#tbl_responsabilidades_wrapper .dataTables_length select').select2({
        theme: "classic"
    });
});