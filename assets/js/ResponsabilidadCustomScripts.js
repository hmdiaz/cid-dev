$(document).ready(function(){
    var oTable = $('#tbl_responsabilidad').DataTable({
        "columns": [
            {"width": "70%"},
            {"width": "30%"},
        ],
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"]],
        "iDisplayLength": 5,
        "bFilter": true,
        "bLengthChange": true
    });

    $('#tbl_responsabilidad_wrapper .dataTables_length select').select2({
        theme: "classic"
    });

    $('#tipo_responsabilidad').select2({
        theme: "classic"
    });

    var oTableTR = $('#tbl_tipo_responsabilidad').DataTable({
        "columns": [
            {"width": "70%"},
            {"width": "30%"},
        ],
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ Filas",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"]],
        "iDisplayLength": 5,
        "bFilter": true,
        "bLengthChange": true
    });

    $('#tbl_tipo_responsabilidad_wrapper .dataTables_length select').select2({
        theme: "classic"
    });
});
